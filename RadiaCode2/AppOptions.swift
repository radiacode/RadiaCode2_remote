//
//  AppOptions.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import Foundation
import Combine
import CoreBluetooth
import CoreData
import SwiftUI

final class  HardOptions: ObservableObject {
    
    var comp: PacketComposer
    var pars: PacketParser
    var prms: ParamsParser

    
    var bdLoadTimer  = Timer()
    var timeoutTimer = Timer()
        
    let managedObjectContext = PersistenceController.shared.container.viewContext

    /// Размеры экрана устройства под iOS для определения масшаба шкал графиков
    let screenSize: CGRect = UIScreen.main.bounds
    /// Расстояние от левого края экрана до оси времени (X). Нужно для вычисления масштаба времени
    let time_Leading : CGFloat = 40
    /// Расстояние от правого края экрана до оси времени (X). Нужно для вычисления масштаба времени
    let timeTrailing : CGFloat =  0
    /// Размер оси времени. Нужно для вычисления масштаба времени
    var timeSize: CGFloat { return  screenSize.width - time_Leading - timeTrailing }
    /// Интервал между последними данными мощности дозы в секундах. Для анимации графика.
    var lastDoseRateTimeRange: Double = 0
    /// Интервал между последними данными счётчика в секундах. Для анимации графика.
    var lastCounter_TimeRange: Double = 0
    /// Диапазон отображаемой части шкалы времени (пока фиксированно 30 секунд). Нужно для вычисления масштаба времени
    @Published var axisXTimeSecRange    : Double  =  30
    
    @Published var axisXTimeIntSecRange    : Int  =  30

    var ble: BLEConnection
    
    var cancellableBLE           : Cancellable?    // Издатель подключения к BLE
    var cancellableBleData       : Cancellable?    // Издатель полученных из прибора данных
    var cancellableDoseRate      : Cancellable?    // Издатель потока данных мощности дозы
    var cancellableLastDoseRate  : Cancellable?    // Издатель потока последней мощности дозы
    var cancellableLastDoseRateE : Cancellable?    // Издатель потока последней средней ошибки мощности дозы
    var cancellableCountRate     : Cancellable?    // Издатель потока данных счётчика импульсов
    var cancellableLastCountRate : Cancellable?    // Издатель потока последнего значения данных счётчика импульсов
    var cancellableLastCountRateE: Cancellable?    // Издатель потока последнего значения данных средней ошибки счётчика импульсов
    var cancellableRawDoseRate   : Cancellable?    // Издатель потока сырых данных мощности дозы
    var cancellableRawCountRate  : Cancellable?    // Издатель потока сырых данных счётчика импульсов
    var cancellableCommand       : Cancellable?    // Издатель завершённых команд прибора
    var cancellablePeripheral    : Cancellable?    // Издатель найденного переферийного устройства
    var cancellableStatus        : Cancellable?    // Издатель enum состояния устройства
    var cancellableGrpCount      : Cancellable?    // Издатель счётчика групп
    var cancellableInt           : Cancellable?    // Издатель целочисленных данных

    var cancellableAdvertisementData : Cancellable? // Издатель подробного описания переферийного устройства


    // Минимальные/максимальные значения мощности и времени дозы (для графика)
    @Published var grpDose_RateArrayMinTime: Double = 0
    @Published var grpDose_RateArrayMaxTime: Double = 0
    @Published var grpDose_RateArrayMin_Val: Double = 0
    @Published var grpDose_RateArrayMax_Val: Double = 0
    
    @Published var grpRawDose_RateArrayMinTime: Double = 0
    @Published var grpRawDose_RateArrayMaxTime: Double = 0

    // Минимальные/максимальные значения импульсов и их времени  (для графика)
    @Published var grpCountRateArrayMinTime: Double = 0
    @Published var grpRawCountRateArrayMaxTime: Double = 0
    @Published var grpRawCountRateArrayMinTime: Double = 0
    @Published var grpCountRateArrayMaxTime: Double = 0
    @Published var grpCountRateArrayMinVal: Double  = 0.0
    @Published var grpCountRateArrayMaxVal: Double  = 0.0

    // Минимальные/максимальные значения мощности и времени дозы (для графика)
    @Published var curDoseVal   : Double = 0.0
    @Published var maxDoseVal   : Double = 0.0
    @Published var minDoseVal   : Double = 0.0
    @Published var stepDoseVal  : Double = 0.0
    
    @Published var curCountVal   : Double = 0.0
    @Published var maxCountVal   : Double = 0.0
    @Published var minCountVal   : Double = 0.0
    @Published var stepCountVal  : Double = 0.0

    @Published var max_Dose_Time        : Double = 0.0
    @Published var min_Dose_Time        : Double = 0.0
    @Published var max_RawDose_Time     : Double = 0.0
    @Published var min_RawDose_Time     : Double = 0.0
    @Published var stepDose_Time        : Double = 0.0
    
    // Значения, используемые для создания оси времени X
    @Published var max_CountTime        : Double = 0.0  // Максимальное (текущее время)
    @Published var min_CountTime        : Double = 0.0  // Точка начала отсчёта
    @Published var max_RawCountTime     : Double = 0.0  // Максимальное (текущее время)
    @Published var min_RawCountTime     : Double = 0.0  // Точка начала отсчёта
    @Published var startTime            : Int    = 30   // Приведённый диапазон – если отображаемое время меньше 30 секунд (в начале)
    
    @Published var minTicDoseVal : Double = 0
    @Published var maxTicDoseVal : Double = 0
    @Published var minTicCountVal: Double = 0
    @Published var maxTicCountVal: Double = 0

    @Published var gridDoseValRange: Double = 0
    @Published var gridCountValRange: Double = 0
    @Published var gridTimeRange: Double = 0
    @Published var gridTimeCount: Int = 0

    // Секция подключения
    @Published var statusStr        : String             = String(localized: "Disconnect")
    @Published var peripheral       : CBPeripheral? //
    @Published var advertisementData: [String : Any]?
    @Published var peripheralUUID   : UUID?
    @Published var namePeripheral = ""
    @Published var sn               : UInt32 = 0
    @Published var isConnect = false
    @Published var status           : Status?
    @Published var isError   = false
               var isFirstChangeCycle = true
    @Published var groupCount = 0
    
    /// Подключение по умолчанию. Сообщать не нужно
               var isDefault = false
    
    /// Текущая выбранная закладка `Устройства-Настройки-Журнал-Графики` в интерфейсе
    @Published var selectedMenu = String(localized: "")
    
    /// Отображать процесс подключения?
    @Published var isConnectionProgerss = false
    
//    @Published var isFindNewPeripheral = false {        // Найдено новое устройство требуется показать сообщение в DevicesListView
//        didSet {
//            print("isFindNewPeripheral = \(isFindNewPeripheral)")
//            print("findDevices.count = \(findDevices.count)")
//            Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false) { (Timer) in
//                DispatchQueue.main.async {
//                    if  self.findDevices.count > 0 && !self.isFindNewPeripheral  {
//                        self.isFindNewPeripheral = true
//                    }
//                }
//            }
//        }
//    }
//               var findDevices : [FindDevice] = [] {    // Массив найденных устройств для определения в БД
//                   didSet {
//                       if findDevices.count > 0 {
//                           isFindNewPeripheral = true   // Нужен запрос на принятие решения по устройству
//                       }
//                   }
//    }
    
    @Published var doseRatePoints   : [GraphDoseRateArray]  = []
    @Published var countRatePoints  : [GraphCountRateArray] = []
    @Published var countRatePoints2 : [CountRateStruct] = []
    @Published var countRatePoints3 : [GraphCountRateArray] = [] // Упрощённый счётчик делений скорости счёта (меняется только кнопкой масштабирования)
    @Published var doseRatePoints3   : [GraphDoseRateArray]  = []

    @Published var timeDosePoints   : [TimeTic]             = [TimeTic(secTime: 0, showText: true)]
    @Published var timeCountPoints  : [TimeTic]             = []
    
    // По скорости счёта Для графиков
    @Published var countRateDB              : [ValRateDB]? = []
               var minCountRateDB           : Float?
               var maxCountRateDB           : Float?
               var minDateCountRateDB       : Date?
               var maxDateCountRateDB       : Date?
    @Published var curMinDateCountRateDB    : Date?
    @Published var curMidDateCountRateDB    : Date?
    @Published var curMaxDateCountRateDB    : Date?
    @Published var curMaxCountRateValDB     : Float? = 1.0
    
    /// Показ окна журнала только при показе самого окна
    @Published var isShowLogList = true
    
    @Published var curTimeStepScale         : DiagramTimeScaleStep = .min30
    @Published var curTimeScaleRange        : Double = 1800 // 30 минут в секундах
    {
        didSet {
            if  curTo__DiagramDate != nil {
                curFromDiagramDate = curTo__DiagramDate!.addingTimeInterval(-curTimeScaleRange)
                let ticsAndUnit = timeTicsForRange(fromDate: curFromDiagramDate!, toDate: curTo__DiagramDate!)
                ticVals = ticsAndUnit.tics
                ticValRange = ticsAndUnit.valRange
                ticValUnits = ticsAndUnit.curUnit
            }
        }
    }
    
    @Published var curTo__DiagramDate       : Date?
    {
        didSet {
                curFromDiagramDate = curTo__DiagramDate!.addingTimeInterval(-curTimeScaleRange)
 //           if  curFromDiagramDate != nil {
                let ticsAndUnit = timeTicsForRange(fromDate: curFromDiagramDate!, toDate: curTo__DiagramDate!)
                ticVals = ticsAndUnit.tics
                ticValRange = ticsAndUnit.valRange
                ticValUnits = ticsAndUnit.curUnit
//            }
        }
    }
    @Published var curFromDiagramDate       : Date?

    @Published var ticVals                  : [DiagramTick] = []
    @Published var ticValRange              : CGFloat       = 0
    @Published var ticValUnits              : Units         = .second

    // По мощности дозы Для графиков
    @Published var dose_RateDB               : [ValRateDB]? = []
               var minDoseRateDB            : Float?
               var maxDoseRateDB            : Float?
               var minDateDoseRateDB        : Date?
               var maxDateDoseRateDB        : Date?

    /// Текущее минимальное значение даты для отображения графика.
    ///  При изменении при наличии максимального значения вычисляется отображаемый диапазон в секундах.
    @Published var curMinDateDoseRateDB     = Date().addingTimeInterval( -30 * 60)// В начале на графике отобржается 30-ти минутный диапазон
    {
        didSet {
            let diffs = Calendar.current.dateComponents([.second], from: curMinDateDoseRateDB, to: curMaxDateDoseRateDB)
            curDateSecRangeDoseRateDB = diffs.second!
        }
    }
    
    @Published var curMidDateDoseRateDB     : Date?
    
    /// Текущее максимальное значение даты для отображения графика.
    ///  При изменении при наличии минимального значения вычисляется отображаемый диапазон в секундах.
    @Published var curMaxDateDoseRateDB     = Date()
    {
        didSet {
            let diffs = Calendar.current.dateComponents([.second], from: curMinDateDoseRateDB, to: curMaxDateDoseRateDB)
            curDateSecRangeDoseRateDB = diffs.second!
        }
    }
    @Published var curMaxDoseRateValDB      : Float? = 10.0
    
    @Published var isConnectDiagrammToCurDate = true
    
    /// Диапазон в секундах между текущими минимальными и максимальными значениям дат графика
    @Published var curDateSecRangeDoseRateDB   : Int = 0

    @Published var deleteTime: Double = 0

    /// Последнее значение мощности дозы для вывода в числовое значение диаграммы
    @Published var grpDoseRateLast  : Double = 0
    
    /// Последнее значение средней ошибки мощности дозы для вывода в числовое значение диаграммы
    @Published var grpDoseRateErrLast  : Double = 0
    

    
    @Published var grpDoseRateArray : [RC_GRP_DOSE_RATE]    = [] {
        didSet {
            defineMaxMinDoseRateVal()// определение максимальных/минимальных значений массива данных мощности дозы
            defineDoseRateValAxis()
// DEBUG:
//            print("grpDoseRateArray")
//            for doseItem in grpDoseRateArray {
//                print(doseItem.msTime, doseItem.chnDoseRate)
//            }
        }
    }
    
    /// Массив значений для периодического по таймеру вывода значений на график
    @Published var grpDose_RateTimerArray     : [ValRateDB]    = []
    @Published var grpRawDoseRateTimerArray  : [ValRateDB]    = [] {
        didSet {
            let fromDate = grpRawDoseRateTimerArray.first?.timestamp ?? Date()
            let diffs = Calendar.current.dateComponents([.minute], from: fromDate, to: Date())
            if diffs.minute ?? 0 > 10 {
                grpRawDoseRateTimerArray.remove(at: 0)
            }
        }
    }
    

    @Published var grpCountRateTimerArray    : [ValRateDB]    = []
    @Published var grpRawCountRateTimerArray : [ValRateDB]    = [] {
        didSet {
            let fromDate = grpRawCountRateTimerArray.first?.timestamp ?? Date()
            let diffs = Calendar.current.dateComponents([.minute], from: fromDate, to: Date())
            if diffs.minute ?? 0 > 10 {
                grpRawCountRateTimerArray.remove(at: 0)
            }
        }
    }
    
    /// Последнее значение скорости счётчика импульсов для вывода в числовое значение диаграммы
    @Published var grpCountRateLast  : Double = 0
    
    /// Последнее значение средней ошибки скорости счётчика импульсов для вывода в числовое значение диаграммы
    @Published var grpCountErrLast  : Double = 0


    @Published var grpCountRateArray : [CountRateStruct]  = [] {
        didSet {
            defineMaxMinCountRateVal()// определение максимальных/минимальных значений массива данных числа импульсов
            defineCountRateValAxis()
//            defineTimeValAxis2() // ******************************************************************
            // DEBUG:
//            print("grpCountRateArray")
//            for doseItem in grpCountRateArray {
//                print(doseItem.msTime, doseItem.chnCountRate)
//            }
        }
    }
    
    @Published var grpRawDoseRateArray : [RC_GRP_DOSE_RATE]    = [] {
        didSet {
            defineMaxMinRawDoseRateVal()
            // DEBUG:
//            print("grpRawDoseRateArray")
//            for doseItem in grpRawDoseRateArray {
//                print(doseItem.msTime, doseItem.chnDoseRate)
//            }
        }
    }
    
    
    
    
    @Published var grpRawCountRateArray : [CountRateStruct]  = [] {
        didSet {
            defineMaxMinRawCountRateVal()
            // DEBUG:
//            print("grpRawCountRateArray")
//            for doseItem in grpRawCountRateArray {
//                print(doseItem.msTime, doseItem.chnCountRate)
//            }
        }
    }
    
    // MARK: - Из DeviceOptions
    
//    var newBits: UInt8 = 0
    var strDoseNotEditFlag      = false // Защита от выполнения didSet при изменении строк без необходимости записи в прибор
    
    /// Разрешение записывать значение свойств только после того, как они будут загружены из прибора и будут установлены правильно в окне опций.
    /// Устанавливается после загрузки конфигурации _RD_VIRT_STRING VS_CONFIGURATION_
    var enableWriteToDeviceFlag = false // TODO: убрать этот костыль – нужно ориентироваться на nil свойств (но пока нет из-за невозможности @State <Bool?>)
    
    @Published var enableStartDataBufCycle      = false
    @Published var enableStopDataBufCycle  = false
    @Published var enableSetOptions  = false

    
    @Published var isRusLang = true   // DEBUG:
    
    @Published var curLangVal = 0 {
        didSet {
            curLang = Lang(rawValue: curLangVal)
        }
    }
    @Published var curLang: Lang? {
        didSet {
            if oldValue != curLang && oldValue != nil && curLang != nil {
                comp.compose_VSFR_DEVICE_LANG(lang: curLang!)
            }
        }
    }
    
    @Published var curAlarmModeVal = 0 { didSet { curAlarmMode = AlarmMode(rawValue: curAlarmModeVal) } }
    @Published var curAlarmMode: AlarmMode? {
        didSet {
            if oldValue != curAlarmMode && oldValue != nil {
                comp.compose_VSFR_ALARM_MODE(alarm: curAlarmMode!)
            }
        }
    }

    @Published var curUnitVal = 0 { didSet { curUnit = Unit(rawValue: curUnitVal) ?? .sv } }
    @Published var curUnit: Unit? {
        didSet {
            strDoseNotEditFlag = true
            if curUnit == Unit.sv { // Переводим текстовку в мкЗиверты
                LEV1_uR_hStr    = String(Double(LEV1_uR_h)  * 0.001)
                LEV2_uR_hStr    = String(Double(LEV2_uR_h)  * 0.001)
                LEV1_100uR_Str = String(Double(LEV1_100uR) * 0.001)
                LEV2_100uR_Str = String(Double(LEV2_100uR) * 0.001)
            }
            if curUnit == Unit.r { // Переводим текстовку в мкРентгены
                LEV1_uR_hStr    = String(Double(LEV1_uR_h)  * 0.1 )
                LEV2_uR_hStr    = String(Double(LEV2_uR_h)  * 0.1 )
                LEV1_100uR_Str = String(Double(LEV1_100uR) * 0.1 )
                LEV2_100uR_Str = String(Double(LEV2_100uR) * 0.1 )
            }
            strDoseNotEditFlag = false
            if oldValue != curUnit  && oldValue != nil {
                comp.compose_VSFR_DEVICE_UNIT(unit: curUnit!)
            }
        }
    }

    @Published var curLightMode = LightMode.off {
        didSet {
            if enableWriteToDeviceFlag {
                var tempVal = DISP_CTRL
                    tempVal = tempVal! | RC_DISP_CTRL_LED_DIS_msk
                    tempVal = tempVal! ^ RC_DISP_CTRL_LED_DIS_msk

                switch curLightMode {
                    case .ena:    tempVal = tempVal! | RC_DISP_CTRL_LED_ENA
                    case .auto:   tempVal = tempVal! | RC_DISP_CTRL_LED_AUTO
                    case .off:    tempVal = tempVal! | RC_DISP_CTRL_LED_DIS_msk
                                  tempVal = tempVal! ^ RC_DISP_CTRL_LED_DIS_msk
                }
                DISP_CTRL = tempVal
            }
        }
    }
    
    @Published var curDispMode = DisplayMode.auto {
        didSet {
            if enableWriteToDeviceFlag {
                var tempVal = DISP_CTRL
                    tempVal = tempVal! | RC_DISP_CTRL_DIR_AUTO_msk
                    tempVal = tempVal! ^ RC_DISP_CTRL_DIR_AUTO_msk
                    switch curDispMode {
                        case .normal:
                            tempVal = tempVal! | RC_DISP_CTRL_DIR_NORMAL
                        case .rotate:
                            tempVal = tempVal! | RC_DISP_CTRL_DIR_ROTATED
                        case .auto:
                            tempVal = tempVal! | RC_DISP_CTRL_DIR_AUTO_msk
                            tempVal = tempVal! ^ RC_DISP_CTRL_DIR_AUTO_msk
                    }
                DISP_CTRL = tempVal
            }
        }
    }



    @Published var curOffTime = OffTime.sec10 {
        didSet {
            switch curOffTime {
                case .sec05: DISP_OFF_TIME = OffTime.sec05.rawValue
                case .sec10: DISP_OFF_TIME = OffTime.sec10.rawValue
                case .sec15: DISP_OFF_TIME = OffTime.sec15.rawValue
                case .sec30: DISP_OFF_TIME = OffTime.sec30.rawValue
            }
        }
    }
    
    @Published var DISP_OFF_TIME: UInt32? {
        didSet {
            if oldValue == nil {
                setOffTime()
            } else {
                if enableWriteToDeviceFlag {
                    comp.compose_VSFR_DISP_OFF_TIME(offTime: DISP_OFF_TIME!)
                }
            }
        }
    }
    
    @Published var brightness : Double = 1.0 {
        didSet { // TODO: Установка правильной яркости как было на приборе
            if brightness != oldValue {
                DISP_BRT = UInt32(brightness)
            }
        }
    }
    
    @Published var DISP_BRT: UInt32? {
        didSet {
            if enableWriteToDeviceFlag {
                print(self.DISP_BRT!)
                comp.compose_VSFR_DISP_BRT(brightness: self.DISP_BRT!)
            } else {
                brightness = Double(self.DISP_BRT!)
            }
        }
    }
    
    @Published var LEV1_uR_hStr = "0.4" {
        didSet {
            if enableWriteToDeviceFlag {
                if !strDoseNotEditFlag {                        // Это не внутреннее редактирование можем менять Float-оригинал
                    if let floatVal = Float(LEV1_uR_hStr) {     // Даём сигнал записать в прибор
                        if curUnit == Unit.sv {
                            LEV1_uR_h = UInt32(floatVal * 100)  // Речь идёт о микрозивертах
                        } else {
                            LEV1_uR_h = UInt32(floatVal)        // Речь идёт о микрорентгенах
                        }
                    }
                }
            }
        }
    }
    
    @Published var LEV2_uR_hStr = "1.2" {
        didSet {
            if enableWriteToDeviceFlag {
                if !strDoseNotEditFlag {                        // Это не внутреннее редактирование можем менять Float-оригинал
                    if let floatVal = Float(LEV2_uR_hStr) {     // Даём сигнал записать в прибор
                        if curUnit == Unit.sv {
                            LEV2_uR_h = UInt32(floatVal * 100)  // Речь идёт о микрозивертах
                        } else {
                            LEV2_uR_h = UInt32(floatVal)        // Речь идёт о микрорентгенах
                        }
                    }
                }
            }
        }
    }
    
    @Published var LEV1_100uR_Str = "" {
        didSet {
            if enableWriteToDeviceFlag {
                if !strDoseNotEditFlag { // Это не внутреннее редактирование можем менять Float-оригинал
                    if let floatVal = Float(LEV1_100uR_Str) {
                        if curUnit == Unit.sv {
                            LEV1_100uR = UInt32(floatVal * 1000)    // Речь идёт о миллизивертах
                        } else {
                            LEV1_100uR = UInt32(floatVal * 10)      // Речь идёт о миллирентгенах
                        }
                    }
                }
            }
        }
    }
    
    @Published var LEV2_100uR_Str = "" {
        didSet {
            if enableWriteToDeviceFlag {
                if !strDoseNotEditFlag { // Это не внутреннее редактирование можем менять Float-оригинал
                    if let floatVal = Float(LEV2_100uR_Str) {
                        if curUnit == Unit.sv {
                            LEV2_100uR = UInt32(floatVal * 1000)    // Речь идёт о миллизивертах
                        } else {
                            LEV2_100uR = UInt32(floatVal * 10)      // Речь идёт о миллирентгенах
                        }
                    }
                }
            }
        }
    }
    
    /// Текстовое значение усреднения "серой" RAW зоны
    @Published var RAW_FILTERStr = "" { // Пустая строка при старте приложения
        didSet {                        // При изменении строки либо запись из прибора, либо запись пользователем в окне настроек
            if let curVal = UInt32(RAW_FILTERStr) {             // Если строку удаётся превратить в целое
                if !oldValue.isEmpty {                          // и предыдущая строка не пустая
                    if curVal >= 0 && curVal <= 30 {            // и полученное целое находится в диапазоне 0-30
                        if  RAW_FILTER != nil {                 // и предыдущее числовое значение не пустое (мы не в момент старта приложения)
                            RAW_FILTER  = UInt32(RAW_FILTERStr) // Записываем целую интерпретацию строки в числовую ячейку
                        }
                    }
                }
            }
        }
    }
    
    @Published var LEV1_uR_h  = UInt32(0.0) { /* RW, Опция - первый порог по мощности дозы */
        didSet {
            if enableWriteToDeviceFlag {
                print("\nЗаписываем порог по мощности: \(LEV1_uR_h)")
                comp.compose_VSFR_DEVICE_LEV1_uR_h(rate: LEV1_uR_h) // Запись производится в мкРентгенах!
            } else {
                if curUnit == Unit.sv { // Переводим текстовку в мкЗиверты
                    LEV1_uR_hStr = String(Double(LEV1_uR_h) * 0.01)
                }
                if curUnit == Unit.r { // Переводим текстовку в мкРентгены
                    LEV1_uR_hStr = String(Double(LEV1_uR_h) )
                }
            }
        }
    }
    
    @Published var LEV2_uR_h  = UInt32(0.0) {  /* RW, Опция - второй порог по мощности дозы */
        didSet {
            if enableWriteToDeviceFlag {
                print("\nЗаписываем порог по мощности: \(LEV2_uR_h)")
                comp.compose_VSFR_DEVICE_LEV2_uR_h(rate: LEV2_uR_h) // Запись производится в мкРентгенах!
            } else {
                if curUnit == Unit.sv { // Переводим текстовку в мкЗиверты
                    LEV2_uR_hStr = String(Double(LEV2_uR_h) * 0.01)
                }
                if curUnit == Unit.r { // Переводим текстовку в мкРентгены
                    LEV2_uR_hStr = String(Double(LEV2_uR_h))
                }
            }
        }
    }

    @Published var LEV1_100uR = UInt32(0.0) { /* RW, Опция - первый порог по дозе (1 соответствует 100 uR) */
        didSet {
            if enableWriteToDeviceFlag {
                print("\nЗаписываем порог по дозе: \(LEV1_100uR)")
                comp.compose_RC_VSFR_DS_LEV1_100uR(rate: LEV1_100uR)
            } else {
                if curUnit == Unit.sv { // Переводим текстовку в мкЗиверты
                    LEV1_100uR_Str = String(Double(LEV1_100uR) * 0.001)
                }
                if curUnit == Unit.r { // Переводим текстовку в мкРентены
                    LEV1_100uR_Str = String(Double(LEV1_100uR) * 0.1 )
                }
            }
        }
    }
    
    @Published var LEV2_100uR = UInt32(0.0) { /* RW, Опция - второй порог по дозе (1 соответствует 100 uR) */
        didSet {
            if enableWriteToDeviceFlag {
                print("\nЗаписываем порог по дозе: \(LEV2_100uR)")
                comp.compose_RC_VSFR_DS_LEV2_100uR(rate: LEV2_100uR)
            } else {
                if curUnit == Unit.sv { // Переводим текстовку в мкЗиверты
                    LEV2_100uR_Str = String(Double(LEV2_100uR) * 0.001)
                }
                if curUnit == Unit.r { // Переводим текстовку в мкРентены
                    LEV2_100uR_Str = String(Double(LEV2_100uR) * 0.1 )
                }
            }
        }
    }

    /// RW, Опция - глубина фильтрации значений, выводимых в серой зоне графиков (Ширина окна усреднения оперативных данных при отображении на графиках)
    @Published var RAW_FILTER: UInt32? {
        didSet {
            if oldValue == nil {                                        // Если мы в моменте считывания опций из прибора при старте приложения…
                RAW_FILTERStr = String(UInt32(RAW_FILTER ?? 0))         // Записываем в строку для окна Опций текстовое значение значения
            } else {                                                    // Если произошло изменение значения пользователем…
                print("\nШирина окна усреднения: \(   RAW_FILTER!)")
                comp.compose_RC_VSFR_RAW_FILTER(rate: RAW_FILTER!)      // Отправляем изменённое значение в прибор
            }
        }
    }
    
    /// Усовершенствованная версия поля настроек прибора с `willSet` для того, чтобы компенсировать отсутствие в `Toggle` возможности использованиия `option` для логической переменной `IsOn`
    var DEVICE_CTRL: UInt32? {
        willSet(newValue) { if DEVICE_CTRL == nil &&
                               newValue    != nil { setBooleanDevice(options: newValue!) } }                        // Установка первоначальных значений
         didSet           { if oldValue    != nil &&
                               DEVICE_CTRL != nil { comp.compose_VSFR_DEVICE_CTRL(deviceOptions: DEVICE_CTRL!) }}   // Запись в прибор
    }
    
    
    
    var SOUND_CTRL : UInt32? { didSet { if oldValue == nil && SOUND_CTRL != nil { setBooleanSoundOptions() } else if SOUND_CTRL != nil { comp.compose_VSFR_SOUND_CTRL(soundOptions  : SOUND_CTRL!) }}}
    var VIBRO_CTRL : UInt32? { didSet { if oldValue == nil && VIBRO_CTRL != nil { setBooleanVibroOptions() } else if VIBRO_CTRL != nil { comp.compose_VSFR_VIBRO_CTRL(vibroOptions  : VIBRO_CTRL!) }}}
    var DISP_CTRL  : UInt32? { didSet { if oldValue == nil && DISP_CTRL  != nil { setEnumDisplayOptions () } else if DISP_CTRL  != nil { comp.compose_VSFR_DISP_CTRL (displayOptions: DISP_CTRL!)  }}}
    
    
    // Логическая проверяемая переменная для Toggle     | при изменении если значение другое     и   значение UInt32 не пустое | устанавливаем соответствующий бит в поле настроек прибора
    @Published var DEVICE_CTRL_PWR_ON    : Bool = false { didSet { if oldValue != DEVICE_CTRL_PWR_ON     && DEVICE_CTRL != nil { DEVICE_CTRL = DEVICE_CTRL_PWR_ON     ? DEVICE_CTRL! | RC_DEVICE_CTRL_PWR_ON     : DEVICE_CTRL! ^ RC_DEVICE_CTRL_PWR_ON     }}}
    @Published var DEVICE_CTRL_DISPLAY_ON: Bool = false { didSet { if oldValue != DEVICE_CTRL_DISPLAY_ON && DEVICE_CTRL != nil { DEVICE_CTRL = DEVICE_CTRL_DISPLAY_ON ? DEVICE_CTRL! | RC_DEVICE_CTRL_DISPLAY_ON : DEVICE_CTRL! ^ RC_DEVICE_CTRL_DISPLAY_ON }}}
    @Published var DEVICE_CTRL_SOUND_ENA : Bool = false { didSet { if oldValue != DEVICE_CTRL_SOUND_ENA  && DEVICE_CTRL != nil { DEVICE_CTRL = DEVICE_CTRL_SOUND_ENA  ? DEVICE_CTRL! | RC_DEVICE_CTRL_SOUND_ENA  : DEVICE_CTRL! ^ RC_DEVICE_CTRL_SOUND_ENA  }}}
    @Published var DEVICE_CTRL_LEDS_ENA  : Bool = false { didSet { if oldValue != DEVICE_CTRL_LEDS_ENA   && DEVICE_CTRL != nil { DEVICE_CTRL = DEVICE_CTRL_LEDS_ENA   ? DEVICE_CTRL! | RC_DEVICE_CTRL_LEDS_ENA   : DEVICE_CTRL! ^ RC_DEVICE_CTRL_LEDS_ENA   }}}
    @Published var DEVICE_CTRL_VIBRO_ENA : Bool = false { didSet { if oldValue != DEVICE_CTRL_VIBRO_ENA  && DEVICE_CTRL != nil { DEVICE_CTRL = DEVICE_CTRL_VIBRO_ENA  ? DEVICE_CTRL! | RC_DEVICE_CTRL_VIBRO_ENA  : DEVICE_CTRL! ^ RC_DEVICE_CTRL_VIBRO_ENA  }}}
    @Published var DEVICE_CTRL_CHARGER_ON: Bool = false { didSet { if oldValue != DEVICE_CTRL_CHARGER_ON && DEVICE_CTRL != nil { DEVICE_CTRL = DEVICE_CTRL_CHARGER_ON ? DEVICE_CTRL! | RC_DEVICE_CTRL_CHARGER_ON : DEVICE_CTRL! ^ RC_DEVICE_CTRL_CHARGER_ON }}}
    @Published var DEVICE_CTRL_USER_RUN  : Bool = false { didSet { if oldValue != DEVICE_CTRL_USER_RUN   && DEVICE_CTRL != nil { DEVICE_CTRL = DEVICE_CTRL_USER_RUN   ? DEVICE_CTRL! | RC_DEVICE_CTRL_USER_RUN   : DEVICE_CTRL! ^ RC_DEVICE_CTRL_USER_RUN   }}}


    @Published var SOUND_CTRL_POWER_ENA:      Bool = false {
        didSet { if enableWriteToDeviceFlag { SOUND_CTRL = SOUND_CTRL_POWER_ENA ? SOUND_CTRL! | RC_SOUND_CTRL_POWER_ENA   : SOUND_CTRL! ^ RC_SOUND_CTRL_POWER_ENA   }}
    }
    @Published var isRC_SOUND_CTRL_CONNECT_ENA:    Bool = false {
        didSet { if enableWriteToDeviceFlag { SOUND_CTRL = isRC_SOUND_CTRL_CONNECT_ENA ? SOUND_CTRL! | RC_SOUND_CTRL_CONNECT_ENA : SOUND_CTRL! ^ RC_SOUND_CTRL_CONNECT_ENA }}
    }
    @Published var isRC_SOUND_CTRL_BUTTON_ENA    : Bool = false {  // Опция - разрешение/запрет сигнала по нажатию на кнопку
        didSet { if enableWriteToDeviceFlag { SOUND_CTRL = isRC_SOUND_CTRL_BUTTON_ENA  ? SOUND_CTRL! | RC_SOUND_CTRL_BUTTON_ENA  : SOUND_CTRL! ^ RC_SOUND_CTRL_BUTTON_ENA  }}
    }
    @Published var isRC_SOUND_CTRL_CLICK_ENA     : Bool = false {  // Опция - разрешение/запрет сигнала по регистрации частиц
        didSet {if enableWriteToDeviceFlag  { SOUND_CTRL = isRC_SOUND_CTRL_CLICK_ENA   ? SOUND_CTRL! | RC_SOUND_CTRL_CLICK_ENA   : SOUND_CTRL! ^ RC_SOUND_CTRL_CLICK_ENA   }}
    }
    
    @Published var isRC_SOUND_CTRL_DR_ALM_L1_ENA : Bool = false { // Опция - разрешение/запрет сигнала по превышению первого порога по мощности дозы
        didSet {if enableWriteToDeviceFlag {SOUND_CTRL = isRC_SOUND_CTRL_DR_ALM_L1_ENA ? SOUND_CTRL! | RC_SOUND_CTRL_DR_ALM_L1_ENA : SOUND_CTRL! ^ RC_SOUND_CTRL_DR_ALM_L1_ENA}}
    }
    @Published var isRC_SOUND_CTRL_DR_ALM_L2_ENA : Bool = false { // Опция - разрешение/запрет сигнала по превышению второго порога по мощности дозы
        didSet {if enableWriteToDeviceFlag {SOUND_CTRL = isRC_SOUND_CTRL_DR_ALM_L2_ENA ? SOUND_CTRL! | RC_SOUND_CTRL_DR_ALM_L2_ENA : SOUND_CTRL! ^ RC_SOUND_CTRL_DR_ALM_L2_ENA }}
    }
    @Published var isRC_SOUND_CTRL_DR_ALM_OS_ENA : Bool = false {   // Опция - разрешение/запрет сигнала по зашкаливанию мощности дозы
        didSet {if enableWriteToDeviceFlag {SOUND_CTRL = isRC_SOUND_CTRL_DR_ALM_OS_ENA ? SOUND_CTRL! | RC_SOUND_CTRL_DR_ALM_OS_ENA : SOUND_CTRL! ^ RC_SOUND_CTRL_DR_ALM_OS_ENA }}
    }
    @Published var isRC_SOUND_CTRL_DS_ALM_L1_ENA : Bool = false {   // Опция - разрешение/запрет сигнала по превышению первого порога по дозе
        didSet {if enableWriteToDeviceFlag {SOUND_CTRL = isRC_SOUND_CTRL_DS_ALM_L1_ENA ? SOUND_CTRL! | RC_SOUND_CTRL_DS_ALM_L1_ENA : SOUND_CTRL! ^ RC_SOUND_CTRL_DS_ALM_L1_ENA }}
    }
    @Published var isRC_SOUND_CTRL_DS_ALM_L2_ENA : Bool = false {  // Опция - разрешение/запрет сигнала по превышению второго порога по дозе
        didSet {if enableWriteToDeviceFlag {SOUND_CTRL = isRC_SOUND_CTRL_DS_ALM_L2_ENA ? SOUND_CTRL! | RC_SOUND_CTRL_DS_ALM_L2_ENA : SOUND_CTRL! ^ RC_SOUND_CTRL_DS_ALM_L2_ENA }}
    }
    @Published var isRC_SOUND_CTRL_DS_ALM_OS_ENA : Bool = false {  // Опция - разрешение/запрет сигнала по зашкаливанию дозы
        didSet {if enableWriteToDeviceFlag {SOUND_CTRL = isRC_SOUND_CTRL_DS_ALM_OS_ENA ? SOUND_CTRL! | RC_SOUND_CTRL_DS_ALM_OS_ENA : SOUND_CTRL! ^ RC_SOUND_CTRL_DS_ALM_OS_ENA }}
    }
    @Published var isRC_VIBRO_CTRL_BUTTON_ENA    : Bool = false {  // Опция - разрешение/запрет сигнала по нажатию на кнопку
        didSet {if enableWriteToDeviceFlag {VIBRO_CTRL = isRC_VIBRO_CTRL_BUTTON_ENA ? VIBRO_CTRL! | RC_VIBRO_CTRL_BUTTON_ENA : VIBRO_CTRL! ^ RC_VIBRO_CTRL_BUTTON_ENA }}
    }

    @Published var isRC_VIBRO_CTRL_CLICK_ENA     : Bool?  // Опция - разрешение/запрет сигнала по регистрации частиц
    
    
    @Published var isRC_VIBRO_CTRL_DR_ALM_L1_ENA : Bool = false {   // Опция - разрешение/запрет сигнала по превышению первого порога по мощности дозы
        didSet {if enableWriteToDeviceFlag {VIBRO_CTRL = isRC_VIBRO_CTRL_DR_ALM_L1_ENA ? VIBRO_CTRL! | RC_VIBRO_CTRL_DR_ALM_L1_ENA : VIBRO_CTRL! ^ RC_VIBRO_CTRL_DR_ALM_L1_ENA }}
    }
    @Published var isRC_VIBRO_CTRL_DR_ALM_L2_ENA : Bool = false {  // Опция - разрешение/запрет сигнала по превышению второго порога по мощности дозы
        didSet {if enableWriteToDeviceFlag {VIBRO_CTRL = isRC_VIBRO_CTRL_DR_ALM_L2_ENA ? VIBRO_CTRL! | RC_VIBRO_CTRL_DR_ALM_L2_ENA : VIBRO_CTRL! ^ RC_VIBRO_CTRL_DR_ALM_L2_ENA }}
    }
    @Published var isRC_VIBRO_CTRL_DR_ALM_OS_ENA : Bool = false {  // Опция - разрешение/запрет сигнала по зашкаливанию мощности дозы
        didSet {if enableWriteToDeviceFlag {VIBRO_CTRL = isRC_VIBRO_CTRL_DR_ALM_OS_ENA ? VIBRO_CTRL! | RC_VIBRO_CTRL_DR_ALM_OS_ENA : VIBRO_CTRL! ^ RC_VIBRO_CTRL_DR_ALM_OS_ENA }}
    }
    @Published var isRC_VIBRO_CTRL_DS_ALM_L1_ENA : Bool = false {  // Опция - разрешение/запрет сигнала по превышению первого порога по дозе
        didSet {if enableWriteToDeviceFlag {VIBRO_CTRL = isRC_VIBRO_CTRL_DS_ALM_L1_ENA ? VIBRO_CTRL! | RC_VIBRO_CTRL_DS_ALM_L1_ENA : VIBRO_CTRL! ^ RC_VIBRO_CTRL_DS_ALM_L1_ENA }}
    }
    @Published var isRC_VIBRO_CTRL_DS_ALM_L2_ENA : Bool = false {  // Опция - разрешение/запрет сигнала по превышению второго порога по дозе
        didSet {if enableWriteToDeviceFlag {VIBRO_CTRL = isRC_VIBRO_CTRL_DS_ALM_L2_ENA ? VIBRO_CTRL! | RC_VIBRO_CTRL_DS_ALM_L2_ENA : VIBRO_CTRL! ^ RC_VIBRO_CTRL_DS_ALM_L2_ENA }}
    }
    @Published var isRC_VIBRO_CTRL_DS_ALM_OS_ENA : Bool = false {  // Опция - разрешение/запрет сигнала по зашкаливанию дозы
        didSet {if enableWriteToDeviceFlag {VIBRO_CTRL = isRC_VIBRO_CTRL_DS_ALM_OS_ENA ? VIBRO_CTRL! | RC_VIBRO_CTRL_DS_ALM_OS_ENA : VIBRO_CTRL! ^ RC_VIBRO_CTRL_DS_ALM_OS_ENA }}
    }

    
    var curLangStr     : String { return      langStr(lang     : curLang) }
    var curUnitStr     : String { return      unitStr(unit     : curUnit ?? .sv) } // TODO: Не факт, что '?? .sv'
    var curAlarmModeStr: String { return alarmModeStr(alarmMode: curAlarmMode ?? .many) } // TODO: Не факт, что '?? .many'
    var curCommand     : COMMAND = .NONE

    
    
    // MARK: - Init
    
    init() {
        ble  = BLEConnection()                  // Создаём модуль подключения к BLE
        ble.startCentralManager()               // Запускаем попытку подключения
        comp = PacketComposer(ble)              // Создаём объект формирования запросов к устройству
        prms = ParamsParser()                   // Объект декодировки строки параметров ([DeviceParams])
        pars = PacketParser(prms)               // Создаём объект декодировки полученных ответов с устройства

        sharePars = pars
        
        // Подписываемся на издателей нужных нам данных
        cancellableBLE        = blePublisher  () // Издатель установки строки статуса
        cancellableStatus     = statPublisher () // Издатель определения состояния устройстваа
        cancellableBleData    = dataPublisher () // Издатель полученных из прибора данных
        cancellableDoseRate   = dosePublisher () // Издатель потока данных мощности дозы
        cancellableCountRate  = countPublisher() // Издатель потока данных счётчика импульсов
        
        
        cancellableLastDoseRate   = lastDosePublisher()    // Издатель потока последней мощности дозы
        cancellableLastCountRate  = lastCountPublisher()   // Издатель потока последнего значения данных счётчика импульсов
        cancellableLastDoseRateE  = lastDoseEPublisher()    // Издатель потока последней средней ошибки мощности дозы
        cancellableLastCountRateE = lastCountEPublisher()   // Издатель потока последнего значения данных средней ошибки счётчика импульсов

        
        cancellableRawDoseRate       = doseRawPublisher () // Издатель потока сырых данных мощности дозы
        cancellableRawCountRate      = countRawPublisher() // Издатель потока сырых данных счётчика импульсов
        cancellableCommand           = comPublisher  () // Издатель завершённых команд прибора
        cancellablePeripheral        = peripheralPublisher()               // Издатель переферийного устройства (прибор)
        cancellableAdvertisementData = advertisementDataPublisher() // Издатель подробного описания устройства
        cancellableGrpCount          = grpCountPublisher()                 // Издатель счётчика распознанных групп
        cancellableInt               = intPublisher()                      // Издатель целочисленного значения парсинга
        
        bdLoadTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { [self] timer in
            
            if curFromDiagramDate == nil { // Мы в начале - ставим значения по-умолчанию
                curTo__DiagramDate = Date()
                curTimeScaleRange  = 30 * 60
            }
            if isConnectDiagrammToCurDate { // Привязываем только по правому значению
                curTo__DiagramDate = Date()
            }
            
//            // Определения временного диапазона для захвата доп. точек
//            let curDateRange            = Calendar.current.dateComponents([.second], from: curFromDiagramDate!, to: curTo__DiagramDate!) // Вычисление диапазона
//            let curSecDataRange         = Double(curDateRange.second!) // Получаем число секунд диапазоне
//            let wideDataDeltaSecRange   = curSecDataRange * 0.15 // Будем раздвигать запрос к БД на 15% в каждую сторону…
//            let wideNewDateFrom         = curFromDiagramDate!.addingTimeInterval(-wideDataDeltaSecRange)
//            let wideNewDate__To         = curTo__DiagramDate!.addingTimeInterval( wideDataDeltaSecRange)
//
//            grpDose_RateTimerArray  = Log.getDoseRateDB (in: managedObjectContext, from: wideNewDateFrom, to: wideNewDate__To) ?? []
//            grpCountRateTimerArray = Log.getCountRateDB(in: managedObjectContext, from: wideNewDateFrom, to: wideNewDate__To) ?? []
            
            defineOutOffBordersForTimeRange()
        }
    }
    
    // MARK: - Использование Combine
    
    func intPublisher() -> Cancellable { // Издатель целочисленного значения парсинга
        return pars.intPublisher
            .receive(on: DispatchQueue.main)
            .sink { [self] value in
                switch curCommand {
                    case .VSFR_DS_UNITS         : curUnitVal      = value
                    case .VSFR_DEVICE_LANG      : curLangVal      = value
                    case .VSFR_ALARM_MODE       : curAlarmModeVal = value
                    case .VSFR_DEVICE_CTRL      : DEVICE_CTRL    = UInt32(value)
                    case .VSFR_SOUND_CTRL       : SOUND_CTRL     = UInt32(value)
                    case .VSFR_VIBRO_CTRL       : VIBRO_CTRL     = UInt32(value)
                    case .VSFR_DISP_CTRL        : DISP_CTRL      = UInt32(value)
                    case .VSFR_DISP_BRT         : DISP_BRT       = UInt32(value)
                    case .VSFR_DISP_OFF_TIME    : DISP_OFF_TIME  = UInt32(value)
                    case .RC_VSFR_DR_LEV1_uR_h  : LEV1_uR_h      = UInt32(value)
                    case .RC_VSFR_DR_LEV2_uR_h  : LEV2_uR_h      = UInt32(value)
                    case .RC_VSFR_DS_LEV1_100uR : LEV1_100uR     = UInt32(value)
                    case .RC_VSFR_DS_LEV2_100uR : LEV2_100uR     = UInt32(value)
                    case .RC_VSFR_RAW_FILTER    : RAW_FILTER     = UInt32(value)
                    default: break
                }
            }
    }

    func blePublisher() -> Cancellable { // Издатель установки строки статуса
        return ble.connectPublisher
            .receive(on: DispatchQueue.main)
            .sink { [self] value in
                statusStr = value
            }
    }
    
    func grpCountPublisher() -> Cancellable { // Издатель установки строки статуса
        return pars.groupCountPublisher
            .receive(on: DispatchQueue.main)
            .sink { [self] value in
                groupCount = value
            }
    }

    func statPublisher() -> Cancellable { // Издатель определения состояния устройства
        return ble.statPublisher
            .receive(on: DispatchQueue.main)
            .sink { [self] value in
                status = value
                switch status {
                case .connected :  isConnect = true
                    timeoutTimer.invalidate()
                    Device.clearAllConnection(in: managedObjectContext)
                    Device.setConnect(uuid: ble.connectUUID!, in: managedObjectContext)
                case .serviceFound: compose_SET_EXCHANGE() // Здесь начало подачи команд в устройство
                case .disconnected: isConnect = false
                    Device.clearAllConnection(in: managedObjectContext)
                    if curCommand != .BACKGROUND && isDefault {
                        statusStr = String(localized: "Reconnect…")
                        DispatchQueue.main.asyncAfter(deadline: .now() + 4) { [self] in
                                           print("ЗАДЕРЖКА ПОДКЛЮЧЕНИЯ!")
                            pars.isPacketContinue   = false
                            enableWriteToDeviceFlag = false
                            ble.startCentralManager()
                            startConnecting()
                        }
                    }
                case .none:
                    statusStr = String(localized: "NOT DEFINDED")
//                case .some(.serviceFound):
//                    break
                case .some(.timeout): break
                case .connecting:
                    timeoutTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: false) { [self] timer in
                        ble.stop_Scan()
                        isConnectionProgerss = false
                        statusStr = String(localized: "Timeout…")
                        Device.clearAllConnection(in: managedObjectContext)
                    }
                }
            }
    }

    func dataPublisher () -> Cancellable { // Издатель полученных из прибора данных
        return ble.dataPublisher
            .receive(on: DispatchQueue.main)
            .sink { value in
                let data = value
                let bytesGetArray = toByteArray(data: data)
//                var bytesGetArray = [UInt8](repeating: 0, count: (data.count)) // Создание пустого массива байт размером под полученные данные
//                (data as NSData).getBytes(&bytesGetArray, length:(data.count) * MemoryLayout<UInt8>.size) // Заполнение массива байт данными
//                for element in bytesGetArray { print(String(format:"%02X ", element), terminator: "") }
                self.pars.defineCommand(bytesGetArray)
            }
    }
    
    func dosePublisher() -> Cancellable { // Издатель потока данных мощности дозы
        return pars.doseRatePublisher
            .receive(on: DispatchQueue.main)
            .sink { [self] value in
                curDoseVal = value.chnDoseRate
                // FIXME: Правильно вычислять удаление старых данных
                if  grpDoseRateArray.count > 10000 {
                    grpDoseRateArray.remove(at: 0)
                }
                // Cортировка массива
                let newElement = value
                let index = grpDoseRateArray.firstIndex(where: { newElement.msTime > $0.msTime })
                grpDoseRateArray.insert(newElement, at: index ?? grpDoseRateArray.endIndex)
//              grpDoseRateArray.append(value)
            }
    }
    
    func lastDosePublisher() -> Cancellable { // Издатель потока текущих данных мощности дозы
        return pars.lastDoseRatePublisher
            .receive(on: DispatchQueue.main)
            .sink { [self] value in
                grpDoseRateLast = value
            }
    }
    
    func lastDoseEPublisher() -> Cancellable { // Издатель потока текущих данных средней ошибки мощности дозы
        return pars.lastDoseRateErrPublisher
            .receive(on: DispatchQueue.main)
            .sink { [self] value in
                grpDoseRateErrLast = value
            }
    }

    
    func lastCountPublisher() -> Cancellable { // Издатель потока текущих данных скорости счёта
        return pars.lastCountPublisher
            .receive(on: DispatchQueue.main)
            .sink { [self] value in
                grpCountRateLast = value
            }
    }
    
    func lastCountEPublisher() -> Cancellable { // Издатель потока текущих данных средней ошибки скорости счёта
        return pars.lastCountErrPublisher
            .receive(on: DispatchQueue.main)
            .sink { [self] value in
                grpCountErrLast = value
            }
    }


    func countPublisher() -> Cancellable { // Издатель потока данных счётчика импульсов
        return pars.countPublisher
            .receive(on: DispatchQueue.main)
            .sink { [self] value in
//                curDoseVal = value.chnCountRate
                print(value.chnCountRate)
                if  grpCountRateArray.count > 10000 {
                    grpCountRateArray.remove(at: 0)
                }
                
                // Cортировка массива
                let newElement = value
                let index = grpCountRateArray.firstIndex(where: { newElement.msTime > $0.msTime })
                grpCountRateArray.insert(newElement, at: index ?? grpCountRateArray.endIndex)
            }
    }
    
    func doseRawPublisher() -> Cancellable { // Издатель потока данных мощности дозы
        return pars.doseRawRatePublisher
            .receive(on: DispatchQueue.main)
            .sink { [self] value in
//                curDoseVal = value.chnDoseRate
                if  grpDoseRateArray.count > 10000 { // FIXME: Правильно вычислять размер счётчика времени
                    grpDoseRateArray.remove(at: 0)
                }
//                grpRawDoseRateArray.append(value)
                let timestamp = measureDate(value.msTime, startDate: pars.baseDate!)
                grpRawDoseRateTimerArray.append(ValRateDB(timestamp: timestamp, val_rate: Float(value.chnDoseRate)))
            }
    }

    func countRawPublisher() -> Cancellable { // Издатель потока данных счётчика импульсов
        return pars.countRawPublisher
            .receive(on: DispatchQueue.main)
            .sink { [self] value in
                print(value.chnCountRate)
                if  grpCountRateArray.count > 10000 {
                    grpCountRateArray.remove(at: 0)
                }
                let timestamp = measureDate(value.msTime, startDate: pars.baseDate!)
                grpRawCountRateTimerArray.append(ValRateDB(timestamp: timestamp, val_rate: Float(value.chnCountRate)))
            }
    }

    func comPublisher() -> Cancellable {
        return pars.comPublisher  // Издатель завершённых команд прибора
            .receive(on: DispatchQueue.main)
            .sink {  [self] value in
                if curCommand != .STOP {
                    curCommand = value
                    switch value {
                        case .SET_EXCHANGE          :   comp.compose_VSFR_DEVICE_LANG()
                        case .VSFR_DEVICE_LANG      :   comp.compose_RC_VSFR_DS_UNITS()
                        case .VSFR_DS_UNITS         :   comp.compose_VSFR_ALARM_MODE()
                        case .VSFR_ALARM_MODE       :   comp.compose_RC_VSFR_DISP_CTRL()
                        case .VSFR_DISP_CTRL        :   comp.compose_VSFR_DISP_OFF_TIME()
                        case .VSFR_DISP_OFF_TIME    :   comp.compose_VSFR_DISP_BRT()
                        case .VSFR_DISP_BRT         :   comp.compose_RC_VSFR_DR_LEV1_uR_h()
                        case .RC_VSFR_DR_LEV1_uR_h  :   comp.compose_RC_VSFR_DR_LEV2_uR_h()
                        case .RC_VSFR_DR_LEV2_uR_h  :   comp.compose_RC_VSFR_DS_LEV1_100uR()
                        case .RC_VSFR_DS_LEV1_100uR :   comp.compose_RC_VSFR_DS_LEV2_100uR()
                        case .RC_VSFR_DS_LEV2_100uR :   comp.compose_VSFR_DEVICE_CTRL()
                        case .VSFR_DEVICE_CTRL      :   comp.compose_VSFR_SOUND_CTRL()
                        case .VSFR_SOUND_CTRL       :   comp.compose_VSFR_VIBRO_CTRL()
                        case .VSFR_VIBRO_CTRL       :   comp.compose_VS_CONFIGURATION()
                        case .VS_CONFIGURATION      :
                            enableWriteToDeviceFlag = true
                            comp.compose_RC_VSFR_RAW_FILTER()
                        case .RC_VSFR_RAW_FILTER    :   comp.compose_SET_LOCAL_TIME()
                        case .SET_LOCAL_TIME        :   comp.compose_VSFR_DEVICE_TIME(); pars.baseDate = Date()
                        case .VSFR_DEVICE_TIME      :
                        isConnectionProgerss = false                        // Мы выполнили все подготовительные операции: песочные часы больше не нужны…
                            if selectedMenu == "" {                         // Если мы в начале запуска приложения
                                selectedMenu = String(localized: "Graph")   // теперь выбираем закладку с графиками
                            }
                            comp.compose_VS_DATA_BUF()                      // Запускаем цикл построения графиков
                        
//                        case .VSFR_DEVICE_TIME      :
//                            enableStartDataBufCycle = true
//                            enableSetOptions        = true
//                            break // Пока здесь останавливаемся
                        case .VS_DATA_BUF           : comp.compose_VS_DATA_BUF()
                        case .VS_DATA_BUF_empty     :                       // Прибор не успел ничего передать: нужно немного подождать…
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                                               print("ЗАДЕРЖКА!")
                                            self.comp.compose_VS_DATA_BUF()
                            }
                        default: print(String(localized: "UNKNOWN COMMAND!"))// statusStr = String(localized: "UNKNOWN COMMAND!")
                    }
                }
            }
    }
    
    func peripheralPublisher() -> Cancellable { // Издатель подключения к BLE
        return ble.peripheralPublisher
            .receive(on: DispatchQueue.main)
            .sink { [self] value in
                peripheral          = value
//                if ble.connectUUID == nil {
//                    isFindNewPeripheral = true  // Только если это действительно новое устройство
//                }
            }
    }
    
    func advertisementDataPublisher() -> Cancellable {
        return ble.advertisementDataPublisher
            .receive(on: DispatchQueue.main)
            .sink { [self] value in
                advertisementData = value
                getPeripheralData()
            }
    }

    // MARK: -
    
    func startConnecting() {
        
        let device = Device.findDefaultDevice(in: managedObjectContext)
        if  device != nil {
            
            
//            if curCommand == .INACTIVE ||
//               curCommand == .BACKGROUND {
//                    ble  = BLEConnection()                  // Создаём модуль подключения к BLE
//                    ble.startCentralManager()               // Запускаем попытку подключения
//                    compose_SET_EXCHANGE()
//            }

            isConnectionProgerss = true
            isDefault = true
            ble.connect(device: device!)
        }
        countGraphPrepare()
        doseGraphPrepare()


    }
    
    func countGraphPrepare() {
        
        countRateDB = Log.getCountRateDB(in: managedObjectContext)
        
// DEBUG:
//        print("countRateDB.count = \(countRateDB?.count ?? 0)")
//        for item in countRateDB ?? [] {
//            print(item.timestamp, item.val_rate)
//        }
//        let countRateDBMax = countRateDB!.max(by: { (a, b) -> Bool in return a.val_rate < b.val_rate })
//        print(countRateDBMax?.timestamp, countRateDBMax?.count_rate)
        
        let range = Log.getCountRateDB_range(in: managedObjectContext)
        minCountRateDB = range?.min ?? 0
        maxCountRateDB = range?.max ?? 0
        
        minDateCountRateDB = countRateDB?.first?.timestamp
        maxDateCountRateDB = countRateDB?.last? .timestamp
         
        
        curMinDateCountRateDB = countRateDB?.first?.timestamp
        if minDateCountRateDB != nil {
            curMidDateCountRateDB = getMidDate(from: minDateCountRateDB!, to: maxDateCountRateDB!)
        }
        curMaxDateCountRateDB = countRateDB?.last? .timestamp
        
        // Одноразово определяем максимальное значение для фиксации начального масштаба графика
        var graphCounts: [ValRangeDB] = []
        if minDateCountRateDB == nil || maxDateCountRateDB  == nil {
             graphCounts = logDecimation(counts: countRateDB!, from: Date(), to: Date(), pixelRange: 600) // FIXME: Сейчас это только для того, чтобы определить максимальное значение
        } else {
             graphCounts = logDecimation(counts: countRateDB!, from: minDateCountRateDB!, to: maxDateCountRateDB!, pixelRange: 600) // FIXME: Сейчас это только для того, чтобы определить максимальное значение
        }

        curMaxCountRateValDB = graphCounts.max(by: { (a, b) -> Bool in return a.val_max < b.val_max })?.val_max
        if curMaxCountRateValDB == nil {
            curMaxCountRateValDB = 20.0
        }


// DEBUG:
//        print(curMaxCountRateValDB)
        
        if curMaxCountRateValDB != nil {
            defineCountRateValAxis_Lite(maxVal: Double(curMaxCountRateValDB!))
        }
//        let _ = print(options.curMaxDateCountRateValDB)

    }
    
    func doseGraphPrepare() {
        
        // Обработка мощности дозы
        dose_RateDB = Log.getDoseRateDB(in: managedObjectContext)
        
// DEBUG:
//        print("doseRateDB.count = \(doseRateDB?.count ?? 0)")
//        for doseRange in doseRateDB! {
//            print(doseRange.timestamp, doseRange.val_rate)
//        }
//        let doseRateDBMax = doseRateDB!.max(by: { (a, b) -> Bool in return a.val_rate < b.val_rate })
// DEBUG:
//        print(doseRateDBMax?.timestamp, doseRateDBMax?.val_rate)

        let range2 = Log.getDoseRateDB_range(in: managedObjectContext)
       minDoseRateDB = range2?.min ?? 0
       maxDoseRateDB = range2?.max ?? 0
       
       minDateDoseRateDB = dose_RateDB?.first?.timestamp
       maxDateDoseRateDB = dose_RateDB?.last? .timestamp
        
//    print("ДАТА minDateDoseRateDB \(minDateDoseRateDB!) - maxDateDoseRateDB  \(maxDateDoseRateDB!)")
       
        curMinDateDoseRateDB = (dose_RateDB?.first?.timestamp ?? Date())
        curMinDateDoseRateDB        = Date().addingTimeInterval(-30 * 60)
        
        if minDateDoseRateDB != nil {
            curMidDateDoseRateDB = getMidDate(from: minDateDoseRateDB!, to: maxDateDoseRateDB!)

        }
        curMaxDateDoseRateDB = (dose_RateDB?.last?.timestamp ?? Date())!
        
        // Одноразово определяем максимальное значение для фиксации начального масштаба графика
        var graphDoses: [ValRangeDB] = []
        graphDoses = logDecimation(counts: dose_RateDB!, from: curMinDateDoseRateDB, to: curMaxDateDoseRateDB, pixelRange: 600) // FIXME: Сейчас это только для того, чтобы определить максимальное значение
        let graphDosesMax = graphDoses.max(by: { (a, b) -> Bool in return a.val_max < b.val_max })
        curMaxDoseRateValDB = graphDosesMax?.val_max
        if curMaxDoseRateValDB == nil {
            curMaxDoseRateValDB = 0.2
        }
        curMaxDoseRateValDB = 0.4 // *******************************************************************
        print("curMaxDoseRateValDB = \(curMaxDoseRateValDB!)")

// DEBUG:
//        print("graphDoses.count =\(graphDoses.count)")
//        for doseRange in graphDoses {
//            print(doseRange.timestamp, doseRange.count_rate_max)
//        }

        if curMaxDoseRateValDB != nil {
            defineDoseRateValAxis_Lite(maxVal: Double(curMaxDoseRateValDB!))
        }

    }
    
//    func magnificationCountPlus() {
//        countRateDB = Log.getCountRateDB(in: managedObjectContext, from: "2022-02-19 14:47:28", to: "2022-02-19 20:45:27")
//    }

    
    func getPeripheralData() {
        
        print("Peripheral Name: \(String(describing: peripheral!.name))")
            peripheralUUID = peripheral!.identifier
        print("peripheral.identifier = \(String(describing: peripheralUUID))")
        let nameOfDeviceFound  = (advertisementData! as NSDictionary).object(forKey: CBAdvertisementDataLocalNameKey)    //   as? NSString
        if  nameOfDeviceFound != nil {
            namePeripheral = (nameOfDeviceFound! as? String)!
            print("Имя: " + namePeripheral)
        }
        
        let manufacturerData  = (advertisementData! as NSDictionary).object(forKey: CBAdvertisementDataManufacturerDataKey) // as? NSString
        if  manufacturerData != nil {
            
            let dataNS = manufacturerData! as! NSData   // получаем из типа Any данные в формате NSData
            let data = Data(referencing: dataNS)        // Преобразуем NSData в Data
            var ar   = toByteArray(data: data)          // Превращаем Data в массив байтов
                ar.removeFirst()                        // Убираем первый служебный байт
                ar.removeFirst()                        // Убираем второй служебный байт
                ar.append(0)                            // Добавляем пустой байт с другой стороны, чтобы распознать как UInt32
                sn = intFrom(ar)                        // Получаем серийный номер
            print("Сериный номер: \(sn) ")
        }
        let serviceData       = (advertisementData! as NSDictionary).object(forKey: CBAdvertisementDataServiceDataKey)     // as? NSString
        if  serviceData != nil {
                                                          print(" Обслуживание: \(serviceData!)")
        }
        let txPowerLevel      = (advertisementData! as NSDictionary).object(forKey: CBAdvertisementDataTxPowerLevelKey)     //as? NSString
        if  txPowerLevel != nil {
                                                          print("       Зарядка: \(txPowerLevel!)")
        }
        let serviceUUIDsKey      = (advertisementData! as NSDictionary).object(forKey: CBAdvertisementDataServiceUUIDsKey)    // as? NSString
        if  serviceUUIDsKey != nil {
                                                          print("   UUID: \(serviceUUIDsKey!)")
        }
        if Device.isUniqueDevice(uuid: peripheralUUID!, in: managedObjectContext) {
            
                withAnimation {
                    let newItem = Device(context: managedObjectContext)
                        newItem.timestamp  = Date()
                        newItem.name       =       namePeripheral
                        newItem.sn         = Int32(sn) // Для сохранения в CD
                        newItem.uuid       =       peripheralUUID!
                        newItem.is_default = false
                        newItem.user_name = String(localized: "Device")
                        newItem.chip = peripheral!.name
                    do {
                        try managedObjectContext.save()
                    } catch {
                        let nsError = error as NSError
                        fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                    }
                    
        //            Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false) { (Timer) in
        //                DispatchQueue.main.async {
        //                    if  options.findDevices.count > 0 {
        //                        options.isFindNewPeripheral = true
        //                    }
        //                }
        //            }
                }

//            print("Добавляю в массив \(sn)")
//            findDevices.append(
//                                FindDevice(peripheral: peripheral!,
//                                           sn: sn,
//                                           name: namePeripheral,
//                                           uuid: peripheralUUID!,
//                                           chip: peripheral!.name)
//            )
        }

    }
    
    // С какого места начинаются действительные числа в val и Сколько действительных знаков realDigits мы собираемся использовать
    func realDigitCount(_ val: Double, realDigits: Int = 2) -> Int {
        let decimal     = 10        // Значение одного десятичного порядка
        var digitsCount = 0         // Счётчик числа действительных знаков
        var currangeVal = val       // Число, для которого ищутся действительные знаки
        while true { // Повторяем пока не найдём нужное число действительных знаков, а затем выйдем сами xthtp return
            digitsCount += 1                                        // Увеличиваем счётчик порядка
            if val < 1 {                                            // От сравнения с единицей разный вид обработки
                currangeVal = currangeVal * Double(decimal)         // Увеличиваем на порядок (0.012 * 10 = 0.12)
                if  Int(currangeVal) > 0 {                          // Берём целую часть увеличенного на порядок
                    return -(digitsCount + (realDigits - 1 /* один знак уже нашли */))
                }
            } else if val == 1 {
                digitsCount = 1
                    return   digitsCount + (realDigits - 1 /* один знак уже нашли */)
            } else { // val > 1
                currangeVal = currangeVal / Double(decimal)
                if  Int(currangeVal) < 1 {                          // Берём целую часть увеличенного на порядок
                    return digitsCount - realDigits
                }
            }
        }
    }
    
    // Определение максимальных/минимальных значений массива данных
    
//    func defineMaxMinDoseRateDBVal() { // Для мощности дозы значения/время
//        grpDose_RateDBArrayMax_Val =        grpDoseRateDBArray.max(by: { (a, b) -> Bool in return a.chnDoseRate < b.chnDoseRate })!.chnDoseRate
//        grpDose_RateDBArrayMin_Val =        grpDoseRateDBArray.min(by: { (a, b) -> Bool in return a.chnDoseRate < b.chnDoseRate })!.chnDoseRate
//        grpDose_RateDBArrayMaxTime = Double(grpDoseRateDBArray.max(by: { (a, b) -> Bool in return a.msTime      < b.msTime      })!.msTime)
//        grpDose_RateDBArrayMinTime = Double(grpDoseRateDBArray.min(by: { (a, b) -> Bool in return a.msTime      < b.msTime      })!.msTime)
//        maxDoseDBVal = Double(grpDose_RateDBArrayMax_Val * 1.4)
//        minDoseDBVal = Double(grpDose_RateDBArrayMin_Val * 0.4)
//        max_DoseDB_Time = grpDose_RateDBArrayMaxTime / Double(100) // FIXME: 20 июня 2021 изменено с 1000 на 100
//        min_DoseDB_Time = grpDose_RateDBArrayMinTime / Double(100) // FIXME: 20 июня 2021 изменено с 1000 на 100
//    }

    
    func defineMaxMinDoseRateVal() { // Для мощности дозы значения/время
        grpDose_RateArrayMax_Val =        grpDoseRateArray.max(by: { (a, b) -> Bool in return a.chnDoseRate < b.chnDoseRate })!.chnDoseRate
        grpDose_RateArrayMin_Val =        grpDoseRateArray.min(by: { (a, b) -> Bool in return a.chnDoseRate < b.chnDoseRate })!.chnDoseRate
        grpDose_RateArrayMaxTime = Double(grpDoseRateArray.max(by: { (a, b) -> Bool in return a.msTime      < b.msTime      })!.msTime)
        grpDose_RateArrayMinTime = Double(grpDoseRateArray.min(by: { (a, b) -> Bool in return a.msTime      < b.msTime      })!.msTime)
        maxDoseVal = Double(grpDose_RateArrayMax_Val * 1.4)
        minDoseVal = Double(grpDose_RateArrayMin_Val * 0.4)
        max_Dose_Time = grpDose_RateArrayMaxTime / Double(100) // FIXME: 20 июня 2021 изменено с 1000 на 100
        min_Dose_Time = grpDose_RateArrayMinTime / Double(100) // FIXME: 20 июня 2021 изменено с 1000 на 100
    }
    
    func defineMaxMinRawDoseRateVal() { // Для мощности дозы сырых значений/времени
        grpRawDose_RateArrayMaxTime = Double(grpRawDoseRateArray.max(by: { (a, b) -> Bool in return a.msTime      < b.msTime      })!.msTime)
        grpRawDose_RateArrayMinTime = Double(grpRawDoseRateArray.min(by: { (a, b) -> Bool in return a.msTime      < b.msTime      })!.msTime)
        max_RawDose_Time = grpRawDose_RateArrayMaxTime / Double(100) // FIXME: 20 июня 2021 изменено с 1000 на 100
        min_RawDose_Time = grpRawDose_RateArrayMinTime / Double(100) // FIXME: 20 июня 2021 изменено с 1000 на 100
    }
    
    
    func defineMaxMinCountRateVal() { // Для счётчика импульсов значения/время
        
        grpCountRateArrayMaxVal  =        grpCountRateArray.max(by: { (a, b) -> Bool in return a.chnCountRate < b.chnCountRate })!.chnCountRate
        print(grpCountRateArrayMaxVal)
        grpCountRateArrayMinVal  =        grpCountRateArray.min(by: { (a, b) -> Bool in return a.chnCountRate < b.chnCountRate })!.chnCountRate
        grpCountRateArrayMaxTime = Double(grpCountRateArray.max(by: { (a, b) -> Bool in return a.msTime       < b.msTime      })!.msTime)
        grpCountRateArrayMinTime = Double(grpCountRateArray.min(by: { (a, b) -> Bool in return a.msTime       < b.msTime      })!.msTime)
        
        if Double(maxCountRateDB ?? 0) > grpCountRateArrayMaxVal { grpCountRateArrayMaxVal = Double(maxCountRateDB ?? 0) }
        if Double(minCountRateDB ?? 0) < grpCountRateArrayMinVal { grpCountRateArrayMinVal = Double(minCountRateDB ?? 0) }

        maxCountVal = grpCountRateArrayMaxVal //* 1.1
        print(maxCountVal)
        minCountVal = Double(grpCountRateArrayMinVal * 0.9)
        print(minCountVal)
        max_CountTime = grpCountRateArrayMaxTime / Double(100) // FIXME: 20 июня 2021 изменено с 1000 на 100
        min_CountTime = grpCountRateArrayMinTime / Double(100) // FIXME: 20 июня 2021 изменено с 1000 на 100
    }
    
    func defineMaxMinRawCountRateVal() { // Для счётчика импульсов сырых значений/времени
        grpRawCountRateArrayMaxTime = Double(grpRawCountRateArray.max(by: { (a, b) -> Bool in return a.msTime      < b.msTime      })!.msTime)
        grpRawCountRateArrayMinTime = Double(grpRawCountRateArray.min(by: { (a, b) -> Bool in return a.msTime      < b.msTime      })!.msTime)
        max_RawCountTime = grpRawCountRateArrayMaxTime / Double(100) // FIXME: 20 июня 2021 изменено с 1000 на 100
        min_RawCountTime = grpRawCountRateArrayMinTime / Double(100) // FIXME: 20 июня 2021 изменено с 1000 на 100
    }


    func defineDoseRateValAxis() {
        let decimalRange = realDigitCount(maxDoseVal)
            stepDoseVal = decimalRange > 0 ? Double(powTen(decimalRange)) : powPointOne(-decimalRange)
            minTicDoseVal = Double(Int(minDoseVal / stepDoseVal)) * stepDoseVal
            maxTicDoseVal = Double(Int(maxDoseVal / stepDoseVal)) * stepDoseVal
            gridDoseValRange = maxTicDoseVal - minTicDoseVal
        let gridValCount = Int(gridDoseValRange / stepDoseVal)
            doseRatePoints = []
        for gridNum in 0...gridValCount {
            doseRatePoints.append(GraphDoseRateArray(id: gridNum, chnDoseRate: Double(minTicDoseVal) + Double(gridNum) * stepDoseVal))
        }
    }
    
    func defineCountRateValAxis() {
        let decimalRange = realDigitCount(maxCountVal)
            stepCountVal = decimalRange > 0 ? Double(powTen(decimalRange)) : powPointOne(-decimalRange)
            minTicCountVal  = Double(Int(minCountVal / stepCountVal)) * stepCountVal
            maxTicCountVal  = Double(Int(maxCountVal / stepCountVal)) * stepCountVal
//            minTicCountVal = Double(minCountRateDB ?? 0) < curMinTicCountVal ? Double(minCountRateDB ?? 0) : curMinTicCountVal
//            maxTicCountVal = Double(maxCountRateDB ?? 0) > curMaxTicCountVal ? Double(maxCountRateDB ?? 0) : curMaxTicCountVal
            gridCountValRange = maxTicCountVal - minTicCountVal
        let gridValCount = Int(gridCountValRange / stepCountVal)
//        print(gridCountValRange, stepCountVal)
            countRatePoints = []
//        print("maxCountVal = \(maxCountVal)")
//        print("decimalRange = \(decimalRange)")
//        print("stepCountVal = \(stepCountVal)")
//        print("gridCountValRange = \(gridCountValRange)")
//        print("gridValCount = \(gridValCount)")
        for gridNum in 0...gridValCount {
            countRatePoints.append(GraphCountRateArray(id: gridNum, chnCountRate: Double(minTicCountVal) + Double(gridNum) * stepCountVal))
//            print(Double(minTicCountVal) + Double(gridNum) * stepCountVal)
        }
//        for count in grpCountRateArray {
//            print(count.chnCountRate)
//        }
    }
    
    
//    func defineCountRateValAxis_Lite(maxVal: Double) {
//        var ticCountStep = maxVal / 10 // Считаем, что нам нужно десять делений по оси Y
//        var ticCountStepInt = Int(ticCountStep)
//        var decimalStep = 0
//        while ticCountStepInt == 0 {
//            decimalStep += 1
//            ticCountStep *= 10
//            ticCountStepInt = Int(ticCountStep)
//        }
//        countRatePoints3 = []
//        for gridNum in 0...10 {
//            countRatePoints3.append(GraphCountRateArray(id: gridNum, chnCountRate: Double(gridNum) * Double(ticCountStepInt)))
//            print(Double(minTicCountVal) + Double(gridNum) * stepCountVal)
//        }
//
//    }
    
    /// Более простая версия расчёта делений шкалы скорости счётчика
    func defineCountRateValAxis_Lite(maxVal: Double) {
        var ticCountStep = maxVal / 10 // Считаем, что нам нужно десять делений по оси Y
        var ticCountStepInt = Double(Int(ticCountStep))
        var decimalPlaces = 10

        if ticCountStepInt == 0 {   // Если значения шага меньше 0
            var decimalStep = 0
            while ticCountStepInt == 0 {
                decimalStep += 1
                ticCountStep *= 10
                ticCountStepInt = Double(Int(ticCountStep))
            }
            for _ in 1...decimalStep {
                ticCountStepInt /= 10
                decimalPlaces *= 10   // Определяем значения округления для избежания 0,2000000001
            }
            
        } else if ticCountStepInt > 10 { // Если значения шага больше 10
            var decimalStep = 0
            while ticCountStepInt > 10 {
                decimalStep += 1
                ticCountStep /= 10
                ticCountStepInt = Double(Int(ticCountStep))
            }
            for _ in 1...decimalStep {
                ticCountStepInt *= 10
            }
         }
        countRatePoints3 = []
        for gridNum in 0...15 {
            
            // Процесс округления Double до дейсевующик знаков decimalPlaces
            let curTicCount = Double(gridNum) * ticCountStepInt
            let curTicCountInt = round(curTicCount * Double(decimalPlaces)) / Double(decimalPlaces)
            
            countRatePoints3.append(GraphCountRateArray(id: gridNum, chnCountRate: curTicCountInt))
        }

// DEBUG:
//        print("maxVal = \(maxVal)")
//        print("ticCountStepInt = \(ticCountStepInt)")
//        for ticCount in countRatePoints3 {
//            print(ticCount.chnCountRate)
//        }

    }
    
    /// Более простая версия расчёта делений шкалы мощности дозы
    func defineDoseRateValAxis_Lite(maxVal: Double) {
        // TODO: далее объединить универсально с defineCountRateValAxis_Lite
        var ticCountStep = maxVal / 10 // Считаем, что нам нужно десять делений по оси Y
        var ticCountStepInt = Double(Int(ticCountStep)) // FIXME: Fatal error: Double value cannot be converted to Int because the result would be greater than Int.max
        var decimalPlaces = 10

        if ticCountStepInt == 0 {   // Если значения шага меньше 0
            var decimalStep = 0
            while ticCountStepInt == 0 {
                decimalStep += 1
                ticCountStep *= 10
                ticCountStepInt = Double(Int(ticCountStep))
            }
            for _ in 1...decimalStep {
                ticCountStepInt /= 10
                decimalPlaces *= 10   // Определяем значения округления для избежания 0,2000000001
            }
            
        } else if ticCountStepInt > 10 { // Если значения шага больше 10
            var decimalStep = 0
            while ticCountStepInt > 10 {
                decimalStep += 1
                ticCountStep /= 10
                ticCountStepInt = Double(Int(ticCountStep))
            }
            for _ in 1...decimalStep {
                ticCountStepInt *= 10
            }
         }
        doseRatePoints3 = []
        for gridNum in 0...15 {
            
            // Процесс округления Double до дейсевующик знаков decimalPlaces
            let curTicCount = Double(gridNum) * ticCountStepInt
            let curTicCountInt = round(curTicCount * Double(decimalPlaces)) / Double(decimalPlaces)
            
            doseRatePoints3.append(GraphDoseRateArray(id: gridNum, chnDoseRate: curTicCountInt))
        }
        
//        print("maxVal = \(maxVal)")
//        print("ticCountStepInt = \(ticCountStepInt)")
//        for ticCount in doseRatePoints3 {
//            print(ticCount.chnDoseRate)
//        }

    }

    
    func defineTimeValAxis2() {
        
        let msMaxTime = grpCountRateArray.map { $0.msTime }.max()   // Максимальное время в милисекундах в массиве grpCountRateArray
        let msMinTime = grpCountRateArray.map { $0.msTime }.min()   // Минимальное время в милисекунда в массиве grpCountRateArray
        let secMaxTime = msMaxTime! / 1000 + 1                      // Максимальное время секундах плюс одна в массиве grpCountRateArray для того, чтобы быть внутри диапазона
        let secMinTime = msMinTime! / 1000 - 1                      // Минимальное время секундах минус одна в массиве grpCountRateArray для того, чтобы быть внутри диапазона
        
        timeCountPoints = []
        for time in secMinTime...secMaxTime {
            timeCountPoints.append(TimeTic(secTime: time, showText: false))
        }

        
//        var timeFirstCountPoints: [TimeTic] = [] // Начальная часть массива шкалы времени
//        var firstTime = secMinTime - 1 // Чтобы гарантировано firstTime было меньше secMinTime
//
//        if !timeCountPoints.isEmpty {              // Если массив шкалы времени не пустой
//            firstTime = timeCountPoints[0].secTime // запоминаем первое значение его времени
//        }
//
//        for time in secMinTime...secMaxTime { // Создание посекундной шкалы времени
//
//            if let _ = timeCountPoints.firstIndex(where: { $0.secTime == time }) {  // Поиск в шкале времени текущего значения времени
//            } else { // Если элемент не найден…
//                if firstTime > time { // Если минимальное значение шкалы времени больше нового минимального значения новых данных
//                    timeFirstCountPoints.append(TimeTic(secTime: time, showText: false)) // Добавляем начальную часть шкалы времени во вспомогательный массив
//                } else {
//                    timeCountPoints.append(TimeTic(secTime: time, showText: false)) // Создаём/добавляем часть шкалы времени в основной массив
//                }
//            }
//        }
//        timeCountPoints = timeFirstCountPoints + timeCountPoints       // Соединяем два массива в единое целое
//        for time in timeCountPoints { print(time.secTime, terminator: ",") }; print()
    }

    func compose_SET_EXCHANGE() {
        comp.compose_SET_EXCHANGE()
    }
    
    func compose_VS_CONFIGURATION() {
        comp.compose_VS_CONFIGURATION()
    }

    // MARK: - Из DeviceOptions
    
    
    // TODO: Убрать уже есть в Data
    func unitStr(unit: Unit) -> String {
        switch   unit {
            case .r : return String(localized: "Roentgen")
            case .sv: return String(localized: "Sievert")
        }
    }
    
    // TODO: Убрать уже есть в Data
    func alarmModeStr(alarmMode: AlarmMode) -> String {
        switch   alarmMode {
            case .many : return String(localized: "Many")
            case .one  : return String(localized: "One")
        }
    }
    
    func  displayModeStr(displayMode: DisplayMode) -> String {
        switch   displayMode {
            case .auto   : return("Авто")
            case .normal : return("Обычный")
            case .rotate : return("Перевёрнуто")
        }
    }
    
    func isCurLang(lang: Lang) -> Bool { return curLang == lang }
    func isCurUnit(unit: Unit) -> Bool { return curUnit == unit }
    func isCurAlarmMode(alarmMode: AlarmMode) -> Bool { return curAlarmMode == alarmMode }
    
    func setBooleanDevice(options: UInt32) {
        DEVICE_CTRL_PWR_ON       = (options & RC_DEVICE_CTRL_PWR_ON     ) > 0
        DEVICE_CTRL_DISPLAY_ON   = (options & RC_DEVICE_CTRL_DISPLAY_ON ) > 0
        DEVICE_CTRL_SOUND_ENA    = (options & RC_DEVICE_CTRL_SOUND_ENA  ) > 0
        DEVICE_CTRL_LEDS_ENA     = (options & RC_DEVICE_CTRL_LEDS_ENA   ) > 0
        DEVICE_CTRL_VIBRO_ENA    = (options & RC_DEVICE_CTRL_VIBRO_ENA  ) > 0
        DEVICE_CTRL_CHARGER_ON   = (options & RC_DEVICE_CTRL_CHARGER_ON ) > 0
        DEVICE_CTRL_USER_RUN     = (options & RC_DEVICE_CTRL_USER_RUN   ) > 0
    }
    
    func setBooleanSoundOptions() {
        SOUND_CTRL_POWER_ENA      = (SOUND_CTRL! & RC_SOUND_CTRL_POWER_ENA    ) > 0
        isRC_SOUND_CTRL_CONNECT_ENA    = (SOUND_CTRL! & RC_SOUND_CTRL_CONNECT_ENA  ) > 0
        isRC_SOUND_CTRL_BUTTON_ENA     = (SOUND_CTRL! & RC_SOUND_CTRL_BUTTON_ENA   ) > 0
        isRC_SOUND_CTRL_CLICK_ENA      = (SOUND_CTRL! & RC_SOUND_CTRL_CLICK_ENA    ) > 0
        isRC_SOUND_CTRL_DR_ALM_L1_ENA  = (SOUND_CTRL! & RC_SOUND_CTRL_DR_ALM_L1_ENA) > 0
        isRC_SOUND_CTRL_DR_ALM_L2_ENA  = (SOUND_CTRL! & RC_SOUND_CTRL_DR_ALM_L2_ENA) > 0
        isRC_SOUND_CTRL_DR_ALM_OS_ENA  = (SOUND_CTRL! & RC_SOUND_CTRL_DR_ALM_OS_ENA) > 0
        isRC_SOUND_CTRL_DS_ALM_L1_ENA  = (SOUND_CTRL! & RC_SOUND_CTRL_DS_ALM_L1_ENA) > 0
        isRC_SOUND_CTRL_DS_ALM_L2_ENA  = (SOUND_CTRL! & RC_SOUND_CTRL_DS_ALM_L2_ENA) > 0
        isRC_SOUND_CTRL_DS_ALM_OS_ENA  = (SOUND_CTRL! & RC_SOUND_CTRL_DS_ALM_OS_ENA) > 0
    }
    
    func setBooleanVibroOptions() {
        isRC_VIBRO_CTRL_BUTTON_ENA     = (VIBRO_CTRL! & RC_VIBRO_CTRL_BUTTON_ENA   ) > 0
        isRC_VIBRO_CTRL_CLICK_ENA      = (VIBRO_CTRL! & RC_VIBRO_CTRL_CLICK_ENA    ) > 0
        isRC_VIBRO_CTRL_DR_ALM_L1_ENA  = (VIBRO_CTRL! & RC_VIBRO_CTRL_DR_ALM_L1_ENA) > 0
        isRC_VIBRO_CTRL_DR_ALM_L2_ENA  = (VIBRO_CTRL! & RC_VIBRO_CTRL_DR_ALM_L2_ENA) > 0
        isRC_VIBRO_CTRL_DR_ALM_OS_ENA  = (VIBRO_CTRL! & RC_VIBRO_CTRL_DR_ALM_OS_ENA) > 0
        isRC_VIBRO_CTRL_DS_ALM_L1_ENA  = (VIBRO_CTRL! & RC_VIBRO_CTRL_DS_ALM_L1_ENA) > 0
        isRC_VIBRO_CTRL_DS_ALM_L2_ENA  = (VIBRO_CTRL! & RC_VIBRO_CTRL_DS_ALM_L2_ENA) > 0
        isRC_VIBRO_CTRL_DS_ALM_OS_ENA  = (VIBRO_CTRL! & RC_VIBRO_CTRL_DS_ALM_OS_ENA) > 0
    }
    
    func setEnumDisplayOptions() {
        curDispMode = .auto
        if (self.DISP_CTRL! & RC_DISP_CTRL_DIR_NORMAL ) > 0 { curDispMode = .normal }
        if (self.DISP_CTRL! & RC_DISP_CTRL_DIR_ROTATED) > 0 { curDispMode = .rotate }
        curLightMode = .off
        if (self.DISP_CTRL! & RC_DISP_CTRL_LED_ENA    ) > 0 { curLightMode = .ena }
        if (self.DISP_CTRL! & RC_DISP_CTRL_LED_AUTO   ) > 0 { curLightMode = .auto }
        printBin(UInt32(self.DISP_CTRL!))
    }
    
    func setOffTime() {
        curOffTime = OffTime(rawValue: DISP_OFF_TIME!)!
    }
    
    func appendArrayWithDelay(ar: [RC_GRP_DOSE_RATE]) {
        for dose in ar {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.grpDoseRateArray.append(dose)
            }
        }
    }
    
    func getTimeRangeForMagnificationGesture(magnifyBy: Double) {
        
        isConnectDiagrammToCurDate = false

        let rangeDate = getMagnificationDateRange2(from: curFromDiagramDate!, to: curTo__DiagramDate!, on: (1 / magnifyBy) )

        if  rangeDate.toDate > Date() {  // чтобы не забегать вперёд по времени
            isConnectDiagrammToCurDate = true
            let diffs = Calendar.current.dateComponents([.second], from: rangeDate.fromDate, to: rangeDate.toDate)
            curTimeScaleRange  = Double(diffs.second!)
            curTo__DiagramDate = Date()
        } else {
            let diffs = Calendar.current.dateComponents([.second], from: rangeDate.fromDate, to: rangeDate.toDate)
            if  diffs.second! >= Units.minute.rawValue * 10 {
                curTimeScaleRange  = Double(diffs.second!)
                curTo__DiagramDate = rangeDate  .toDate
            }
        }
        
        defineOutOffBordersForTimeRange()
    }
    
    
    func defineOutOffBordersForTimeRange() {
        
        // Определения временного диапазона для захвата доп. точек
        let curDateRange            = Calendar.current.dateComponents([.second], from: curFromDiagramDate!, to: curTo__DiagramDate!) // Вычисление диапазона
        let curSecDataRange         = Double(curDateRange.second!)                                                                   // Получаем число секунд диапазоне
        let wideDataDeltaSecRange   = curSecDataRange * 0.50                                                                         // Будем раздвигать запрос к БД на 50% в каждую сторону…
        let wideNewDateFrom         = curFromDiagramDate!.addingTimeInterval(-wideDataDeltaSecRange)
        let wideNewDate__To         = curTo__DiagramDate!.addingTimeInterval( wideDataDeltaSecRange)
        
        
        let curDose_Array           = Log.getDoseRateDB (in: managedObjectContext, from: wideNewDateFrom, to: wideNewDate__To) ?? []
        let curCountArray           = Log.getCountRateDB(in: managedObjectContext, from: wideNewDateFrom, to: wideNewDate__To) ?? []
        if  curDose_Array.count >= 3 && curCountArray.count >= 3 {
            // TODO: Проверить не будет ли в результате этого огранчения сбита текущая шкала увеличения?
            grpDose_RateTimerArray = deleteOutBordersData(timerArray: curDose_Array)
            grpCountRateTimerArray = deleteOutBordersData(timerArray: curCountArray)
        }
    }
    
    func deleteOutBordersData(timerArray: [ValRateDB]) -> [ValRateDB] {
        var newArray: [ValRateDB] = []
        var predItem:  ValRateDB?
        for cur_Item in timerArray {
            if predItem != nil {
               // Если предыдущее значение меньше, а новое больше левой границы временного диапазона, то уже оставляем и предыдущее
               if predItem!.timestamp <  curFromDiagramDate! &&
                  cur_Item .timestamp >= curFromDiagramDate! {
                       newArray.append(predItem!)
               // Если новое попадает во временной диапазон, то оставляем его
               } else if cur_Item .timestamp >= curFromDiagramDate! &&
                         cur_Item .timestamp <= curTo__DiagramDate! {
                       newArray.append(cur_Item)
               // Если предыдущее значение меньше, а новое больше правой границы временного диапазона, то оставляем и новое
               } else if predItem!.timestamp <  curTo__DiagramDate! &&
                         cur_Item .timestamp >= curTo__DiagramDate! {
                       newArray.append(cur_Item)
               }
            }
            predItem = cur_Item
        }
        return         newArray
    }
    
    /// Определение оптимальной размерности для отображения значений Рентгенов и Зивертов
    func getOptimalVarUnitStr(roentgenVal: Double,
                                    isVal: Bool = true,
                                   isUnit: Bool = true,
                                  isSpace: Bool = false,
                                   isHour: Bool = false) -> String {
        
        var r   = ""
        var sv  = ""
        var mR  = ""
        var mSv = ""
        var µR  = ""
        var µSv = ""
        let sp = isSpace ? " " : ""
        var h  =  ""
         
//        if isUnit { // Нужно ли нам указывать единицы измерения
//            r   = curLangVal == 0 ?   "Р" :  "R" String(localized: "Sievert")
//            sv  = curLangVal == 0 ?  "Зв" : "Sv"
//            mR  = curLangVal == 0 ?  "мР" : "mR" //
//            mSv = curLangVal == 0 ? "мЗв" : "mSv" // миллизиверт
//            µR  = curLangVal == 0 ? "мкР" : "µR" //
//            µSv = curLangVal == 0 ? "мкЗв": "µSv" // микрозиверт
//            h   = curLangVal == 0 ?  "/ч" : "/h"
//        }
        
        if isUnit { // Нужно ли нам указывать единицы измерения
            r   = String(localized: "R")
            sv  = String(localized: "Sv")
            mR  = String(localized: "mR")  // миллирентген
            mSv = String(localized: "mSv") // миллизиверт
            µR  = String(localized: "µR")  // микрорентген
            µSv = String(localized: "µSv") // микрозиверт
            h   = String(localized: "/h")  // в час
        }
            h = isHour ? h : ""
        
        let approximateCount = curUnitVal == 0 ? roentgenVal : roentgenVal / 100.0
        var naturalCount = ""
        
        if isVal {
            switch approximateCount {
            case        0..<0.00001 :   naturalCount = String(format:"%1.2F", approximateCount * 1000000)
            case  0.00001..<0.0001  :   naturalCount = String(format:"%1.2F", approximateCount * 1000000)
            case   0.0001..<0.001   :   naturalCount = String(format:"%1.2F", approximateCount * 1000)
            case    0.001..<0.01    :   naturalCount = String(format:"%1.2F", approximateCount * 1000)
            case     0.01..<0.1     :   naturalCount = String(format:"%1.3F", approximateCount)
            case      0.1..<1       :   naturalCount = String(format:"%1.2F", approximateCount)
            case        1..<10      :   naturalCount = String(format:"%1.1F", approximateCount)
            case       10..<100     :   naturalCount = String(format:"%1.1F", approximateCount)
            case      100..<10000   :   naturalCount = String(format:"%1.0F", approximateCount)

            default:                    naturalCount = String(approximateCount); print("1..<10")
            }
        }

        switch approximateCount {
        case        0..<0.00001 :   naturalCount += sp + (curUnitVal == 0 ? µR : µSv) + h
        case  0.00001..<0.0001  :   naturalCount += sp + (curUnitVal == 0 ? µR : µSv) + h
        case   0.0001..<0.001   :   naturalCount += sp + (curUnitVal == 0 ? mR : mSv) + h
        case    0.001..<0.01    :   naturalCount += sp + (curUnitVal == 0 ? mR : mSv) + h
        case     0.01..<0.1     :   naturalCount += sp + (curUnitVal == 0 ? r : sv)   + h
        case      0.1..<1       :   naturalCount += sp + (curUnitVal == 0 ? r : sv)   + h
        case        1..<10      :   naturalCount += sp + (curUnitVal == 0 ? r : sv)   + h
        case       10..<100     :   naturalCount += sp + (curUnitVal == 0 ? r : sv)   + h
        case      100..<10000   :   naturalCount += sp + (curUnitVal == 0 ? r : sv)   + h

        default:                    naturalCount += sp + (curUnitVal == 0 ? r : sv)   + h  ; print("1..<10")
        }
        return naturalCount
    }

}
