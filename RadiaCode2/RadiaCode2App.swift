//
//  RadiaCode2App.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import SwiftUI

@main
struct RadiaCode2App: App {
    
    @StateObject private var options = HardOptions()
//    @StateObject private var radiaCodeOptions = RadiaCodeOptions(options: nil)
    
    let persistenceController = PersistenceController.shared
    

    var body: some Scene {
        WindowGroup {
            ContentView2()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
                .environmentObject(options)
        }
    }
}
