//
//  Device+CoreDataProperties.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//
//

import Foundation
import CoreData


extension Device {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Device> {
        return NSFetchRequest<Device>(entityName: "Device")
    }

    @NSManaged public var chip: String?
    @NSManaged public var is_connected: Bool
    @NSManaged public var is_default: Bool
    @NSManaged public var name: String?
    @NSManaged public var sn: Int32
    @NSManaged public var sn_str: String?
    @NSManaged public var timestamp: Date?
    @NSManaged public var user_name: String?
    @NSManaged public var uuid: UUID?
    @NSManaged public var log: NSSet?

}

// MARK: Generated accessors for log
extension Device {

    @objc(addLogObject:)
    @NSManaged public func addToLog(_ value: Log)

    @objc(removeLogObject:)
    @NSManaged public func removeFromLog(_ value: Log)

    @objc(addLog:)
    @NSManaged public func addToLog(_ values: NSSet)

    @objc(removeLog:)
    @NSManaged public func removeFromLog(_ values: NSSet)

}

extension Device : Identifiable {
    
    static func isUniqueDevice(uuid: UUID, in managedObjectContext: NSManagedObjectContext) -> Bool {
        let fetchRequest = fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "%K == %@", #keyPath(Device.uuid), uuid  as CVarArg)
        do {
                let        results = try managedObjectContext.fetch(fetchRequest)
                if         results.count > 0 {
                    return false
                } else {
                    return true
                }
        } catch let error { print("Find in Name error :", error) }
        return true
    }
    
    static func findDefaultDevice(in managedObjectContext: NSManagedObjectContext) -> Device? {
        let fetchRequest = fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "%K == %@", #keyPath(Device.is_default), NSNumber(value: true))
        do {
                let        results = try managedObjectContext.fetch(fetchRequest)
                if         results.count > 0 {
                    return results[0]
                } else {
                    return nil
                }
        } catch let error { print("Find in Name error :", error) }
        return nil
    }
    
    static func findConnectedDevice(in managedObjectContext: NSManagedObjectContext) -> Device? {
        let fetchRequest = fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "%K == %@", #keyPath(Device.is_connected), NSNumber(value: true))
        do {
                let        results = try managedObjectContext.fetch(fetchRequest)
                if         results.count > 0 {
                    return results[0]
                } else {
                    return nil
                }
        } catch let error { print("Find in Name error :", error) }
        return nil
    }

    
    static func isConnectedDevice(in managedObjectContext: NSManagedObjectContext) -> Bool {
        let fetchRequest = fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "%K == %@", #keyPath(Device.is_connected), NSNumber(value: true))
        do {
                let        results = try managedObjectContext.fetch(fetchRequest)
                if         results.count > 0 {
                    return true
                } else {
                    return false
                }
        } catch let error { print("Find in Name error :", error) }
        return false
    }
    
    static func clearAllDefaultTo(_ on: Bool, in managedObjectContext: NSManagedObjectContext) {
        
        let fetchRequest = fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "%K == %@", #keyPath(Device.is_default), NSNumber(value: !on))
        do {
                let             results = try managedObjectContext.fetch(fetchRequest)
                if              results.count > 0 {
                    for item in results {
                        item.is_default = on
                    }
                }
        } catch let error { print("Find in Name error :", error) }
        
        do {
            try managedObjectContext.save()
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
    
    static func clearAllConnection(in managedObjectContext: NSManagedObjectContext) {
        
        let fetchRequest = fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "%K == %@", #keyPath(Device.is_connected), NSNumber(value: true))
        do {
                let             results = try managedObjectContext.fetch(fetchRequest)
                if              results.count > 0 {
                    for item in results {
                        item.is_connected = false
                    }
                }
        } catch let error { print("Find in Name error :", error) }
        
        do {
            try managedObjectContext.save()
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }

    
    static func setConnect(uuid: UUID, in managedObjectContext: NSManagedObjectContext) {
        
        let fetchRequest = fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "%K == %@", #keyPath(Device.uuid), uuid  as CVarArg)
        do {
                let       results = try managedObjectContext.fetch(fetchRequest)
                if        results.count > 0 {
                   let device = results.first
                       device?.is_connected = true
                }
        } catch let error { print("Find in Name error :", error) }
        
        do {
            try managedObjectContext.save()
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }

    }


}
