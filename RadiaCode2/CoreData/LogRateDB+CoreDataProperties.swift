//
//  LogRateDB+CoreDataProperties.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//
//

import Foundation
import CoreData


extension LogRateDB {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LogRateDB> {
        return NSFetchRequest<LogRateDB>(entityName: "LogRateDB")
    }

    @NSManaged public var count: Int32
    @NSManaged public var count_rate: Float
    @NSManaged public var dose_rate: Float
    @NSManaged public var dose_rate_err: Float
    @NSManaged public var log: Log?

}

extension LogRateDB : Identifiable {

    static func deleteAllData(in managedObjectContext: NSManagedObjectContext) {
        let fetchRequest = fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as NSManagedObject? else { continue }
                managedObjectContext.delete(objectData)
            }
        } catch let error { print("Delete all data in Name error :", error) }
    }
    

}
