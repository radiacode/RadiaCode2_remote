//
//  Log+CoreDataProperties.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//
//

import Foundation
import CoreData


extension Log {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Log> {
        return NSFetchRequest<Log>(entityName: "Log")
    }

    @NSManaged public var flags: Int32
    @NSManaged public var timestamp: Date?
    @NSManaged public var event: Event?
    @NSManaged public var rare_data: LogRareData?
    @NSManaged public var rate_db: LogRateDB?
    @NSManaged public var device: Device?

}

extension Log : Identifiable {
    
    static func deleteAllData(in managedObjectContext: NSManagedObjectContext) {
        let fetchRequest = fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as NSManagedObject? else { continue }
                managedObjectContext.delete(objectData)
            }
        } catch let error { print("Delete all data in Name error :", error) }
    }


}
