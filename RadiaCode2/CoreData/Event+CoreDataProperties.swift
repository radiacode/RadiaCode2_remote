//
//  Event+CoreDataProperties.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 15.05.2022.
//
//

import Foundation
import CoreData


extension Event {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Event> {
        return NSFetchRequest<Event>(entityName: "Event")
    }

    @NSManaged public var event: Int32
    @NSManaged public var flags: Int32
    @NSManaged public var mask: Int32
    @NSManaged public var count_rate: Float
    @NSManaged public var count_rate_err: Float
    @NSManaged public var dose_rate: Float
    @NSManaged public var dose_rate_err: Float
    @NSManaged public var duration: Int32
    @NSManaged public var dose: Float
    @NSManaged public var temperature: Float
    @NSManaged public var charge: Float
    @NSManaged public var log: Log?

}

extension Event : Identifiable {

    static func deleteAllData(in managedObjectContext: NSManagedObjectContext) {
        let fetchRequest = fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as NSManagedObject? else { continue }
                managedObjectContext.delete(objectData)
            }
        } catch let error { print("Delete all data in Name error :", error) }
    }

}
