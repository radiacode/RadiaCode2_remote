//
//  Persistence.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import CoreData

struct PersistenceController {
    static let shared = PersistenceController()

    static var preview: PersistenceController = {
        let result = PersistenceController(inMemory: true)
        let viewContext = result.container.viewContext
        for _ in 0..<10 {
            let newItem = Device(context: viewContext)
                newItem.timestamp = Date()
                newItem.name = "RadiaCode-101"
                newItem.sn = 1234
                newItem.uuid = UUID(uuidString: "00000000-0000-0000-0000-000000000000")!
                newItem.chip = "blunrg"
        }
        let newDevice = Device(context: viewContext)
            newDevice.timestamp = Date()
            newDevice.name = "RadiaCode-101"
            newDevice.sn = 1234
            newDevice.uuid = UUID(uuidString: "00000000-0000-0000-0000-000000000000")!
            newDevice.chip = "blunrg"

        var newLogItem              = Log      (context: viewContext)
            newLogItem.timestamp    = Date()
            newLogItem.device       = newDevice
        let newDBItem               = LogRateDB(context: viewContext)
            newDBItem.log           = newLogItem
            newDBItem.count         = 379
            newDBItem.count_rate    = 5.7
            newDBItem.dose_rate     = 0.000122
            newDBItem.dose_rate_err = 10.3
        
            newLogItem              = Log      (context: viewContext)
            newLogItem.timestamp    = Date()
            newLogItem.device       = newDevice

        let newRareItem             = LogRareData(context: viewContext)
            newRareItem.log         = newLogItem
            newRareItem.temperature = 36.6
            newRareItem.duration    = 27
            newRareItem.charge      = 0.8
            newRareItem.dose        = 13.4
        
        for i: Int32 in 0..<17 {
                newLogItem              = Log      (context: viewContext)
                newLogItem.timestamp    = Date()
                newLogItem.device       = newDevice
            var newEvent                = Event(context: viewContext)
                newEvent.log            = newLogItem
                newEvent.event          = i

        }
        
        do {
            try viewContext.save()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return result
    }()

    let container: NSPersistentContainer

    init(inMemory: Bool = false) {
        container = NSPersistentContainer(name: "RadiaCode2")
        if inMemory {
            container.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
        }
        container.viewContext.automaticallyMergesChangesFromParent = true
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

                /*
                Typical reasons for an error here include:
                * The parent directory does not exist, cannot be created, or disallows writing.
                * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                * The device is out of space.
                * The store could not be migrated to the current model version.
                Check the error message to determine what the actual problem was.
                */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
    }
    
    func saveContext () {
        let context = container.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}
