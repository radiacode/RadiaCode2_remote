//
//  DataStrucktureGRP+CoreDataProperties.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//
//

import Foundation
import CoreData


extension DataStrucktureGRP {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DataStrucktureGRP> {
        return NSFetchRequest<DataStrucktureGRP>(entityName: "DataStrucktureGRP")
    }

    @NSManaged public var flg_msk: Int32
    @NSManaged public var id: Int16
    @NSManaged public var name: String?
    @NSManaged public var chn: NSSet?

}

// MARK: Generated accessors for chn
extension DataStrucktureGRP {

    @objc(addChnObject:)
    @NSManaged public func addToChn(_ value: DataStrucktureCHN)

    @objc(removeChnObject:)
    @NSManaged public func removeFromChn(_ value: DataStrucktureCHN)

    @objc(addChn:)
    @NSManaged public func addToChn(_ values: NSSet)

    @objc(removeChn:)
    @NSManaged public func removeFromChn(_ values: NSSet)

}

extension DataStrucktureGRP : Identifiable {

    static func deleteAllData(in managedObjectContext: NSManagedObjectContext) {
        let fetchRequest = fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as NSManagedObject? else { continue }
                managedObjectContext.delete(objectData)
            }
        } catch let error { print("Delete all data in Name error :", error) }
    }

}
