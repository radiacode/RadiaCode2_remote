//
//  DataStrucktureCHN+CoreDataProperties.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//
//

import Foundation
import CoreData


extension DataStrucktureCHN {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DataStrucktureCHN> {
        return NSFetchRequest<DataStrucktureCHN>(entityName: "DataStrucktureCHN")
    }

    @NSManaged public var d_type: Int16
    @NSManaged public var expr: Int16
    @NSManaged public var id: Int16
    @NSManaged public var max_val: Double
    @NSManaged public var min_val: Double
    @NSManaged public var name: String?
    @NSManaged public var p1: Double
    @NSManaged public var p2: Double
    @NSManaged public var position: Int16
    @NSManaged public var r_type: Int16
    @NSManaged public var scaled_unit: Int16
    @NSManaged public var sensor: String?
    @NSManaged public var unit: String?
    @NSManaged public var grp: DataStrucktureGRP?

}

extension DataStrucktureCHN : Identifiable {

    static func deleteAllData(in managedObjectContext: NSManagedObjectContext) {
        let fetchRequest = fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as NSManagedObject? else { continue }
                managedObjectContext.delete(objectData)
            }
        } catch let error { print("Delete all data in Name error :", error) }
    }
    
    static func channelSequence(for grpID: UInt8, in managedObjectContext: NSManagedObjectContext) -> [DataStrucktureCHN]? {
//        let predicate = NSPredicate(format: "grp.id == %@", String(grpID))
        let fetchRequest = fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
            fetchRequest.sortDescriptors = [NSSortDescriptor(keyPath:              \DataStrucktureCHN.position, ascending: true)]
            fetchRequest.predicate       =  NSPredicate(format: "grp.id == %@", String(grpID))
        do {
            return try managedObjectContext.fetch(fetchRequest)
        }   catch let error { print("Find in Name error :", error) }
            return nil
    }

}
