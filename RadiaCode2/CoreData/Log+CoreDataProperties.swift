//
//  Log+CoreDataProperties.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//
//

import Foundation
import CoreData


extension Log {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Log> {
        return NSFetchRequest<Log>(entityName: "Log")
    }

    @NSManaged public var flags: Int32
    @NSManaged public var timestamp: Date?
    @NSManaged public var event: Event?
    @NSManaged public var rare_data: LogRareData?
    @NSManaged public var rate_db: LogRateDB?
    @NSManaged public var device: Device?

}

extension Log : Identifiable {
    
    static func deleteAllData(in managedObjectContext: NSManagedObjectContext) {
        let fetchRequest = fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as NSManagedObject? else { continue }
                managedObjectContext.delete(objectData)
            }
        } catch let error { print("Delete all data in Name error :", error) }
    }

    /// Отбор из БД всего по скорости счёта в обыкновенный массив (для графиков)
    static func getCountRateDB(in managedObjectContext: NSManagedObjectContext) -> [ValRateDB]? {
        // TODO: Возможно, создать единый тип из CountRateDB с массивом текущих данных
        var logs: [Log]
        var counts: [ValRateDB] = []
        let fetchRequest = fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
            fetchRequest.sortDescriptors = [NSSortDescriptor(keyPath: \Log.timestamp, ascending: true)]
            fetchRequest.predicate       =  NSPredicate(format: "rate_db.count_rate >= %@", "0")
        do {
            try logs = managedObjectContext.fetch(fetchRequest)
            for log in logs {
                if log.rate_db?.count_rate != nil {
                    if log.rate_db?.count_rate ?? 0 < 10000000 { // FIXME: Временная защита от непонятных зашкалов (2022-02-22 17:41:48 +0000 3.8369223e+22)
                        counts.append(ValRateDB(timestamp: log.timestamp!, val_rate: log.rate_db?.count_rate ?? 0))
//                        print(log.timestamp!, log.rate_db?.dose_rate ?? 0)
                    }
                }
            }
            print("counts.count = \(counts.count)")
            return counts
        }   catch let error { print("Find in Name error :", error) }
            return nil
    }
    
    /// Определение  из БД минимального и максимального значения скорости счёта  (для графиков)
    static func getCountRateDB_range(in managedObjectContext: NSManagedObjectContext) -> (min: Float, max: Float)? {
        // TODO: Возможно, создать единый тип из CountRateDB с массивом текущих данных
        var logs: [Log]
        var logCount: (min: Float, max: Float)
        let fetchRequest = fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
            fetchRequest.sortDescriptors = [NSSortDescriptor(keyPath: \Log.rate_db?.count_rate, ascending: true)]
            fetchRequest.predicate       =  NSPredicate(format: "rate_db.count_rate < 10000000 AND rate_db.count_rate >= %@", "0")
        do {
            try logs = managedObjectContext.fetch(fetchRequest)
            if logs.count > 0 {
                logCount.min = logs.first!.rate_db!.count_rate
                logCount.max = logs.last!.rate_db!.count_rate
                return logCount
            } else {
                return nil
            }
        }   catch let error { print("Find in Name error :", error) }
            return nil
    }

    static func getCountRateDB(in managedObjectContext: NSManagedObjectContext, from fromdate: String, to todate: String ) -> [ValRateDB]? {
        let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let startDate:NSDate = dateFormatter.date(from: fromdate)! as NSDate
        let   endDate:NSDate = dateFormatter.date(from: todate)!   as NSDate
        let predicateStr = "rate_db.count_rate >= %@ AND %@ <= timestamp AND %@ >= timestamp"
        let fetchRequest = fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
            fetchRequest.sortDescriptors = [NSSortDescriptor(keyPath: \Log.timestamp, ascending: true)]
            fetchRequest.predicate       =  NSPredicate(format: predicateStr, argumentArray: ["0", startDate, endDate])

        var logs: [Log]
        var counts: [ValRateDB] = []

        do {
            try    logs = managedObjectContext.fetch(fetchRequest)
            for log in logs {
                if log.rate_db?.count_rate != nil {
                    if log.rate_db?.count_rate ?? 0 < 10000000 { // FIXME: Временная защита от непонятных зашкалов (2022-02-22 17:41:48 +0000 3.8369223e+22)
                        counts.append(ValRateDB(timestamp: log.timestamp!, val_rate: log.rate_db?.count_rate ?? 0))
//                        print(log.timestamp!, log.rate_db?.dose_rate ?? 0)
                    }

                }
            }
            return counts
        }   catch let error { print("Find in Name error :", error) }
            return nil
    }

    static func getCountRateDB(in managedObjectContext: NSManagedObjectContext, from fromdate: Date, to todate: Date ) -> [ValRateDB]? {
        
        let startDate:NSDate = fromdate as NSDate
        let   endDate:NSDate = todate   as NSDate
        let predicateStr = "rate_db.count_rate >= %@ AND %@ <= timestamp AND %@ >= timestamp"
        let fetchRequest = fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
            fetchRequest.sortDescriptors = [NSSortDescriptor(keyPath: \Log.timestamp, ascending: true)]
            fetchRequest.predicate       =  NSPredicate(format: predicateStr, argumentArray: ["0", startDate, endDate])

        var logs: [Log]
        var counts: [ValRateDB] = []

        do {
            try    logs = managedObjectContext.fetch(fetchRequest)
            for log in logs {
                if log.rate_db?.count_rate != nil {
                    if log.rate_db?.count_rate ?? 0 < 10000000 { // FIXME: Временная защита от непонятных зашкалов (2022-02-22 17:41:48 +0000 3.8369223e+22)
                        counts.append(ValRateDB(timestamp: log.timestamp!, val_rate: log.rate_db?.count_rate ?? 0))
//                        print(log.timestamp!, log.rate_db?.dose_rate ?? 0)
                    }
                }
            }
            return counts
        }   catch let error { print("Find in Name error :", error) }
            return nil
    }
    
    

    /// Отбор из БД всего по мощности дозы в обыкновенный массив (для графиков)
    static func getDoseRateDB(in managedObjectContext: NSManagedObjectContext) -> [ValRateDB]? {
        // TODO: Возможно, создать единый тип из CountRateDB с массивом текущих данных
        var logs: [Log]
        var counts: [ValRateDB] = []
        let fetchRequest = fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
            fetchRequest.sortDescriptors = [NSSortDescriptor(keyPath: \Log.timestamp, ascending: true)]
            fetchRequest.predicate       =  NSPredicate(format: "rate_db.dose_rate >= %@", "0")
        do {
            try logs = managedObjectContext.fetch(fetchRequest)
            for log in logs {
                if log.rate_db?.count_rate != nil {  
                    if log.rate_db?.dose_rate ?? 0 < 20 { // FIXME: Временная защита от непонятных зашкалов (2022-02-22 17:41:48 +0000 3.8369223e+22)
                        let curDoseRate = (log.rate_db?.dose_rate ?? 0) * 1000
                        counts.append(ValRateDB(timestamp: log.timestamp!, val_rate: curDoseRate ))
//                        print(log.timestamp!, log.rate_db?.dose_rate ?? 0)
//                        print(curDoseRate)
                    }
                }
            }
            print("counts.count = \(counts.count)")
            return counts
        }   catch let error { print("Find in Name error :", error) }
            return nil
    }
    
    static func getDoseRateDB(in managedObjectContext: NSManagedObjectContext, from fromdate: Date, to todate: Date ) -> [ValRateDB]? {
        
        let startDate:NSDate = fromdate as NSDate
        let   endDate:NSDate = todate   as NSDate
        let predicateStr = "rate_db.dose_rate >= %@ AND %@ <= timestamp AND %@ >= timestamp"
        let fetchRequest = fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
            fetchRequest.sortDescriptors = [NSSortDescriptor(keyPath: \Log.timestamp, ascending: true)]
            fetchRequest.predicate       =  NSPredicate(format: predicateStr, argumentArray: ["0", startDate, endDate])

        var logs: [Log]
        var counts: [ValRateDB] = []

        do {
            try    logs = managedObjectContext.fetch(fetchRequest)
            for log in logs {
                if log.rate_db?.dose_rate != nil {
                    if log.rate_db?.dose_rate ?? 0 < 10000000 { // FIXME: Временная защита от непонятных зашкалов (2022-02-22 17:41:48 +0000 3.8369223e+22)
                        counts.append(ValRateDB(timestamp: log.timestamp!, val_rate: (log.rate_db?.dose_rate ?? 0) * 1000))
//                        print(log.timestamp!, log.rate_db?.dose_rate ?? 0)
                    }

                }
            }
            return counts
        }   catch let error { print("Find in Name error :", error) }
            return nil
    }

    /// Определение  из БД минимального и максимального значения дозы (для графиков)
    static func getDoseRateDB_range(in managedObjectContext: NSManagedObjectContext) -> (min: Float, max: Float)? {
        // TODO: Возможно, создать единый тип из CountRateDB с массивом текущих данных
        var logs: [Log]
        var logCount: (min: Float, max: Float)
        let fetchRequest = fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
            fetchRequest.sortDescriptors = [NSSortDescriptor(keyPath: \Log.rate_db?.dose_rate, ascending: true)]
            fetchRequest.predicate       =  NSPredicate(format: "rate_db.dose_rate < 10000000 AND rate_db.dose_rate >= %@", "0")
        do {
            try logs = managedObjectContext.fetch(fetchRequest)
            if logs.count > 0 {
                logCount.min = logs.first!.rate_db!.dose_rate * 1000
                logCount.max = logs .last!.rate_db!.dose_rate * 1000
                return logCount
            } else {
                return nil
            }
        }   catch let error { print("Find in Name error :", error) }
            return nil
    }

}
 
