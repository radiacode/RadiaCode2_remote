//
//  LogRareData+CoreDataProperties.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//
//

import Foundation
import CoreData


extension LogRareData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LogRareData> {
        return NSFetchRequest<LogRareData>(entityName: "LogRareData")
    }

    @NSManaged public var charge: Float
    @NSManaged public var dose: Float
    @NSManaged public var duration: Int32
    @NSManaged public var temperature: Float
    @NSManaged public var log: Log?

}

extension LogRareData : Identifiable {

    static func deleteAllData(in managedObjectContext: NSManagedObjectContext) {
        let fetchRequest = fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as NSManagedObject? else { continue }
                managedObjectContext.delete(objectData)
            }
        } catch let error { print("Delete all data in Name error :", error) }
    }

}
