//
//  Utilites.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import Foundation
import UIKit

// Преобразование данных в массив байтов
func toByteArray(data: Data) -> [UInt8] {
    var bytesArray = [UInt8](repeating: 0, count: (data.count)) // Создание пустого массива байт размером под полученные данные
    (data as NSData).getBytes(&bytesArray, length:(data.count) * MemoryLayout<UInt8>.size) // Заполнение массива байт данными
//    for element in bytesArray { print(String(format:"%02X ", element), terminator: "") }
    return bytesArray
}

// Преобразование целого числа в массив байтов
func toByteArray<T: BinaryInteger>(_ word: T) -> [UInt8] {
    var    value = word                                                     // Превращаем константу в переменную
    let    data  = NSData(bytes: &value, length: MemoryLayout<T>.size)      // Число в данные
    let    count = data.length                                              // Длина данных в байтах
    var    array = [UInt8](repeating: 0, count: count)                      // Пустой массив байтов по числу байтов в данных
           data.getBytes(        &array, length: MemoryLayout<T>.size)      // Заполняем массив данными
    return array
}

// Целое из массива байтов
func intFrom<T: BinaryInteger>(_ bytesArray:[UInt8], start: Int = 0, length: Int = 4) -> T {
    var lengthByteArray:[UInt8] = []                                        // Готовим пустой массив
    for i in start..<start+length { lengthByteArray.append(bytesArray[i]) } // Заполнение массива байтами длины пакета
    let value = lengthByteArray.withUnsafeBufferPointer(
        { UnsafeRawPointer($0.baseAddress!).load(as: T.self) }              // Забираем сплошное значение
    )
    return value                                                            // Возвращаем уже любого целого типа
}

// Должно быть UInt32 но для CoreData делаем Int32 чтобы потом вернуть в UInt32
func hexStringToInt32(_ hexStr: String) -> Int32? {
    let str = String(hexStr.dropFirst(2))
    return Int32(str, radix: 16)
}

func printBin<T: BinaryInteger>(_ value: T)  {
    let binStr = String(value, radix: 2)// Печатаем саму строку битов "10011"
    let lengthBinStr = binStr.count
    let size = MemoryLayout<T>.size * 8 // Умножаем число байт в целом на число бит в байте
    let deltaToFill = size - lengthBinStr
    for i in (0..<size).reversed() { let      j = i/10;
                                          print(j%10, terminator: "") }; print("")
    for i in (0..<size).reversed()      { print(i%10, terminator: "") }; print("")
    for _ in (0..<size).reversed()      { print("|",  terminator: "") }; print("")
    for _ in 0..<deltaToFill            { print("0",  terminator: "") }; print(binStr)
}

//func binStr<T: BinaryInteger>(_ value: T) -> String {
//    return ""
//}

func dateComp() -> DateComponents {
    let currentDateTime = Date()
    let userCalendar = Calendar.current
    let requestedComponents: Set<Calendar.Component> = [
        .year,
        .month,
        .day,
        .hour,
        .minute,
        .second
    ]
    let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: currentDateTime)
    return dateTimeComponents
}

func BCD_DATE() -> UInt32 {
    
    let   dayByteArray = toByteArray(dateComp().day!)
    let monthByteArray = toByteArray(dateComp().month!)
    let  yearByteArray = toByteArray(dateComp().year! - 2000) // ГОд без тысячелетия
    
// DEBUG:
//    print(dayByteArray)
//    print(monthByteArray)
//    print(yearByteArray)
    
    var arrayBCD_DATE : [UInt8] = []
        arrayBCD_DATE.append( yearByteArray[0])
        arrayBCD_DATE.append(monthByteArray[0])
        arrayBCD_DATE.append(  dayByteArray[0])
        arrayBCD_DATE.append(0) // 0-й байт будет пустым (BCD_DATE - дата в BCD-формате 0x00YYMMDD;)
    let      BCD_DATE:UInt32 = intFrom(arrayBCD_DATE)
    return   BCD_DATE
}

func BCD_TIME() -> UInt32 {
    
    let   hourByteArray = toByteArray(dateComp().hour!)
    let minuteByteArray = toByteArray(dateComp().minute!)
    let secondByteArray = toByteArray(dateComp().second!)
    
// DEBUG:
//    print(hourByteArray)
//    print(minuteByteArray)
//    print(secondByteArray)

    var arrayBCD_TIME : [UInt8] = []
        arrayBCD_TIME.append(secondByteArray[0] )
        arrayBCD_TIME.append(minuteByteArray[0] )
        arrayBCD_TIME.append(  hourByteArray[0] )
        arrayBCD_TIME.append(0) // 4-й байт будет пустым (BCD_DATE - дата в BCD-формате 0x00hhmmss;)
    let      BCD_TIME:UInt32 = intFrom(arrayBCD_TIME)
    print(String(format:"%02X", BCD_TIME))
    return   BCD_TIME
}

func measureDate(_ ms: Int, startDate: Date = Date()) -> Date {
    let calendar = Calendar.current
    let  second = Int(Float(ms) / 100) // TODO: Исправлено 23 мая 2022 с 1000 на 100 в связи изменением формата времени
    let    date = calendar.date(byAdding: .second, value: second, to: startDate)
    return date!
}

func pow(_ x: Int, _ y: Int) -> Int  { var result = 1;         for _ in 0..<y { result *= x };   return result }
func powTen(_ y: Int)        -> Int  { var result = 1;         for _ in 0..<y { result *= 10 };  return result }
func powPointOne(_ y: Int) -> Double { var result: Double = 1; for _ in 0..<y { result *= 0.1 }; return result }

func rearrange(_ array: [UInt8]) -> [UInt8] {
    var arr2: [UInt8] = []
    for element in array {
        arr2.insert(element, at: 0)
    }
    return arr2
}

let itemFormatter: DateFormatter = {
    let    formatter = DateFormatter()
           formatter.dateStyle = .short
           formatter.timeStyle = .medium
    return formatter
}()

let dateOnlyFormatter: DateFormatter = {
    let    formatter = DateFormatter()
           formatter.dateStyle = .short
           formatter.timeStyle = .none
    return formatter
}()

let timeOnlyFormatter: DateFormatter = {
    let    formatter = DateFormatter()
           formatter.dateStyle = .none
           formatter.timeStyle = .medium
    return formatter
}()


/*
/// Создание массива точек для построения графика с учётом числа необходимых точек `pixelRange`
/// Диапазон `from to`  разбивается на поддиапазоны, соответствующие одному пикселю для последующего отображения в виде вертикального отрезка
///  когда он попадает в диапазон с данными `counts`
func logDecimation (counts: [ValRateDB], from fromDate: Date, to toDate: Date, pixelRange: Double) -> [ValRangeDB] {
    
    if counts.count == 0 { return [] }          // Источник пустой -> значит пустой и приёмник
    
    // Обработка трёх диапазонов:
    // 1-го между fromDate и counts.first?.timestamp (может и не быть, если даты совпадают)
    // 2-го между counts.first?.timestamp и counts.last?.timestamp (среднего непосредственно с данными)
    // 3-го между counts.last?.timestamp и toDate (может и не быть, если даты совпадают)

    // Обработка среднего (второго) диапазона с данными counts
    let minDataDate  = counts.first?.timestamp      // Минимальная дата диапазона данных counts для отображения графика
    let maxDataDate  = counts.last?.timestamp       // Максимальная дата диапазона данных counts для отображения графика
    let deltaDataSec = Calendar.current.dateComponents([.second], from: minDataDate!, to: maxDataDate!) // Определение объекта диапазона данных counts с секундами
    let timeSecDataRange = Double(deltaDataSec.second!) // Получаем число секунд в диапазоне данных counts
    
    // Обработка 1-го диапазона
    let firstDateRange = Calendar.current.dateComponents([.second], from: fromDate, to: minDataDate!)
    let firstSecDataRange = Double(firstDateRange.second!) // Получаем число секунд в 1-м диапазоне
    
    // Обработка 3-го диапазона
    let thirdDateRange = Calendar.current.dateComponents([.second], from: maxDataDate!, to: toDate)
    let thirdSecDataRange = Double(thirdDateRange.second!) // Получаем число секунд в 3-м диапазоне

    let secPerPixel = (firstSecDataRange + timeSecDataRange + thirdSecDataRange) / pixelRange // Сколько секунд приходится на каждый пиксель
    
    var timeCountsRange   : [ValRangeDB] = []   // Будущий возвращаемый массив диапазона с минимальными/максимальными значениями
    var curTimeRangeCounts: [ ValRateDB] = []   // Временный массив для последующего определения поддиапазона с минимальными/максимальными значениями
    
    var lastTime = counts[0].timestamp          // Текущая точка отсчёта времени для определения нового поддиапазона
    
    
    // Заполняем 1-й диапазон нулевыми значениями
    let pixelsOnFirstSecDataRange = Int(firstSecDataRange / secPerPixel) // Число пикселей, приходящееся на 1-й диапазон
    if  pixelsOnFirstSecDataRange >= 1 {
        for pixel in 1...pixelsOnFirstSecDataRange {
            let curDate = fromDate.addingTimeInterval(Double(pixel) * secPerPixel)
            timeCountsRange.append(ValRangeDB(timestamp: curDate, val_min: 0, val_max: 0))
        }
    } // Иначе 1-го диапазона просто нет…

    // Обработка среднего (второго) диапазона с данными counts
    for countTime in counts {                   // Повторяем для всех значений диапазона
        
        let curSecDiffs = Calendar.current.dateComponents([.second], from: lastTime, to: countTime.timestamp)
        let timeSecRange = Double(curSecDiffs.second!) //  Расстояние между началом поддиапазона и текущим элементом в нём
        
// DEBUG:
//        print(secPerPixel, timeSecRange)
        
        if Double(counts.count) <= pixelRange {
            timeCountsRange.append(ValRangeDB(timestamp: countTime.timestamp, val_min: -1, val_max: countTime.val_rate))
        } else {
            if secPerPixel > timeSecRange {
                curTimeRangeCounts.append(countTime)
            } else {
    //            print(lastTime)
    //            print(curTimeRangeCounts.count)
                if secPerPixel == 0 {
    //                print(secPerPixel)
                    return []
                }
                let ticInTimeSecRange = Int(timeSecRange / secPerPixel)
                
    //            let curTimeRangeCountsMax_Val =  curTimeRangeCounts.max(by: { (a, b) -> Bool in return a.val_rate < b.val_rate })?.val_rate
    //            let curTimeRangeCountsMin_Val =  curTimeRangeCounts.min(by: { (a, b) -> Bool in return a.val_rate < b.val_rate })?.val_rate
    //            timeCountsRange.append(ValRangeDB(timestamp: countTime.timestamp, val_min: curTimeRangeCountsMin_Val ?? 0, val_max: curTimeRangeCountsMax_Val ?? 0))

                if ticInTimeSecRange > 1 {
                    for _ in 1...ticInTimeSecRange {
                        timeCountsRange.append(ValRangeDB(timestamp: countTime.timestamp, val_min: 0, val_max: 0))
                    }
                } else {
                    let curTimeRangeCountsMax_Val =  curTimeRangeCounts.max(by: { (a, b) -> Bool in return a.val_rate < b.val_rate })?.val_rate
                    let curTimeRangeCountsMin_Val =  curTimeRangeCounts.min(by: { (a, b) -> Bool in return a.val_rate < b.val_rate })?.val_rate
                    timeCountsRange.append(ValRangeDB(timestamp: countTime.timestamp, val_min: curTimeRangeCountsMin_Val ?? 0, val_max: curTimeRangeCountsMax_Val ?? 0))
                }
                lastTime = countTime.timestamp
                
// DEBUG:
//
//                print()
//                print ("curTimeRangeCounts.count: \(curTimeRangeCounts.count) pixelRange: \(pixelRange)")
//                for count in curTimeRangeCounts { print("\(count.timestamp),\(count.val_rate)") }

                curTimeRangeCounts = []
            }
        }
    }
    
    // Заполняем 3-й диапазон нулевыми значениями
    let pixelsOnThirdSecDataRange = Int(thirdSecDataRange / secPerPixel) // Число пикселей, приходящееся на 1-й диапазон
    if  pixelsOnThirdSecDataRange >= 1 {
        for pixel in 1...pixelsOnThirdSecDataRange {
            let curDate = maxDataDate!.addingTimeInterval(Double(pixel) * secPerPixel)
            timeCountsRange.append(ValRangeDB(timestamp: curDate, val_min: 0, val_max: 0))
        }
    } // Иначе 3-го диапазона просто нет…

    
// DEBUG:
    print ("timeCountsRange.count: \(timeCountsRange.count)  pixelRange: \(pixelRange) fromDate: \(fromDate)  toDate: \(toDate)")
    for count in timeCountsRange { print("\(count.timestamp),\(count.val_min),\(count.val_max)") }
    
    return timeCountsRange
}



/// Механизм создания поддиапазонов данных для отображения графиков minVal-maxVal,  где каждая пара (отрезок) соответствует одному пикселю оси X
/// При наличии пробелов в данных отрезки заполняются нулями
func dataDecimation (dataArray: [ValRateDB], pixelRange: Double) -> [ValRangeDB] {
    
    // Сколько секунд приходится на каждый поддиапазон
    let minDataDate  = dataArray.first?.timestamp                                                       // Минимальная дата диапазона данных counts для отображения графика
    let maxDataDate  = dataArray.last?.timestamp                                                        // Максимальная дата диапазона данных counts для отображения графика
    let deltaDataSec = Calendar.current.dateComponents([.second], from: minDataDate!, to: maxDataDate!) // Определение объекта диапазона данных counts с секундами
    let timeSecDataRange = Double(deltaDataSec.second!)                                                 // Получаем число секунд в диапазоне данных counts
    let secPerPixel = timeSecDataRange / pixelRange                                                     // Сколько секунд приходится на каждый пиксель
    if  secPerPixel == 0 { return [] }                                                                  // Делать тут нечего!

    var timeValRanges  : [ValRangeDB] = []                                                              // Будущий возвращаемый массив поддиапазонов с минимальными/максимальными значениями
    var tempRange      : [ ValRateDB] = []                                                              // Временный массив для откладывания значений одного поддиапазона

    var lastTime = dataArray[0].timestamp                                                               // Текущая точка отсчёта времени для определения начала нового поддиапазона
    for data in dataArray {
        let curSecDiffs = Calendar.current.dateComponents([.second], from: lastTime, to: data.timestamp)// Расстояние между началом поддиапазона и текущим элементом в DateComponents
        let timeSecRange = Double(curSecDiffs.second!)                                                  // Расстояние между началом поддиапазона и текущим элементом в секундах
        if Double(dataArray.count) <= pixelRange { // Случай, когда данных меньше, чем точек
            // Всё равно пройдём весь диапазон (но минимальное значение будет -1, чтобы соединять в графике точки, а не рисовать отрезки, а справа просто ничего не будет рисоваться
            timeValRanges.append(ValRangeDB(timestamp: data.timestamp, val_min: -1, val_max: data.val_rate))
        } else {                            // Стандартный вариант
            if secPerPixel > timeSecRange { // Мы ещё не накопили в диапазоне секунд timeSecRange больше приходящегося на один пиксель secPerPixel
                tempRange.append(data)      // Мы копим в tempRange значения диапазона
            } else {                        // Мы перешагнули диапазон
                
                let ticInTimeSecRange = Int(timeSecRange / secPerPixel)
                if ticInTimeSecRange > 1 {
                    for _ in 1...ticInTimeSecRange {
                        timeValRanges.append(ValRangeDB(timestamp: data.timestamp, val_min: 0, val_max: 0))
                    }
                } else {
                    let rangeMaxVal =  tempRange.max(by: { (a, b) -> Bool in return a.val_rate < b.val_rate })?.val_rate
                    let rangeMinVal =  tempRange.min(by: { (a, b) -> Bool in return a.val_rate < b.val_rate })?.val_rate
                        timeValRanges.append(ValRangeDB(timestamp: data.timestamp, val_min: rangeMinVal ?? 0, val_max: rangeMaxVal ?? 0))
                }
                lastTime = data.timestamp
                tempRange = []
            }
        }
    } // Конец цикла
    return timeValRanges // Возвращаем массив поддиапазонов
}

*/


/// Механизм создания поддиапазонов данных для отображения графиков minVal-maxVal,  где каждая пара (отрезок) соответствует одному пикселю оси X
/// При наличии пробелов в данных отрезки заполняются нулями
func dataDecimation (dataArray: [ValRateDB], pixelRange: Double) -> [ValRangeDB] {
    
    // Сколько секунд приходится на каждый поддиапазон
    let minDataDate  = dataArray.first?.timestamp                                                       // Минимальная дата диапазона данных counts для отображения графика
    let maxDataDate  = dataArray.last?.timestamp                                                        // Максимальная дата диапазона данных counts для отображения графика
    let deltaDataSec = Calendar.current.dateComponents([.second], from: minDataDate!, to: maxDataDate!) // Определение объекта диапазона данных counts с секундами
    let timeSecDataRange = Double(deltaDataSec.second!)                                                 // Получаем число секунд в диапазоне данных counts
    let secPerPixel = pixelRange == 0 ? 0 : timeSecDataRange / pixelRange                                                     // Сколько секунд приходится на каждый пиксель
    if  secPerPixel == 0 { return [] }                                                                  // Делать тут нечего!

    var timeValRanges  : [ValRangeDB] = []                                                              // Будущий возвращаемый массив поддиапазонов с минимальными/максимальными значениями
    var tempRange      : [ ValRateDB] = []                                                              // Временный массив для откладывания значений одного поддиапазона

    var lastTime = dataArray[0].timestamp                                                               // Текущая точка отсчёта времени для определения начала нового поддиапазона
    for data in dataArray {
        let curSecDiffs = Calendar.current.dateComponents([.second], from: lastTime, to: data.timestamp)// Расстояние между началом поддиапазона и текущим элементом в DateComponents
        let timeSecRange = Double(curSecDiffs.second!)                                                  // Расстояние между началом поддиапазона и текущим элементом в секундах
        if Double(dataArray.count) <= pixelRange { // Случай, когда данных меньше, чем точек
            // Всё равно пройдём весь диапазон (но минимальное значение будет -1, чтобы соединять в графике точки, а не рисовать отрезки, а справа просто ничего не будет рисоваться
            timeValRanges.append(ValRangeDB(timestamp: data.timestamp, val_min: -1, val_max: data.val_rate))
        } else {                            // Стандартный вариант
            if secPerPixel > timeSecRange { // Мы ещё не накопили в диапазоне секунд timeSecRange больше приходящегося на один пиксель secPerPixel
                tempRange.append(data)      // Мы копим в tempRange значения диапазона
            } else {                        // Мы перешагнули диапазон
                
                let ticInTimeSecRange = Int(timeSecRange / secPerPixel)
                if ticInTimeSecRange > 1 {
                    for _ in 1...ticInTimeSecRange {
                        timeValRanges.append(ValRangeDB(timestamp: data.timestamp, val_min: 0, val_max: 0))
                    }
                } else {
                    let rangeMaxVal =  tempRange.max(by: { (a, b) -> Bool in return a.val_rate < b.val_rate })?.val_rate
                    let rangeMinVal =  tempRange.min(by: { (a, b) -> Bool in return a.val_rate < b.val_rate })?.val_rate
                        timeValRanges.append(ValRangeDB(timestamp: data.timestamp, val_min: rangeMinVal ?? 0, val_max: rangeMaxVal ?? 0))
                }
                lastTime = data.timestamp
                tempRange = []
            }
        }
    } // Конец цикла
    return timeValRanges // Возвращаем массив поддиапазонов
}

/// Обрезает лишние данные не входящие в диапазон, оставляя максимальные и минимальные значения входящего диапазона для последующего отображения на графике, с учётом числа используемых пикселей `pixelRange`
func logDecimation (counts: [ValRateDB], from fromDate: Date, to toDate: Date, pixelRange: Double) -> [ValRangeDB] {
    
    if counts.count == 0 { return [] }              // Источник пустой -> значит пустой и приёмник
    let minDataDate  = counts.first?.timestamp      // Минимальная дата диапазона данных counts для отображения графика
    let maxDataDate  = counts.last?.timestamp       // Максимальная дата диапазона данных counts для отображения графика
    let deltaDataSec = Calendar.current.dateComponents([.second], from: minDataDate!, to: maxDataDate!) // Определение объекта диапазона данных counts с секундами
    let timeSecDataRange = Double(deltaDataSec.second!) // Получаем число секунд в диапазоне данных counts

    let firstDateRange = Calendar.current.dateComponents([.second], from: fromDate, to: minDataDate!)
    let firstSecDataRange = Double(firstDateRange.second!) // Получаем число секунд в 1-м диапазоне
    
    // Обработка 3-го диапазона
    let thirdDateRange = Calendar.current.dateComponents([.second], from: maxDataDate!, to: toDate)
    let thirdSecDataRange = Double(thirdDateRange.second!) // Получаем число секунд в 3-м диапазоне
    
    let secPerPixel = (firstSecDataRange + timeSecDataRange + thirdSecDataRange) / pixelRange // Сколько секунд приходится на каждый пиксель

    var timeCountsRange   : [ValRangeDB] = []   // Будущий возвращаемый массив диапазона с минимальными/максимальными значениями
    
    // Заполняем 1-й диапазон нулевыми значениями
    let pixelsOnFirstSecDataRange = secPerPixel == 0 ? 0 : Int(firstSecDataRange / secPerPixel) // Число пикселей, приходящееся на 1-й диапазон
    if  pixelsOnFirstSecDataRange >= 1 {
        for pixel in 1...pixelsOnFirstSecDataRange {
            let curDate = fromDate.addingTimeInterval(Double(pixel) * secPerPixel)
            timeCountsRange.append(ValRangeDB(timestamp: curDate, val_min: 0, val_max: 0))
        }
    } // Иначе 1-го диапазона просто нет…

    let pixelsOnRange = secPerPixel == 0 ? 0 : timeSecDataRange / secPerPixel
    
//    let graphValues = dataDecimation(dataArray: counts, pixelRange: pixelsOnRange)
//    for count in graphValues { print("\(count.timestamp),\(count.val_min),\(count.val_max)") }
//    timeCountsRange = timeCountsRange + graphValues
    
    return timeCountsRange
}

func rawMsToDate(array: [RC_GRP_DOSE_RATE], baseDate: Date) -> [ValRateDB] {
    
    if array.count == 0 { return [] }                       // Источник пустой -> значит пустой и приёмник
    var tempArray: [ValRateDB] = []                         // Создаём временный массив, но с реальными значениям дат вместо относительных миллисекунд
    for item in array {
        let realDate = measureDate(item.msTime, startDate: baseDate)
        tempArray.append(ValRateDB(timestamp: realDate, val_rate: Float(item.chnDoseRate)))
    }
    // DEBUG:
//    for item in tempArray {
//        print(item.timestamp, item.val_rate)
//    }
    
    return tempArray
}


/// Структура из трёх дат, включая промежуточную, для построения графиков
struct DateRange {
    var fromDate    : Date  // Начальная дата диапазона
    var midDate     : Date  // Средняя   дата диапазона
    var toDate      : Date  // Конечная  дата диапазона
    
    /// Инициатор, позволяющий создать все даты диапазона на основе даты `date`, смещения в секундах `secDelta` и расстояния между датами `secRange` в секундах
    init(from date: Date, secDelta: Double, secRange: Double) {
        fromDate = date.addingTimeInterval(secDelta)                    // Начальная дата (смещение относительно date)
         midDate = date.addingTimeInterval(secDelta + secRange / 2.0)   // Средняя   дата (средина между диапазоном secRange плюс смещение secDelta)
          toDate = date.addingTimeInterval(secDelta + secRange)         // Конечная  дата с учётом разницы между датами и смещением secDelta
    }
}


//func getHalphDateRange(from fromDate: Date, to toDate: Date) -> Date {
//    let diffs = Calendar.current.dateComponents([.second], from: fromDate, to: toDate)
//    let timeRange = Double(diffs.second!) / 2.0
//    return fromDate.addingTimeInterval(timeRange)
//}
//
//func getDoubleDateRange(from fromDate: Date, to toDate: Date) -> Date {
//    let diffs = Calendar.current.dateComponents([.second], from: fromDate, to: toDate)
//    let timeRange = Double(diffs.second!) * 2.0
//    let date = fromDate.addingTimeInterval(-timeRange)
//    return date
//}

func getMagnificationDateStep(from fromDate: Date, to toDate: Date, on curStep: DiagramTimeScaleStep, isCompressed: Bool) -> DateRange  {
    
    let oldSecRange = curStep.rawValue
    let newSecRange = isCompressed ? curStep.next().rawValue : curStep.pred().rawValue
    let deltaSec    = Double(oldSecRange - newSecRange) / 2.0
    let newRange    = DateRange(from: fromDate, secDelta: deltaSec, secRange: Double(newSecRange))
    return  newRange
}

func getMagnificationDateRange2(from fromDate: Date, to toDate: Date, on rate: Double) -> DateRange  {
    
    let oldSecRange = Double(Calendar.current.dateComponents([.second], from: fromDate, to: toDate).second!)
    let newSecRange =  oldSecRange * rate
    let deltaSec    = (oldSecRange - newSecRange) / 2.0
    let newRange = DateRange(from: fromDate, secDelta: deltaSec, secRange: newSecRange)
    return  newRange
}

func getMagnificationDateRange(from fromDate: Date, to toDate: Date, on rate: Double) -> (fromDate: Date, midDate: Date, toDate: Date)  {
    
    let oldSecRange = Double(Calendar.current.dateComponents([.second], from: fromDate, to: toDate).second!)
    let newSecRange =  oldSecRange * rate
    let deltaSec    = (oldSecRange - newSecRange) / 2.0
    let midDelta    =  deltaSec / 2.0
    
    var     newRange: (fromDate: Date, midDate: Date, toDate: Date)
            newRange.fromDate = fromDate.addingTimeInterval( deltaSec)
            newRange.midDate  = fromDate.addingTimeInterval( midDelta)
            newRange  .toDate =   toDate.addingTimeInterval(-deltaSec)
    return  newRange
}

/// Вычисляем начальную, среднюю и конечную даты структуры `DateRange` сдвинутые на `rate` по отношению к разнице между датами
func getShiftDateRange2(from fromDate: Date, to toDate: Date, on rate: Double) -> DateRange {
    let  newSecRange = Double(Calendar.current.dateComponents([.second], from: fromDate, to: toDate).second!)    // Число секунд между двумя датами
    let     deltaSec = newSecRange * rate                                                                        // На сколько секунд смещаем даты
    let     newRange = DateRange(from: fromDate, secDelta: deltaSec, secRange: newSecRange) // начальная, средняя и конечная даты сдвинутые на deltaSec
    return  newRange
}


func getShiftDateRange(from fromDate: Date, to toDate: Date, on rate: Double) -> (fromDate: Date, toDate: Date) {
    let diffs = Calendar.current.dateComponents([.second], from: fromDate, to: toDate)
    let timeRange = Double(diffs.second!) * rate
    
    var     newRange: (fromDate: Date, toDate: Date)
            newRange.fromDate = fromDate.addingTimeInterval(timeRange)
            newRange  .toDate =   toDate.addingTimeInterval(timeRange)
    return  newRange
}

func getMidDate(from fromDate: Date, to toDate: Date) -> Date {
    let secRange = Double(Calendar.current.dateComponents([.second], from: fromDate, to: toDate).second!)
    let midDelta = secRange / 2.0
    return fromDate.addingTimeInterval(midDelta)
}

func secondsToHoursMinutesSeconds(_ seconds: Int32) -> (Int32, Int32, Int32, Int32) {
    return ((seconds / 3600) / 24, (seconds / 3600) % 24, (seconds % 3600) / 60, (seconds % 3600) % 60)
}

/** Превращает битовую маску целого числа любой размерности (_Int, UInt32_) в логический массив, указывающий на наличие флага маски
 
 *Example:* `UInt8 = 1 (0b0001) -> [true, false, false, false]`*/
func bitMaskInBoolArray<T: BinaryInteger>(_ value: T) -> [Bool] {
    var boolArray: [Bool] = []                              // Массив для последующего возврата
    let arrayCount = MemoryLayout.size(ofValue: value) * 4  // Размер в битах
    for (index, _) in (0..<arrayCount).enumerated() {       // Проход по всем битам маски
        boolArray.append((value & (1 << index)) != 0)       // Добавляем в массив факт действительного бита маски
    }
    boolArray.reverse() // Массив в обратном порядке (для byte 0001 будет true, false, false, false)
    return boolArray
}


func mergeDoseRateTimerArray(firstArray: [ValRateDB], secondArray: [ValRateDB]) -> [ValRateDB] {
    var firstArray0: [ValRateDB] = []
    for item1 in firstArray {
        for item2 in secondArray {
            if item2.timestamp != item1.timestamp {
                firstArray0.append(ValRateDB(timestamp: item2.timestamp, val_rate: item2.val_rate))
            }
        }
    }
    return firstArray0
}

/// Определяет подходящие единицы измерения графика, значения и число меток сетки
func timeTicsForRange(fromDate: Date, toDate: Date) -> (curUnit: Units, valRange: CGFloat, tics: [DiagramTick]) {
        
    let             calendar = Calendar.current
    let     diffs = calendar.dateComponents([.second], from: fromDate, to: toDate)
    var ticNumber: CGFloat = 0
    var ticVal   : CGFloat = 0
    var valRange : CGFloat = 0
    var curUnit  : Units   = .second

    if diffs.second! <= Units.second.rawValue {
        ticNumber = 10
        curUnit = .second
        valRange = CGFloat(diffs.second!) / CGFloat(Units.second.rawValue) // Число минут в диапазоне
        ticVal = valRange / ticNumber
        if      ticVal > 1  && ticVal <=  5 { ticVal =  5 }
        else if ticVal > 5  && ticVal <= 10 { ticVal = 10 }
        else if ticVal > 10 && ticVal <= 15 { ticVal = 15 }
    } else if diffs.second! < Units.minute.rawValue * 10 {
        ticNumber = 20
        curUnit = .second
        valRange = CGFloat(diffs.second!) / CGFloat(Units.second.rawValue) // Число секунд в диапазоне
        ticVal = valRange / ticNumber
        if      ticVal > 1  && ticVal <=  5 { ticVal =  5 }
        else if ticVal > 5  && ticVal <= 10 { ticVal = 10 }
        else if ticVal > 10 && ticVal <= 15 { ticVal = 15 }
    } else if diffs.second! <= Units.minute.rawValue * 180 {
        ticNumber = 10
        curUnit = .minute
        valRange = CGFloat(diffs.second!) / CGFloat(Units.minute.rawValue) // Число минут в диапазоне
        ticVal = valRange / ticNumber
        if      ticVal > 1  && ticVal <=  5 { ticVal =  5 }
        else if ticVal > 5  && ticVal <= 10 { ticVal = 10 }
        else if ticVal > 10 && ticVal <= 15 { ticVal = 15 }
        
    } else if diffs.second! <= Units.hour.rawValue * 48 {
        ticNumber = 10
        curUnit = .hour
        valRange = CGFloat(diffs.second!) / CGFloat(Units.hour.rawValue) // Число часов в диапазоне
        ticVal = valRange / ticNumber
        if      ticVal >= 0  && ticVal <=  1 { ticVal =  1 }
        else if ticVal > 1  && ticVal <=  6 { ticVal =  6 }
        else if ticVal > 6  && ticVal <= 12 { ticVal = 12 }
        else if ticVal > 12 && ticVal <= 24 { ticVal = 24 }

    } else if diffs.second! <= Units.day.rawValue * 30 {
        ticNumber = 10
        curUnit = .day
        valRange = CGFloat(diffs.second!) / CGFloat(Units.day.rawValue) // Число дней в диапазоне
        ticVal = valRange / ticNumber
        if      ticVal >= 0  && ticVal <=  1 { ticVal =  1 }
        else if ticVal >  1  && ticVal <=  7 { ticVal =  7 }
        else if ticVal >  7  && ticVal <= 15 { ticVal = 15 }
        else if ticVal > 15  && ticVal <= 30 { ticVal = 30 }
    } else if diffs.second! <= Units.month.rawValue * 12 {
        ticNumber = 10
        curUnit = .day
        valRange = CGFloat(diffs.second!) / CGFloat(Units.day.rawValue) // Число дней в диапазоне
        ticVal = valRange / ticNumber
        if      ticVal >= 0  && ticVal <=  1 { ticVal =  1 }
        else if ticVal >  1  && ticVal <=  6 { ticVal =  6 }
        else if ticVal >  6  && ticVal <= 12 { ticVal = 12 }
        else if ticVal > 15  && ticVal <= 30 { ticVal = 30 }
    }

    var curTicArray: [DiagramTick] = []
    var curTicVal = 0
    if ticVal < 1 {
        print("Нет данных для \(diffs.second!)")
    }
    if ticVal >= 1 { // Если где-то ticVal не определилось…
        repeat {
            curTicArray.append(DiagramTick(val: curTicVal))
            curTicVal += Int(ticVal)
        } while curTicVal < Int(valRange)
        curTicArray.append(DiagramTick(val: curTicVal))
    }
    return (curUnit, valRange, curTicArray)

}


extension Date {
    
    func distance(from date: Date, only component: Calendar.Component, calendar: Calendar = .current) -> Int {
        let days1 = calendar.component(component, from: self)
        let days2 = calendar.component(component, from: date)
        return days1 - days2
    }

    func hasSame(_ component: Calendar.Component, as date: Date) -> Bool {
        distance(from: date, only: component) == 0
    }
}

extension Array where Element: Comparable {
    /// Insert element in the correct location of a sorted array
    mutating func insertSorted(_ element: Element) {
        let index = firstIndex(where: { element < $0 })
        insert(element, at: index ?? endIndex)
    }
}

extension UIApplication {

    static var appVersion: String {
        if let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") {
            return "\(appVersion)"
        } else {
            return ""
        }
    }

    static var build: String {
        if let buildVersion = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) {
            return "\(buildVersion)"
        } else {
            return ""
        }
    }

    static var versionBuild: String {
        let version = UIApplication.appVersion
        let build = UIApplication.build

        var versionAndBuild = "v\(version)"

        if version != build {
            versionAndBuild = "v\(version)(\(build))"
        }

        return versionAndBuild
    }

}


