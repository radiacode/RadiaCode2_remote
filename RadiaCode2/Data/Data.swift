//
//  Data.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import Foundation
import CoreBluetooth

enum ShowDiagram {
    case dose
    case count
    case both
}

struct FindDevice {
    var peripheral: CBPeripheral
    var sn: UInt32
    var name: String?
    var uuid: UUID?
    var chip: String?
}

/// Структура для использования в графиках как для мощности дозы, так и для скорости счёта
struct RC_GRP_DOSE_RATE {   // TODO: переименовать позже во что-то более универсальное
    var msTime: Int         // Время значения в приведённых милисекундах от базового времени
    var chnDoseRate: Double // Значение для последующего графического отображения
}

struct ValRateDB {
    var timestamp: Date
    var val_rate: Float
}

/// Структура для массива определения максимальных и минималных точек диапазона графика
struct ValRangeDB {
    var timestamp: Date
    var val_min: Float
    var val_max: Float
}

/// Для построения графиков cкорости счёта
struct CountRateStruct: Identifiable {
    var id = UUID()             // Для протокола Identifiable
    var msTime: Int             // Время из прибора в милисекундах
    var chnCountRate: Double    // Значение cкорости счёта
}

// Для работы с ForEach SwiftUI
struct GraphDoseRateArray: Identifiable {
    var id: Int
    var chnDoseRate: Double
}


struct GraphCountRateArray: Identifiable {
    var id: Int
    var chnCountRate: Double
}

/// Для построения оси времени графиков
struct TimeTic: Identifiable {
    var id = UUID()
    var secTime: Int
    var showText = false
}

struct DiagramTick: Identifiable {
    let id = UUID()
    let val: Int
}

/// Единицы измерения графиков
enum Units: Int {
    case second =          1
    case minute =         60
    case hour   =      3_600  // 60 мин * 60 сек
    case day    =     86_400  // 60 мин * 60 сек * 24 часа
    case month  =  2_592_000  // 30 дней 60 мин * 60 сек * 24 часа
    case year   = 31_104_000
}



enum Status {
    case timeout
    case connecting
    case connected
    case serviceFound
    case disconnected
}

enum RC_FRM: UInt8 {
    case RC_FRM_SINGLE_MEAS     = 0
    case RC_FRM_MEAS            = 1
}

// Виды группы (GRP) данных, получаемых с прибора в реальном времени
enum RC_GRP: UInt8 {
    case RC_GRP_COUNT_RATE      =  0
    case RC_GRP_DOSE_RATE       =  1
    case RC_GRP_DOSE_RATE_DB    =  2
    case RC_GRP_RARE_DATA       =  3
    case RC_GRP_USER_DATA       =  4
    case RC_GRP_SHEDULE_DATA    =  5
    case RC_GRP_ACCEL_DATA      =  6
    case RC_GRP_EVENT           =  7
    case RC_GRP_RAW_COUNT_RATE  =  8
    case RC_GRP_RAW_DOSE_RATE   =  9
}

/*--- RC_DataType ---*/  // Идентификатор типа данных канала для хранания одного отсчета.
enum RC_DataType: UInt8 {
    case RC_DTP_VOID            = 0              // Канал не используется или не инициализирован.
    case RC_DTP_FLOAT           = 1              // Float32 Точность 32 в данном случае принципиальна
    case RC_DTP_UINT8           = 2              // UInt8
    case RC_DTP_INT8            = 3              //  Int8
    case RC_DTP_UINT16          = 4              // UInt16
    case RC_DTP_INT16           = 5              //  Int16
    case RC_DTP_UINT24          = 6              //     24bit Необходимы дополнительные преобразования
    case RC_DTP_INT24           = 7              //     24bit Необходимы дополнительные преобразования
    case RC_DTP_UINT32          = 8              // UInt32
    case RC_DTP_INT32           = 9              //  Int32
}

/*--- RC_ChannelId ---*/ // Идентификатор измерительного канала.
enum RC_ChannelId: UInt8 {
    case RC_CHN_FLAGS            =  0              /* Маска флагов                                         */
    case RC_CHN_TIME             =  1              /* Метка времени                                        */
    case RC_CHN_COUNT            =  2              /* Количество зарегистрированных частиц                 */
    case RC_CHN_COUNT_RATE       =  3              /* Скорость счета                                       */
    case RC_CHN_DOSE_RATE        =  4              /* Мощность дозы                                        */
    case RC_CHN_DOSE_RATE_ERR    =  5              /* Случайная ошибка мощности дозы                       */
    case RC_CHN_DURATION         =  6              /* Длительность                                         */
    case RC_CHN_DOSE             =  7              /* Доза                                                 */
    case RC_CHN_TEMPERATURE      =  8              /* Температура                                          */
    case RC_CHN_BTRY_CHRG        =  9              /* Уровень заряда аккумулятора                          */
    case RC_CHN_ACC_X            = 10              /* Ускорение вдоль оси X                                */
    case RC_CHN_ACC_Y            = 11              /* Ускорение вдоль оси Y                                */
    case RC_CHN_ACC_Z            = 12              /* Ускорение вдоль оси Z                                */
    case RC_CHN_OPTIC            = 13              /* Данные оптического сенсора                           */
    case RC_CHN_EVENT            = 14              /* Событие                                              */
    case RC_CHN_EVENT_CHN_MSK    = 15              /* Маска каналов, передаваемых вместе с событием        */
    case RC_CHN_COUNT_RATE_ERR   = 16              /* Случайная ошибка скорости счета                      */
}

/*--- RC_EventCode ---*/ // Идентификатор события.
enum RC_EventCode: UInt8 {
    case RC_EV_PwrOff            =  0           // Обесточивание прибора
    case RC_EV_PwrOn             =  1           // Включение питания прибора
    case RC_EV_LowVBat_PwOff     =  2           // Аварийное выключение прибора по разряду элементов питания
    case RC_EV_SetConfig         =  3           // Изменились параметры (один или несколько) конфигурации прибора
    case RC_EV_DoseReset         =  4           // Обнуление дозы
    case RC_EV_UserEvent         =  5           // Пользовательская метка
    case RC_EV_LowVBat           =  6           // Сигнал разряда элементов питания перед аварийным выключением прибора
    case RC_EV_ChargeOn          =  7           // Начало заряда аккумулятора
    case RC_EV_ChargeOff         =  8           // Окончание заряда аккумулятора
    case RC_EV_DRA_Lev1          =  9           // Тревога 1 по мощности дозы
    case RC_EV_DRA_Lev2          = 10           // Тревога 2 по мощности дозы
    case RC_EV_DRA_OutSc         = 11           // Зашкал по мощности дозы
    case RC_EV_DSA_Lev1          = 12           // Тревога 1 по дозе
    case RC_EV_DSA_Lev2          = 13           // Тревога 2 по дозе
    case RC_EV_DSA_OutSc         = 14           // Зашкал по дозе
    case RC_EV_TA_TooLow         = 15           // Слишком низкая  температура (ниже диапазона допустимых значений)
    case RC_EV_TA_TooHigh        = 16           // Слишком высокая температура (выше диапазона допустимых значений)
};


// TODO: возможно потом сделать типом UInt32 и брать значение команды отсюда
enum COMMAND {
    case SET_EXCHANGE
    case VSFR_DEVICE_CTRL
    case VSFR_DEVICE_LANG
    case VSFR_DS_UNITS
    case VSFR_ALARM_MODE
    case VSFR_DISP_CTRL
    case VSFR_DISP_BRT
    case VSFR_SOUND_CTRL
    case VSFR_VIBRO_CTRL
    case VSFR_DISP_OFF_TIME
    case RC_VSFR_DR_LEV1_uR_h
    case RC_VSFR_DR_LEV2_uR_h
    case RC_VSFR_DS_LEV1_100uR
    case RC_VSFR_DS_LEV2_100uR
    case RC_VSFR_RAW_FILTER
    case VS_CONFIGURATION
    case VS_DATA_BUF
    case VS_DATA_BUF_empty
    case VSFR_DEVICE_TIME
    case SET_LOCAL_TIME
    case NONE
    case STOP
    case ERROR
    case INACTIVE
    case BACKGROUND
}

let HOST_ID:                UInt32 = 0xFF12FF01
let TARGET_ID:              UInt32 = 0xFF03FF33

let NAME          = "RadiaCode-101"  //@RC-101"

let SET_EXCHANGE          : UInt32  = 0x80000007
let VS_DATA_BUF           : UInt32  = 0x00000100    // Буфер потоковых данных (например, результатов измерений).
let RD_VIRT_STRING        : UInt32  = 0x80000826    /* Read virtual string */
let WR_VIRT_SFR           : UInt32  = 0x80000825    // Запись виртуального регист
let SET_LOCAL_TIME        : UInt32  = 0x80000A04    // Установка локального времени прибора
let     GET_STATUS        : UInt32  = 0x80000005

let    RD_VIRT_SFR        : UInt32  = 0x80000824 // Чтение виртуального регистра


let VSFR_DEVICE_TIME      : UInt32  = 0x00000504    /* RC, Чтение и сброс миллисекундного счетчика прибора */

let VS_CONFIGURATION      : UInt32  = 0x00000002    /* Replacement for obsolete STRING_CONFIGURATION */

let LENGTH_SIZE           = 4    // Длина в байтах поля данных длины пакета, прикрепляемого за командой
var BUFFER_TX_SIZE: UInt16 = 0 // BUFFER_IN_MAX_SIZE
var BUFFER_RX_SIZE: UInt16 = 0 // BUFFER_OUT_MAX_SIZE

var    SFR_ID             : UInt32? = nil        // Идентификатор виртуального регистра (если есть) единственный способ узать что он запрашивался
var     VALID             : UInt32? = nil        // Подтверждение правильности записи WR_VIRT_SFR

// МАСКИ
var rc_CHN_FLAGS          : UInt16?              // Канал флагов входит в каждую группу и используется host-ом совместно с параметром FldMsk
var RC_FLG_DB_WRITE_BIT   : UInt16  =  1 << 12   // результат текущего измерения должен попасть в базу данных
var RC_FLG_OLD_DATA_BIT   : UInt16  =  1 << 15   // Данные достаточно древние, т.е. побывали в EEPROM-буфере прибора

let dataStructureStr = "[DataStructure]"

/// Канал флагов RC_CHN_FLAGS входит в каждую группу и используется host-ом совместно с параметром FldMsk.
struct RC_CHN_FLAGS: OptionSet {
    
    let rawValue: UInt16 // Значение флагов
    
    // Однобитовые флаги
    static let rcFlgDoseRateAlarmLev1 = RC_CHN_FLAGS(rawValue: 1 <<  0)  // Тревога 1-го уровня по мощности дозы
    static let rcFlgDoseRateAlarmLev2 = RC_CHN_FLAGS(rawValue: 1 <<  1)  // Тревога 2-го уровня по мощности дозы
    static let rcFlgDoseRateAlarmPlay = RC_CHN_FLAGS(rawValue: 1 <<  2)  // Орать тревогу по мощности дозы
    static let rcFlgDoseAlarmLev1     = RC_CHN_FLAGS(rawValue: 1 <<  3)  // Тревога 1-го уровня по дозе
    static let rcFlgDoseAlarmLev2     = RC_CHN_FLAGS(rawValue: 1 <<  4)  // Тревога 2-го уровня по дозе
    static let rcFlgDoseAlarmPlay     = RC_CHN_FLAGS(rawValue: 1 <<  5)  // Орать тревогу по дозе
    static let rcFlgTemperatureNorm   = RC_CHN_FLAGS(rawValue: 1 <<  6)  // Нормальная температура
    static let rcFlgTemperatureLow    = RC_CHN_FLAGS(rawValue: 1 <<  7)  // Низкая температура
    static let rcFlgEmpty             = RC_CHN_FLAGS(rawValue: 1 <<  8)  // Флаг не используется!!!
    static let rcFlgScheduleRun       = RC_CHN_FLAGS(rawValue: 1 <<  9)  // Выполняется измерение по расписанию
    static let rcFlgUserRun           = RC_CHN_FLAGS(rawValue: 1 << 10)  // Выполняется измерение, запущенное пользователем
    static let rcFlgMotion            = RC_CHN_FLAGS(rawValue: 1 << 11)  // Прибор движется (в смысле не покоится)
    static let rcFlg_DB_Write         = RC_CHN_FLAGS(rawValue: 1 << 12)  // Результат текущего измерения должен попасть в базу данных
    static let rcFlgPowerUSB          = RC_CHN_FLAGS(rawValue: 1 << 13)  // Питание от внешнего источника, подключеного к USB разъему
    static let rcFlgPowerCharged      = RC_CHN_FLAGS(rawValue: 1 << 14)  // Заряд аккумулятора от внешнего источника
    static let rcFlgOldData           = RC_CHN_FLAGS(rawValue: 1 << 15)  // Старые данные из EEPROM-буфера

    // Комбинированные флаги
    static let cFlgDoseRateAlarmOutOfData: RC_CHN_FLAGS = [.rcFlgDoseRateAlarmLev1, .rcFlgDoseRateAlarmLev2]    // Зашкаливание по мощности дозы
    static let cFlgDoseAlarmOutOfData    : RC_CHN_FLAGS = [.rcFlgDoseAlarmLev1,     .rcFlgDoseAlarmLev2    ]    // Зашкаливание по дозе
    static let rcFlgTemperatureHot       : RC_CHN_FLAGS = [.rcFlgTemperatureNorm,   .rcFlgTemperatureLow   ]    // Высокая температура
    static let rcFlgPowerTooLow          : RC_CHN_FLAGS = [.rcFlgPowerUSB,          .rcFlgPowerCharged     ]    // Аккумулятор сильно разряжен
}


// Язык прибора
enum Lang: Int {
    case rus = 0
    case eng = 1
}
func langStr(lang: Lang?) -> String {
    switch   lang {
        case .rus : return String(localized: "Russian")
        case .eng : return String(localized: "English")
        case .none: return String(localized: "ERROR")
    }
}

// TODO: Изменить enum на более универсальный:
// Использование одновременно и текста и целого в enum
// https://stackoverflow.com/questions/32952248/how-to-get-all-enum-values-as-an-array
//enum EstimateItemStatus: String, CaseIterable {
//    case pending = "Pending"
//    case onHold = "OnHold"
//    case done = "Done"
//
//    init?(id : Int) {
//        switch id {
//        case 1: self = .pending
//        case 2: self = .onHold
//        case 3: self = .done
//        default: return nil
//        }
//    }
//}

enum Unit: Int {
    case  r = 0 // Рентгены
    case sv = 1 // Зиверты
}
func unitStr(unit: Unit?) -> String {
    switch   unit {
        case .r :   return String(localized: "Roentgen")
        case .sv :  return String(localized: "Sievert")
        case .none: return String(localized: "ERROR")
    }
}


enum AlarmMode: Int {
    case many = 0
    case one  = 1
}
func alarmModeStr(alarm: AlarmMode?) -> String {
    switch   alarm {
        case .many :   return String(localized: "Many")
        case .one  :   return String(localized: "One")
        case .none :   return String(localized: "ERROR")
    }
}


enum DisplayMode: Int {
    case auto   = 0
    case normal = 1
    case rotate = 2
}

enum LightMode: Int {
    case off  = 0
    case ena  = 1
    case auto = 3
}

enum OffTime: UInt32 {
    case sec05  = 0
    case sec10 = 1
    case sec15 = 2
    case sec30 = 3
}

/// Последовательность шагов увеличения/уменьшения масштаба временной шкали графиков.
/// Сырые значения указывают на число секунд в интервале, подключен протокол последовательной иттерации `enum`.
/// Включает в себя механизм перехода к предыдущему и последующему значению шага.
/// См. `DiagramTimeScaleStep` в `Playground`
enum DiagramTimeScaleStep: Int, CaseIterable {
    case
        min1   =         60,    //  1 минута  = 60 сек
        min2   =        120,    //  2 минуты  = 60 сек * 2 мин
        min4   =        240,    //  4 минуты  = 60 сек * 4 мин
        min10  =        600,    // 10 минут   = 60 сек * 10 мин
        min30  =      1_800,    // 30 минут   = 60 сек * 30 мин
        hour1  =      3_600,    //  1 час     = 60 сек * 60 мин * 1 час
        hour2  =      7_200,    //  2 час     = 60 сек * 60 мин * 2 час
        hour4  =     14_400,    //  4 часа    = 60 сек * 60 мин * 4 час
        hour8  =     28_800,    //  8 часа    = 60 сек * 60 мин * 8 часов
        hour12 =     43_200,    // 12 часов   = 60 сек * 60 мин * 12 часов
        day1   =     86_400,    //  1 день    = 60 сек * 60 мин * 24 часа
        day2   =    172_800,    //  2 дня     = 60 сек * 60 мин * 24 часа *  2 дня
        day4   =    345_600,    //  4 дня     = 60 сек * 60 мин * 24 часа *  4 дня
        day7   =    604_800,    //  7 дней    = 60 сек * 60 мин * 24 часа *  7 дней
        day14  =  1_209_600,    // 14 дней    = 60 сек * 60 мин * 24 часа * 14 дней
        month1 =  2_592_000,    //  1 месяц   = 60 сек * 60 мин * 24 часа * 30 дней
        month2 =  5_184_000,    //  2 месяца  = 60 сек * 60 мин * 24 часа * 30 дней * 2 месяца
        month6 = 15_552_000,    //  6 месяцев = 60 сек * 60 мин * 24 часа * 30 дней * 6 месяцев
        year1  = 31_104_000     //  1 год     = 60 сек * 60 мин * 24 часа * 30 дней * 12 месяцев
    
    /// Возвращает следующий шаг для текущего шага
    func next() ->  DiagramTimeScaleStep {
        for step in DiagramTimeScaleStep.allCases {             // Запускаем иттерацию по шагам
            if step.rawValue > self.rawValue {                  // Если новый шаг больше текущего
                return step                                     // Возвращаем больший (следующий) шаг
            }
        }
        return self                                             // Мы в конце: следующего нет - оставляем шаг неизменным
    }
    
    /// Возвращает предыдущий шаг для текущего шага
    func pred() ->  DiagramTimeScaleStep {
        for step in DiagramTimeScaleStep.allCases.reversed() {  // Запускаем обратную иттерацию по шагам
            if step.rawValue < self.rawValue {                  // Если новый шаг меньше текущего
                return step                                     // Возвращаем меньший (предыдущий) шаг
            }
        }
        return self                                             // Мы в начале: предыдущего нет - оставляем шаг неизменным
    }
}

var sharePars: PacketParser?

