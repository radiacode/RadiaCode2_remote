//
//  DeviceData.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import Foundation

let VSFR_DEVICE_CTRL          : UInt32 = 0x00000500 // Структура флагов и битовых полей для управления устройством
var RC_DEVICE_CTRL_PWR_ON     : UInt32  = 0x01 << 0   //  Команда/признак включения/выключения прибора
var RC_DEVICE_CTRL_DISPLAY_ON : UInt32  = 0x01 << 1   //  Команда/признак включения/выключения дисплея
var RC_DEVICE_CTRL_SOUND_ENA  : UInt32  = 0x01 << 2   //  Опция - разрешает/запрещает звуковые сигналы
var RC_DEVICE_CTRL_LEDS_ENA   : UInt32  = 0x01 << 3   //  Опция - разрешает/запрещает световые сигналы
var RC_DEVICE_CTRL_VIBRO_ENA  : UInt32  = 0x01 << 4   //  Опция - разрешает/запрещает вибро-сигналы
var RC_DEVICE_CTRL_CHARGER_ON : UInt32  = 0x01 << 5   //  Команда/признак включения/выключения заряда аккумулятора
var RC_DEVICE_CTRL_USER_RUN   : UInt32  = 0x01 << 6   //  Команда/признак запуска/останова пользовательского измерения

let VSFR_DEVICE_LANG          : UInt32 = 0x00000502 // Идентификаторы для выбора языка устройства
var RC_DEVICE_LANG            : UInt32 = 0x00000000       // Текущий установленный язык (Русский)

let VSFR_DISP_CTRL            : UInt32 = 0x00000510 // Основные флаги для управления дисплеем
var RC_DISP_CTRL_PWR_ON       : UInt8  = 0x00       // (1UL << 0)  // Команда/признак включения/выключения дисплея
var RC_DISP_CTRL_LED_ON       : UInt8  = 0x00       // (1UL << 1)  // Команда/признак включения/выключения подсветки дисплея

//var RC_DISP_CTRL_DIR_NORMAL   : UInt8  = 0x01 << 4  // Только обычная ориентация
//var RC_DISP_CTRL_DIR_ROTATED  : UInt8  = 0x01 << 5  // Только перевернутая ориентация
//var RC_DISP_CTRL_DIR_AUTO     : UInt8  = 0x01 << 5  // Только перевернутая ориентация

var RC_DISP_CTRL_DIR_Pos = 4
var RC_DISP_CTRL_DIR_AUTO_msk = UInt32(3) << RC_DISP_CTRL_DIR_Pos // Aвтоматический поворот изображения на дисплее
var RC_DISP_CTRL_DIR_NORMAL   = UInt32(1) << RC_DISP_CTRL_DIR_Pos  // Только обычная ориентация
var RC_DISP_CTRL_DIR_ROTATED  = UInt32(2) << RC_DISP_CTRL_DIR_Pos  // Только перевернутая ориентация

var RC_DISP_CTRL_LED_Pos = 2
var RC_DISP_CTRL_LED_DIS_msk  = UInt32(3) << RC_DISP_CTRL_LED_Pos  // Подсветка запрещена (для установки 00)
var RC_DISP_CTRL_LED_ENA      = UInt32(1) << RC_DISP_CTRL_LED_Pos  // Подсветка разрешена и включается по каждому нажатию на кнопку
var RC_DISP_CTRL_LED_AUTO     = UInt32(2) << RC_DISP_CTRL_LED_Pos  // Подсветка разрешена и включается в зависимости от освещенности

//#define RC_DISP_CTRL_LED_Pos              2
//#define RC_DISP_CTRL_LED_Msk              (3UL << RC_DISP_CTRL_LED_Pos)  // Опция - поле отражает текущий способ включения подсветки дисплея
//#define RC_DISP_CTRL_LED_DIS              (0UL << RC_DISP_CTRL_LED_Pos)  // Подсветка запрещена
//#define RC_DISP_CTRL_LED_ENA              (1UL << RC_DISP_CTRL_LED_Pos)  // Подсветка разрешена и включается по каждому нажатию на кнопку
//#define RC_DISP_CTRL_LED_AUTO             (2UL << RC_DISP_CTRL_LED_Pos)  // Подсветка разрешена и включается в зависимости от освещенности

let VSFR_DISP_OFF_TIME        : UInt32 = 0x00000513      /* RW, Задержка до автоматического выключения дисплея */
let VSFR_DISP_BRT             : UInt32 = 0x00000511      /* RW, Установка яркости дисплея */

let RC_VSFR_DS_UNITS          : UInt32 = 0x00008004 // Опция - единицы измерения дозы
var RC_UNITS                  : UInt32 = 0x00000000 // установлееные единицы измерения

var BOOT_BIT:               UInt8 = 0x00  // ((u32)(1 <<  0)) BOOT_BIT mask
var INIT_BIT:               UInt8 = 0x00  // ((u32)(1 <<  1)) INIT_BIT mask
var RUN_BIT:                UInt8 = 0x00  // ((u32)(1 <<  2)) RUN_BIT mask
var DEBUG_BIT:              UInt8 = 0x00  // ((u32)(1 <<  3)) DEBUG_BIT mask
var LCD_BIT:                UInt8 = 0x00  // ((u32)(1 <<  4)) LCD test failed
var SDC_BIT:                UInt8 = 0x00  // ((u32)(1 <<  5)) SD-card test failed

let RC_VSFR_DR_LEV1_uR_h      : UInt32 = 0x00008000 /* RW, Опция - первый порог по мощности дозы */
let RC_VSFR_DR_LEV2_uR_h      : UInt32 = 0x00008001 /* RW, Опция - второй порог по мощности дозы */
let RC_VSFR_DS_LEV1_100uR     : UInt32 = 0x00008002 /* RW, Опция - первый порог по дозе (1 соответствует 100 uR) */
let RC_VSFR_DS_LEV2_100uR     : UInt32 = 0x00008003 /* RW, Опция - второй порог по дозе (1 соответствует 100 uR) */
let RC_VSFR_RAW_FILTER        : UInt32 = 0x00008006 /* RW, Опция - глубина фильтрации значений, выводимых в серой зоне графиков (Ширина окна усреднения оперативных данных при отображении на графиках) */


let VSFR_SOUND_CTRL           : UInt32 = 0x00000520      /* RW, Флаги включения/выключения звуковых сигналов */
let VSFR_VIBRO_CTRL           : UInt32 = 0x00000530      /* RW, Флаги включения/выключения вибро сигналов */
let VSFR_ALARM_MODE           : UInt32 = 0x000005E0      /* RW, Режим выдачи сигналов тревоги (однократно/непрерывно) */

var RC_SOUND_CTRL_BUTTON_ENA    : UInt32  = 0x01 << 0  // Опция - разрешение/запрет сигнала по нажатию на кнопку
var RC_SOUND_CTRL_CLICK_ENA     : UInt32  = 0x01 << 1  // Опция - разрешение/запрет сигнала по регистрации частиц
var RC_SOUND_CTRL_DR_ALM_L1_ENA : UInt32  = 0x01 << 2  // Опция - разрешение/запрет сигнала по превышению первого порога по мощности дозы
var RC_SOUND_CTRL_DR_ALM_L2_ENA : UInt32  = 0x01 << 3  // Опция - разрешение/запрет сигнала по превышению второго порога по мощности дозы
var RC_SOUND_CTRL_DR_ALM_OS_ENA : UInt32  = 0x01 << 4  // Опция - разрешение/запрет сигнала по зашкаливанию мощности дозы
var RC_SOUND_CTRL_DS_ALM_L1_ENA : UInt32  = 0x01 << 5  // Опция - разрешение/запрет сигнала по превышению первого порога по дозе
var RC_SOUND_CTRL_DS_ALM_L2_ENA : UInt32  = 0x01 << 6  // Опция - разрешение/запрет сигнала по превышению второго порога по дозе
var RC_SOUND_CTRL_DS_ALM_OS_ENA : UInt32  = 0x01 << 7  // Опция - разрешение/запрет сигнала по зашкаливанию дозы
var RC_SOUND_CTRL_CONNECT_ENA   : UInt32  = 0x01 << 8  // Опция - разрешение/запрет сигналов по установке/разрыву связи с host-ом
var RC_SOUND_CTRL_POWER_ENA     : UInt32  = 0x01 << 9  // Опция - разрешение/запрет сигналов по включению/выключению прибора

var RC_VIBRO_CTRL_BUTTON_ENA    : UInt32  = 0x01 << 0  // Опция - разрешение/запрет сигнала по нажатию на кнопку
var RC_VIBRO_CTRL_CLICK_ENA     : UInt32  = 0x01 << 1  // Опция - разрешение/запрет сигнала по регистрации частиц
var RC_VIBRO_CTRL_DR_ALM_L1_ENA : UInt32  = 0x01 << 2  // Опция - разрешение/запрет сигнала по превышению первого порога по мощности дозы
var RC_VIBRO_CTRL_DR_ALM_L2_ENA : UInt32  = 0x01 << 3  // Опция - разрешение/запрет сигнала по превышению второго порога по мощности дозы
var RC_VIBRO_CTRL_DR_ALM_OS_ENA : UInt32  = 0x01 << 4  // Опция - разрешение/запрет сигнала по зашкаливанию мощности дозы
var RC_VIBRO_CTRL_DS_ALM_L1_ENA : UInt32  = 0x01 << 5  // Опция - разрешение/запрет сигнала по превышению первого порога по дозе
var RC_VIBRO_CTRL_DS_ALM_L2_ENA : UInt32  = 0x01 << 6  // Опция - разрешение/запрет сигнала по превышению второго порога по дозе
var RC_VIBRO_CTRL_DS_ALM_OS_ENA : UInt32  = 0x01 << 7  // Опция - разрешение/запрет сигнала по зашкаливанию дозы

var VSFR_SOUND_CTRL_VALUE       : UInt8?                // Флаги звуковых настроек
var VSFR_VIBRO_CTRL_VALUE       : UInt8?                // Флаги вибро настроек
var VSFR_DISP_CTRL_VALUE        : UInt8?                // Флаги настроек дисплея
var VSFR_DISP_OFF_TIME_VALUE    : UInt32?               // Задержка выключения подсветки дисплея
var VSFR_DISP_BRT_VALUE         : UInt32?               // Яркость подсветки дисплея
