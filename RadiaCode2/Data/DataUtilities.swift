//
//  DataUtilities.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import Foundation
import SwiftUI

/// Определение числа байтов канала в потоке по *RC_DataType* строки конфигурации
/// Используется для определения диапазона данных, занимаемых данным каналом в
/// потоке данных с прибора, что позволяет пропускать новые неизвестные каналы
func channelDataSize(_ rc_DataType: Int16) -> Int? {
    switch RC_DataType(rawValue: UInt8(rc_DataType))  {
        case .RC_DTP_VOID       :   return 0
        case .RC_DTP_FLOAT      :   return 4
        case .RC_DTP_UINT8      :   return 1
        case .RC_DTP_INT8       :   return 1
        case .RC_DTP_UINT16     :   return 2
        case .RC_DTP_INT16      :   return 2
        case .RC_DTP_UINT24     :   return 3
        case .RC_DTP_INT24      :   return 3
        case .RC_DTP_UINT32     :   return 4
        case .RC_DTP_INT32      :   return 4
        default                 :   return nil
    }
}

/// Определение названия группы по её ID
/// Нужно для отладочной информации при парсинге потока данных с прибора
func groupName(by id: UInt8) -> String {
    switch RC_GRP(rawValue: id)  {
        case .RC_GRP_COUNT_RATE      : return "RC_GRP_COUNT_RATE"
        case .RC_GRP_DOSE_RATE       : return "RC_GRP_DOSE_RATE"
        case .RC_GRP_DOSE_RATE_DB    : return "RC_GRP_DOSE_RATE_DB"
        case .RC_GRP_RARE_DATA       : return "RC_GRP_RARE_DATA"
        case .RC_GRP_USER_DATA       : return "RC_GRP_USER_DATA"
        case .RC_GRP_SHEDULE_DATA    : return "RC_GRP_SHEDULE_DATA"
        case .RC_GRP_ACCEL_DATA      : return "RC_GRP_ACCEL_DATA"
        case .RC_GRP_EVENT           : return "RC_GRP_EVENT"
        case .RC_GRP_RAW_COUNT_RATE  : return "RC_GRP_RAW_COUNT_RATE"
        case .RC_GRP_RAW_DOSE_RATE   : return "RC_GRP_RAW_DOSE_RATE"
        case .none                   : return "UNKNOWN GRP"
    }
}

func eventName(by id: UInt8) -> String {
    switch RC_EventCode(rawValue: id)  {

    case .RC_EV_PwrOff            : return String(localized: "Power off")              // "Обесточивание прибора"
    case .RC_EV_PwrOn             : return String(localized: "Power on")                // "Включение питания прибора"
    case .RC_EV_LowVBat_PwOff     : return String(localized: "Lost power on low batt")  // "Аварийное выключение прибора по разряду элементов питания"
    case .RC_EV_SetConfig         : return String(localized: "Params changed")          // "Изменились параметры (один или несколько) конфигурации прибора"
    case .RC_EV_DoseReset         : return String(localized: "Dose reset")              // "Обнуление дозы"
    case .RC_EV_UserEvent         : return String(localized: "User mark")               // "Пользовательская метка"
    case .RC_EV_LowVBat           : return String(localized: "Batt low")                // "Сигнал разряда элементов питания перед аварийным выключением прибора"
    case .RC_EV_ChargeOn          : return String(localized: "Charge start")            // "Начало заряда аккумулятора"
    case .RC_EV_ChargeOff         : return String(localized: "Charge end")              //"Окончание заряда аккумулятора"
    case .RC_EV_DRA_Lev1          : return String(localized: "Alarm 1 on dose rate")    // "Тревога 1 по мощности дозы"
    case .RC_EV_DRA_Lev2          : return String(localized: "Alarm 2 on dose rate")    // "Тревога 2 по мощности дозы"
    case .RC_EV_DRA_OutSc         : return String(localized: "Overload on dose rate")   // "Зашкал по мощности дозы"
    case .RC_EV_DSA_Lev1          : return String(localized: "Alarm 1 on dose")         // "Тревога 1 по дозе"
    case .RC_EV_DSA_Lev2          : return String(localized: "Alarm 2 on dose")         // "Тревога 2 по дозе"
    case .RC_EV_DSA_OutSc         : return String(localized: "Overload on dose")        // "Зашкал по дозе"
    case .RC_EV_TA_TooLow         : return String(localized: "Very cool")               // "Слишком низкая  температура (ниже диапазона допустимых значений)"
    case .RC_EV_TA_TooHigh        : return String(localized: "Very hot")                // "Слишком высокая температура (выше диапазона допустимых значений)"
    case .none                    : return String(localized: "UNKNOWN EVENT")           // "Неизвестное событие"
    }
}

func eventImageName(by id: UInt8) -> String {
    switch RC_EventCode(rawValue: id)  {

    case .RC_EV_PwrOff            : return "poweroff"                           // "Обесточивание прибора"
    case .RC_EV_PwrOn             : return "power"                              // "Включение питания прибора"
    case .RC_EV_LowVBat_PwOff     : return "power.dotted"                       // "Аварийное выключение прибора по разряду элементов питания"
    case .RC_EV_SetConfig         : return "gear.badge.checkmark"               // "Изменились параметры (один или несколько) конфигурации прибора"
    case .RC_EV_DoseReset         : return "exclamationmark.arrow.circlepath"   // "Обнуление дозы"
    case .RC_EV_UserEvent         : return "person.fill.checkmark"              // "Пользовательская метка"
    case .RC_EV_LowVBat           : return "battery.0"                          // "Сигнал разряда элементов питания перед аварийным выключением прибора"
    case .RC_EV_ChargeOn          : return "battery.100.bolt"                   // "Начало заряда аккумулятора"
    case .RC_EV_ChargeOff         : return "battery.100.bolt"                   // "Окончание заряда аккумулятора"
    case .RC_EV_DRA_Lev1          : return "exclamationmark.octagon"            // "Тревога 1 по мощности дозы"
    case .RC_EV_DRA_Lev2          : return "exclamationmark.octagon"            // "Тревога 2 по мощности дозы"
    case .RC_EV_DRA_OutSc         : return "exclamationmark.3"                  // "Зашкал по мощности дозы"
    case .RC_EV_DSA_Lev1          : return "exclamationmark.octagon"            // "Тревога 1 по дозе"
    case .RC_EV_DSA_Lev2          : return "exclamationmark.octagon"            // "Тревога 2 по дозе"
    case .RC_EV_DSA_OutSc         : return "exclamationmark.3"                  // "Зашкал по дозе"
    case .RC_EV_TA_TooLow         : return "thermometer.snowflake"              // "Слишком низкая  температура (ниже диапазона допустимых значений)"
    case .RC_EV_TA_TooHigh        : return "thermometer.sun.fill"               // "Слишком высокая температура (выше диапазона допустимых значений)"
    case .none                    : return "camera.metering.unknown"            // "Неизвестное событие"
    }
}

func eventColor(by id: UInt8) -> Color {
    switch RC_EventCode(rawValue: id)  {

    case .RC_EV_PwrOff            : return .red                             // "Обесточивание прибора"
    case .RC_EV_PwrOn             : return .green                           // "Включение питания прибора"
    case .RC_EV_LowVBat_PwOff     : return .red                             // "Аварийное выключение прибора по разряду элементов питания"
    case .RC_EV_SetConfig         : return .blue                            // "Изменились параметры (один или несколько) конфигурации прибора"
    case .RC_EV_DoseReset         : return .blue                            // "Обнуление дозы"
    case .RC_EV_UserEvent         : return .blue                            // "Пользовательская метка"
    case .RC_EV_LowVBat           : return .red                             // "Сигнал разряда элементов питания перед аварийным выключением прибора"
    case .RC_EV_ChargeOn          : return .red                             // "Начало заряда аккумулятора"
    case .RC_EV_ChargeOff         : return .green                           // "Окончание заряда аккумулятора"
    case .RC_EV_DRA_Lev1          : return .red                             // "Тревога 1 по мощности дозы"
    case .RC_EV_DRA_Lev2          : return .red                             // "Тревога 2 по мощности дозы"
    case .RC_EV_DRA_OutSc         : return .red                             // "Зашкал по мощности дозы"
    case .RC_EV_DSA_Lev1          : return .red                             // "Тревога 1 по дозе"
    case .RC_EV_DSA_Lev2          : return .red                             // "Тревога 2 по дозе"
    case .RC_EV_DSA_OutSc         : return .red                             // "Зашкал по дозе"
    case .RC_EV_TA_TooLow         : return .blue                            // "Слишком низкая  температура (ниже диапазона допустимых значений)"
    case .RC_EV_TA_TooHigh        : return .red                             // "Слишком высокая температура (выше диапазона допустимых значений)"
    case .none                    : return .red                             // "Неизвестное событие"
    }
}

