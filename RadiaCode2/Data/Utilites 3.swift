//
//  Utilites.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import Foundation

// Преобразование данных в массив байтов
func toByteArray(data: Data) -> [UInt8] {
    var bytesArray = [UInt8](repeating: 0, count: (data.count)) // Создание пустого массива байт размером под полученные данные
    (data as NSData).getBytes(&bytesArray, length:(data.count) * MemoryLayout<UInt8>.size) // Заполнение массива байт данными
    for element in bytesArray { print(String(format:"%02X ", element), terminator: "") }
    return bytesArray
}

// Преобразование целого числа в массив байтов
func toByteArray<T: BinaryInteger>(_ word: T) -> [UInt8] {
    var    value = word                                                     // Превращаем константу в переменную
    let    data  = NSData(bytes: &value, length: MemoryLayout<T>.size)      // Число в данные
    let    count = data.length                                              // Длина данных в байтах
    var    array = [UInt8](repeating: 0, count: count)                      // Пустой массив байтов по числу байтов в данных
           data.getBytes(        &array, length: MemoryLayout<T>.size)      // Заполняем массив данными
    return array
}

// Целое из массива байтов
func intFrom<T: BinaryInteger>(_ bytesArray:[UInt8], start: Int = 0, length: Int = 4) -> T {
    var lengthByteArray:[UInt8] = []                                        // Готовим пустой массив
    for i in start..<start+length { lengthByteArray.append(bytesArray[i]) } // Заполнение массива байтами длины пакета
    let value = lengthByteArray.withUnsafeBufferPointer(
        { UnsafeRawPointer($0.baseAddress!).load(as: T.self) }              // Забираем сплошное значение
    )
    return value                                                            // Возвращаем уже любого целого типа
}

// Должно быть UInt32 но для CoreData делаем Int32 чтобы потом вернуть в UInt32
func hexStringToInt32(_ hexStr: String) -> Int32? {
    let str = String(hexStr.dropFirst(2))
    return Int32(str, radix: 16)
}

func printBin<T: BinaryInteger>(_ value: T)  {
    let binStr = String(value, radix: 2)// Печатаем саму строку битов "10011"
    let lengthBinStr = binStr.count
    let size = MemoryLayout<T>.size * 8 // Умножаем число байт в целом на число бит в байте
    let deltaToFill = size - lengthBinStr
    for i in (0..<size).reversed() { let      j = i/10;
                                          print(j%10, terminator: "") }; print("")
    for i in (0..<size).reversed()      { print(i%10, terminator: "") }; print("")
    for _ in (0..<size).reversed()      { print("|",  terminator: "") }; print("")
    for _ in 0..<deltaToFill            { print("0",  terminator: "") }; print(binStr)
}

//func binStr<T: BinaryInteger>(_ value: T) -> String {
//    return ""
//}

func dateComp() -> DateComponents {
    let currentDateTime = Date()
    let userCalendar = Calendar.current
    let requestedComponents: Set<Calendar.Component> = [
        .year,
        .month,
        .day,
        .hour,
        .minute,
        .second
    ]
    let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: currentDateTime)
    return dateTimeComponents
}

func BCD_DATE() -> UInt32 {
    var arrayBCD_DATE : [UInt8] = [0] // 0-й байт будет пустым (BCD_DATE - дата в BCD-формате 0x00YYMMDD;)
        arrayBCD_DATE.append(contentsOf: toByteArray(dateComp().day!)  )
        arrayBCD_DATE.append(contentsOf: toByteArray(dateComp().month!))
        arrayBCD_DATE.append(contentsOf: toByteArray(dateComp().year!) )
    let      BCD_DATE:   UInt32 = intFrom(arrayBCD_DATE)
    return   BCD_DATE
}

func BCD_TIME() -> UInt32 {
    var arrayBCD_TIME : [UInt8] = [0] // 0-й байт будет пустым (BCD_DATE - дата в BCD-формате 0x00YYMMDD;)
        arrayBCD_TIME.append(contentsOf: toByteArray(dateComp().hour!)   )
        arrayBCD_TIME.append(contentsOf: toByteArray(dateComp().minute!) )
        arrayBCD_TIME.append(contentsOf: toByteArray(dateComp().second!) )
    let      BCD_TIME:        UInt32 = intFrom(arrayBCD_TIME)
    return   BCD_TIME
}

func measureDate(_ ms: Int, startDate: Date = Date()) -> Date {
    let calendar = Calendar.current
    let  second = Int(Float(ms) / 1000)
    let    date = calendar.date(byAdding: .second, value: second, to: startDate)
    return date!
}

func pow(_ x: Int, _ y: Int) -> Int  { var result = 1;         for _ in 0..<y { result *= x };   return result }
func powTen(_ y: Int)        -> Int  { var result = 1;         for _ in 0..<y { result *= 10 };  return result }
func powPointOne(_ y: Int) -> Double { var result: Double = 1; for _ in 0..<y { result *= 0.1 }; return result }

func rearrange(_ array: [UInt8]) -> [UInt8] {
    var arr2: [UInt8] = []
    for element in array {
        arr2.insert(element, at: 0)
    }
    return arr2
}

let itemFormatter: DateFormatter = {
    let    formatter = DateFormatter()
           formatter.dateStyle = .short
           formatter.timeStyle = .medium
    return formatter
}()
