//
//  Data.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import Foundation
import CoreBluetooth

struct FindDevice {
    var peripheral: CBPeripheral
    var sn: UInt32
    var name: String?
    var uuid: UUID?
    var chip: String?
}

struct RC_GRP_DOSE_RATE {
    var msTime: Int
    var chnDoseRate: Double
}

struct RC_GRP_COUNT_RATE {
    var msTime: Int
    var chnCountRate: Double
}

// Для работы с ForEach SwiftUI
struct GraphDoseRateArray: Identifiable {
    var id: Int
    var chnDoseRate: Double
}

struct GraphCountRateArray: Identifiable {
    var id: Int
    var chnCountRate: Double
}

struct TimeTic: Identifiable {
    var id: Int
    var secTime: Int
    var showText: Bool
}

enum Status {
    case connected
    case serviceFound
    case disconnected
}

enum RC_FRM: UInt8 {
    case RC_FRM_SINGLE_MEAS     = 0
    case RC_FRM_MEAS            = 1
}

// Виды группы (GRP) данных, получаемых с прибора в реальном времени
enum RC_GRP: UInt8 {
    case RC_GRP_COUNT_RATE      =  0
    case RC_GRP_DOSE_RATE       =  1
    case RC_GRP_DOSE_RATE_DB    =  2
    case RC_GRP_RARE_DATA       =  3
    case RC_GRP_USER_DATA       =  4
    case RC_GRP_SHEDULE_DATA    =  5
    case RC_GRP_ACCEL_DATA      =  6
    case RC_GRP_EVENT           =  7
    case RC_GRP_RAW_COUNT_RATE  =  8
    case RC_GRP_RAW_DOSE_RATE   =  9
}

/*--- RC_DataType ---*/  // Идентификатор типа данных канала для хранания одного отсчета.
enum RC_DataType: UInt8 {
    case RC_DTP_VOID            = 0              // Канал не используется или не инициализирован.
    case RC_DTP_FLOAT           = 1              // Float32 Точность 32 в данном случае принципиальна
    case RC_DTP_UINT8           = 2              // UInt8
    case RC_DTP_INT8            = 3              //  Int8
    case RC_DTP_UINT16          = 4              // UInt16
    case RC_DTP_INT16           = 5              //  Int16
    case RC_DTP_UINT24          = 6              //     24bit Необходимы дополнительные преобразования
    case RC_DTP_INT24           = 7              //     24bit Необходимы дополнительные преобразования
    case RC_DTP_UINT32          = 8              // UInt32
    case RC_DTP_INT32           = 9              //  Int32
}

/*--- RC_ChannelId ---*/ // Идентификатор измерительного канала.
enum RC_ChannelId: UInt8 {
    case RC_CHN_FLAGS            =  0              /* Маска флагов                                         */
    case RC_CHN_TIME             =  1              /* Метка времени                                        */
    case RC_CHN_COUNT            =  2              /* Количество зарегистрированных частиц                 */
    case RC_CHN_COUNT_RATE       =  3              /* Скорость счета                                       */
    case RC_CHN_DOSE_RATE        =  4              /* Мощность дозы                                        */
    case RC_CHN_DOSE_RATE_ERR    =  5              /* Случайная ошибка мощности дозы                       */
    case RC_CHN_DURATION         =  6              /* Длительность                                         */
    case RC_CHN_DOSE             =  7              /* Доза                                                 */
    case RC_CHN_TEMPERATURE      =  8              /* Температура                                          */
    case RC_CHN_BTRY_CHRG        =  9              /* Уровень заряда аккумулятора                          */
    case RC_CHN_ACC_X            = 10              /* Ускорение вдоль оси X                                */
    case RC_CHN_ACC_Y            = 11              /* Ускорение вдоль оси Y                                */
    case RC_CHN_ACC_Z            = 12              /* Ускорение вдоль оси Z                                */
    case RC_CHN_OPTIC            = 13              /* Данные оптического сенсора                           */
    case RC_CHN_EVENT            = 14              /* Событие                                              */
    case RC_CHN_EVENT_CHN_MSK    = 15              /* Маска каналов, передаваемых вместе с событием        */
    case RC_CHN_COUNT_RATE_ERR   = 16              /* Случайная ошибка скорости счета                      */
}

/*--- RC_EventCode ---*/ // Идентификатор события.
enum RC_EventCode: UInt8 {
    case RC_EV_PwrOff            =  0           // Обесточивание прибора
    case RC_EV_PwrOn             =  1           // Включение питания прибора
    case RC_EV_LowVBat_PwOff     =  2           // Аварийное выключение прибора по разряду элементов питания
    case RC_EV_SetConfig         =  3           // Изменились параметры (один или несколько) конфигурации прибора
    case RC_EV_DoseReset         =  4           // Обнуление дозы
    case RC_EV_UserEvent         =  5           // Пользовательская метка
    case RC_EV_LowVBat           =  6           // Сигнал разряда элементов питания перед аварийным выключением прибора
    case RC_EV_ChargeOn          =  7           // Начало заряда аккумулятора
    case RC_EV_ChargeOff         =  8           // Окончание заряда аккумулятора
    case RC_EV_DRA_Lev1          =  9           // Тревога 1 по мощности дозы
    case RC_EV_DRA_Lev2          = 10           // Тревога 2 по мощности дозы
    case RC_EV_DRA_OutSc         = 11           // Зашкал по мощности дозы
    case RC_EV_DSA_Lev1          = 12           // Тревога 1 по дозе
    case RC_EV_DSA_Lev2          = 13           // Тревога 2 по дозе
    case RC_EV_DSA_OutSc         = 14           // Зашкал по дозе
    case RC_EV_TA_TooLow         = 15           // Слишком низкая  температура (ниже диапазона допустимых значений)
    case RC_EV_TA_TooHigh        = 16           // Слишком высокая температура (выше диапазона допустимых значений)
};


// TODO: возможно потом сделать типом UInt32 и брать значение команды отсюда
enum COMMAND {
    case SET_EXCHANGE
    case VSFR_DEVICE_LANG
    case VS_CONFIGURATION
    case VS_DATA_BUF
    case VS_DATA_BUF_empty
    case VSFR_DEVICE_TIME
    case SET_LOCAL_TIME
    case NONE
    case STOP
    case ERROR
}

let HOST_ID:                UInt32 = 0xFF12FF01
let TARGET_ID:              UInt32 = 0xFF03FF33

let NAME          = "RadiaCode-101"  //@RC-101"

let SET_EXCHANGE          : UInt32  = 0x80000007
let VS_DATA_BUF           : UInt32  = 0x00000100    // Буфер потоковых данных (например, результатов измерений).
let RD_VIRT_STRING        : UInt32  = 0x80000826    /* Read virtual string */
let WR_VIRT_SFR           : UInt32  = 0x80000825    // Запись виртуального регист
let SET_LOCAL_TIME        : UInt32  = 0x80000A04    // Установка локального времени прибора
let     GET_STATUS        : UInt32  = 0x80000005

let    RD_VIRT_SFR        : UInt32  = 0x80000824 // Чтение виртуального регистра


let VSFR_DEVICE_TIME      : UInt32  = 0x00000504    /* RC, Чтение и сброс миллисекундного счетчика прибора */

let VS_CONFIGURATION      : UInt32  = 0x00000002    /* Replacement for obsolete STRING_CONFIGURATION */

let LENGTH_SIZE           = 4    // Длина в байтах поля данных длины пакета, прикрепляемого за командой
var BUFFER_TX_SIZE: UInt16 = 0 // BUFFER_IN_MAX_SIZE
var BUFFER_RX_SIZE: UInt16 = 0 // BUFFER_OUT_MAX_SIZE

var    SFR_ID             : UInt32? = nil        // Идентификатор виртуального регистра (если есть) единственный способ узать что он запрашивался
var     VALID             : UInt32? = nil        // Подтверждение правильности записи WR_VIRT_SFR

// МАСКИ
var rc_CHN_FLAGS          : UInt16?              // Канал флагов входит в каждую группу и используется host-ом совместно с параметром FldMsk
var RC_FLG_DB_WRITE_BIT   : UInt16  =  1 << 12   // результат текущего измерения должен попасть в базу данных
var RC_FLG_OLD_DATA_BIT   : UInt16  =  1 << 15   // Данные достаточно древние, т.е. побывали в EEPROM-буфере прибора

let dataStructureStr = "[DataStructure]"

/// Канал флагов RC_CHN_FLAGS входит в каждую группу и используется host-ом совместно с параметром FldMsk.
struct RC_CHN_FLAGS: OptionSet {
    
    let rawValue: UInt16 // Значение флагов
    
    // Однобитовые флаги
    static let rcFlgDoseRateAlarmLev1 = RC_CHN_FLAGS(rawValue: 1 <<  0)  // Тревога 1-го уровня по мощности дозы
    static let rcFlgDoseRateAlarmLev2 = RC_CHN_FLAGS(rawValue: 1 <<  1)  // Тревога 2-го уровня по мощности дозы
    static let rcFlgDoseRateAlarmPlay = RC_CHN_FLAGS(rawValue: 1 <<  2)  // Орать тревогу по мощности дозы
    static let rcFlgDoseAlarmLev1     = RC_CHN_FLAGS(rawValue: 1 <<  3)  // Тревога 1-го уровня по дозе
    static let rcFlgDoseAlarmLev2     = RC_CHN_FLAGS(rawValue: 1 <<  4)  // Тревога 2-го уровня по дозе
    static let rcFlgDoseAlarmPlay     = RC_CHN_FLAGS(rawValue: 1 <<  5)  // Орать тревогу по дозе
    static let rcFlgTemperatureNorm   = RC_CHN_FLAGS(rawValue: 1 <<  6)  // Нормальная температура
    static let rcFlgTemperatureLow    = RC_CHN_FLAGS(rawValue: 1 <<  7)  // Низкая температура
    static let rcFlgEmpty             = RC_CHN_FLAGS(rawValue: 1 <<  8)  // Флаг не используется!!!
    static let rcFlgScheduleRun       = RC_CHN_FLAGS(rawValue: 1 <<  9)  // Выполняется измерение по расписанию
    static let rcFlgUserRun           = RC_CHN_FLAGS(rawValue: 1 << 10)  // Выполняется измерение, запущенное пользователем
    static let rcFlgMotion            = RC_CHN_FLAGS(rawValue: 1 << 11)  // Прибор движется (в смысле не покоится)
    static let rcFlg_DB_Write         = RC_CHN_FLAGS(rawValue: 1 << 12)  // Результат текущего измерения должен попасть в базу данных
    static let rcFlgPowerUSB          = RC_CHN_FLAGS(rawValue: 1 << 13)  // Питание от внешнего источника, подключеного к USB разъему
    static let rcFlgPowerCharged      = RC_CHN_FLAGS(rawValue: 1 << 14)  // Заряд аккумулятора от внешнего источника
    static let rcFlgOldData           = RC_CHN_FLAGS(rawValue: 1 << 15)  // Старые данные из EEPROM-буфера

    // Комбинированные флаги
    static let cFlgDoseRateAlarmOutOfData: RC_CHN_FLAGS = [.rcFlgDoseRateAlarmLev1, .rcFlgDoseRateAlarmLev2]    // Зашкаливание по мощности дозы
    static let cFlgDoseAlarmOutOfData    : RC_CHN_FLAGS = [.rcFlgDoseAlarmLev1,     .rcFlgDoseAlarmLev2    ]    // Зашкаливание по дозе
    static let rcFlgTemperatureHot       : RC_CHN_FLAGS = [.rcFlgTemperatureNorm,   .rcFlgTemperatureLow   ]    // Высокая температура
    static let rcFlgPowerTooLow          : RC_CHN_FLAGS = [.rcFlgPowerUSB,          .rcFlgPowerCharged     ]    // Аккумулятор сильно разряжен

}



enum Unit: Int {
    case  r = 0 // Рентгены
    case sv = 1 // Зиверты
}

enum AlarmMode: Int {
    case many = 0
    case one  = 1
}

enum DisplayMode: Int {
    case auto   = 0
    case normal = 1
    case rotate = 2
}

enum LightMode: Int {
    case off  = 0
    case ena  = 1
    case auto = 3
}

enum OffTime: UInt32 {
    case sec05  = 0
    case sec10 = 1
    case sec15 = 2
    case sec30 = 3
}
