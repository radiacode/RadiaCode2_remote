//
//  RadiaCodeOptions.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.06.2022.
//

import Foundation
import SwiftUI
import Combine

final class  RadiaCodeOptions: ObservableObject {
    
    var options: HardOptions?
    
    var cancellableInt           : Cancellable?     // Издатель целочисленных данных
    
    init(options: HardOptions) {
        self.options = options
        self.cancellableInt = intPublisher()// Издатель целочисленного значения парсинга
    }

    func intPublisher() -> Cancellable {            // Издатель целочисленного значения парсинга
        return options!.pars.intPublisher
            .receive(on: DispatchQueue.main)
            .sink { [self] value in
                print(options!.curCommand)
                switch options!.curCommand {
                    case .RC_VSFR_RAW_FILTER :
                        RAW_FILTER = UInt32(value)
                    default: break
                }
            }
    }

    /// Текстовое значение усреднения "серой" RAW зоны
    @Published var RAW_FILTERStr = ""
//    { // Пустая строка при старте приложения
//        didSet {                        // При изменении строки либо запись из прибора, либо запись пользователем в окне настроек
//            if let curVal = UInt32(RAW_FILTERStr) {             // Если строку удаётся превратить в целое
//                if !oldValue.isEmpty {                          // и предыдущая строка не пустая
//                    if curVal >= 0 && curVal <= 30 {            // и полученное целое находится в диапазоне 0-30
//                        if  RAW_FILTER != nil {                 // и предыдущее числовое значение не пустое (мы не в момент старта приложения)
//                            RAW_FILTER  = UInt32(RAW_FILTERStr) // Записываем целую интерпретацию строки в числовую ячейку
//                        }
//                    }
//                }
//            }
////            else {                                            // Если конвертировать в целое не удаётся
////                                                                // TODO: Проверить сей факт…
////                RAW_FILTERStr = oldValue                        // оставляем значение строки старым
////            }
//        }
//    }
    
    /// RW, Опция - глубина фильтрации значений, выводимых в серой зоне графиков (Ширина окна усреднения оперативных данных при отображении на графиках)
    @Published var RAW_FILTER: UInt32? {
        didSet {
            if oldValue == nil {                                        // Если мы в моменте считывания опций из прибора при старте приложения…
                RAW_FILTERStr = String(UInt32(RAW_FILTER ?? 0))         // Записываем в строку для окна Опций текстовое значение значения
            } else {                                                    // Если произошло изменение значения пользователем…
                print("\nШирина окна усреднения: \(   RAW_FILTER!)")
                options!.comp.compose_RC_VSFR_RAW_FILTER(rate: RAW_FILTER!)      // Отправляем изменённое значение в прибор
            }
        }
    }


}
