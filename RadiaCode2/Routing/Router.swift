//
//  Router.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 11.12.2022.
//

import SwiftUI

final class Router: ObservableObject {
    
    static let shared = Router()
    
    @Published var path = [Route]()
    
//    func showDevices () { path.append(.devices)  }
    func showOptions () { path.append(.options)  }
//    func showLogs    () { path.append(.logs)     }
//    func showDiagrams() { path.append(.diagrams) }
//    func showMaps    () { path.append(.maps)     }
    func backToRoot  () { path.removeAll ()      }
    func back        () { path.removeLast()      }
}

