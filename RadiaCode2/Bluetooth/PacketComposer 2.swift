//
//  PacketComposer.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import Foundation
import CoreBluetooth

class PacketComposer {
    
    var ble: BLEConnection?
    var bytesArray:[UInt8] = []
    
    init(_ ble: BLEConnection) { self.ble = ble }
    
    func compose_SET_EXCHANGE()  {      print("\nПосылаемая комманда: SET_EXCHANGE")                       // DEBUG: отладочный вывод
        bytesArray  = toByteArray(SET_EXCHANGE)                             // Команда
        bytesArray += toByteArray(HOST_ID)                                  // Кому
        bytesArray  = toByteArray(UInt32(bytesArray.count)) + bytesArray    // В начало длину того, что получилось
        ble!.writeCommandBuffer(bytesArray)                                 // Запись в устройство по BLE
    }

    func compose_VS_DATA_BUF() {        print("\nПосылаемая комманда: RD_VIRT_STRING VS_DATA_BUF")          // DEBUG: отладочный вывод
        SFR_ID = VS_DATA_BUF
        bytesArray = toByteArray(RD_VIRT_STRING) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_SET_LOCAL_TIME()  {    print("\nПосылаемая комманда: SET_LOCAL_TIME")                      // DEBUG: отладочный вывод
        bytesArray  = toByteArray(SET_LOCAL_TIME)                           // Команда
        bytesArray += toByteArray(BCD_DATE())                               // Данные даты
        bytesArray += toByteArray(BCD_TIME())                               // Данные времени
        bytesArray  = toByteArray(UInt32(bytesArray.count)) + bytesArray    // В начало длину того, что получилось
        ble!.writeCommandBuffer(bytesArray)                                 // Запись в устройство по BLE
    }
    
    func compose_VSFR_DEVICE_TIME() {   print("\nПосылаемая комманда: WR_VIRT_SFR VSFR_DEVICE_TIME")        // DEBUG: отладочный вывод
        SFR_ID = VSFR_DEVICE_TIME                                           // Запоминаем текущую параметр записи
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(UInt32(0)) // Обнуляем счётчик
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray    // В начало длину того, что получилось
        
        for element in bytesArray {     print(String(format:"%02X ", element), terminator: "") }; print()   // DEBUG: отладочный вывод

        ble!.writeCommandBuffer(bytesArray)                                 // Запись в устройство по BLE
    }
    
    func compose_VS_CONFIGURATION() {   print("\nПосылаемая комманда: RD_VIRT_STRING VS_CONFIGURATION")     // DEBUG: отладочный вывод
        SFR_ID = VS_CONFIGURATION
        bytesArray = toByteArray(RD_VIRT_STRING) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray     // В начало длину того, что получилось
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_DISP_OFF_TIME() {
        print("\nПосылаемая комманда: VSFR_DISP_OFF_TIME")
        SFR_ID = VSFR_DISP_OFF_TIME
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_VSFR_DISP_OFF_TIME(offTime: UInt32) {
        print("\nПосылаемая комманда: VSFR_DISP_OFF_TIME запись")
        SFR_ID = VSFR_DISP_OFF_TIME
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(offTime)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_VSFR_SOUND_CTRL() {
        print("\nПосылаемая комманда: VSFR_SOUND_CTRL")
        SFR_ID = VSFR_SOUND_CTRL
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_SOUND_CTRL(soundOptions: UInt8) {
        print("\nПосылаемая комманда: VSFR_SOUND_CTRL запись")
        SFR_ID = VSFR_SOUND_CTRL
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(UInt32(soundOptions))
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_VIBRO_CTRL() {
        print("\nПосылаемая комманда: VSFR_VIBRO_CTRL")
        SFR_ID = VSFR_VIBRO_CTRL
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_VIBRO_CTRL(vibroOptions: UInt8) {
        print("\nПосылаемая комманда: VSFR_VIBRO_CTRL запись")
        SFR_ID = VSFR_VIBRO_CTRL
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(UInt32(vibroOptions))
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_DISP_BRT() {
        print("\nПосылаемая комманда: VSFR_DISP_BRT")
        SFR_ID = VSFR_DISP_BRT
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_DISP_BRT(brightness: UInt32) {
        print("\nПосылаемая комманда: VSFR_DISP_BRT запись")
        SFR_ID = VSFR_DISP_BRT
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(brightness)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_DEVICE_LEV1_uR_h(rate: UInt32) {
        print("\nПосылаемая комманда: VSFR_DEVICE_LEV1_uR_h запись")
        SFR_ID = RC_VSFR_DR_LEV1_uR_h
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(rate)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_DEVICE_LEV2_uR_h(rate: UInt32) {
        print("\nПосылаемая комманда: VSFR_DEVICE_LEV2_uR_h запись")
        SFR_ID = RC_VSFR_DR_LEV2_uR_h
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(rate)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_RC_VSFR_DS_LEV1_100uR(rate: UInt32) {
        print("\nПосылаемая комманда: RC_VSFR_DS_LEV1_100uR запись")
        SFR_ID = RC_VSFR_DS_LEV1_100uR
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(rate)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_RC_VSFR_DS_LEV2_100uR(rate: UInt32) {
        print("\nПосылаемая комманда: RC_VSFR_DS_LEV2_100uR запись")
        SFR_ID = RC_VSFR_DS_LEV2_100uR
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(rate)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_VSFR_DISP_CTRL(displayOptions: UInt8) {
        print("\nПосылаемая комманда: VSFR_DISP_CTRL запись")
        printBin(UInt32(displayOptions))
        SFR_ID = VSFR_DISP_CTRL
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(UInt32(displayOptions))
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_VSFR_DEVICE_LANG() {
        print("\nПосылаемая комманда: VSFR_DEVICE_LANG")
        SFR_ID = VSFR_DEVICE_LANG
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_DEVICE_LANG(lang: Lang) {
        print("\nПосылаемая комманда: VSFR_DEVICE_LANG запись")
        SFR_ID = VSFR_DEVICE_LANG
        switch lang {
            case  .rus:    RC_DEVICE_LANG = 0
            case  .eng:    RC_DEVICE_LANG = 1
        }
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(RC_DEVICE_LANG)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

}
