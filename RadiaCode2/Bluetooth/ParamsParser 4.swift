//
//  ParamsParser.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import Foundation
import CoreData

class ParamsParser: NSObject {
    
    private var paramStrArray       : [String]?  // Параметры конфигурации прибора
            var managedObjectContext: NSManagedObjectContext?
            let viewContext = PersistenceController.shared.container.viewContext
            var positionCHN: Int16 = 0 // Порядок расположения каналов в потоке данных

    // TODO: Ужесточить проверку: скобки с обоих сторон только вначале/в конце; игнорировать заглавные при названии параметров
    // TODO: Делать анализ секции "[DeviceParams]" (пока var 'isFindDataStructureStr = false' закомментировано)
    // TODO: В случае ошибок чтения выдать сообщение об ошибке и завершить приложение
    func parsingToCoreData(params: String) {
        
//      var isFindDataStructureStr = false
        var isFindDataGroup_Str     = false
        var isFindDataChanelStr    = false

        var cur_GRP_name           = ""
        var new_GRP_Item: DataStrucktureGRP?
        var new_CHN_Item: DataStrucktureCHN?

        paramStrArray = params.components(separatedBy: ["\n"])              // Разбиваем единую строку на массив строк по EOL ('\n')
//        print(paramStrArray ?? ["NO"])                                      // DEBUG: для отладки
        
        DataStrucktureGRP.deleteAllData(in: viewContext)                    // Удаляем все данные групп
        DataStrucktureCHN.deleteAllData(in: viewContext)                    // Удаляем все данные каналов

        for paramStr in paramStrArray! {
            
            
// TODO: Задействовать позже
//            if paramStr == dataStructureStr { // Найден раздел [DataStructure]
//                isFindDataStructureStr = true
//            }
            
            // Обработка заголовков групп
            if paramStr.contains("[[") && !paramStr.contains("[[[") {           // Найдена группа типа "[[GRP_CountRate]]"
                isFindDataChanelStr    = false                                  // Если ранее мы работали с предыдущим каналом
                isFindDataGroup_Str     = true                                  // Запоминаем факт нахождения группы для использования ID в следующей итерации
                cur_GRP_name = clearSquareBrackets(paramStr)                    // Очищаем название от квадратных скобок
                new_GRP_Item = DataStrucktureGRP(context: viewContext)
                new_GRP_Item!.name = cur_GRP_name
            } else if isFindDataGroup_Str {                                     // Если далее ID группы
                let paramArr = paramStr.components(separatedBy: "=")            // Разделяем "Id=3" на "Id" и номер "3" группы
                if        paramArr[0] == "Id" {                                 // Лишний раз проверяем, что это ID
                    new_GRP_Item!.id   = Int16(paramArr[1]) ?? -1
                } else if paramArr[0] == "FlgMsk" {                             // Если далее флаги группы
                    let msk = hexStringToInt32(paramArr[1])
                    new_GRP_Item!.flg_msk     =  msk ?? -1 // FIXME: возможно здесь не стоит ставить -1
                }
                do {
                    try viewContext.save()
                } catch {
                    let nsError = error as NSError
                    fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                }
            }
            
            // Обработка каналов
            if paramStr.contains("[[[") {                                   // Найден канал типа "[[[CHN_CountRate]]]"
                isFindDataChanelStr = true                                  // Запоминаем факт нового канала для использования в следующих итерациях
                isFindDataGroup_Str = false                                 // Отключаем факт нахождения группы до нахождения следующей
                positionCHN += 1                                            // Присвоение уникального ID для однозначной сортировки
                let cur_CHN_name = clearSquareBrackets(paramStr)            // Очищаем название от всех квадратных скобок
                    new_CHN_Item = DataStrucktureCHN(context: viewContext)
                    new_CHN_Item!.grp      = new_GRP_Item!
                    new_CHN_Item!.name     = cur_CHN_name
                    new_CHN_Item!.position = positionCHN
                
                PersistenceController.shared.saveContext()

            } else if isFindDataChanelStr {
                let paramArr = paramStr.components(separatedBy: "=")        // Разделяем строку типа "Id=3" на название и номер значение
                switch paramArr[0] {
                    case "Id"        : new_CHN_Item!.id          =  Int16(paramArr[1]) ?? -1
                    case "Sensor"    : new_CHN_Item!.sensor      =        paramArr[1]
                    case "Unit"      : new_CHN_Item!.unit        =        paramArr[1]
                    case "DType"     : new_CHN_Item!.d_type      =  Int16(paramArr[1]) ?? -1
                    case "RType"     : new_CHN_Item!.r_type      =  Int16(paramArr[1]) ?? -1
                    case "Expr"      : new_CHN_Item!.expr        =  Int16(paramArr[1]) ?? -1
                    case "ScaledUnit": new_CHN_Item!.scaled_unit =  Int16(paramArr[1]) ?? -1
                    case "MaxVal"    : if let curMaxVal          = Double(paramArr[1]) { new_CHN_Item!.max_val = curMaxVal }
                    case "MinVal"    : if let curMinVal          = Double(paramArr[1]) { new_CHN_Item!.min_val = curMinVal }
                    case "P1"        : if let param              = Double(paramArr[1]) { new_CHN_Item!.p1      = param }
                    case "P2"        : if let param              = Double(paramArr[1]) { new_CHN_Item!.p2      = param }
                    default: continue // TODO: придумать "отстойник" для нераспознанных значений (из новых/старых прошивок)
                }
                PersistenceController.shared.saveContext()
            }
        }
    }
    
    func clearSquareBrackets(_ str: String) -> String {
        var    curStr = str
               curStr = curStr.replacingOccurrences(of: "[", with: "")
               curStr = curStr.replacingOccurrences(of: "]", with: "")
        return curStr
    }

}
