//
//  PacketComposer.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import Foundation
import CoreBluetooth

class PacketComposer {
    
    var ble: BLEConnection?
    var bytesArray:[UInt8] = []
    
    init(_ ble: BLEConnection) { self.ble = ble }
    
    func compose_SET_EXCHANGE()  {      print("\nПосылаемая команда: SET_EXCHANGE")                       // DEBUG: отладочный вывод
        bytesArray  = toByteArray(SET_EXCHANGE)                             // Команда
        bytesArray += toByteArray(HOST_ID)                                  // Кому
        bytesArray  = toByteArray(UInt32(bytesArray.count)) + bytesArray    // В начало длину того, что получилось
        ble!.writeCommandBuffer(bytesArray)                                 // Запись в устройство по BLE
    }

    func compose_VS_DATA_BUF() {        print("\nПосылаемая команда: RD_VIRT_STRING VS_DATA_BUF")          // DEBUG: отладочный вывод
        SFR_ID = VS_DATA_BUF
        bytesArray = toByteArray(RD_VIRT_STRING) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_SET_LOCAL_TIME()  {    print("\nПосылаемая команда: SET_LOCAL_TIME")                      // DEBUG: отладочный вывод
        bytesArray  = toByteArray(SET_LOCAL_TIME)                           // Команда
        bytesArray += toByteArray(BCD_DATE())                               // Данные даты
        bytesArray += toByteArray(BCD_TIME())                               // Данные времени
        bytesArray  = toByteArray(UInt32(bytesArray.count)) + bytesArray    // В начало длину того, что получилось
        ble!.writeCommandBuffer(bytesArray)                                 // Запись в устройство по BLE
    }
    
    func compose_VSFR_DEVICE_TIME() {   print("\nПосылаемая команда: WR_VIRT_SFR VSFR_DEVICE_TIME")        // DEBUG: отладочный вывод
        SFR_ID = VSFR_DEVICE_TIME                                           // Запоминаем текущую параметр записи
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(UInt32(0)) // Обнуляем счётчик
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray    // В начало длину того, что получилось
        
        for element in bytesArray {     print(String(format:"%02X ", element), terminator: "") }; print()   // DEBUG: отладочный вывод

        ble!.writeCommandBuffer(bytesArray)                                 // Запись в устройство по BLE
    }
    
    func compose_VS_CONFIGURATION() {   print("\nПосылаемая команда: RD_VIRT_STRING VS_CONFIGURATION")     // DEBUG: отладочный вывод
        SFR_ID = VS_CONFIGURATION
        bytesArray = toByteArray(RD_VIRT_STRING) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray     // В начало длину того, что получилось
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_DISP_OFF_TIME() {
        print("\nПосылаемая команда: VSFR_DISP_OFF_TIME")
        SFR_ID = VSFR_DISP_OFF_TIME
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_VSFR_DISP_OFF_TIME(offTime: UInt32) {
        print("\nПосылаемая команда: VSFR_DISP_OFF_TIME запись")
        SFR_ID = VSFR_DISP_OFF_TIME
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(offTime)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_VSFR_SOUND_CTRL() {
        print("\nПосылаемая команда: VSFR_SOUND_CTRL")
        SFR_ID = VSFR_SOUND_CTRL
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_VSFR_DEVICE_CTRL(deviceOptions: UInt32) {
        print("\nПосылаемая команда: VSFR_DEVICE_CTRL запись")
        SFR_ID = VSFR_DEVICE_CTRL
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(UInt32(deviceOptions))
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_VSFR_DEVICE_CTRL() {
        print("\nПосылаемая команда: VSFR_DEVICE_CTRL")
        SFR_ID = VSFR_DEVICE_CTRL
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_SOUND_CTRL(soundOptions: UInt32) {
        print("\nПосылаемая команда: VSFR_SOUND_CTRL запись")
        SFR_ID = VSFR_SOUND_CTRL
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(UInt32(soundOptions))
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_VIBRO_CTRL() {
        print("\nПосылаемая команда: VSFR_VIBRO_CTRL")
        SFR_ID = VSFR_VIBRO_CTRL
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_VIBRO_CTRL(vibroOptions: UInt32) {
        print("\nПосылаемая команда: VSFR_VIBRO_CTRL запись")
        SFR_ID = VSFR_VIBRO_CTRL
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(UInt32(vibroOptions))
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_RC_VSFR_DISP_CTRL() {
        print("\nПосылаемая команда: RC_VSFR_DISP_CTRL")
        SFR_ID = VSFR_DISP_CTRL
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_DISP_CTRL(displayOptions: UInt32) {
        print("\nПосылаемая команда: VSFR_DISP_CTRL запись")
        printBin(UInt32(displayOptions))
        SFR_ID = VSFR_DISP_CTRL
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(UInt32(displayOptions))
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_DISP_BRT() {
        print("\nПосылаемая команда: VSFR_DISP_BRT")
        SFR_ID = VSFR_DISP_BRT
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_DISP_BRT(brightness: UInt32) {
        print("\nПосылаемая команда: VSFR_DISP_BRT запись")
        SFR_ID = VSFR_DISP_BRT
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(brightness)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_VSFR_DEVICE_LANG() {                       print("\nПосылаемая команда: VSFR_DEVICE_LANG")
        SFR_ID = VSFR_DEVICE_LANG
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_DEVICE_LANG(lang: Lang) {             print("\nПосылаемая команда: VSFR_DEVICE_LANG запись")
        SFR_ID = VSFR_DEVICE_LANG
        RC_DEVICE_LANG = UInt32(lang.rawValue)
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(RC_DEVICE_LANG)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_RC_VSFR_DS_UNITS() {                       print("\nПосылаемая команда: VSFR_DEVICE_UNITS")
        SFR_ID = RC_VSFR_DS_UNITS
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_DEVICE_UNIT(unit: Unit) {             print("\nПосылаемая команда: VSFR_DEVICE_UNITS запись")
        SFR_ID = RC_VSFR_DS_UNITS
        RC_UNITS = UInt32(unit.rawValue)
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(RC_UNITS)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_ALARM_MODE() {                       print("\nПосылаемая команда: VSFR_ALARM_MODE")
        SFR_ID = VSFR_ALARM_MODE
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_ALARM_MODE(alarm: AlarmMode) {       print("\nПосылаемая команда: VSFR_ALARM_MODE запись")
        SFR_ID = VSFR_ALARM_MODE
        RC_UNITS = UInt32(alarm.rawValue)
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(RC_UNITS)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_VSFR_DEVICE_LEV1_uR_h(rate: UInt32) {
        print("\nПосылаемая команда: VSFR_DEVICE_LEV1_uR_h запись")
        SFR_ID = RC_VSFR_DR_LEV1_uR_h
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(rate)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_VSFR_DEVICE_LEV2_uR_h(rate: UInt32) {
        print("\nПосылаемая команда: VSFR_DEVICE_LEV2_uR_h запись")
        SFR_ID = RC_VSFR_DR_LEV2_uR_h
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(rate)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_RC_VSFR_DS_LEV1_100uR(rate: UInt32) {  print("\nПосылаемая команда: RC_VSFR_DS_LEV1_100uR запись")
        SFR_ID = RC_VSFR_DS_LEV1_100uR
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(rate)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_RC_VSFR_DS_LEV2_100uR(rate: UInt32) {  print("\nПосылаемая команда: RC_VSFR_DS_LEV2_100uR запись")
        SFR_ID = RC_VSFR_DS_LEV2_100uR
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(rate)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_RC_VSFR_DR_LEV1_uR_h() {                 print("\nПосылаемая команда: RC_VSFR_DR_LEV1_uR_h")
        SFR_ID = RC_VSFR_DR_LEV1_uR_h
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_RC_VSFR_DR_LEV2_uR_h() {                 print("\nПосылаемая команда: RC_VSFR_DR_LEV2_uR_h")
        SFR_ID = RC_VSFR_DR_LEV2_uR_h
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_RC_VSFR_DS_LEV1_100uR() {                print("\nПосылаемая команда: RC_VSFR_DS_LEV1_100uR")
        SFR_ID = RC_VSFR_DS_LEV1_100uR
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_RC_VSFR_DS_LEV2_100uR() {                print("\nПосылаемая команда: RC_VSFR_DS_LEV2_100uR")
        SFR_ID = RC_VSFR_DS_LEV2_100uR
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }

    func compose_RC_VSFR_RAW_FILTER(rate: UInt32) {     print("\nПосылаемая команда: RC_VSFR_RAW_FILTER  запись")
        SFR_ID = RC_VSFR_RAW_FILTER
        bytesArray = toByteArray(WR_VIRT_SFR) + toByteArray(SFR_ID!) + toByteArray(rate)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }
    
    func compose_RC_VSFR_RAW_FILTER() {                print("\nПосылаемая команда: RC_VSFR_RAW_FILTER")
        SFR_ID = RC_VSFR_RAW_FILTER
        bytesArray = toByteArray(RD_VIRT_SFR) + toByteArray(SFR_ID!)
        bytesArray = toByteArray(UInt32(bytesArray.count)) + bytesArray
        ble!.writeCommandBuffer(bytesArray)
    }


}
