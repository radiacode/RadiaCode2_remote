//
//  PacketParser.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import Foundation
import Combine

class PacketParser: ObservableObject {
    
            var prms    : ParamsParser
            var baseDate: Date?
    
            var isPacketContinue = false
    private var command: UInt32 = 0
    private var  commandArray : [UInt8] = []
    private var     dataArray : [UInt8] = []
    
    /// Массив для парсинга `DATA_BUF`
    private var  decodeArray : [UInt8] = []
    
    /// Массив масок для определения каналов, используемых после расшифровки канала  _RC_CHN_EVENT_CHN_MSK_
    private var maskBoolArray : [Bool]  = []
    
    /// Индикатор факта обнаружения дальнейшего считывания каналов согласно маске _RC_CHN_EVENT_CHN_MSK_
            var flagStartReadChanelByMask = true
    
   /// Счётчик считывания каналов согласно  маске _RC_CHN_EVENT_CHN_MSK_ ( пока не будет присвоено значение  0 в save_RC_CHN_FLAGS)
            var maskChannelCount: Int? = nil
    
            var isValidData  = true         // По умолчанию верны, по мере проверок может оказаться - нет
            var commandName = String(localized: "NOT DEFINDED")
    private var totalLength = 0 // Общая длина склееваемого пакета
    private var lastLength = 0
    private var frameLastNum : UInt8?
    private let viewContext = PersistenceController.shared.container.viewContext
    
    /// Кол-во интервалов времени `FrameTimeStep_us` между соседними отсчетами (период выборки).
    /// Нужно для определения времени каждого измерения при наличии во фрейме нескольких измерений `RC_FRM_MEAS`
    private var smplTime_ms: Int32 = 0
    
    /// `OffsTime_ms` Количество милисекунд от базового времени до данного отсчета
    private var cur_ms_Time: Int = 0
    
    // MARK: Переменные каналов
    
    private var timestump: Date?
    private var uint_16RC_CHN_Flags         : UInt16?
    private var float32RC_CHN_DOSE_RATE     : Float32?
    private var float32RC_CHN_DOSE_RATE_ERR : Float32?
    private var float32RC_CHN_COUNT_RATE    : Float32?
    private var float32RC_CHN_COUNT_RATE_ERR: Float32?
    private var uint_32RC_CHN_COUNT         : UInt32?
    private var uint_32RC_CHN_DURATION      : UInt32?
    private var float32RC_CHN_DOSE          : Float32?
    private var float32RC_CHN_TEMPERATURE   : Float32?
    private var float32RC_CHN_BTRY_CHRG     : Float32?
    private var uint_32RC_CHN_EVENT         : UInt32?

    // Сущности (entity) для сохранения в Core Data
    var new_log     : Log?
    var logRateDB   : LogRateDB?
    var logRareData : LogRareData?
    var logEvent    : Event?

    // MARK: - Использование Combine
    
    // Передача полученных данных дозы
    private      let doseRateSubj =     PassthroughSubject<RC_GRP_DOSE_RATE, Never>()
                 var doseRatePublisher  : AnyPublisher<RC_GRP_DOSE_RATE, Never> { doseRateSubj.eraseToAnyPublisher() }
    private(set) var dose_RatePublished  :              RC_GRP_DOSE_RATE?  {
        didSet { doseRateSubj.send(dose_RatePublished!) }
    }
    
    // Передача последнего полученного значения мощности дозы
    private      let lastDoseRateSubj =     PassthroughSubject<Double, Never>()
                 var lastDoseRatePublisher  : AnyPublisher<Double, Never> { lastDoseRateSubj.eraseToAnyPublisher() }
    private(set) var lastDose_RatePublished  :              Double?  {
        didSet { lastDoseRateSubj.send(lastDose_RatePublished!) }
    }
    
    // Передача последнего полученного значения средней ошибки мощности дозы
    private      let lastDoseRateErrSubj =     PassthroughSubject<Double, Never>()
                 var lastDoseRateErrPublisher  : AnyPublisher<Double, Never> { lastDoseRateErrSubj.eraseToAnyPublisher() }
    private(set) var lastDose_RateErrPublished  :              Double?  {
        didSet { lastDoseRateErrSubj.send(lastDose_RateErrPublished!) }
    }

    // Передача полученных данных импульсов
    private      let countSubj = PassthroughSubject<CountRateStruct, Never>()
                 var countPublisher  : AnyPublisher<CountRateStruct, Never> { countSubj.eraseToAnyPublisher() }
    private(set) var countRatePublished                    : CountRateStruct?  {
        didSet { countSubj.send(countRatePublished!) }
    }
    
    // Передача полученных данных импульсов
    private      let lastCountSubj = PassthroughSubject<Double, Never>()
                 var lastCountPublisher  : AnyPublisher<Double, Never> { lastCountSubj.eraseToAnyPublisher() }
    private(set) var lastCountRatePublished                    : Double?  {
        didSet { lastCountSubj.send(lastCountRatePublished!) }
    }
    
    // Передача полученных данных средней ошибки
    private      let lastCountErrSubj = PassthroughSubject<Double, Never>()
                 var lastCountErrPublisher  : AnyPublisher<Double, Never> { lastCountErrSubj.eraseToAnyPublisher() }
    private(set) var lastCountErrRatePublished                    : Double?  {
        didSet { lastCountErrSubj.send(lastCountErrRatePublished!) }
    }

    // Передача полученных сырых данных дозы
    private      let dataRawSubj = PassthroughSubject<RC_GRP_DOSE_RATE, Never>()
                 var doseRawRatePublisher  : AnyPublisher<RC_GRP_DOSE_RATE, Never> { dataRawSubj.eraseToAnyPublisher() }
    private(set) var dose_RawRatePublished                    : RC_GRP_DOSE_RATE?  {
        didSet { dataRawSubj.send(dose_RawRatePublished!) }
    }

    // Передача полученных сырых данных импульсов
    private      let countRawSubj = PassthroughSubject<CountRateStruct, Never>()
                 var countRawPublisher  : AnyPublisher<CountRateStruct, Never> { countRawSubj.eraseToAnyPublisher() }
    private(set) var countRawRatePublished                    : CountRateStruct?  {
        didSet { countRawSubj.send(countRawRatePublished!) }
    }

    // Вызов команды
    private      let сomSubj = PassthroughSubject<COMMAND, Never>()
                 var comPublisher :  AnyPublisher<COMMAND, Never> { сomSubj.eraseToAnyPublisher() }
    private(set) var enumCommandPublished  :               COMMAND?  {
        didSet { сomSubj.send(enumCommandPublished!) }
    }
    
    // Счётчик групп
    private      let groupCountSubj = PassthroughSubject<Int, Never>()
                 var groupCountPublisher :  AnyPublisher<Int, Never> { groupCountSubj.eraseToAnyPublisher() }
    private(set) var groupCountPublished  :               Int?  {
        didSet { groupCountSubj.send(groupCountPublished!) }
    }
    
    // Передача целочисленных данных настроек прибора
    private      let intSubj = PassthroughSubject<Int, Never>()
                 var intPublisher :  AnyPublisher<Int, Never> { intSubj.eraseToAnyPublisher() }
    private(set) var intPublished  :               Int?  {
        didSet { intSubj.send(intPublished!) }
    }


    // MARK: - init/func
    
    init(_ prms: ParamsParser) { self.prms = prms }


    /// Начало обработки команды
    func defineCommand(_ bytes : [UInt8]) {
        commandArray =   bytes
        
        if !isPacketContinue { // Мы в начале пакета
            self.command = intFrom(commandArray, start: 4, length: LENGTH_SIZE)
            
// DEBUG:
//            print("Начало пакета")
//            print("self.command = 0x\(String(format:"%02X ", self.command) )")
//            for element in commandArray { print(String(format:"%02X ", element), terminator: "") } // Печать в 16-тиричной форме
//            print(commandArray) // Печать в 10-тиричной форме
            
        } else {
//            print("продолжение пакета…")
//            for element in commandArray { print(String(format:"%02X ", element), terminator: "") } // Печать в 16-тиричной форме
//            print()
//            print(commandArray) // Печать в 10-тиричной форме
        }
        // FIXME: switch не работает в else, а должен быть там…
        switch self.command {
//          case      GET_STATUS: parser_GET_STATUS()
            case     RD_VIRT_SFR    : parser_SFR_ID()                           // Чтение виртуальных регистров
            case SET_EXCHANGE       : parser_SET_EXCHANGE()
            case WR_VIRT_SFR        : parser_SFR_ID_validation()                // Подтверждение команды записи
            case RD_VIRT_STRING     : parser_SFR_ID()
            case SET_LOCAL_TIME     : enumCommandPublished = .SET_LOCAL_TIME
            case RC_VSFR_RAW_FILTER : enumCommandPublished = .RC_VSFR_RAW_FILTER
            default                 : enumCommandPublished = .ERROR
        }
    }
    
    /// Чтение виртуальных регистров
    func parser_SFR_ID() {
        switch SFR_ID! {
        case  VSFR_DEVICE_CTRL      : parser_VSFR_DEVICE_CTRL()
        case  VSFR_DEVICE_LANG      : parser_VSFR_DEVICE_LANG()
        case  VSFR_DISP_CTRL        : parser_VSFR_DISP_CTRL()
        case  RC_VSFR_DS_UNITS      : parser_VSFR_DEVICE_UNITS()
        case  VSFR_ALARM_MODE       : parser_VSFR_ALARM_MODE()
        case  RC_VSFR_DR_LEV1_uR_h  : parser_RC_VSFR_DR_LEV1_uR_h()
        case  RC_VSFR_DR_LEV2_uR_h  : parser_RC_VSFR_DR_LEV2_uR_h()
        case  RC_VSFR_DS_LEV1_100uR : parser_RC_VSFR_DS_LEV1_100uR()
        case  RC_VSFR_DS_LEV2_100uR : parser_RC_VSFR_DS_LEV2_100uR()
        case  RC_VSFR_RAW_FILTER    : parser_RC_VSFR_RAW_FILTER()
        case  VSFR_SOUND_CTRL       : parser_VSFR_SOUND_CTRL()
        case  VSFR_VIBRO_CTRL       : parser_VSFR_VIBRO_CTRL()
        case  VSFR_DISP_OFF_TIME    : parser_VSFR_DISP_OFF_TIME()
        case  VSFR_DISP_BRT         : parser_VSFR_DISP_BRT()
        case  VS_CONFIGURATION      : parser_VS_CONFIGURATION()
        case  VS_DATA_BUF           : parser_VS_DATA_BUF()              // Получение данных в реальном времени
       default: isValidData = false
        }
    }

    /// Подтверждение команды записи
    func parser_SFR_ID_validation() {
        
        let  startVALID = LENGTH_SIZE + MemoryLayout.size(ofValue: WR_VIRT_SFR)
        VALID = intFrom(commandArray, start: startVALID, length: LENGTH_SIZE)
        switch SFR_ID! {
            case  VSFR_DEVICE_TIME: enumCommandPublished = (VALID == 1 ? .VSFR_DEVICE_TIME : .ERROR)
            default:                enumCommandPublished = .ERROR
        }
    }
    
    func parser_SET_EXCHANGE() { commandName =  "SET_EXCHANGE"
        
        let targetStart = LENGTH_SIZE + MemoryLayout.size(ofValue: SET_EXCHANGE)
        let  buff1Start = targetStart + MemoryLayout.size(ofValue: TARGET_ID)
        let  buff2Start = targetStart + MemoryLayout.size(ofValue: BUFFER_TX_SIZE)
        
        let commandLength: UInt32 = intFrom(commandArray, start: 0,           length: LENGTH_SIZE)
        let curTARGET_ID : UInt32 = intFrom(commandArray, start: targetStart, length: MemoryLayout.size(ofValue: TARGET_ID))
            BUFFER_TX_SIZE        = intFrom(commandArray, start:  buff1Start, length: MemoryLayout.size(ofValue: BUFFER_TX_SIZE))
            BUFFER_RX_SIZE        = intFrom(commandArray, start:  buff2Start, length: MemoryLayout.size(ofValue: BUFFER_RX_SIZE))
        
        print(              "Длина команды  = \(commandLength)")
        print(String(format:"TARGET_ID = %02X ", curTARGET_ID))
        print(              "BUFFER_TX_SIZE = \(BUFFER_TX_SIZE)\nBUFFER_RX_SIZE = \(BUFFER_RX_SIZE)")
        
        if  TARGET_ID != curTARGET_ID { print("Неправильный TARGET_ID"); return }
        
        enumCommandPublished = .SET_EXCHANGE // Отработанная команда
    }
    
    // MARK: Обработка опций прибора
    
    func parser_VSFR_DEVICE_CTRL() { commandName =  "VSFR_DEVICE_CTRL"
            VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
        if  VALID == 0  { print("Регистр НЕ существует!") }
        let VALUE: UInt32 = intFrom(commandArray, start: 12, length: LENGTH_SIZE)
        enumCommandPublished = .VSFR_DEVICE_CTRL // Отработанная команда
        intPublished = Int(VALUE) // Передаём значение после информации об выполении команды
    }

    /// Установка языка прибора
    func parser_VSFR_DEVICE_LANG() { commandName =  "VSFR_DEVICE_LANG"
            VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
        if  VALID == 0  { print("Регистр НЕ существует!") }
        let VALUE: UInt32 = intFrom(commandArray, start: 12, length: LENGTH_SIZE)
        enumCommandPublished = .VSFR_DEVICE_LANG // Отработанная команда
        intPublished = Int(VALUE) // Передаём значение после информации об выполении команды
    }
    
    func parser_VSFR_DEVICE_UNITS() { commandName =  "VSFR_DS_UNITS"
            VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
        if  VALID == 0  { print("Регистр НЕ существует!") }
        let VALUE: UInt32 = intFrom(commandArray, start: 12, length: LENGTH_SIZE)
        enumCommandPublished = .VSFR_DS_UNITS // Отработанная команда
        intPublished = Int(VALUE) // Передаём значение после информации об выполении команды
    }

    func parser_VSFR_ALARM_MODE() { commandName =  "VSFR_ALARM_MODE"
            VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
        if  VALID == 0  { print("Регистр НЕ существует!") }
        let VALUE: UInt32 = intFrom(commandArray, start: 12, length: LENGTH_SIZE)
        enumCommandPublished = .VSFR_ALARM_MODE // Отработанная команда
        intPublished = Int(VALUE) // Передаём значение после информации об выполении команды
    }

    func parser_VSFR_SOUND_CTRL() { commandName =  "VSFR_SOUND_CTRL"
            VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
        if  VALID == 0  { print("Регистр НЕ существует!") }
        let VALUE: UInt32 = intFrom(commandArray, start: 12, length: LENGTH_SIZE)
        enumCommandPublished = .VSFR_SOUND_CTRL // Отработанная команда
        intPublished = Int(VALUE) // Передаём значение после информации об выполении команды
    }

    func parser_VSFR_VIBRO_CTRL() { commandName =  "VSFR_VIBRO_CTRL"
            VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
        if  VALID == 0  { print("Регистр НЕ существует!") }
        let VALUE: UInt32 = intFrom(commandArray, start: 12, length: LENGTH_SIZE)
        enumCommandPublished = .VSFR_VIBRO_CTRL // Отработанная команда
        intPublished = Int(VALUE) // Передаём значение после информации об выполении команды
    }

    func parser_VSFR_DISP_CTRL() { commandName =  "VSFR_DISP_CTRL"
            VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
        if  VALID == 0  { print("Регистр НЕ существует!") }
        let VALUE: UInt32 = intFrom(commandArray, start: 12, length: LENGTH_SIZE)
        enumCommandPublished = .VSFR_DISP_CTRL // Отработанная команда
        intPublished = Int(VALUE) // Передаём значение после информации об выполении команды
    }

    func parser_VSFR_DISP_OFF_TIME() { commandName =  "VSFR_DISP_OFF_TIME"
            VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
        if  VALID == 0  { print("Регистр НЕ существует!") }
        let VALUE: UInt32 = intFrom(commandArray, start: 12, length: LENGTH_SIZE)
        enumCommandPublished = .VSFR_DISP_OFF_TIME // Отработанная команда
        intPublished = Int(VALUE) // Передаём значение после информации об выполении команды
    }

    func parser_VSFR_DISP_BRT() { commandName =  "VSFR_DISP_BRT"
            VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
        if  VALID == 0  { print("Регистр НЕ существует!") }
        let VALUE: UInt32 = intFrom(commandArray, start: 12, length: LENGTH_SIZE)
        enumCommandPublished = .VSFR_DISP_BRT // Отработанная команда
        intPublished = Int(VALUE) // Передаём значение после информации об выполении команды
    }
    
    func parser_RC_VSFR_DR_LEV1_uR_h() { commandName =  "RC_VSFR_DR_LEV1_uR_h"
            VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
        if  VALID == 0  { print("Регистр НЕ существует!") }
        let VALUE: UInt32 = intFrom(commandArray, start: 12, length: LENGTH_SIZE)
        enumCommandPublished = .RC_VSFR_DR_LEV1_uR_h // Отработанная команда
        intPublished = Int(VALUE) // Передаём значение после информации об выполении команды
        print("\nRC_VSFR_DR_LEV1_uR_h = \(VALUE)")
    }
    
    func parser_RC_VSFR_DR_LEV2_uR_h() { commandName =  "RC_VSFR_DR_LEV2_uR_h"
            VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
        if  VALID == 0  { print("Регистр НЕ существует!") }
        let VALUE: UInt32 = intFrom(commandArray, start: 12, length: LENGTH_SIZE)
        enumCommandPublished = .RC_VSFR_DR_LEV2_uR_h // Отработанная команда
        intPublished = Int(VALUE) // Передаём значение после информации об выполении команды
        print("\nRC_VSFR_DR_LEV2_uR_h = \(VALUE)")
    }

    func parser_RC_VSFR_DS_LEV1_100uR() { commandName =  "RC_VSFR_DS_LEV1_100uR"
            VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
        if  VALID == 0  { print("Регистр НЕ существует!") }
        let VALUE: UInt32 = intFrom(commandArray, start: 12, length: LENGTH_SIZE)
        enumCommandPublished = .RC_VSFR_DS_LEV1_100uR // Отработанная команда
        intPublished = Int(VALUE) // Передаём значение после информации об выполении команды
        print("\nRC_VSFR_DS_LEV1_100uR = \(VALUE)")
    }

    func parser_RC_VSFR_DS_LEV2_100uR() { commandName =  "RC_VSFR_DS_LEV2_100uR"
            VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
        if  VALID == 0  { print("Регистр НЕ существует!") }
        let VALUE: UInt32 = intFrom(commandArray, start: 12, length: LENGTH_SIZE)
        enumCommandPublished = .RC_VSFR_DS_LEV2_100uR // Отработанная команда
        intPublished = Int(VALUE) // Передаём значение после информации об выполении команды
        print("\nRC_VSFR_DS_LEV2_100uR = \(VALUE)")
    }

    func parser_RC_VSFR_RAW_FILTER() { commandName =  "RC_VSFR_RAW_FILTER"
            VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
        if  VALID == 0  { print("Регистр НЕ существует!") }
        let VALUE: UInt32 = intFrom(commandArray, start: 12, length: LENGTH_SIZE)
        enumCommandPublished = .RC_VSFR_RAW_FILTER // Отработанная команда
        intPublished = Int(VALUE) // Передаём значение после информации об выполении команды
        print("\nRC_VSFR_RAW_FILTER = \(VALUE)")
    }
    
    // MARK: Обработка конфигурации
    // текстовая строка типа [[GRP_CountRate]] для декодировки приходящих данных

    /// После получения результатов команды  *VS_CONFIGURATION*
    func parser_VS_CONFIGURATION() { commandName =  "VS_CONFIGURATION"
        if !isPacketContinue { isPacketContinue = true // Раз мы оказались здесь сразу метим начало пакета
                VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
            if  VALID == 0  { print("Регистр НЕ существует!") }
            let STRING_LEN: UInt32 = intFrom(commandArray, start: 8 + 4, length: LENGTH_SIZE) // Длина всего пакета данных
            totalLength = Int(STRING_LEN)
            dataArray = commandArray
            dataArray.removeFirst(8 + 4 + 4) // Удаляем коммандную часть первой части пакета
            lastLength = totalLength - dataArray.count // Полученная длина данных
            
// DEBUG: Отладочная информация
//            print("Получены данные длиной: \(STRING_LEN) байт")
//            print(commandArray)
//            print("Извлечённые данные:")
//            for element in dataArray { print(String(format:"%02X ", element), terminator: "") }
//            print()
//            let byteStr = String(decoding: dataArray, as: UTF8.self)
//            print(byteStr)
            
        } else {
            dataArray = dataArray + commandArray
            lastLength = totalLength - dataArray.count // Оставшаяся часть данных
            if lastLength <= 1 { isPacketContinue = false
                let byteStr = String(data: Data(dataArray), encoding: .windowsCP1251)
                prms.parsingToCoreData(params: byteStr ?? "")
                print("Полный пакет:")
                print(byteStr!)
                enumCommandPublished = .VS_CONFIGURATION // Отработанная команда
            }
        }
    }

    // MARK: Обработка VS_DATA_BUF
    /// Получение данных в реальном времени
    func parser_VS_DATA_BUF() { commandName =  "VS_DATA_BUF"
        
        if !isPacketContinue { isPacketContinue = true // Раз мы оказались здесь сразу метим начало пакета
            
// DEBUG: Отладочная информация
//            NSLog("Полный ответ:")
//            for element in commandArray { print(String(format:"%02X ", element), terminator: "") }
//            print()

                VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
            if  VALID == 0  { print("Регистр НЕ существует!") }
            let STRING_LEN: UInt32 = intFrom(commandArray, start: 8 + 4, length: LENGTH_SIZE) // Длина всего пакета данных
            
            totalLength = Int(STRING_LEN)
            dataArray = commandArray
            dataArray.removeFirst(8 + 4 + 4)            // Удаляем коммандную часть первой части пакета
            lastLength = totalLength - dataArray.count  // Полученная длина данных

// DEBUG: Отладочная информация
//            NSLog("Извлечённые данные:")
//            for element in dataArray { print(String(format:"%02X ", element), terminator: "") }
//            print()
            
            if dataArray.count == 0 {                       // Мы ничего не получили
                isPacketContinue = false
                enumCommandPublished = .VS_DATA_BUF_empty   // Следующая команда с задержкой!
            }

        } else { // у нас продолжение пакета
            
            dataArray = dataArray + commandArray            // Склеиваем с предыдущими данными
            lastLength = totalLength - dataArray.count      // Оставшаяся часть данных
            if lastLength < 1 {                             // Мы получили конец пакета

                isPacketContinue = false                    // Ставим флаг конца пакет
                
// DEBUG:  Отладочная информация
//                print("Полный пакет:")
//                for element in dataArray { print(String(format:"%02X ", element), terminator: "") }; print()
//                for element in dataArray { NSLog("%02X ", element) }
//                NSLog(dataArray.description)
//                let byteStr = String(decoding: dataArray, as: UTF8.self)
//                let byteStr = String(data: Data(dataArray), encoding: .windowsCP1251)
//                print(byteStr)
                
                decodeArray = dataArray
                
                var isError = false
                while decodeArray.count > 0 && !isError {
                    isError = decodeFirstFrame()  // Декодируем первый фрейм в пакете
                }
                enumCommandPublished = .VS_DATA_BUF // Декодировать больше нечего просим следующую команду
            }
        }
    }

    /// Декодирование всех групп и каналов в фрейме согласно конфигурациии
    func decodeFirstFrame() -> Bool {
        
        var samplesNum: UInt32 = 1
        
            print() // для отделения значений фреймов в логах

            // Определяемся с номером фрейма
        let frameNum = decodeArray[0]                               // Байт номера фрейма для отслеживания целостности данных
            NSLog("#\(frameNum)")
            decodeArray = Array(decodeArray.dropFirst(1))           // Убираем байт номера фрейма
        
        if frameLastNum == nil || frameLastNum! + 1 == frameNum {
            
            frameLastNum = frameNum                                 // Меняем старый счётчик на новый
            if frameLastNum == 255 { frameLastNum = nil }           // Сброс на 0 чтобы не превышать значение байта 255

            // Определяем число измерений в фрейме (пока только печатаем логах)
            let    RC_FRM_val = RC_FRM(rawValue: decodeArray[0])    // Байт мультиизмерений
            switch RC_FRM_val {
                case .RC_FRM_SINGLE_MEAS: print("RC_FRM_SINGLE_MEAS")
                case .RC_FRM_MEAS       : print("RC_FRM_MEAS")
                case .none              : print("RC_FRM ERROR")
                }
                decodeArray = Array(decodeArray.dropFirst(1))       // Убираем байт информации о факте мультиизмерений
            
            // TODO: Определиться с парсингом данных – где UInt, где Int
            
            // Определяемся с группой структур
            let grpID = decodeArray[0]                              // Идентификатор группы каналов
//              print("Код группы: \(String(format:"%02X ", grpID))")
                print("\(groupName(by: grpID)) Id = \(grpID)")
//          let RC_GRP_val = RC_GRP(rawValue: grpID)
                decodeArray = Array(decodeArray.dropFirst(1))       // Убираем байт группы структур
            
            // Определяемся со временем
            var ms: Int32 = intFrom(decodeArray);
            
// DEBUG: Печать полученного массива в 16-ричной форме
//            for i in 0...3 { print(String(format:"%02X ", commandArray[i]), terminator: "") } // Печать в 16-тиричной форме
//            print()
//            for element in decodeArray { print(String(format:"%02X ", element), terminator: "") }; print()

            let measure = measureDate(Int(ms), startDate: baseDate ?? Date()) // FIXME: Date() заглушка на пустую БД: обязательно перезапустить инициализацию прибора

            let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd MMM yyy H:mm:ss"
            let newDateStr = dateFormatter.string(from: measure)
                print("startTime = \(ms) (\(newDateStr))")
                decodeArray = Array(decodeArray.dropFirst(4))       // Убираем 4 байта милисекунд
                cur_ms_Time = Int(ms)
            
            // Делать это нужно после чтения заголовка RCFrame_Hd_t
            if RC_FRM_val == .RC_FRM_MEAS   { samplesNum = process_RCFrame_Meas_t() } // Число повторов
            else                            { samplesNum = 1 }
//            print("START decodeArray.count = \(decodeArray.count)")

            for _ in 1...samplesNum {   // Исходя из числа повторов
                
                let measure = measureDate(Int(ms), startDate: baseDate ?? Date()) // FIXME: Date() заглушка на пустую БД: обязательно перезапустить инициализацию прибора
                    timestump = measure                                 // Для последующей записи в БД
                let newDateStr = dateFormatter.string(from: measure)
                    print("+ms = \(ms) (\(newDateStr))")
                ms += smplTime_ms
                processGroup(grp: grpID) // читаем каналы
//              print("END decodeArray.count = \(decodeArray.count)")
            }
            return false
        } else {
//            for element in decodeArray { print(String(format:"%02X ", element), terminator: "") }; print()
            return true
        }
    }
    
/// Обработка фрейма `RCFrame_Meas_t` серий данных
    func process_RCFrame_Meas_t() -> UInt32 {
        
        // Количество отсчетов во фрейме
        let samplesNumArray: [UInt8] = [decodeArray[0], decodeArray[1], UInt8(0), UInt8(0)] // Последние байты для совместимости
        let samplesNum: UInt32 = intFrom(samplesNumArray);    print("SamplesNum = \(samplesNum)")
        decodeArray = Array(decodeArray.dropFirst(2))   // Убираем 2 байта отсчетов во фрейме

        // Кол-во милисекунд между соседними отсчетами (период выборки)
        let smplTime_msArray: [UInt8] = [decodeArray[0], decodeArray[1], decodeArray[2], decodeArray[3]]
        smplTime_ms = intFrom(smplTime_msArray);  print("SmplTime_ms = \(smplTime_ms)") //
        decodeArray = Array(decodeArray.dropFirst(4))   // Убираем 4 байта Кол-во милисекунд
        
        return samplesNum
    }
    
    /// Процесс обработки каналов указанного ID группы согласно заявленному в строке конфигурации (DataStructure в CoreData)
    /// Оттуда вызываются процедуры парсинга каждого канала отдельно. Затем в конце запускается процесс обработки группы process_GRP
    func processGroup(grp: UInt8) {
        
        maskBoolArray = []; for _ in 0..<32 { maskBoolArray.append(true) }      // Заполняем все позиции массива значанием True
        flagStartReadChanelByMask = false // Пока ничего не знаем о наличии правил считывания по маске RC_CHN_EVENT_CHN_MSK
        maskChannelCount          = nil   // поэтому и счётчик считывания каналов по маске RC_CHN_EVENT_CHN_MSK не определён

        groupCountPublished = groupCountPublished == nil ? 0 : groupCountPublished! + 1
        if  let channels = DataStrucktureCHN.channelSequence(for: grp, in: viewContext) {
            for channel in channels {
                let RC_CHN_val = RC_ChannelId(rawValue: UInt8(channel.id)) // Определяем канал
                switch RC_CHN_val {
                case .RC_CHN_FLAGS         : process_RC_CHN_FLAGS        (channel)    // Маска флагов
                case .RC_CHN_TIME          : process_RC_CHN_TIME         (channel)    // Метка времени
                case .RC_CHN_COUNT         : process_RC_CHN_COUNT        (channel)    // Количество зарегистрированных частиц
                case .RC_CHN_COUNT_RATE    :
                    if isChanelActiveByMask() {
                        process_RC_CHN_COUNT_RATE    (channel)
                    } // Скорость счета
                case .RC_CHN_COUNT_RATE_ERR:
                    if isChanelActiveByMask() {
                        process_RC_CHN_COUNT_RATE_ERR(channel, grp: grp)
                    } // Случайная ошибка скорости счета
                case .RC_CHN_DOSE_RATE     :
                    if isChanelActiveByMask() {
                        process_RC_CHN_DOSE_RATE     (channel)
                    } // Мощность дозы
                case .RC_CHN_DOSE_RATE_ERR :
                    if isChanelActiveByMask() {
                        process_RC_CHN_DOSE_RATE_ERR (channel, grp: grp)
                    } // Случайная ошибка мощности дозы
                case .RC_CHN_DURATION      :
                    if isChanelActiveByMask() {
                        process_RC_CHN_DURATION      (channel)
                    } // Длительность
                case .RC_CHN_DOSE          :
                    if isChanelActiveByMask() {
                        process_RC_CHN_DOSE          (channel)
                    } // Доза
                case .RC_CHN_TEMPERATURE   :
                    if isChanelActiveByMask() {
                        process_RC_CHN_TEMPERATURE   (channel)
                    } // Температура
                case .RC_CHN_BTRY_CHRG     :
                    if isChanelActiveByMask() {
                        process_RC_CHN_BTRY_CHRG     (channel)
                    } // Уровень заряда аккумулятора
                case .RC_CHN_ACC_X         : process_RC_CHN_ACC_X(channel)            // Ускорение вдоль оси X
                case .RC_CHN_ACC_Y         : process_RC_CHN_ACC_Y(channel)            // Ускорение вдоль оси Y
                case .RC_CHN_ACC_Z         : process_RC_CHN_ACC_Z(channel)            // Ускорение вдоль оси Z
                case .RC_CHN_OPTIC         : process_RC_CHN_OPTIC(channel)            // Данные оптического сенсора
                case .RC_CHN_EVENT         :
                    process_RC_CHN_EVENT(channel)            // Событие
                case .RC_CHN_EVENT_CHN_MSK :
                    process_RC_CHN_EVENT_CHN_MSK(channel)    // Маска каналов, передаваемых вместе с событием
                default:
                    print("ОШИБКА: Неизвестный канал данных ")
                }
            }
            process_GRP(grp) //***********************************************************
        } else {    print("ОШИБКА: Неизвестная группа данных ") }
    }
    
    /// Вспомогательная функция, определяющая необходимость считывания канала при задействовании RC_CHN_EVENT_CHN_MSK
    func isChanelActiveByMask() -> Bool {
        if flagStartReadChanelByMask {
            if maskBoolArray[maskChannelCount!] {
                maskChannelCount! += 1
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }

    // MARK: - Обработка групп

    /// Процесс распределения алгоритмов обработки найденных каналов в зависимости от принадлежности их той или иной группе
    func process_GRP(_ groupID: UInt8) {
        let RC_GRP_val = RC_GRP(rawValue: groupID) // Определяем канал
        switch RC_GRP_val {
            case .RC_GRP_COUNT_RATE      :processRC_GRP_COUNT_RATE()
            case .RC_GRP_DOSE_RATE       :processRC_GRP_DOSE__RATE()
            case .RC_GRP_DOSE_RATE_DB    :
                countRatePublished = CountRateStruct (msTime: cur_ms_Time, chnCountRate: Double(float32RC_CHN_COUNT_RATE!))
                dose_RatePublished = RC_GRP_DOSE_RATE(msTime: cur_ms_Time, chnDoseRate:  Double(float32RC_CHN_DOSE_RATE!) * 1000.0)
                saveRC_GRP_DB(groupID)
            case .RC_GRP_RARE_DATA       :
                saveRC_GRP_DB(groupID)
            
            case .RC_GRP_USER_DATA       :processCurGRP()
            case .RC_GRP_SHEDULE_DATA    :processCurGRP()
            case .RC_GRP_ACCEL_DATA      :processCurGRP()
            
            case .RC_GRP_EVENT           :save_RC_CHN_EVENT_CHN_MSK()
            case .RC_GRP_RAW_COUNT_RATE  :processRC_GRP_RAW_COUNT_RATE()
            case .RC_GRP_RAW_DOSE_RATE   :processRC_GRP_RAW_DOSE__RATE()
            
            default                      :processCurGRP()
                print("ОШИБКА: Неизвестная группа данных ")
        }
    }
    
    func processCurGRP() {}  // Пропускаем…
    
    
    //  СОЗДАТЬ ОТДЕЛЬНЫЕ ПЕРЕМЕННЫЕ RAW ВМЕСТО float32RC_CHN_COUNT_RATE
    
    // Публикуем значения скорости счётчика и мощности дозы для дальнейшей обработки
    func processRC_GRP_COUNT_RATE()     {
        lastCountRatePublished = Double(float32RC_CHN_COUNT_RATE!)
//        countRatePublished = CountRateStruct (msTime: cur_ms_Time, chnCountRate: Double(float32RC_CHN_COUNT_RATE!))
    }
    func processRC_GRP_DOSE__RATE()     {
        lastDose_RatePublished = Double(float32RC_CHN_DOSE_RATE!) * 1000.0
//        dose_RatePublished = RC_GRP_DOSE_RATE(msTime: cur_ms_Time, chnDoseRate:  Double(float32RC_CHN_DOSE_RATE!) * 1000.0)
    }
    
    func processRC_GRP_RAW_COUNT_RATE() { countRawRatePublished = CountRateStruct (msTime: cur_ms_Time, chnCountRate: Double(float32RC_CHN_COUNT_RATE!)) }
    func processRC_GRP_RAW_DOSE__RATE() { dose_RawRatePublished = RC_GRP_DOSE_RATE(msTime: cur_ms_Time, chnDoseRate:  Double(float32RC_CHN_DOSE_RATE!) * 1000.0) }

    
    /// Цикл обработки каналов на предмет сохранения выделенных данных
    func saveRC_GRP_DB(_ id: UInt8) {
        
        // Все сохраняемые данные должны быть привязаны к какому-то событию…
        new_log = Log(context: viewContext)     // Создаём событие даже если оно пустое (будет индикатор неизвестного события)
        new_log?.timestamp = timestump          // Время события
        new_log?.device = Device.findConnectedDevice(in: viewContext)           // Устройство, на котором произошло событие

        if  let channels = DataStrucktureCHN.channelSequence(for: id, in: viewContext) { // Определение списка каналов группы id
            // Делаем так, т.к. мы "не знаем" сколько здесь каналов
            for channel in channels {                                           // Цикл по всем каналам группы
                let RC_CHN_val = RC_ChannelId(rawValue: UInt8(channel.id))      // Определяем канал
                switch RC_CHN_val {
                case .RC_CHN_FLAGS         : save_RC_CHN_FLAGS          ()      // Маска флагов
                case .RC_CHN_TIME          : save_RC_CHN_TIME           ()      // Метка времени
                case .RC_CHN_COUNT         : save_RC_CHN_COUNT          ()      // Количество зарегистрированных частиц
                case .RC_CHN_COUNT_RATE    : save_RC_CHN_COUNT_RATE     ()      // Скорость счета
                case .RC_CHN_DOSE_RATE     : save_RC_CHN_DOSE_RATE      ()      // Мощность дозы
                case .RC_CHN_DOSE_RATE_ERR : save_RC_CHN_DOSE_RATE_ERR  ()      // Случайная ошибка мощности дозы
                case .RC_CHN_DURATION      : save_RC_CHN_DURATION       ()      // Длительность
                case .RC_CHN_DOSE          : save_RC_CHN_DOSE           ()      // Доза
                case .RC_CHN_TEMPERATURE   : save_RC_CHN_TEMPERATURE    ()      // Температура
                case .RC_CHN_BTRY_CHRG     : save_RC_CHN_BTRY_CHRG      ()      // Уровень заряда аккумулятора
                case .RC_CHN_ACC_X         : save_RC_CHN_ACC_X          ()      // Ускорение вдоль оси X
                case .RC_CHN_ACC_Y         : save_RC_CHN_ACC_Y          ()      // Ускорение вдоль оси Y
                case .RC_CHN_ACC_Z         : save_RC_CHN_ACC_Z          ()      // Ускорение вдоль оси Z
                case .RC_CHN_OPTIC         : save_RC_CHN_OPTIC          ()      // Данные оптического сенсора
                case .RC_CHN_EVENT         : save_RC_CHN_EVENT          ()      // Событие
                case .RC_CHN_EVENT_CHN_MSK : save_RC_CHN_EVENT_CHN_MSK()        // Маска каналов, передаваемых вместе с событием
                case .RC_CHN_COUNT_RATE_ERR: save_RC_CHN_COUNT_RATE_ERR ()      // Случайная ошибка скорости счета
                default:
                    print("ОШИБКА: Неизвестный канал данных ")
                }
            }
            logRateDB   = nil // Индикатор то, что группа для занесения в БД уже обработана
            logEvent    = nil // Индикатор то, что группа событий уже обработана
            logRareData = nil // Индикатор то, что группа редких данных уже обработана
        }
    }
    
    // MARK: - Сохранение каналов
    
    func save_RC_CHN_EVENT_CHN_MSK() {
        if logEvent == nil {
           logEvent = Event(context: viewContext)
        }
        logEvent?.event = Int32(uint_32RC_CHN_EVENT!)
        logEvent?.log = new_log
        
        if float32RC_CHN_COUNT_RATE     != nil { logEvent?.count_rate     = float32RC_CHN_COUNT_RATE!       }
        if float32RC_CHN_COUNT_RATE_ERR != nil { logEvent?.count_rate_err = float32RC_CHN_COUNT_RATE_ERR!   }
        if float32RC_CHN_DOSE_RATE      != nil { logEvent?.dose_rate      = float32RC_CHN_DOSE_RATE!        }
        if float32RC_CHN_DOSE_RATE_ERR  != nil { logEvent?.dose_rate_err  = float32RC_CHN_DOSE_RATE_ERR!    }
        if uint_32RC_CHN_DURATION       != nil { logEvent?.duration       = Int32(uint_32RC_CHN_DURATION!)  }
        if float32RC_CHN_DOSE           != nil { logEvent?.dose           = float32RC_CHN_DOSE!             }
        if float32RC_CHN_TEMPERATURE    != nil { logEvent?.temperature    = float32RC_CHN_TEMPERATURE!      }
        if float32RC_CHN_BTRY_CHRG      != nil { logEvent?.charge         = float32RC_CHN_BTRY_CHRG!        }

        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_FLAGS() {
        new_log?.flags = Int32(uint_16RC_CHN_Flags!)
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_TIME() {}
    
    func save_RC_CHN_COUNT() { // Создаём событие с счётчиком
        if logRateDB == nil {
           logRateDB = LogRateDB(context: viewContext)
        }
        logRateDB?.count = Int32(uint_32RC_CHN_COUNT!)

        logRateDB?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_COUNT_RATE() { // Создаём событие со скоростью счёта
        if logRateDB == nil {
           logRateDB = LogRateDB(context: viewContext)
        }
        logRateDB?.count_rate = Float32(float32RC_CHN_COUNT_RATE!)
        if logRateDB?.count_rate ?? 0 > 100 {
            print("Значение RC_CHN_COUNT_RATE слишком высокое: \(logRateDB?.count_rate ?? 0)")
        }

        logRateDB?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_DOSE_COUNT_ERR() {
    }
    
    func save_RC_CHN_DOSE_RATE() {
        if logRateDB == nil {
           logRateDB = LogRateDB(context: viewContext)
        }
        logRateDB?.dose_rate = Float32(float32RC_CHN_DOSE_RATE!)
        if logRateDB?.dose_rate ?? 0 > 100 {
            print("Значение RC_CHN_DOSE_RATE слишком высокое: \(logRateDB?.dose_rate ?? 0)")
        }
        logRateDB?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_DOSE_RATE_ERR() {
        if logRateDB == nil {
           logRateDB = LogRateDB(context: viewContext)
        }
        logRateDB?.dose_rate_err = Float32(float32RC_CHN_DOSE_RATE_ERR!)
        logRateDB?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_DURATION() {
        if logRareData == nil {
           logRareData = LogRareData(context: viewContext)
        }
        logRareData?.duration = Int32(uint_32RC_CHN_DURATION!) // FIXME: Not enough bits to represent the passed value
        logRareData?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_DOSE() {
        if logRareData == nil {
           logRareData = LogRareData(context: viewContext)
        }
        logRareData?.dose = Float32(float32RC_CHN_DOSE!)
        logRareData?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_TEMPERATURE() {
        if logRareData == nil {
           logRareData = LogRareData(context: viewContext)
        }
        logRareData?.temperature = Float32(float32RC_CHN_TEMPERATURE!)
        logRareData?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_BTRY_CHRG() {
        if logRareData == nil {
           logRareData = LogRareData(context: viewContext)
        }
        logRareData?.charge = Float32(float32RC_CHN_BTRY_CHRG!)
        logRareData?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_ACC_X() {}
    func save_RC_CHN_ACC_Y() {}
    func save_RC_CHN_ACC_Z() {}
    func save_RC_CHN_OPTIC() {}
    
    func save_RC_CHN_EVENT() {
        if logEvent == nil {
           logEvent = Event(context: viewContext)
        }
        logEvent?.event = Int32(uint_32RC_CHN_EVENT!) // FIXME: Not enough bits to represent the passed value
        logEvent?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_COUNT_RATE_ERR() {}

    
    // MARK: - Обработка каналов
    // TODO: сделать проверку на минимум и максимум MAxVal MinVal
    
/// Удаление из начала массива `decodeArray` уже обработанных данных
/// - Parameter channel: Текущий канал типа `CoreData entity` из виртуальной строки
///  [VS_DATA_BUF](http://127.0.0.1:49541/rc_ms_results.htm).
///
    func deleteItemsFromDecodeArray(for channel: DataStrucktureCHN) {
        if let channelDataSize = channelDataSize(channel.d_type) {      // Если в канале найден тип данных
            decodeArray = Array(decodeArray.dropFirst(channelDataSize)) // Убирам из массива байты канала
        } else {
            print("ОШИБКА: Неизвестный тип канала данных")
        }
    }
    
/// Изъятие из массива потока *decodeArray* в массива данных канала
    func channelDataArray(_ channel: DataStrucktureCHN) -> [UInt8] {
        // TODO: Сделать проверку на nil для всех size
        let streamSize = channelDataSize(channel.d_type)    // Размер данных из потока  прибора
        let store_Size = channelDataSize(channel.r_type)    // Размер данных для хранения в приложении
        var dataArray: [UInt8] = []                         // Байтовый массив для преобразования данных
        for index in 0...(streamSize! - 1) {                // По числу байт в потоке
            dataArray.append(decodeArray[index])            // Добавляем в массив
        }
        let delta_Size = store_Size! - streamSize!          // Разница типов в байтах
        if  delta_Size > 0 {                                // Только если разница размеров типов больше нуля!
            for _ in 0...(delta_Size - 1) {                 // По числу байт в потоке
                dataArray.append(UInt8(0))                  // Добавляем в массив пустые байты для конвертации
            }
        }
            deleteItemsFromDecodeArray(for: channel)        // Удаляем обработанные байты массива потока
        return dataArray                                    // Возвращаем отрезанный массив данных канала
    }
    
    /// Исполнение заложенного в конфигурацию арифметического выражения P1 • X + P2
    func exprChannelRestore(_ channel: DataStrucktureCHN, val: Double) -> Double { return channel.p1 * val + channel.p2 }
    
    /// Пропускаем неизвестный нам поток данных согласно правилам строки конфигурации
    func processUnknown(_ groupID: UInt8) {
        print("Пропускаем неизвестную группу данных ")
        if  let channels = DataStrucktureCHN.channelSequence(for: groupID, in: viewContext) {
            for channel in channels {
                deleteItemsFromDecodeArray(for: channel)
            }
        } else {
            print("ОШИБКА: Неизвестная группа данных ")
        }
    }
    
/// Обработка канала флагов *`RC_CHN_FLAGS`*
    func process_RC_CHN_FLAGS(_ channel: DataStrucktureCHN) { print("RC_CHN_FLAGS")
        if flagStartReadChanelByMask {
            maskChannelCount = 0
        }
        let channelArray = channelDataArray(channel)      // Получаем массив данных канала
        // TODO: Сделать проверку на несоответствие хранимых типов конфигурации и приложения
            uint_16RC_CHN_Flags = intFrom(channelArray);      // Превращаем в целое
            printBin(UInt32(uint_16RC_CHN_Flags!))              // DEBUG: печать флагов в двоичном виде
    }
    
    func process_RC_CHN_TIME(_ channel: DataStrucktureCHN) { print("RC_CHN_TIME")
        deleteItemsFromDecodeArray(for: channel)            // Удаляем обработанные байты массива потока
    }
    
/// Обработка канала **Количество импульсов детектора за время измерения** *`RC_CHN_COUNT`*
    func process_RC_CHN_COUNT(_ channel: DataStrucktureCHN) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        uint_32RC_CHN_COUNT = intFrom(channelArray);
        print("CHN_Count = \(uint_32RC_CHN_COUNT!)")
    }
    
/// Обработка канала **Значение скорости счета детектора SiPM** `RC_CHN_COUNT_RATE`
    func process_RC_CHN_COUNT_RATE(_ channel: DataStrucktureCHN) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        let data = Data(channelArray)
            float32RC_CHN_COUNT_RATE = data.withUnsafeBytes { $0.load(as: Float.self) }
            print(String(format: "CHN_CountRate =  %.2f имп/с", float32RC_CHN_COUNT_RATE!))
    }
    
/// Обработка канала **Случайная ошибка скорости счета** *`RC_CHN_COUNT_RATE_ERR`*
    func process_RC_CHN_COUNT_RATE_ERR(_ channel: DataStrucktureCHN, grp: UInt8) { print("RC_CHN_COUNT_RATE_ERR")
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        let doseCountErr: UInt32 = intFrom(channelArray);
        if channel.expr == 1 {
            float32RC_CHN_COUNT_RATE_ERR = Float32( exprChannelRestore(channel, val: Double(doseCountErr)))
        } else {
            float32RC_CHN_COUNT_RATE_ERR = Float32(doseCountErr)
        }
        if grp == 0 {
            lastCountErrRatePublished = Double(float32RC_CHN_COUNT_RATE_ERR!)
        }
        print("CHN_CountRateErr = \(float32RC_CHN_COUNT_RATE_ERR!)")
    }

/// Обработка канала **Значение мощности дозы** *`RC_CHN_DOSE_RATE`*
    func process_RC_CHN_DOSE_RATE(_ channel: DataStrucktureCHN) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        let data = Data(channelArray)
            float32RC_CHN_DOSE_RATE = data.withUnsafeBytes { $0.load(as: Float.self) }  * 10 // FIXME: А это правильно умножать на 10???
            print(String(format: "CHN_DoseRate =  %.2f мкР/ч", float32RC_CHN_DOSE_RATE! * 1000))// Заодно переводим в мкР/ч FIXME: может не стоит для хранения
    }
    
/// Обработка канала **Случайная ошибка мощности дозы** *`RC_CHN_DOSE_RATE_ERR`*
    func process_RC_CHN_DOSE_RATE_ERR(_ channel: DataStrucktureCHN, grp: UInt8) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        let doseRateErr: UInt32 = intFrom(channelArray);
        if channel.expr == 1 {
            float32RC_CHN_DOSE_RATE_ERR = Float32( exprChannelRestore(channel, val: Double(doseRateErr)))
        } else {
            float32RC_CHN_DOSE_RATE_ERR = Float32(doseRateErr)
        }
        if grp == 1 {
            lastDose_RateErrPublished = Double(float32RC_CHN_DOSE_RATE_ERR!)
        }
        print("CHN_DoseRateErr = \(float32RC_CHN_DOSE_RATE_ERR!)")
    }
    
    func process_RC_CHN_DURATION(_ channel: DataStrucktureCHN) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        uint_32RC_CHN_DURATION = intFrom(channelArray)
        print("CHN_Duration = \(uint_32RC_CHN_DURATION!) с")
    }
    
    func process_RC_CHN_DOSE(_ channel: DataStrucktureCHN) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        let data = Data(channelArray)
        float32RC_CHN_DOSE = data.withUnsafeBytes { $0.load(as: Float32.self) }
        print(String(format: "CHN_Dose =  %.2f Р", float32RC_CHN_DOSE!))
    }
    
    func process_RC_CHN_TEMPERATURE(_ channel: DataStrucktureCHN) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        let temperature: Int16 = intFrom(channelArray)
        if channel.expr == 1 {
            float32RC_CHN_TEMPERATURE = Float32( exprChannelRestore(channel, val: Double(temperature)))
        } else {
            float32RC_CHN_TEMPERATURE = Float32(temperature)
        }
        print("CHN_Temperature = \(float32RC_CHN_TEMPERATURE!)°C")
    }
    
    func process_RC_CHN_BTRY_CHRG(_ channel: DataStrucktureCHN) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        let charge: UInt32 = intFrom(channelArray)
        float32RC_CHN_BTRY_CHRG = channel.expr == 1 ? Float32(exprChannelRestore(channel, val: Double(charge))) : Float32(charge)
        print("CHN_ChargeLevel = \(float32RC_CHN_BTRY_CHRG!)%")
    }
    
    func process_RC_CHN_ACC_X(_ channel: DataStrucktureCHN) { print("RC_CHN_ACC_X")
        deleteItemsFromDecodeArray(for: channel)
    }
    
    func process_RC_CHN_ACC_Y(_ channel: DataStrucktureCHN) { print("RC_CHN_ACC_Y")
        deleteItemsFromDecodeArray(for: channel)
    }
    
    func process_RC_CHN_ACC_Z(_ channel: DataStrucktureCHN) { print("RC_CHN_ACC_Z")
        deleteItemsFromDecodeArray(for: channel)
    }
    
    func process_RC_CHN_OPTIC(_ channel: DataStrucktureCHN) { print("RC_CHN_OPTIC")
        deleteItemsFromDecodeArray(for: channel)
    }
    
    func process_RC_CHN_EVENT(_ channel: DataStrucktureCHN) { print("RC_CHN_EVENT")
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        let uint: UInt32 = UInt32(channelArray[0])
        print(uint)
        uint_32RC_CHN_EVENT = UInt32(channelArray[0])
    }
    
    func process_RC_CHN_EVENT_CHN_MSK(_ channel: DataStrucktureCHN) { print("RC_CHN_EVENT_CHN_MSK")
        
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        var mask: UInt32 = 0
        switch RC_DataType(rawValue: UInt8(channel.d_type))  { // В любом целом виде преобразуем в беззнаковое
            case    .RC_DTP_UINT8,
                    .RC_DTP_INT8,
                    .RC_DTP_UINT16,
                    .RC_DTP_INT16,
                    .RC_DTP_UINT24,
                    .RC_DTP_INT24,
                    .RC_DTP_UINT32,
                    .RC_DTP_INT32: mask = intFrom(channelArray)
            default :   print("ОШИБКА ОПРЕДЕЛЕНИЯ ТИПА МАСКИ")
        }

        var resultMask: UInt32 = 0
        switch RC_DataType(rawValue: UInt8(channel.r_type))  {
            case .RC_DTP_UINT8  ,
                 .RC_DTP_INT8   ,
                 .RC_DTP_UINT16 ,
                 .RC_DTP_INT16  ,
                 .RC_DTP_UINT24 ,
                 .RC_DTP_INT24  ,
                 .RC_DTP_UINT32 ,
                 .RC_DTP_INT32  : resultMask = UInt32(channel.p1) * mask + UInt32(channel.p2)
            default             : print("ОШИБКА ОПРЕДЕЛЕНИЯ ТИПА ФОРМУЛЫ")
        }
        
        printBin(resultMask)
        
        flagStartReadChanelByMask = true                // Раз натолкнулись на RC_CHN_EVENT_CHN_MSK, значит нужно обрабатывать отдельные каналы
        maskBoolArray = bitMaskInBoolArray(resultMask)  // Получаем только те каналы из конфигурации, которые будут использованы
        
//         DEBUG: Печать полученного массива в 16-ричной форме
//        for element in decodeArray { print(String(format:"%02X ", element), terminator: "") }; print()
          for element in maskBoolArray { print(element, terminator: "") }; print()

//        deleteItemsFromDecodeArray(for: channel)
    }
    
     

}
