//
//  RadiaCodeBLEManager.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import Foundation
import CoreBluetooth
import Combine

// TODO: Проверять список уже найденных устройств для быстрого переподключения


let radiaCode101ServiceCBUUID = CBUUID(string: "E63215E5-7003-49D8-96B0-B024798FB901")

open class BLEConnection: NSObject, ObservableObject {
    
    var connectUUID: UUID? = nil
    
    // MARK: - Combine publisher protocol
    
    // Передача факта подключения
    // TODO: может будет достаточно использовать "var peripheral"
    
    // Далее также процесс маскировки Publisher от возможности его изменения извне…
    // Пеередача строки статуса
    private      let connectSubj = PassthroughSubject<String, Never>() // Можно лишь передавать новые значения в него из самого класса
                 var connectPublisher:   AnyPublisher<String, Never> { connectSubj.eraseToAnyPublisher() } // Здесь мы "стираем" информацию о типе нашего субъекта
    private(set) var statusStr       : String = String(localized: "No connection") {
        didSet { connectSubj.send(statusStr) }
    }
    
    // Передача статуса состояния устройства enum
    private      let statSubj = PassthroughSubject<Status, Never>()
                 var statPublisher  : AnyPublisher<Status, Never> { statSubj.eraseToAnyPublisher() }
    private(set) var status         : Status?  {
        didSet { statSubj.send(status!) }
    }

    
    // Передача полученных данных
    private      let dataSubj = PassthroughSubject<Data, Never>()
                 var dataPublisher  : AnyPublisher<Data, Never> { dataSubj.eraseToAnyPublisher() }
    private(set) var data           : Data?  {
        didSet { dataSubj.send(data!) }
    }
    
    // Передача информации о найденном устройстве RadiaCode-101
    private      let peripheralSubj = PassthroughSubject<CBPeripheral, Never>()
                 var peripheralPublisher  : AnyPublisher<CBPeripheral, Never> { peripheralSubj.eraseToAnyPublisher() }
    private(set) var newPeripheral        : CBPeripheral?  {
        didSet { peripheralSubj.send(newPeripheral!) }
    }
    
    // Передача подробной информации о найденном устройстве RadiaCode-101
    private      let advertisementDataSubj = PassthroughSubject<[String : Any], Never>()
                 var advertisementDataPublisher  : AnyPublisher<[String : Any], Never> { advertisementDataSubj.eraseToAnyPublisher() }
    private(set) var newAdvertisementData        : [String : Any]? {
        didSet { advertisementDataSubj.send(newAdvertisementData!) }
    }

    // MARK: -

    // Переменные подключения к BLE
    private      var peripheral       :  CBPeripheral?
    private      var characteristicRX :  CBCharacteristic?
    private      var centralManager   :  CBCentralManager! = nil
          
    func startCentralManager() {
        self.centralManager = CBCentralManager(delegate: self, queue: nil)
        print("Central Manager State: \(self.centralManager.state)")
        self.centralManagerDidUpdateState(self.centralManager)
    }

    public func connect(device: Device) {
        connectUUID = device.uuid // Автоматическое подключение если UUID не nil
        statusStr = "Connection…"
        status = .connecting
//        print("Попытка подключения!")
//        print(connectUUID ?? "НЕТ")
//        centralManager.scanForPeripherals(withServices:  [radiaCode101ServiceCBUUID] )
    }
    public func startScan()  {
        statusStr = String(localized: "Looking device…")
        centralManager.scanForPeripherals(withServices:  [radiaCode101ServiceCBUUID] )
    }    // Ищем подходящие BLE устройства
    
    public func stop_Scan()  { centralManager.stopScan() }
    public func disconnect() { centralManager.cancelPeripheralConnection(peripheral!) }
    
    func writeCommandBuffer(_ bytesArray:[UInt8]) {
        let testBuffer = Data(bytes: UnsafePointer<UInt8>(bytesArray), count: bytesArray.count) // Создаём буфер данных из массива
// DEBUG:        print("Write UUID: \(String(describing: characteristicRX))")
        peripheral!.writeValue(testBuffer as Data, for: characteristicRX!, type: .withResponse) // Посылаем буфер по BT
    }
}

// MARK: - CBCentralManager extension

extension BLEConnection: CBCentralManagerDelegate {
    
    // Обнаружение изменения состояния центрального BlueTooth-модуля
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch (central.state) {
            case .unknown:      print("Непонятно")          ; statusStr = String(localized: "unknown")
            case .resetting:    print("'Прервано'")         ; statusStr = String(localized: "resetting")
            // TODO: Предупреждаем пользователя, что его устройство не поддерживает Bluetooth, и приложение не будет работать должным образом
            case .unsupported:  print("'Не поддерживается'"); statusStr = String(localized: "unsupported")
            case .unauthorized: print("'Не авторизировано'"); statusStr = String(localized: "unauthorized")
            // TODO: Предупреждаем пользователя о необходимости включении Bluetooth
            case .poweredOff:   print("'Выключено'")        ; statusStr = String(localized: "poweredOff")
            case .poweredOn:
                print("'Включено'")         ;
                statusStr = String(localized: "poweredOn");
                if connectUUID != nil {
                    
                    startScan()
                }
            @unknown default:   print("BLE is Unknown")     ; statusStr = String(localized: "BLE is Unknown")
        }
    }

    // Мы нашли какое-то переферийное устройство
    public func centralManager(_
                                central: CBCentralManager,
                 didDiscover peripheral: CBPeripheral,
                      advertisementData: [String : Any],
                              rssi RSSI: NSNumber) {
    
//        central.stopScan()
        newPeripheral        = peripheral           // Передаём по Combine
        print("Найдено устройство!")
        newAdvertisementData = advertisementData    // Передаём по Combine
//        self.peripheral = peripheral
        
        // Происходит подключение по имени
        if peripheral.identifier == connectUUID  {
            central.stopScan()      // Нужно остановить сканирование, чтобы не тратить энергию
            centralManager.connect(newPeripheral!, options: nil)  // ПОДКЛЮЧАЕМСЯ
        }
    }
  
    // Произошло подключение переферийного устройства
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print      ("Подключено: \(peripheral.name!)!")
        statusStr = String(localized: "Connected")
        status = .connected
        self.peripheral = peripheral
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        central.stopScan()
  }
    
    // Произошло отключение переферийного устройства
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print      ("Отключено: \(peripheral.name!)!")
        statusStr = String(localized: "Disconnected!")
        status = .disconnected
    }
}

// MARK: - CBPeripheral extension

extension BLEConnection: CBPeripheralDelegate {
    
    // Обнаружен сервис переферийного устройства
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in peripheral.services! {
            print("Обнаружен сервис: \(service)")
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }

    // Обнаружена характеристика переферийного устройства
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        for characteristic in service.characteristics! {
            print("  Характеристика: \(characteristic)")
            if characteristic.properties.contains(.write)   {
                characteristicRX = characteristic
                print("  Характеристика: .write")
                status = .serviceFound // Можно начинать подавать команды
            }
            if characteristic.properties.contains(.notify)  {
                peripheral.setNotifyValue(true, for: characteristic)
                print("  Характеристика: .notify")
                statusStr = String(localized: "Connected: \(peripheral.name!)!")
                statusStr = String(localized: "Connected: \("RadiaCode 101")")
            }
        }
    }

    // Происходит попытка записи в переферийное устройство
    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
// DEBUG:          print("Запись в: \(characteristic)")
        if (error != nil) { print("Ошибка записи CBCharacteristic: \(error!.localizedDescription)")}
    }

    // Подписка сообщила об изменении характеристики в переферийном устройстве
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
// DEBUG:          print("Чтение из: \(characteristic)")
        if (error != nil) { print("Ошибка чтения: %@", error!.localizedDescription) }
        data = characteristic.value;
    }

}
