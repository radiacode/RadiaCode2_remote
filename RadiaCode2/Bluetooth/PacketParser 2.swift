//
//  PacketParser.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import Foundation
import Combine

class PacketParser: ObservableObject {
    
            var prms    : ParamsParser
            var baseDate: Date?
    
    private var isPacketContinue = false
    private var command: UInt32 = 0
    private var commandArray : [UInt8] = []
    private var    dataArray : [UInt8] = []
    
    /// Массив для парсинга `DATA_BUF`
    private var  decodeArray : [UInt8] = []
            var isValidData  = true         // По умолчанию верны, по мере проверок может оказаться - нет
            var commandName = String(localized: "NOT DEFINDED")
    private var totalLength = 0 // Общая длина склееваемого пакета
    private var lastLength = 0
    private var frameLastNum : UInt8?
    private var cur_ms_Time: Int = 0
    private let viewContext = PersistenceController.shared.container.viewContext
    
    // MARK: Переменные каналов
    
    private var timestump: Date?
    private var uint_16RC_CHN_Flags         : UInt16?
    private var float32RC_CHN_DOSE_RATE     : Float32?
    private var float32RC_CHN_DOSE_RATE_ERR : Float32?
    private var float32RC_CHN_COUNT_RATE    : Float32?
    private var uint_32RC_CHN_COUNT         : UInt32?
    private var uint_32RC_CHN_DURATION      : UInt32?
    private var float32RC_CHN_DOSE          : Float32?
    private var float32RC_CHN_TEMPERATURE   : Float32?
    private var float32RC_CHN_BTRY_CHRG     : Float32?
    private var uint_32RC_CHN_EVENT         : UInt32?

    // Сущности (entity) для сохранения в Core Data
    var new_log     : Log?
    var logRateDB   : LogRateDB?
    var logRareData : LogRareData?
    var logEvent    : Event?


    
    // MARK: - Использование Combine
    
    // Передача полученных данных дозы
    private      let dataSubj = PassthroughSubject<RC_GRP_DOSE_RATE, Never>()
                 var doseRatePublisher  : AnyPublisher<RC_GRP_DOSE_RATE, Never> { dataSubj.eraseToAnyPublisher() }
    private(set) var doseRatePublished                    : RC_GRP_DOSE_RATE?  {
        didSet { dataSubj.send(doseRatePublished!) }
    }
    
    // Передача полученных данных импульсов
    private      let countSubj = PassthroughSubject<RC_GRP_COUNT_RATE, Never>()
                 var countPublisher  : AnyPublisher<RC_GRP_COUNT_RATE, Never> { countSubj.eraseToAnyPublisher() }
    private(set) var countRatePublished                    : RC_GRP_COUNT_RATE?  {
        didSet { countSubj.send(countRatePublished!) }
    }

    // Вызов команды
    private      let сomSubj = PassthroughSubject<COMMAND, Never>()
                 var comPublisher :  AnyPublisher<COMMAND, Never> { сomSubj.eraseToAnyPublisher() }
    private(set) var enumCommandPublished  :               COMMAND?  {
        didSet { сomSubj.send(enumCommandPublished!) }
    }
    
    // Счётчик групп
    private      let groupCountSubj = PassthroughSubject<Int, Never>()
                 var groupCountPublisher :  AnyPublisher<Int, Never> { groupCountSubj.eraseToAnyPublisher() }
    private(set) var groupCountPublished  :               Int?  {
        didSet { groupCountSubj.send(groupCountPublished!) }
    }
    
    // Передача целочисленных данных настроек прибора
    private      let intSubj = PassthroughSubject<Int, Never>()
                 var intPublisher :  AnyPublisher<Int, Never> { intSubj.eraseToAnyPublisher() }
    private(set) var intPublished  :               Int?  {
        didSet { intSubj.send(intPublished!) }
    }


    
    // MARK: - init/func
    
    init(_ prms: ParamsParser) { self.prms = prms }


    func defineCommand(_ bytes : [UInt8]) {
        commandArray =   bytes
        
        if !isPacketContinue { // Мы в начале пакета
            self.command = intFrom(commandArray, start: 4, length: LENGTH_SIZE)
            
// DEBUG:
//            print("Начало пакета")
//            print("self.command = 0x\(String(format:"%02X ", self.command) )")
//            for element in commandArray { print(String(format:"%02X ", element), terminator: "") } // Печать в 16-тиричной форме
//            print(commandArray) // Печать в 10-тиричной форме
            
        } else {
//            print("продолжение пакета…")
//            for element in commandArray { print(String(format:"%02X ", element), terminator: "") } // Печать в 16-тиричной форме
//            print()
//            print(commandArray) // Печать в 10-тиричной форме
        }
        // FIXME: switch не работает в else, а должен быть там…
        switch self.command {
//            case     GET_STATUS: parser_GET_STATUS()
            case    RD_VIRT_SFR: parser_SFR_ID()

            case SET_EXCHANGE   : parser_SET_EXCHANGE()
            case WR_VIRT_SFR    : parser_SFR_ID_validation()
            case RD_VIRT_STRING : parser_SFR_ID()
            case SET_LOCAL_TIME : enumCommandPublished = .SET_LOCAL_TIME
            default             : enumCommandPublished = .ERROR
        }

    }
    
    func parser_SFR_ID() {
        switch SFR_ID! {
//        case  VSFR_DEVICE_CTRL      : parser_VSFR_DEVICE_CTRL()
        case  VSFR_DEVICE_LANG      : parser_VSFR_DEVICE_LANG()
//        case  VSFR_DISP_CTRL        : parser_VSFR_DISP_CTRL()
//        case  RC_VSFR_DS_UNITS      : parser_VSFR_DEVICE_UNITS()
//        case  RC_VSFR_DR_LEV1_uR_h  : parser_RC_VSFR_DR_LEV1_uR_h()
//        case  RC_VSFR_DR_LEV2_uR_h  : parser_RC_VSFR_DR_LEV2_uR_h()
//        case  RC_VSFR_DS_LEV1_100uR : parser_RC_VSFR_DS_LEV1_100uR()
//        case  RC_VSFR_DS_LEV2_100uR : parser_RC_VSFR_DS_LEV2_100uR()
//        case  VSFR_SOUND_CTRL       : parser_VSFR_SOUND_CTRL()
//        case  VSFR_VIBRO_CTRL       : parser_VSFR_VIBRO_CTRL()
//        case  VSFR_DISP_OFF_TIME    : parser_VSFR_DISP_OFF_TIME()
//        case  VSFR_DISP_BRT         : parser_VSFR_DISP_BRT()
        case  VS_CONFIGURATION      : parser_VS_CONFIGURATION()
        case  VS_DATA_BUF           : parser_VS_DATA_BUF()
       default: isValidData = false
        }
    }

    func parser_SFR_ID_validation() {
        
        let  startVALID = LENGTH_SIZE + MemoryLayout.size(ofValue: WR_VIRT_SFR)
        VALID = intFrom(commandArray, start: startVALID, length: LENGTH_SIZE)
        switch SFR_ID! {
            case  VSFR_DEVICE_TIME: enumCommandPublished = (VALID == 1 ? .VSFR_DEVICE_TIME : .ERROR)
            default:                enumCommandPublished = .ERROR
        }
    }
    
    func parser_SET_EXCHANGE() { commandName =  "SET_EXCHANGE"
        
        let targetStart = LENGTH_SIZE + MemoryLayout.size(ofValue: SET_EXCHANGE)
        let  buff1Start = targetStart + MemoryLayout.size(ofValue: TARGET_ID)
        let  buff2Start = targetStart + MemoryLayout.size(ofValue: BUFFER_TX_SIZE)
        
        let commandLength: UInt32 = intFrom(commandArray, start: 0,           length: LENGTH_SIZE)
        let curTARGET_ID : UInt32 = intFrom(commandArray, start: targetStart, length: MemoryLayout.size(ofValue: TARGET_ID))
            BUFFER_TX_SIZE        = intFrom(commandArray, start:  buff1Start, length: MemoryLayout.size(ofValue: BUFFER_TX_SIZE))
            BUFFER_RX_SIZE        = intFrom(commandArray, start:  buff2Start, length: MemoryLayout.size(ofValue: BUFFER_RX_SIZE))
        
        print(              "Длина команды  = \(commandLength)")
        print(String(format:"TARGET_ID = %02X ", curTARGET_ID))
        print(              "BUFFER_TX_SIZE = \(BUFFER_TX_SIZE)\nBUFFER_RX_SIZE = \(BUFFER_RX_SIZE)")
        
        if  TARGET_ID != curTARGET_ID { print("Неправильный TARGET_ID"); return }
        
        enumCommandPublished = .SET_EXCHANGE // Отработанная команда
    }
    
    func parser_VSFR_DEVICE_LANG() { commandName =  "VSFR_DEVICE_LANG"
         VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
        if  VALID == 0  { print("Регистр НЕ существует!") }
        let VALUE: UInt32 = intFrom(commandArray, start: 12, length: LENGTH_SIZE)
//        RC_DEVICE_LANG = VALUE
//        printBin(VALUE)
//        switch VALUE {
//            case  0:    deviceLang = .rus
//            case  1:    deviceLang = .eng
//            default:    deviceLang = .eng
//        }
        
        enumCommandPublished = .VSFR_DEVICE_LANG // Отработанная команда
        intPublished = Int(VALUE) // Передаём значение после информации об выполении комнды
    }

    
    func parser_VS_CONFIGURATION() { commandName =  "VS_CONFIGURATION"
        if !isPacketContinue { isPacketContinue = true // Раз мы оказались здесь сразу метим начало пакета
                VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
            if  VALID == 0  { print("Регистр НЕ существует!") }
            let STRING_LEN: UInt32 = intFrom(commandArray, start: 8 + 4, length: LENGTH_SIZE) // Длина всего пакета данных
            totalLength = Int(STRING_LEN)
            dataArray = commandArray
            dataArray.removeFirst(8 + 4 + 4) // Удаляем коммандную часть первой части пакета
            lastLength = totalLength - dataArray.count // Полученная длина данных
            
// DEBUG: Отладочная информация
//            print("Получены данные длиной: \(STRING_LEN) байт")
//            print(commandArray)
//            print("Извлечённые данные:")
//            for element in dataArray { print(String(format:"%02X ", element), terminator: "") }
//            print()
//            let byteStr = String(decoding: dataArray, as: UTF8.self)
//            print(byteStr)
            
        } else {
            dataArray = dataArray + commandArray
            lastLength = totalLength - dataArray.count // Оставшаяся часть данных
            if lastLength <= 1 { isPacketContinue = false
                let byteStr = String(data: Data(dataArray), encoding: .windowsCP1251)
                prms.parsingToCoreData(params: byteStr ?? "")
                print("Полный пакет:")
                print(byteStr!)
                enumCommandPublished = .VS_CONFIGURATION // Отработанная команда
            }
        }
    }

    func parser_VS_DATA_BUF() { commandName =  "VS_DATA_BUF"
        if !isPacketContinue { isPacketContinue = true // Раз мы оказались здесь сразу метим начало пакета
            
// DEBUG: Отладочная информация
//            NSLog("Полный ответ:")
//            for element in commandArray { print(String(format:"%02X ", element), terminator: "") }
//            print()

                VALID = intFrom(commandArray, start: 8, length: LENGTH_SIZE)
            if  VALID == 0  { print("Регистр НЕ существует!") }
            let STRING_LEN: UInt32 = intFrom(commandArray, start: 8 + 4, length: LENGTH_SIZE) // Длина всего пакета данных
            
            totalLength = Int(STRING_LEN)
            dataArray = commandArray
            dataArray.removeFirst(8 + 4 + 4) // Удаляем коммандную часть первой части пакета
            lastLength = totalLength - dataArray.count // Полученная длина данных

// DEBUG: Отладочная информация
//            NSLog("Извлечённые данные:")
//            for element in dataArray { print(String(format:"%02X ", element), terminator: "") }
//            print()
            
            if dataArray.count == 0 {
                isPacketContinue = false
                enumCommandPublished = .VS_DATA_BUF_empty // Следующая команда с задержкой!
            }

        } else { // у нас продолжение пакета
            dataArray = dataArray + commandArray
            lastLength = totalLength - dataArray.count // Оставшаяся часть данных
            if lastLength < 1 {
                isPacketContinue = false
                
// DEBUG:
//                print("Полный пакет:")
//                for element in dataArray { print(String(format:"%02X ", element), terminator: "") }; print()
//                for element in dataArray { NSLog("%02X ", element) }
//                NSLog(dataArray.description)
                
//                let byteStr = String(decoding: dataArray, as: UTF8.self)
//                let byteStr = String(data: Data(dataArray), encoding: .windowsCP1251)

//                print(byteStr)
                
                decodeArray = dataArray
                
                var isError = false
                while decodeArray.count > 0 && !isError {
                    isError = decode_VS_DATA_BUF()
                }
                enumCommandPublished = .VS_DATA_BUF // Следующая команда
            }
        }
    }

    func decode_VS_DATA_BUF() -> Bool {
        
        var samplesNum: UInt32 = 1
        
            print() // для отделения значений фреймов в логах

            // Определяемся с номером фрейма
        let frameNum = decodeArray[0] // Байт номера фрейма для отслеживания целостности данных
            print("#\(frameNum)")
            decodeArray = Array(decodeArray.dropFirst(1)) // Убираем байт номера фрейма
        
        if frameLastNum == nil || frameLastNum! + 1 == frameNum {
            
            frameLastNum = frameNum // Меняем старый счётчик на новый
            if frameLastNum == 255 { frameLastNum = nil } // Сброс на 0 чтобы не превышать значение байта 255

            // Определяем число измерений в фрейме (пока только печатаем логах)
            let    RC_FRM_val = RC_FRM(rawValue: decodeArray[0]) // Байт мультиизмерений
            switch RC_FRM_val {
                case .RC_FRM_SINGLE_MEAS: print("RC_FRM_SINGLE_MEAS")
                case .RC_FRM_MEAS       : print("RC_FRM_MEAS")
                case .none              : print("RC_FRM ERROR")
                }
                decodeArray = Array(decodeArray.dropFirst(1))   // Убираем байт мультиизмерений
            
            // TODO: Определиться с парсингом данных – где UInt, где Int
            
            // Определяемся с группой структур
            let grpID = decodeArray[0] // Идентификатор группы каналов
//            print("Код группы: \(String(format:"%02X ", grpID))")
            print("\(groupName(by: grpID)) Id = \(grpID)")
//            let RC_GRP_val = RC_GRP(rawValue: grpID)
                decodeArray = Array(decodeArray.dropFirst(1)) // Убираем байт группы структур
            
            // Определяемся со временем
            let ms: Int32 = intFrom(decodeArray);
            
//            for i in 0...3 { print(String(format:"%02X ", commandArray[i]), terminator: "") } // Печать в 16-тиричной форме
//            print()
// DEBUG: Печать полученного массива в 16-ричной форме
//            for element in decodeArray { print(String(format:"%02X ", element), terminator: "") }; print()

            let measure = measureDate(Int(ms), startDate: baseDate!)
                timestump = measure // Для последующей записи в БД
            let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd MMM yyy H:mm:ss"
            let newDateStr = dateFormatter.string(from: measure)
            print("+ms = \(ms) (\(newDateStr))")
                decodeArray = Array(decodeArray.dropFirst(4))   // Убираем 4 байта милисекунд
                cur_ms_Time = Int(ms)
            
            // Делать это нужно после чтения заголовка RCFrame_Hd_t
            if RC_FRM_val == .RC_FRM_MEAS   { samplesNum = process_RCFrame_Meas_t() } // Число повторов
            else                            { samplesNum = 1 }
//            print("START decodeArray.count = \(decodeArray.count)")

            for _ in 1...samplesNum {
                process_CHN(grp: grpID)
//                print("END decodeArray.count = \(decodeArray.count)")
            }
            return false
        } else {
//            for element in decodeArray { print(String(format:"%02X ", element), terminator: "") }; print()
            return true
        }
    }
    
/// Обработка серий данных
    func process_RCFrame_Meas_t() -> UInt32 {
        
        // Количество отсчетов во фрейме
        let samplesNumArray: [UInt8] = [decodeArray[0], decodeArray[1], UInt8(0), UInt8(0)] // Последние байты для совместимости
        let samplesNum: UInt32 = intFrom(samplesNumArray);    print("SamplesNum = \(samplesNum)")
        decodeArray = Array(decodeArray.dropFirst(2))   // Убираем 2 байта отсчетов во фрейме

        // Кол-во милисекунд между соседними отсчетами (период выборки)
        let smplTime_msArray: [UInt8] = [decodeArray[0], decodeArray[1], decodeArray[2], decodeArray[3]]
        let smplTime_ms: UInt32 = intFrom(smplTime_msArray);  print("SmplTime_ms = \(smplTime_ms)")
        decodeArray = Array(decodeArray.dropFirst(4))   // Убираем 4 байта Кол-во милисекунд
        
        return samplesNum
    }
    
    /// Процесс обработки каналов указанного ID группы согласно заявленному в строке конфигурации (DataStructure в CoreData)
    /// Оттуда вызываются процедуры парсинга каждого канала отдельно. Затем в конце запускается процесс обработки группы process_GRP
    func process_CHN(grp: UInt8) {
        groupCountPublished = groupCountPublished == nil ? 0 : groupCountPublished! + 1
        
        if  let channels = DataStrucktureCHN.channelSequence(for: grp, in: viewContext) {
            for channel in channels {
                let RC_CHN_val = RC_ChannelId(rawValue: UInt8(channel.id)) // Определяем канал
                switch RC_CHN_val {
                case .RC_CHN_FLAGS         : process_RC_CHN_FLAGS        (channel)    // Маска флагов
                case .RC_CHN_TIME          : process_RC_CHN_TIME         (channel)    // Метка времени
                case .RC_CHN_COUNT         : process_RC_CHN_COUNT        (channel)    // Количество зарегистрированных частиц
                case .RC_CHN_COUNT_RATE    : process_RC_CHN_COUNT_RATE   (channel)    // Скорость счета
                case .RC_CHN_DOSE_RATE     : process_RC_CHN_DOSE_RATE    (channel)    // Мощность дозы
                case .RC_CHN_DOSE_RATE_ERR : process_RC_CHN_DOSE_RATE_ERR(channel)    // Случайная ошибка мощности дозы
                case .RC_CHN_DURATION      : process_RC_CHN_DURATION(channel)         // Длительность
                case .RC_CHN_DOSE          : process_RC_CHN_DOSE(channel)             // Доза
                case .RC_CHN_TEMPERATURE   : process_RC_CHN_TEMPERATURE(channel)      // Температура
                case .RC_CHN_BTRY_CHRG     : process_RC_CHN_BTRY_CHRG(channel)        // Уровень заряда аккумулятора
                case .RC_CHN_ACC_X         : process_RC_CHN_ACC_X(channel)            // Ускорение вдоль оси X
                case .RC_CHN_ACC_Y         : process_RC_CHN_ACC_Y(channel)            // Ускорение вдоль оси Y
                case .RC_CHN_ACC_Z         : process_RC_CHN_ACC_Z(channel)            // Ускорение вдоль оси Z
                case .RC_CHN_OPTIC         : process_RC_CHN_OPTIC(channel)            // Данные оптического сенсора
                case .RC_CHN_EVENT         : process_RC_CHN_EVENT(channel)            // Событие
                case .RC_CHN_EVENT_CHN_MSK : process_RC_CHN_EVENT_CHN_MSK(channel)    // Маска каналов, передаваемых вместе с событием
                case .RC_CHN_COUNT_RATE_ERR: process_RC_CHN_COUNT_RATE_ERR(channel)   // Случайная ошибка скорости счета
                default:
                    print("ОШИБКА: Неизвестный канал данных ")
                }
            }
            process_GRP(grp)
        } else {    print("ОШИБКА: Неизвестная группа данных ") }
    }

    // MARK: - Обработка групп

    /// Процесс обработки группы каналов на основе данных, полученных в результате парсинга каналов группы
    func process_GRP(_ id: UInt8) {
        let RC_GRP_val = RC_GRP(rawValue: id) // Определяем канал
        switch RC_GRP_val {
            case .RC_GRP_COUNT_RATE      :processRC_GRP_COUNT_RATE()
            case .RC_GRP_DOSE_RATE       :processRC_GRP_DOSE_RATE()
            case .RC_GRP_DOSE_RATE_DB    :saveRC_GRP_DB(id)
            case .RC_GRP_RARE_DATA       :saveRC_GRP_DB(id)
            case .RC_GRP_USER_DATA       :processCurGRP()
            case .RC_GRP_SHEDULE_DATA    :processCurGRP()
            case .RC_GRP_ACCEL_DATA      :processCurGRP()
            case .RC_GRP_EVENT           :saveRC_GRP_DB(id)
            case .RC_GRP_RAW_COUNT_RATE  :processCurGRP()
            case .RC_GRP_RAW_DOSE_RATE   :processCurGRP()
            default:processCurGRP()
                print("ОШИБКА: Неизвестная группа данных ")
        }
    }
    
    func processCurGRP() {}  // Пропускаем…
    
    func processRC_GRP_COUNT_RATE() {
                countRatePublished = RC_GRP_COUNT_RATE(msTime: cur_ms_Time, chnCountRate: Double(float32RC_CHN_COUNT_RATE!))
    }
    
    func processRC_GRP_DOSE_RATE() {
        doseRatePublished = RC_GRP_DOSE_RATE(msTime: cur_ms_Time, chnDoseRate: Double(float32RC_CHN_DOSE_RATE!) * 1000.0)
    }
    
    func saveRC_GRP_DB(_ id: UInt8) {
        new_log = Log(context: viewContext) // Создаём событие даже если оно пустое (будет индикатор неизвестного события)
        new_log?.timestamp = timestump
        new_log?.device = Device.findConnectedDevice(in: viewContext)
        if  let channels = DataStrucktureCHN.channelSequence(for: id, in: viewContext) {
            // Делаем так, т.к. мы "не знаем" сколько здесь каналов
            for channel in channels {
                let RC_CHN_val = RC_ChannelId(rawValue: UInt8(channel.id)) // Определяем канал
                switch RC_CHN_val {
                case .RC_CHN_FLAGS         : save_RC_CHN_FLAGS          ()      // Маска флагов
                case .RC_CHN_TIME          : save_RC_CHN_TIME           ()      // Метка времени
                case .RC_CHN_COUNT         : save_RC_CHN_COUNT          ()      // Количество зарегистрированных частиц
                case .RC_CHN_COUNT_RATE    : save_RC_CHN_COUNT_RATE     ()      // Скорость счета
                case .RC_CHN_DOSE_RATE     : save_RC_CHN_DOSE_RATE      ()      // Мощность дозы
                case .RC_CHN_DOSE_RATE_ERR : save_RC_CHN_DOSE_RATE_ERR  ()      // Случайная ошибка мощности дозы
                case .RC_CHN_DURATION      : save_RC_CHN_DURATION       ()      // Длительность
                case .RC_CHN_DOSE          : save_RC_CHN_DOSE           ()      // Доза
                case .RC_CHN_TEMPERATURE   : save_RC_CHN_TEMPERATURE    ()      // Температура
                case .RC_CHN_BTRY_CHRG     : save_RC_CHN_BTRY_CHRG      ()      // Уровень заряда аккумулятора
                case .RC_CHN_ACC_X         : save_RC_CHN_ACC_X          ()      // Ускорение вдоль оси X
                case .RC_CHN_ACC_Y         : save_RC_CHN_ACC_Y          ()      // Ускорение вдоль оси Y
                case .RC_CHN_ACC_Z         : save_RC_CHN_ACC_Z          ()      // Ускорение вдоль оси Z
                case .RC_CHN_OPTIC         : save_RC_CHN_OPTIC          ()      // Данные оптического сенсора
                case .RC_CHN_EVENT         : save_RC_CHN_EVENT          ()      // Событие
                case .RC_CHN_EVENT_CHN_MSK : save_RC_CHN_EVENT_CHN_MSK  ()      // Маска каналов, передаваемых вместе с событием
                case .RC_CHN_COUNT_RATE_ERR: save_RC_CHN_COUNT_RATE_ERR ()      // Случайная ошибка скорости счета
                default:
                    print("ОШИБКА: Неизвестный канал данных ")
                }
            }
            logRateDB   = nil // Индикатор то, что группа для занесения в БД уже обработана
            logEvent    = nil // Индикатор то, что группа событий уже обработана
            logRareData = nil // Индикатор то, что группа редких данных уже обработана
        }
    }
    
    // MARK: - Сохранение каналов
    
    func save_RC_CHN_FLAGS() {
        new_log?.flags = Int32(uint_16RC_CHN_Flags!)
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_TIME() {}
    
    func save_RC_CHN_COUNT() { // Создаём событие с счётчиком
        if logRateDB == nil {
           logRateDB = LogRateDB(context: viewContext)
        }
        logRateDB?.count = Int32(uint_32RC_CHN_COUNT!)
        logRateDB?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_COUNT_RATE() { // Создаём событие со скоростью счёта
        if logRateDB == nil {
           logRateDB = LogRateDB(context: viewContext)
        }
        logRateDB?.count_rate = Float32(float32RC_CHN_COUNT_RATE!)
        logRateDB?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_DOSE_RATE() {
        if logRateDB == nil {
           logRateDB = LogRateDB(context: viewContext)
        }
        logRateDB?.dose_rate = Float32(float32RC_CHN_DOSE_RATE!)
        logRateDB?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_DOSE_RATE_ERR() {
        if logRateDB == nil {
           logRateDB = LogRateDB(context: viewContext)
        }
        logRateDB?.dose_rate_err = Float32(float32RC_CHN_DOSE_RATE_ERR!)
        logRateDB?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_DURATION() {
        if logRareData == nil {
           logRareData = LogRareData(context: viewContext)
        }
        logRareData?.duration = Int32(uint_32RC_CHN_DURATION!)
        logRareData?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_DOSE() {
        if logRareData == nil {
           logRareData = LogRareData(context: viewContext)
        }
        logRareData?.dose = Float32(float32RC_CHN_DOSE!)
        logRareData?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_TEMPERATURE() {
        if logRareData == nil {
           logRareData = LogRareData(context: viewContext)
        }
        logRareData?.temperature = Float32(float32RC_CHN_TEMPERATURE!)
        logRareData?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_BTRY_CHRG() {
        if logRareData == nil {
           logRareData = LogRareData(context: viewContext)
        }
        logRareData?.charge = Float32(float32RC_CHN_BTRY_CHRG!)
        logRareData?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_ACC_X() {}
    func save_RC_CHN_ACC_Y() {}
    func save_RC_CHN_ACC_Z() {}
    func save_RC_CHN_OPTIC() {}
    
    func save_RC_CHN_EVENT() {
        if logEvent == nil {
           logEvent = Event(context: viewContext)
        }
        logEvent?.event = Int32(uint_32RC_CHN_COUNT!)
        logEvent?.log = new_log
        PersistenceController.shared.saveContext()
    }
    
    func save_RC_CHN_EVENT_CHN_MSK() {}
    func save_RC_CHN_COUNT_RATE_ERR() {}

    
    // MARK: - Обработка каналов
    // TODO: сделать проверку на минимум и максимум MAxVal MinVal
    
/// Удаление из начала массива `decodeArray` уже обработанных данных
/// - Parameter channel: Текущий канал типа `CoreData entity` из виртуальной строки
///  [VS_DATA_BUF](http://127.0.0.1:49541/rc_ms_results.htm).
///
    func deleteItemsFromDecodeArray(for channel: DataStrucktureCHN) {
        if let channelDataSize = channelDataSize(channel.d_type) {      // Если в канале найден тип данных
            decodeArray = Array(decodeArray.dropFirst(channelDataSize)) // Убирам из массива байты канала
        } else {
            print("ОШИБКА: Неизвестный тип канала данных")
        }
    }
    
/// Изъятие из массива потока *decodeArray* в массива данных канала
    func channelDataArray(_ channel: DataStrucktureCHN) -> [UInt8] {
        // TODO: Сделать проверку на nil для всех size
        let streamSize = channelDataSize(channel.d_type)    // Размер данных из потока  прибора
        let store_Size = channelDataSize(channel.r_type)    // Размер данных для хранения в приложении
        var dataArray: [UInt8] = []                         // Байтовый массив для преобразования данных
        for index in 0...(streamSize! - 1) {                // По числу байт в потоке
            dataArray.append(decodeArray[index])            // Добавляем в массив
        }
        let delta_Size = store_Size! - streamSize!          // Разница типов в байтах
        if  delta_Size > 0 {                                // Только если разница размеров типов больше нуля!
            for _ in 0...(delta_Size - 1) {                 // По числу байт в потоке
                dataArray.append(UInt8(0))                  // Добавляем в массив пустые байты для конвертации
            }
        }
            deleteItemsFromDecodeArray(for: channel)        // Удаляем обработанные байты массива потока
        return dataArray                                    // Возвращаем отрезанный массив данных канала
    }
    
    /// Исполнение заложенного в конфигурацию арифметического выражения P1 • X + P2
    func exprChannelRestore(_ channel: DataStrucktureCHN, val: Double) -> Double {
        return channel.p1 * val + channel.p2
    }
    
    /// Пропускаем неизвестный нам поток данных согласно правилам строки конфигурации
    func processUnknown(_ groupID: UInt8) {
        print("Пропускаем неизвестную группу данных ")
        if  let channels = DataStrucktureCHN.channelSequence(for: groupID, in: viewContext) {
            for channel in channels {
                deleteItemsFromDecodeArray(for: channel)
            }
        } else {
            print("ОШИБКА: Неизвестная группа данных ")
        }
    }
    
/// Обработка канала флагов *`RC_CHN_FLAGS`*
    func process_RC_CHN_FLAGS(_ channel: DataStrucktureCHN) { print("RC_CHN_FLAGS")
        let channelArray = channelDataArray(channel)      // Получаем массив данных канала
        // TODO: Сделать проверку на несоответствие хранимых типов конфигурации и приложения
            uint_16RC_CHN_Flags = intFrom(channelArray);      // Превращаем в целое
            printBin(UInt32(uint_16RC_CHN_Flags!))              // DEBUG: печать флагов в двоичном виде
    }
    
    func process_RC_CHN_TIME(_ channel: DataStrucktureCHN) { print("RC_CHN_TIME")
        deleteItemsFromDecodeArray(for: channel)            // Удаляем обработанные байты массива потока
    }
    
/// Обработка канала **Количество импульсов детектора за время измерения** *`RC_CHN_COUNT`*
    func process_RC_CHN_COUNT(_ channel: DataStrucktureCHN) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        uint_32RC_CHN_COUNT = intFrom(channelArray);
        print("CHN_Count = \(uint_32RC_CHN_COUNT!)")
    }
    
/// Обработка канала **Значение скорости счета детектора SiPM** `RC_CHN_COUNT_RATE`
    func process_RC_CHN_COUNT_RATE(_ channel: DataStrucktureCHN) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        let data = Data(channelArray)
            float32RC_CHN_COUNT_RATE = data.withUnsafeBytes { $0.load(as: Float.self) }
            print(String(format: "CHN_CountRate =  %.2f имп/с", float32RC_CHN_COUNT_RATE!))
    }
    
/// Обработка канала **Значение мощности дозы** *`RC_CHN_DOSE_RATE`*
    func process_RC_CHN_DOSE_RATE(_ channel: DataStrucktureCHN) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        let data = Data(channelArray)
            float32RC_CHN_DOSE_RATE = data.withUnsafeBytes { $0.load(as: Float.self) }  * 10 // FIXME: А это правильно умножать на 10???
            print(String(format: "CHN_DoseRate =  %.2f мкР/ч", float32RC_CHN_DOSE_RATE! * 1000))// Заодно переводим в мкР/ч FIXME: может не стоит для хранения
    }
    
/// Обработка канала **Случайная ошибка мощности дозы** *`RC_CHN_DOSE_RATE_ERR`*
    func process_RC_CHN_DOSE_RATE_ERR(_ channel: DataStrucktureCHN) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        let doseRateErr: UInt32 = intFrom(channelArray);
        if channel.expr == 1 {
            float32RC_CHN_DOSE_RATE_ERR = Float32( exprChannelRestore(channel, val: Double(doseRateErr)))
        } else {
            float32RC_CHN_DOSE_RATE_ERR = Float32(doseRateErr)
        }
        print("CHN_DoseRateErr = \(float32RC_CHN_DOSE_RATE_ERR!)")
    }
    
    func process_RC_CHN_DURATION(_ channel: DataStrucktureCHN) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        uint_32RC_CHN_DURATION = intFrom(channelArray)
        print("CHN_Duration = \(uint_32RC_CHN_DURATION!) с")
    }
    
    func process_RC_CHN_DOSE(_ channel: DataStrucktureCHN) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        let data = Data(channelArray)
        float32RC_CHN_DOSE = data.withUnsafeBytes { $0.load(as: Float32.self) }
        print(String(format: "CHN_Dose =  %.2f Р", float32RC_CHN_DOSE!))
    }
    
    func process_RC_CHN_TEMPERATURE(_ channel: DataStrucktureCHN) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        let temperature: Int16 = intFrom(channelArray)
        if channel.expr == 1 {
            float32RC_CHN_TEMPERATURE = Float32( exprChannelRestore(channel, val: Double(temperature)))
        } else {
            float32RC_CHN_TEMPERATURE = Float32(temperature)
        }
        print("CHN_Temperature = \(float32RC_CHN_TEMPERATURE!)°C")
    }
    
    func process_RC_CHN_BTRY_CHRG(_ channel: DataStrucktureCHN) {
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        let charge: UInt32 = intFrom(channelArray)
        float32RC_CHN_BTRY_CHRG = channel.expr == 1 ? Float32(exprChannelRestore(channel, val: Double(charge))) : Float32(charge)
        print("CHN_ChargeLevel = \(float32RC_CHN_BTRY_CHRG!)%")
    }
    
    func process_RC_CHN_ACC_X(_ channel: DataStrucktureCHN) { print("RC_CHN_ACC_X")
        deleteItemsFromDecodeArray(for: channel)
    }
    
    func process_RC_CHN_ACC_Y(_ channel: DataStrucktureCHN) { print("RC_CHN_ACC_Y")
        deleteItemsFromDecodeArray(for: channel)
    }
    
    func process_RC_CHN_ACC_Z(_ channel: DataStrucktureCHN) { print("RC_CHN_ACC_Z")
        deleteItemsFromDecodeArray(for: channel)
    }
    
    func process_RC_CHN_OPTIC(_ channel: DataStrucktureCHN) { print("RC_CHN_OPTIC")
        deleteItemsFromDecodeArray(for: channel)
    }
    
    func process_RC_CHN_EVENT(_ channel: DataStrucktureCHN) { print("RC_CHN_EVENT")
        let channelArray = channelDataArray(channel) // Получаем массив данных канала
        uint_32RC_CHN_EVENT = intFrom(channelArray)
    }
    
    func process_RC_CHN_EVENT_CHN_MSK(_ channel: DataStrucktureCHN) { print("RC_CHN_EVENT_CHN_MSK")
        deleteItemsFromDecodeArray(for: channel)
    }
    
    func process_RC_CHN_COUNT_RATE_ERR(_ channel: DataStrucktureCHN) { print("RC_CHN_COUNT_RATE_ERR")
        deleteItemsFromDecodeArray(for: channel)
    }

}
