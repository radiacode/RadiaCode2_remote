//
//  VertBarView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 27.06.2022.
//

import SwiftUI

struct VertBarView: View {
    
    @EnvironmentObject var options: HardOptions
    @Environment(\.managedObjectContext)
               private var viewContext
    @State     private var systemImageNameStr = "square.split.1x2.fill"     // 􀕱 Символ (двух графиков) по-умолчанию
    @Binding           var curDiagramSelect: ShowDiagram                    // Передаём выбранный тип отображения графиков
    
    var body: some View {
        VStack {
            Group { // Набор кнопок масштабирования справа от графиков
                Button(action: {  changeDiagram()                            }) { Image(systemName: systemImageNameStr)                          .imageScale(.large) } // Переключение между графиками 􀪚􀪛􀕱
                    .padding(EdgeInsets(top: 20, leading: 0, bottom: 0, trailing: 0))
                Button(action: {                        gobackward()         }) { Image(systemName: "gobackward")                                .imageScale(.large) } // 􀎀 Возвращаем назад
                Button(action: {           chevron_backward_square()         }) { Image(systemName: "chevron.backward.square")                   .imageScale(.large) } // 􀯹 Сдвигаем влево
                Button(action: { rectangle_arrowtriangle_2_inward()          }) { Image(systemName: "rectangle.arrowtriangle.2.inward")          .imageScale(.large) } // 􀫺 Сжимаем вертикально
                Button(action: { rectangle_arrowtriangle_2_outward()         }) { Image(systemName: "rectangle.arrowtriangle.2.outward")         .imageScale(.large) } // 􀫹 Растягиваем вертикально
                Button(action: { rectangle_portrait_arrowtriangle_2_outward()}) { Image(systemName: "rectangle.portrait.arrowtriangle.2.outward").imageScale(.large) } // 􀫻 Приближаем по оси времени
                Button(action: { rectangle_portrait_arrowtriangle_2_inward() }) { Image(systemName: "rectangle.portrait.arrowtriangle.2.inward") .imageScale(.large) } // 􀫼 Растягиваем по оси времени
                Button(action: { chevron_forward_square()})                     { Image(systemName: "chevron.forward.square")                    .imageScale(.large) } // 􀯾 Сдвигаем вправо
            }.padding(EdgeInsets(top: 0, leading: 0, bottom: 5, trailing: 0))
            CapsuleDiagramView(
                maxVal: 200000,
                minVal: 0.01,
                curVal: options.grpDoseRateLast * 100,
                alarm1: Double(options.LEV1_uR_h) ,
                alarm2: Double(options.LEV2_uR_h)
            )
                .frame(width: 25)
                .background(.gray)
        }.background(.white)
    }
    
    // MARK: - Вспомогательные функции
    
    // MARK: Управление кнопками
    // TODO: Перенести в 'Options'
    
    private func gobackward() { // 􀎀 Возвращаем назад
        
        options.isConnectDiagrammToCurDate = true
        
        options.curTimeScaleRange = 30 * 60
        options.curTo__DiagramDate = Date()
        options.countRateDB = Log.getCountRateDB(in: viewContext, from: options.curFromDiagramDate!, to: options.curFromDiagramDate!)
    }
        
    private func rectangle_arrowtriangle_2_inward() { // 􀫺 Сжимаем вертикально
        options.curMaxCountRateValDB = options.curMaxCountRateValDB! * 1.2
        options.defineCountRateValAxis_Lite(maxVal: Double(options.curMaxCountRateValDB!))
        
        options.curMaxDoseRateValDB = options.curMaxDoseRateValDB! * 1.2
        options.defineDoseRateValAxis_Lite(maxVal: Double(options.curMaxDoseRateValDB!))
    }
    
    private func rectangle_arrowtriangle_2_outward() { // 􀫹 Растягиваем вертикально
        options.curMaxCountRateValDB = options.curMaxCountRateValDB! * 0.8
        options.defineCountRateValAxis_Lite(maxVal: Double(options.curMaxCountRateValDB!))
        
        options.curMaxDoseRateValDB = options.curMaxDoseRateValDB! * 0.8
        options.defineDoseRateValAxis_Lite(maxVal: Double(options.curMaxDoseRateValDB!))
    }
    
    private func rectangle_portrait_arrowtriangle_2_outward() { // 􀫻 Приближаем по оси времени
        
//            options.isConnectDiagrammToCurDate = false

 //     let rangeDate = getMagnificationDateRange2(from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!, on: 0.8)
        let rangeDate = getMagnificationDateStep  (from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!, on: options.curTimeStepScale, isCompressed: false)
            options.curTimeStepScale = options.curTimeStepScale.pred()
        if  rangeDate.toDate > Date() || options.isConnectDiagrammToCurDate {  // чтобы не забегать вперёд по времени
//            options.isConnectDiagrammToCurDate = true
            options.curTo__DiagramDate = Date()
            options.curTimeScaleRange = Double(options.curTimeStepScale.rawValue)
        } else {
//            let diffs = Calendar.current.dateComponents([.second], from: rangeDate.fromDate, to: rangeDate.toDate)
//            if  diffs.second! > Units.minute.rawValue * 10 { // Временная блокировка от уменьшения размера оси времени меньше 10 минут (нет данных для графика)
//                options.curTo__DiagramDate = rangeDate  .toDate
//                options.curTimeScaleRange   = Double(options.curTimeStepScale.rawValue)
//            }
            options.curTo__DiagramDate = rangeDate  .toDate
            options.curTimeScaleRange   = Double(options.curTimeStepScale.rawValue)

        }
//            options .dose_RateDB = Log.getDoseRateDB (in: viewContext, from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!)// Управление диапазоном мощности дозы
//            options.countRateDB = Log.getCountRateDB(in: viewContext, from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!)// Управление диапазоном скорости счёта
        
        options.defineOutOffBordersForTimeRange()
    }
    
    private func rectangle_portrait_arrowtriangle_2_inward() { // 􀫼 Сжимаем по оси времени
        
            options.isConnectDiagrammToCurDate = false
        
//      let rangeDate = getMagnificationDateRange2(from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!, on: 1.2)
        let rangeDate = getMagnificationDateStep  (from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!, on: options.curTimeStepScale, isCompressed: true)
            options.curTimeStepScale = options.curTimeStepScale.next()
        if  rangeDate.toDate > Date() {  // чтобы не забегать вперёд по времени
            options.isConnectDiagrammToCurDate = true
            options.curTo__DiagramDate = Date()
            options.curTimeScaleRange = Double(options.curTimeStepScale.rawValue)
        } else {
            options.curTo__DiagramDate = rangeDate.toDate
            options.curTimeScaleRange  = Double(options.curTimeStepScale.rawValue)
        }
//            options .dose_RateDB = Log.getDoseRateDB (in: viewContext, from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!)
//            options.countRateDB = Log.getCountRateDB(in: viewContext, from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!)
        
        options.defineOutOffBordersForTimeRange()
    }
    
    private func chevron_forward_square() { // 􀯾 Сдвигаем вправо
        
            options.isConnectDiagrammToCurDate = false
        
        let rangeDate = getShiftDateRange2(from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!, on: 0.1)
        
        if  rangeDate.toDate > Date() {  // чтобы не забегать вперёд по времени
            options.isConnectDiagrammToCurDate = true
            let     diffs = Calendar.current.dateComponents([.second], from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!)
            options.curTo__DiagramDate = Date()
            options.curTimeScaleRange  = Double(diffs.second!)
        } else {
            let     diffs = Calendar.current.dateComponents([.second], from: rangeDate.fromDate, to: rangeDate.toDate)
            options.curTo__DiagramDate = rangeDate  .toDate
            options.curTimeScaleRange  = Double(diffs.second!)
        }
//            options.dose_RateDB = Log.getDoseRateDB(in: viewContext, from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!)
//            options.countRateDB = Log.getCountRateDB(in: viewContext, from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!)
        
        options.defineOutOffBordersForTimeRange()
    }
    
    private func   chevron_backward_square() { // 􀯹 Сдвигаем влево
        
                options.isConnectDiagrammToCurDate = false
            let rangeDate = getShiftDateRange2(from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!, on: -0.1)
            let     diffs = Calendar.current.dateComponents([.second], from: rangeDate.fromDate, to: rangeDate.toDate)
                options.curTimeScaleRange  = Double(diffs.second!)
                options.curTo__DiagramDate = rangeDate  .toDate
            
//                options.dose_RateDB  = Log.getDoseRateDB(in: viewContext, from: options.curFromDiagramDate!,  to: options.curTo__DiagramDate!)
//                options.countRateDB = Log.getCountRateDB(in: viewContext, from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!)
        
        options.defineOutOffBordersForTimeRange()
    }

    
    /// Выбор следующего символа кнопки в зависимости цикла нажатия (нижний график/верхний график/оба графика)
    private func changeDiagram() {
        switch curDiagramSelect {
        case .both : curDiagramSelect = .dose   ;systemImageNameStr = "square.bottomhalf.filled"    // 􀪚
        case .dose : curDiagramSelect = .count  ;systemImageNameStr = "square.tophalf.filled"       // 􀪛
        case .count: curDiagramSelect = .both   ;systemImageNameStr = "square.split.1x2.fill"       // 􀕱
        }
    }
}

struct VertBarView_Previews: PreviewProvider {
    static let options = HardOptions()
    static var previews: some View {
        VertBarView(curDiagramSelect: .constant(.both))
            .environmentObject(options)
            .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
            .previewLayout(.fixed(width: 40, height: 600))
    }
}
