//
//  CapsuleDiagramView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 20.06.2022.
//

import SwiftUI

struct CapsuleDiagramView: View {
    
   var maxVal: Double
   var minVal: Double
   var curVal: Double
   var alarm1: Double
   var alarm2: Double
    
    @EnvironmentObject var options: HardOptions

    var body: some View {
        
        GeometryReader { reader in
            
            let h = reader.size.height //
            
            let maxValLog: CGFloat = log10(maxVal)
            let alarm2Log: CGFloat = log10(alarm2)
            let alarm1Log: CGFloat = log10(alarm1)
            let curValLog: CGFloat = log10(curVal)
            let minValLog: CGFloat = log10(minVal)
 
            let fullRange = maxValLog - minValLog
            
            // FIXME: Должна быть обратная связь размера прямоугольника, а не жёстко 350
//            var rectH =  Double(400)   // Высота прямоугольника формы (reader.size.height)
            let scale = h / fullRange


//            let rectHeight   = (maxValLog - minValLog) * scale
            let alarm2Height = (alarm2Log - minValLog) * scale
            let alarm1Height = (alarm1Log - minValLog) * scale
            let curValHeight = (curValLog - minValLog) * scale
//                curValHeight = curValHeight < 0 ? 0 : curValHeight


            ZStack {
                VStack {
                    Spacer()
                    RoundedRectangle(cornerRadius: 5, style: .continuous)
                         .fill(.green)
                         .frame(width: 25, height: curValHeight)
//                         .animation(.linear(duration: 0.3), value: curValHeight )
                }
                Rectangle()
                    .frame(width: 25, height: 3)
                    .offset(x: 0, y: (-h / 2.0) + 1.5 )
                Rectangle()
                    .frame(width: 25, height: 3)
                    .offset(x: 0, y: (h / 2.0) - 1.5 )

                Rectangle()
                    .fill(.yellow)
                    .frame(width: 25, height: 3)
                    .offset(x: 0, y: (h / 2.0 - 1.5) - alarm1Height )
                Rectangle()
                    .fill(.red)
                    .frame(width: 25, height: 3)
                    .offset(x: 0, y: (h / 2.0 - 1.5) - alarm2Height )

//                Text(options.curUnitVal == 0 ? "μR/h" : "μSv/h" )
//                    .fontWeight(.thin)
//                    .font(.system(size: 9))
//                    .offset(x: 0, y: (h / 2.0) - 18)
                Group {
                    Text("\(options.getOptimalVarUnitStr(roentgenVal: Double(alarm2/1000_000.0), isSpace: true, isHour: true))").font(.system(size: 7))
                        .foregroundColor(Color(hue: 1.0, saturation: 0.107, brightness: 0.954))
                        .multilineTextAlignment(.center)
                        .lineLimit(2)
                        .offset(x: 0, y: (h / 2.0 - 1.5) - alarm2Height + 12)
                        .frame(width: 25, height: 25)
                    Text("\(options.getOptimalVarUnitStr(roentgenVal: Double(alarm1/1000_000.0), isSpace: true, isHour: true))").font(.system(size: 7))
                        .foregroundColor(Color.yellow)
                        .multilineTextAlignment(.center)
                        .lineLimit(2)
                        .offset(x: 0, y: (h / 2.0 - 1.5) - alarm1Height + 12)
                        .frame(width: 25, height: 25)
//                    Text("\(String(format: "%.1f", options.curUnitVal == 0 ? alarm1 : alarm1 / 100))")//  .fontWeight(.thin)
                    Text("\(options.getOptimalVarUnitStr(roentgenVal: Double(curVal/1000_000.0), isSpace: true, isHour: true))").font(.system(size: 7))
                        .lineLimit(2)
                        .offset(x: 0, y: (h / 2.0 - 1.5) - curValHeight + 12)
                }
                .lineLimit(1)
                .font(.system(size: 9))
                .fontWeight(.thin)
            }
        }
    }
}

struct CapsuleDiagramView_Previews: PreviewProvider {
    static let options = HardOptions()
                
    static var previews: some View {
        options.curUnitVal = 1
        return Group {
            CapsuleDiagramView(
                maxVal: 2_0000,
                minVal: 0.0001,
                curVal: 0.001,
                alarm1: 300,
                alarm2: 0.006)
            .environmentObject(options)
            .previewLayout(.fixed(width: 25, height: 300))
            CapsuleDiagramView(
                maxVal: 2_0000,
                minVal: 0.000001,
                curVal: 0.00002,
                alarm1: 0.00005,
                alarm2: 0.00015)
            .environmentObject(options)
            .previewLayout(.fixed(width: 25, height: 300))

        }
    }
}
