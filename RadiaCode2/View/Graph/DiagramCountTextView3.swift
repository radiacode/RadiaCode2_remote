//
//  DiagramCountTextView3.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 06.06.2022.
//

import SwiftUI

struct DiagramCountTextView3: View {

    @EnvironmentObject var options: HardOptions

    var body: some View {
        GeometryReader { reader in
            let rectH = Double(reader.size.height)   // Высота прямоугольника формы
            let step  = rectH / Double(options.curMaxDoseRateValDB!)
            ForEach(options.doseRatePoints3) { line in
                let h = rectH - step * Double(line.chnDoseRate)      // Текущая высота данных
                VStack {
                    if h - 16 > 0 { // Чтобы не вылезать за границы графика
                        Text("\(options.getOptimalVarUnitStr(roentgenVal: Double(line.chnDoseRate / 10000), isUnit: false))")
//                      Text(String(format:"%.2f", line.chnDoseRate))
                            .font(.system(size: 10))
                            .offset(x: 10, y: h - 16)
                    }
                }
            }
        }
    }
}

struct DiagramCountTextView3_Previews: PreviewProvider {
    static var previews: some View {
        DiagramCountTextView3()
    }
}
