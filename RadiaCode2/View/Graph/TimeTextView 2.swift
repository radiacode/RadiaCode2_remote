//
//  TimeTextView.swift
//  RadiaCodeConnection
//
//  Created by Андрей Левчик on 09.11.2021.
//

import SwiftUI

struct TimeTextView: View {
    
    @EnvironmentObject var options: HardOptions

    var body: some View {
        
        GeometryReader { reader in

            let rect_W    : CGFloat = reader.size.width      // Высота прямоугольника формы
            let rect_H    : CGFloat = reader.size.height     // Ширина прямоугольника формы
            let scaleW  = rect_W / CGFloat(options.axisXTimeSecRange)

            ForEach (0..<30) { n in
                ZStack {
                    if  (n + options.startT) % 5 == 0 {
                        let w = scaleW * Double(n) - 10
                        HStack {
                                Text("\(n + options.startT)")
                                        .font(.system(size: 15))
                                    .offset(x: w, y: rect_H - 30)
                        }
                    }
                }
            }
        }
    }
}

struct TimeTextView_Previews: PreviewProvider {
    static var previews: some View {
        let options = HardOptions()
            options.grpCountRateArray = [
                RC_GRP_COUNT_RATE(msTime: -1_500, chnCountRate: 3.09),
                RC_GRP_COUNT_RATE(msTime:    500, chnCountRate: 4.09),
                RC_GRP_COUNT_RATE(msTime:  1_000, chnCountRate: 5.09),
                RC_GRP_COUNT_RATE(msTime:  3_000, chnCountRate: 4.40),
                RC_GRP_COUNT_RATE(msTime:  4_000, chnCountRate: 5.12),
                RC_GRP_COUNT_RATE(msTime:  6_000, chnCountRate: 4.11),
                RC_GRP_COUNT_RATE(msTime:  7_000, chnCountRate: 6.16),
                RC_GRP_COUNT_RATE(msTime:  7_500, chnCountRate: 5.16),
                RC_GRP_COUNT_RATE(msTime:  9_500, chnCountRate: 4.16),
                RC_GRP_COUNT_RATE(msTime: 10_500, chnCountRate: 5.16)
                ]

        return TimeTextView()
            .environmentObject(options)
    }
}
