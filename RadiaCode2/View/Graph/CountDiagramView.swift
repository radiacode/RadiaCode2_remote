//
//  CountDiagramView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 21.06.2022.
//

import SwiftUI

struct CountDiagramView: View {
    @EnvironmentObject var options: HardOptions
    var body: some View {
        
        let maxCountTime = options.max_CountTime > options.max_RawCountTime ? options.max_CountTime : options.max_RawCountTime

        ZStack {
            HStack { // Нижняя часть
                DiagramCountTextView2()
                    .frame(maxWidth: 42)
                    .background(Color("AccentColor"))
                    .zIndex(1)

//                GraphDB_CountShape(options: self.options, counts: options.countRateDB ?? [] , max_Val: options.curMaxCountRateValDB!)
//                    .stroke(Color("OldCountColor"), lineWidth: 1)
                ZStack {
//                    DiagramTimeGridView(ticCount: 30, rangeMin: 30, shiftMin: -decimalShift)
                    DiagramTimeGridView2().offset(x: -7)
                    RawDoseDiagram1(
                        options: self.options,
                        dateArray: options.grpRawCountRateTimerArray,
                        max_Val: Double(options.curMaxCountRateValDB!),
                        min_Val: 0
                    )
                        .stroke(Color("GreyGraphColor"), lineWidth: 0.3)
                    RawDoseDiagram1(
                        options: self.options,
                        dateArray: options.grpCountRateTimerArray,
                        max_Val: Double(options.curMaxCountRateValDB!),
                        min_Val: 0,
                        colosePath: true
                    )
                        .foregroundColor(Color("NewCountFillColor"))
                    RawDoseDiagram1(
                        options: self.options,
                        dateArray: options.grpCountRateTimerArray,
                        max_Val: Double(options.curMaxCountRateValDB!),
                        min_Val: 0
                    )
                        .stroke(Color("NewCountColor"), lineWidth: 2)

                }
            } .background(Color("AccentColor"))
            DiagramCountGridView2()
            VStack {
                HStack {
                    Text("Count rate, cps")//.offset(x: 0, y: -25)// Мощность дозы
                        .font(.system(size: 15))
                        .offset(x: 46, y: 0)
                    Spacer()
                }
                HStack {
                    Spacer()
                    Text(String(format:"%.2f", options.grpCountRateLast))//
                        .font(.system(size: 30))
                        .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 10))
                }
                HStack {
                    Spacer()
                    Text("±\(String(format:"%.1f", options.grpCountErrLast))%")//
                        .font(.system(size: 20))
                        .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 10))
                }

                Spacer()
            }

        }
    }
}

struct DoseDiagramView_Previews: PreviewProvider {
    static let options = HardOptions()
    static var previews: some View {
        CountDiagramView()
            .environmentObject(options)
    }
}
