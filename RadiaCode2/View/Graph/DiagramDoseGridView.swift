//
//  DiagramDoseGridView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 06.06.2022.
//

import SwiftUI

struct DiagramDoseGridView: View {

    @EnvironmentObject var options: HardOptions

    var body: some View {
        GeometryReader { reader in
            let rectH = Double(reader.size.height)   // Высота прямоугольника формы
            let rectW = Double(reader.size.width)    // Ширина прямоугольника формы
            let step  = rectH / Double(options.curMaxDoseRateValDB!)
            ForEach(options.doseRatePoints3) { line in
                let h = rectH - step * Double(line.chnDoseRate)      // Текущая высота данных
                if h - 1 > 0 { // Чтобы не вылезать за границы графика
                    Path { path in
                        path.move(   to: CGPoint(x: 50,    y: h - 1))   // Начинаем с нижнего левого угла
                        path.addLine(to: CGPoint(x: rectW, y: h - 1))   // Ведём к данным линию
                    }
                    .stroke(Color("GridColor"), lineWidth: 1)
                }
            }
        }
    }
}

struct DiagramCountGridView_Previews: PreviewProvider {
    static var previews: some View {
        DiagramDoseGridView()
    }
}
