//
//  FullDiagramView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 08.04.2022.
//

import SwiftUI

struct FullDiagramView: View {
    
    @EnvironmentObject var options: HardOptions
    @Environment(\.managedObjectContext)
            private var viewContext
    
    @State var counts: [ValRateDB] = []
    @State private var offset     = CGSize.zero
    @GestureState  var magnifyBy  = 1.0
    @State private var curDiagramSelect = ShowDiagram.both

    var body: some View {

        let dragGesture = DragGesture()
            .onChanged { gesture in
                
                    options.isConnectDiagrammToCurDate = false
                                
                let lastOffset = offset
                    offset = gesture.translation
                let delta = lastOffset.width - offset.width
                let rate = delta / 300.0
                
                let rangeDate = getShiftDateRange2(from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!, on: rate)
                
                if  rangeDate.toDate > Date() {  // чтобы не забегать вперёд по времени
                    let  diffs = Calendar.current.dateComponents([.second], from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!)
                    options.isConnectDiagrammToCurDate = true
                    options.curTimeScaleRange  = Double(diffs.second!)
                    options.curTo__DiagramDate = Date()
                } else {
                    let  diffs = Calendar.current.dateComponents([.second], from: rangeDate.fromDate, to: rangeDate.toDate)
                    options.curTimeScaleRange  = Double(diffs.second!)
                    options.curTo__DiagramDate = rangeDate  .toDate
                }
//                    options.dose_RateDB  = Log.getDoseRateDB(in: viewContext, from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!)
//                    options.countRateDB = Log.getCountRateDB(in: viewContext, from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!)
                
                    options.defineOutOffBordersForTimeRange()
            }
            .onEnded { _ in
                offset = .zero
            }
        
        let magnificationGesture = MagnificationGesture()
        
            .updating($magnifyBy) { currentState, gestureState, transaction in
                
//                options.getTimeRangeForMagnificationGesture(magnifyBy: magnifyBy)
                
                options.isConnectDiagrammToCurDate = false

                gestureState = currentState
                print("Коэффициент приближения: \(magnifyBy)")
                
                let rangeDate = getMagnificationDateRange2(from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!, on: (1 / magnifyBy) )

                if  rangeDate.toDate > Date() {  // чтобы не забегать вперёд по времени
                    let diffs = Calendar.current.dateComponents([.second], from: rangeDate.fromDate, to: rangeDate.toDate)
                    options.isConnectDiagrammToCurDate = true
                    options.curTimeScaleRange  = Double(diffs.second!)
                    options.curTo__DiagramDate = Date()
                } else {
                    let diffs = Calendar.current.dateComponents([.second], from: rangeDate.fromDate, to: rangeDate.toDate)
                    if  diffs.second! >= Units.minute.rawValue * 10 {
                        options.curTimeScaleRange  = Double(diffs.second!)
                        options.curTo__DiagramDate = rangeDate  .toDate
                    }
                }
//                    options.dose_RateDB  = Log.getDoseRateDB(in: viewContext, from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!)
//                    options.countRateDB = Log.getCountRateDB(in: viewContext, from: options.curFromDiagramDate!, to: options.curTo__DiagramDate!)
                
                    options.defineOutOffBordersForTimeRange()
            }

        let combined = dragGesture.exclusively(before: magnificationGesture)

            VStack {
                if options.curMaxDoseRateValDB != nil {
                    HStack {
                        HStack {
                            VStack {
                                if curDiagramSelect == .both || curDiagramSelect == .count {
                                    CountDiagramView()
                                }
                                if curDiagramSelect == .both || curDiagramSelect == .dose {
                                    DoseDiagramView()
                                }
                            }
                            VertBarView(curDiagramSelect: $curDiagramSelect)
                                .background(Color("VertBarColor"))
                                .padding(EdgeInsets(top: 0, leading: -10, bottom: 0, trailing: 0))
                        }
                    }
                } else { Text("NO DATA") }
                HStack {
                    Group{
                        switch options.ticValUnits {
                            case .second: Text("Seconds")
                            case .minute: Text("Minutes")
                            case .hour  : Text("Hours")
                            case .day   : Text("Days")
                            case .month : Text("Monthes")
                            case .year  : Text("Years")
                        }
                    }
                        .frame(maxWidth: 40).font(.system(size: 10)).offset(x: 0, y: 5)
                    
                    TimeTextView2()
                        .frame(maxHeight: 12)
                        .offset(x: 0, y: -5)
                    Text("3")  // Заглушка для правильного выравнивания шкалы времени
                        .frame(width: 35, height: 12)
                        .foregroundColor(.clear)
                    
                }

                // Значения дат
                HStack {
                    Group{
                       Text("\((options.curFromDiagramDate ?? Date()), formatter: itemFormatter))")
                            .font(.system(size: 10))
                            .frame(maxWidth: 160, maxHeight: 40, alignment: .topLeading)
                            .offset(x: 16, y: 2)
                            .foregroundColor(Color("TimeTextColor"))
                        Spacer()
                        Text("\((options.curTo__DiagramDate ?? Date()), formatter: itemFormatter))")
                            .font(.system(size: 10))
                            .frame(maxWidth: 160, maxHeight: 40, alignment: .topLeading)
                            .offset(x: 53, y: 2)
                            .foregroundColor(Color("TimeTextColor"))
                    }
                }
                .background(.yellow)
            }
            .edgesIgnoringSafeArea(.bottom)
//            .onAppear(perform: startChangeCycle) // Включаем цикл построения графика
            .gesture(combined)
    }
    
    
    // MARK: - Вспомогательные функции
    
    /// Запуск приостановленного цикла обмена с прибором
    private func startChangeCycle() {
        print(options.isConnect, options.isFirstChangeCycle)
        if !options.isFirstChangeCycle {
//            options.curMaxDateDoseRateDB        = Date()
//            options.curMinDateDoseRateDB        = Date().addingTimeInterval(-30 * 60)
            print(options.curMinDateDoseRateDB)
            options.enableStartDataBufCycle     = false
            options.enableStopDataBufCycle      = true
            options.enableSetOptions            = false
            options.comp.compose_VS_DATA_BUF() // Запускаем цикл обмена данными с сенсоров прибора
            options.curCommand = .NONE
            options.isFirstChangeCycle = true
        }
    }
    
    // MARK: Управление кнопками
    // TODO: Перенести в 'Options'
    
}

struct GraphDB_TEST3_Previews: PreviewProvider {
    static let options = HardOptions()
    static var previews: some View {
        Group {
            FullDiagramView()
                .environmentObject(options)
                .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
                .environment(\.locale, .init(identifier: "en"))
            FullDiagramView()
                .environmentObject(options)
                .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
                .environment(\.locale, .init(identifier: "ru"))

        }
    }
}
