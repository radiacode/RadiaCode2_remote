//
//  TimeTextView2.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 21.08.2022.
//

import SwiftUI

struct TimeTextView2: View {
    
    @EnvironmentObject var options: HardOptions
    
    var body: some View {
                
        GeometryReader { reader in
            
            let rectW    : CGFloat = reader.size.width      // Высота прямоугольника формы
            let rectH    : CGFloat = reader.size.height     // Ширина прямоугольника формы

            let step = rectW / CGFloat(options.ticValRange)
            
            ZStack {
                ForEach (options.ticVals) { index in
                    let w = rectW - step * CGFloat(index.val)
                    Text("\(index.val)")
                        .font(.system(size: 7))
                        .offset(x: w - 2, y: rectH - 10)
                }
            }
        }
    }
}

struct TimeTextView2_Previews: PreviewProvider {
    static var previews: some View {
        let options = HardOptions()
            options.ticVals = [
                DiagramTick(val: 0),
                DiagramTick(val: 7),
                DiagramTick(val: 14),
                DiagramTick(val: 21),
                DiagramTick(val: 28)
            ]
            options.ticValRange = 32
        return TimeTextView2()
            .environmentObject(options)
    }
}
