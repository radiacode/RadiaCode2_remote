//
//  DiagramTimeGridView2.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 05.09.2022.
//

import SwiftUI

struct DiagramTimeGridView2: View {
    
    @EnvironmentObject var options: HardOptions
    
    var body: some View {
        GeometryReader { reader in
            
            let rectH = Double(reader.size.height)   // Высота прямоугольника формы
            let rectW = Double(reader.size.width)    // Ширина прямоугольника формы
            
            let step = rectW / CGFloat(options.ticValRange)
            
            
            ForEach (options.ticVals) { index in
                let w = rectW - step * CGFloat(index.val)
                Path { path in
                    path.move(   to: CGPoint(x: w, y: rectH))   // Начинаем с нижнего левого угла
                    path.addLine(to: CGPoint(x: w, y: 0))   // Ведём к данным линию
                }.stroke(Color("GridColor"), lineWidth: 1)
            }
            
            Path() { path in
                path.move(   to: CGPoint(x: 0, y: rectH))   // Начинаем с нижнего левого угла
                path.addLine(to: CGPoint(x: 0, y: 0))   // Ведём к данным линию
            }.stroke(Color("GridColor"), lineWidth: 2)

        }
    }
}

struct DiagramTimeGridView2_Previews: PreviewProvider {
    static let options = HardOptions()
    static var previews: some View {
        DiagramTimeGridView2()
            .environmentObject(options)
    }
}
