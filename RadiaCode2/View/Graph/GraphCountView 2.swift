//
//  GraphCountView.swift
//  RadiaCodeConnection
//
//  Created by Андрей Левчик on 27.10.2021.
//

import SwiftUI

struct DiagramCountTextView: View {

    @EnvironmentObject var options: HardOptions

    var body: some View {
        GeometryReader { reader in
            let rectH = Double(reader.size.height)   // Высота прямоугольника формы
            let rectW = Double(reader.size.width)    // Высота прямоугольника формы
            let step  = rectH / Double(options.countRatePoints.count - 1)

            ForEach(options.countRatePoints) { line in
                let h = rectH - step * Double(line.id)      // Текущая высота данных
                VStack {
                    Text(String(format:"%.2f", line.chnCountRate))
                        .font(.system(size: 10))
                        .offset(x: 10, y: h - 16)
                }
                Path { path in
                    path.move(   to: CGPoint(x: 50,    y: h - 1))   // Начинаем с нижнего левого угла
                    path.addLine(to: CGPoint(x: rectW, y: h - 1))   // Ведём к данным линию
                }
                .stroke(lineWidth: 1)
            }
        }//.background(Color.orange)
    }
}

struct AxisXT: Shape {
    
    var maxTimeReal       : CGFloat = 0
    var maxTimeAnim       : CGFloat = 0
    var axisXTimeSecRange : CGFloat = 0

    var animatableData: Double { // Протокол анимации
        get { return maxTimeAnim }
        set {        maxTimeAnim = newValue }
    }

    func path(in rect: CGRect) -> Path {
        
        let rect_H  = rect.size.height              // Высота анимируемого прямоугольника
        let rect_W  = rect.size.width               // Ширина анимируемого прямоугольника
        let deltaT  = maxTimeReal.truncatingRemainder(dividingBy: 10.0) // Получаем отстаток от деления Double на 10
        let deltaTA = maxTimeAnim.truncatingRemainder(dividingBy: 10.0)
        let scaleW  = rect_W / axisXTimeSecRange
        let deltaW  = deltaT * scaleW
        let deltaWA = deltaTA * scaleW
        let shiftWA = deltaW - deltaWA    // Коррекция длины на анимацию

        var   path = Path()
        var w: CGFloat = 0
        var n: Int     = 0
        while w < rect_W {
                w = scaleW * CGFloat(n) - deltaW
                w += shiftWA   // Коррекция длины на анимацию
            let h : CGFloat
            if  n % 10 == 0 {
                h = rect_H - 20
            } else {
                h = rect_H - 10
            }
                n += 1

            path.move(   to: CGPoint(x: w, y: rect_H - 1))  // Начинаем с нижнего левого угла
            path.addLine(to: CGPoint(x: w, y: h)) // Ведём к данным линию

        }
        return path
    }
}


//struct AxisX: Shape {
//
//    var maxTime             : Double = 0
//    var axisXTimeSecRange   : Double = 0
//
//    func path(in rect: CGRect) -> Path {
//
//        let rect_H = rect.size.height
//        let rect_W = rect.size.width
//        let deltaT = maxTime.truncatingRemainder(dividingBy: 10.0)
//        let scaleW = rect_W /  axisXTimeSecRange
//        let deltaW = deltaT * scaleW
//
//        var   path = Path()
//        for n in 0...(Int(axisXTimeSecRange) - 1) {
//            let w = scaleW * CGFloat(n) - deltaW
//            let h : CGFloat
//            if  n % 10 == 0 {
//                h = rect_H - 20
//            } else {
//                h = rect_H - 10
//            }
//
//            path.move(   to: CGPoint(x: w, y: rect_H - 1))  // Начинаем с нижнего левого угла
//            path.addLine(to: CGPoint(x: w, y: h)) // Ведём к данным линию
//        }
//        return path
//    }
//}

struct GraphAnimationShape: Shape {
    
    var dataArray           : [RC_GRP_COUNT_RATE] = []
    var max_Val             : Double = 0
    var min_Val             : Double = 0
    var maxTime             : Double = 0
    var minTime             : Double = 0
    var full_TimeSecRange   : Double = 0
    var axisXTimeSecRange   : Double = 0
    var animationSecRange   : Double = 0
    
    var animatableData: Double { // Протокол анимации
        get { return animationSecRange }
        set {        animationSecRange = newValue }
    }

    func path(in rect: CGRect) -> Path { // Протокол формы
        let rect_W = Double(rect.size.width)        // Ширина прямоугольника формы
        let rect_H = Double(rect.size.height)       // Высота прямоугольника формы
        let scaleW = rect_W /  axisXTimeSecRange
        let scaleH = rect_H / (max_Val - min_Val)

//        print("rect_W = \(rect_W)")
//        print("rect_H = \(rect_H)")
//        print("scaleW = \(scaleW)")
//        print("scaleH = \(scaleH)")

        var   path = Path()
        
        if dataArray.count > 1  {
            for n in 0...(dataArray.count - 1) {    // Цикл по данным
                
                let dataArrayNmsTimeSec = Double(dataArray[n].msTime) / 1_000.0
                let dataArrayNCountRate =        dataArray[n].chnCountRate

                let h  = rect_H - ( -min_Val + dataArrayNCountRate) * scaleH    // Текущая высота данных
                var w  = rect_W - (  maxTime - dataArrayNmsTimeSec) * scaleW    // Текущая длина данных
                    w +=    (full_TimeSecRange - animationSecRange) * scaleW    // Коррекция длины на анимацию
//                print("n = \(n); w = \(w);  h = \(h) ")

                if n == 0 { path.move(   to: CGPoint(x: w, y: h)) }             // Начинаем с нижнего левого угла
                else      { path.addLine(to: CGPoint(x: w, y: h)) }             // Ведём к данным линию
            }
        }
        return path
    }
}

struct GraphCountView: View {
    @EnvironmentObject var options: HardOptions
    var body: some View {
        VStack {
            ZStack {
                VStack {
                    Text("imp/sec").offset(x: 0, y: 5)
                    Spacer()
                }
                GraphAnimationShape(
                                  dataArray: options.grpCountRateArray,
                                    max_Val: options.maxTicCountVal,
                                    min_Val: options.minTicCountVal,
                                    maxTime: options.max_CountTime,
                                    minTime: options.min_CountTime,
                          full_TimeSecRange: options.max_CountTime - options.min_CountTime,
                          axisXTimeSecRange: 30,
                          animationSecRange: options.max_CountTime - options.min_CountTime
                )
                    .stroke(Color.blue, lineWidth: 3)
                    .padding(EdgeInsets(top: 0, leading: 40, bottom: 50, trailing: 0))
                    .animation(.easeInOut(duration: 0.25))
//                    .background(Color.yellow)
                AxisXT(
                    maxTimeReal: options.max_CountTime,
                    maxTimeAnim: options.max_CountTime,
                    axisXTimeSecRange: 30
                )
                    .stroke(Color.black, lineWidth: 1)
                    .padding(EdgeInsets(top: 0, leading: 40, bottom: 0, trailing: 0))
                    .animation(.easeInOut(duration: 0.25))
                DiagramCountTextView()
                    .padding(EdgeInsets(top: 0, leading: 0, bottom: 50, trailing: 0))
                    .animation(.easeInOut(duration: 0.5))

                TimeTextView()
                    .animation(.easeInOut(duration:0.25))
                    .padding(EdgeInsets(top: 0, leading: 40, bottom: 10, trailing: 0))

            }
        }
    }
}

struct GraphCountView_Previews: PreviewProvider {
    static var previews: some View {
        let options = HardOptions()
            options.grpCountRateArray = [
                RC_GRP_COUNT_RATE(msTime: -1_500, chnCountRate: 3.09),
                RC_GRP_COUNT_RATE(msTime:    500, chnCountRate: 4.09),
                RC_GRP_COUNT_RATE(msTime:  1_000, chnCountRate: 5.09),
                RC_GRP_COUNT_RATE(msTime:  3_000, chnCountRate: 4.40),
                RC_GRP_COUNT_RATE(msTime:  4_000, chnCountRate: 5.12),
                RC_GRP_COUNT_RATE(msTime:  6_000, chnCountRate: 4.11),
                RC_GRP_COUNT_RATE(msTime:  7_000, chnCountRate: 6.16),
                RC_GRP_COUNT_RATE(msTime:  7_500, chnCountRate: 5.16),
                RC_GRP_COUNT_RATE(msTime:  9_500, chnCountRate: 4.16),
                RC_GRP_COUNT_RATE(msTime: 10_500, chnCountRate: 5.16)
                ]

       return GraphCountView()
            .environmentObject(options)
    }
}
