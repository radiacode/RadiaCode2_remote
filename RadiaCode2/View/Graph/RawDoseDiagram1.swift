//
//  RawDoseDiagram1.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 26.07.2022.
//

import SwiftUI

struct RawDoseDiagram1: Shape {
    
    @ObservedObject var options    : HardOptions
                    var dateArray  : [ValRateDB] = []
                    var max_Val             : Double = 0
                    var min_Val             : Double = 0
                    var colosePath          : Bool = false

    
    func path(in rect: CGRect) -> Path {
        
        var   path = Path()
        
        let rect_W = rect.size.width     // Ширина прямоугольника формы
        let rect_H = rect.size.height        // Высота прямоугольника формы
        
        var fromDate = options.curFromDiagramDate
        var   toDate = options.curTo__DiagramDate
        
        if options.curFromDiagramDate == nil {
//          fromDate = options.curFromDiagramDate == nil ? Date().addingTimeInterval(-30 * 60) : options.curFromDiagramDate
            fromDate = Date().addingTimeInterval(-30 * 60)
              toDate = Date()
        }
        
        let diffs = Calendar.current.dateComponents([.second], from: fromDate!, to: toDate!)
//      let curSecRange = 30 * 60           // Число секунд в 30 минутах
        let curSecRange = diffs.second!

        
//      let scaleW = rect_W /  Double(-options.curDateSecRangeDoseRateDB)
        let scaleW = rect_W /  Double(curSecRange)
        let scaleH = rect_H / (max_Val - min_Val)
        
//        print("options.curDateSecRangeDoseRateDB = \(options.curDateSecRangeDoseRateDB)")
//        print("options.curMinDateDoseRateDB      = \(options.curMinDateDoseRateDB)")
//        print("options.curMaxDateDoseRateDB      = \(options.curMaxDateDoseRateDB)")
//        print("options.pars.baseDate             = \(options.pars.baseDate ?? Date())")
//        print("Date()                            = \(Date())")
//        print()
        
        var w: Double = 0
        var firstW: Double = 0
        var firstNode = true
        
        if dateArray.count > 1  { // Если есть что рисовать
            
            for n in dateArray {    // Цикл по данным
                
//              let diffs = Calendar.current.dateComponents([.second], from: options.options.curMaxDateDoseRateDB!, to: n.timestamp)
//              let diffs = Calendar.current.dateComponents([.second], from: n.timestamp, to: Date())
                let diffs = Calendar.current.dateComponents([.second], from: n.timestamp, to: toDate!)

                let dataArrayNmsTimeSec = diffs.second!
                let dataArrayNDoseRate  =  Double(n.val_rate)

                var h  = rect_H - ( -min_Val + dataArrayNDoseRate) * scaleH    // Текущая высота данных
                    w  = rect_W - Double(dataArrayNmsTimeSec) * scaleW          // Текущая длина данных
                
// DEBUG:
//                print("dataArrayNmsTimeSec = \(dataArrayNmsTimeSec), \(n.timestamp), w = \(w), rect_W = \(rect_W), rect_H = \(rect_H)")

                if h < 0 {
                   h = 0
                }
                
//                if w < 0      { w = 0      }
//                if w > rect_W { w = rect_W }
             
                if firstNode {                                                  // Если мы в начале графика
                    firstNode.toggle()
                    path.move(   to: CGPoint(x: w, y: h))
                    firstW = w
                }                                                               // Начинаем с нижнего левого угла
                else      { path.addLine(to: CGPoint(x: w, y: h)) }             // Ведём к данным линию

            }
            if colosePath { // Когда нужна реализация с заливкой замыкаем путь
                path.addLine(to: CGPoint(x:      w, y: rect_H))
                path.addLine(to: CGPoint(x: firstW, y: rect_H))
                path.closeSubpath()
            }

        }
        
        return path
    }
}

struct RawDoseDiagram1_Previews: PreviewProvider {
    static let options = HardOptions()
    static var previews: some View {
        RawDoseDiagram1(options: options)
    }
}
