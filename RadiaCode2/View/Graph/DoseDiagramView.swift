//
//  DoseDiagramView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 21.06.2022.
//

import SwiftUI

struct DoseDiagramView: View {
    
    @EnvironmentObject var options: HardOptions
    @Environment(\.managedObjectContext)
               private var viewContext

    
    var body: some View {
        
//        let timer = Timer.scheduledTimer(withTimeInterval: 20.0, repeats: true) { timer in
//            let curDate = Date()
//            let fromDate = curDate.addingTimeInterval(-20 * 60)
////          options.grpDoseRateTimerArray = Log.getDoseRateDB(in: viewContext, from: options.curMinDateCountRateDB!, to: options.curMaxDateCountRateDB!) ?? []
//            options.grpDoseRateTimerArray = []
//            options.grpDoseRateTimerArray = Log.getDoseRateDB(in: viewContext, from: fromDate, to: curDate) ?? []
//            
//            print("TIMER! \(Date())")
//            print("grpDoseRateTimerArray")
//            for doseItem in options.grpDoseRateTimerArray {
//                print(doseItem.timestamp, doseItem.val_rate)
//            }
//
//        }
        
        let maxCountTime = options.max_CountTime > options.max_RawCountTime ? options.max_CountTime : options.max_RawCountTime
        let minCountTime = options.min_CountTime < options.min_RawCountTime ? options.min_CountTime : options.min_RawCountTime
        let curDate = measureDate(Int((maxCountTime) * 100), startDate: options.pars.baseDate ?? Date())
        let calendar = Calendar.current
        let seconds = calendar.component(.second, from: curDate)
        let shiftMin = Double(seconds) / 60
        let decimalShift = shiftMin.truncatingRemainder(dividingBy: 1)
        
        ZStack {
            HStack { // Верхняя часть
                DiagramCountTextView3()
                    .frame(maxWidth: 42)
                    .background(Color("AccentColor"))
                    .zIndex(1)
//                GraphDB_DoseShape(options: self.options, counts: options.doseRateDB ?? []  , max_Val: options.curMaxDoseRateValDB!)
//                    .stroke(Color("OldDoseColor"), lineWidth: 1)
    //                            .background(.blue)
                ZStack {
                    DiagramTimeGridView2().offset(x: -7)
                    RawDoseDiagram1(
                        options: self.options,
                        dateArray: options.grpRawDoseRateTimerArray,
                        max_Val: Double(options.curMaxDoseRateValDB!),
                        min_Val: 0
                    )
                        .stroke(Color("GreyGraphColor"), lineWidth: 0.3)
                    RawDoseDiagram1(
                        options: self.options,
                        dateArray: options.grpDose_RateTimerArray,
                        max_Val: Double(options.curMaxDoseRateValDB!),
                        min_Val: 0,
                        colosePath: true
                    )
                        .foregroundColor(Color("NewDoseFillColor"))
                    RawDoseDiagram1(
                        options: self.options,
                        dateArray: options.grpDose_RateTimerArray,
                        max_Val: Double(options.curMaxDoseRateValDB!),
                        min_Val: 0
                    )
                        .stroke(Color("NewDoseColor"), lineWidth: 2)
//                    HStack {
//                        Rectangle()
//                            .fill(.red)
//                            .frame(maxWidth: 42)
//                            .position(x: 0)
//                        Spacer()
//                    }

                }
            }.background(Color("AccentColor"))
            DiagramDoseGridView()
            VStack {
                HStack {
                    Text("Dose_rate, \(options.getOptimalVarUnitStr(roentgenVal: Double(options.grpDoseRateLast / 10_000), isVal: false, isHour: true))")
                        .font(.system(size: 15))
                        .offset(x: 46, y: 0)
                    Spacer()
                }
                HStack {
                    Spacer()
                    Text("\(options.getOptimalVarUnitStr(roentgenVal: Double(options.grpDoseRateLast / 10_000), isUnit: false))")
//                  Text(String(format:"%.3f", options.grpDoseRateLast))
                        .font(.system(size: 30))
                        .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 10))
                }
                HStack {
                    Spacer()
                    Text("±\(String(format:"%.1f", options.grpDoseRateErrLast))%")//
                        .font(.system(size: 20))
                        .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 10))
                }

                Spacer()
            }
        }
    }
}

struct CountDiagramView_Previews: PreviewProvider {
    static let options = HardOptions()
    static var previews: some View {
        DoseDiagramView()
            .environmentObject(options)
    }
}
