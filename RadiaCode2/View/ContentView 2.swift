//
//  ContentView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import SwiftUI
import CoreData

struct ContentView: View {
    
    @EnvironmentObject var options: HardOptions

    var body: some View {
        NavigationView {
            Form {
                NavigationLink(destination: DevicesListView())  { Text("Devices"         ) }
                NavigationLink(destination: HardOptionsView())  { Text("Device Options"  ) }

            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static let options = HardOptions()
    static var previews: some View {
        ContentView()
            .environmentObject(options)
    }
}
