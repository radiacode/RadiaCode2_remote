//
//  ContentView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import SwiftUI
import CoreData

struct ContentView: View {
    
    @EnvironmentObject var options: HardOptions
    @Environment(\.managedObjectContext)
            private var viewContext

    var body: some View {
        NavigationView {
            Form {
                HStack {
                    Text(options.statusStr)
                    Spacer()
                    Text("\(options.groupCount)")
                }
                
                Button(action: {
                                options.enableStartDataBufCycle     = false
                                options.enableStopDataBufCycle      = true
                                options.enableSetOptions            = false
                                options.comp.compose_VS_DATA_BUF() // Запускаем цикл обмена данными с сенсоров прибора
                                options.curCommand = .NONE
                })                                                  { Text("Start change"    ) }
                    .disabled(!options.enableStartDataBufCycle)

                Button(action: {
                                options.enableStartDataBufCycle     = true
                                options.enableSetOptions            = true
                                options.enableStopDataBufCycle      = false
                                options.curCommand                  = .STOP
                    
                })                                                  { Text("Stop change"     ) }
                    .disabled(!options.enableStopDataBufCycle)
                Button(action: {
                    LogRareData.deleteAllData(in: viewContext)
                    LogRateDB  .deleteAllData(in: viewContext)
                    Event      .deleteAllData(in: viewContext)
                    Log        .deleteAllData(in: viewContext)
                })                                                      { Text("Delete logs"        ) }


                NavigationLink(destination: DevicesListView())           { Text("Devices"         ) }
                NavigationLink(destination: HardOptionsView(
//                    radiaCodeOptions: RadiaCodeOptions(options: options),
                    alarm1StrDRA: options.LEV1_uR_hStr,
                    alarm2StrDRA: options.LEV2_uR_hStr,
                    alarm1StrDSA: options.LEV1_100uR_Str,
                    alarm2StrDSA: options.LEV2_100uR_Str
                ))                                                      { Text("Device Options"  ) }
                    .disabled(!options.enableSetOptions)

                Group {
                    NavigationLink(destination: ConfigGRP_ListView())   { Text("Configuration"   ) }
                    NavigationLink(destination: LogView())          { Text("Log"             ) }
                    NavigationLink(destination: FullDiagramView())        { Text("TEST") }
                }
            }
//            .onAppear(perform: connectBLEDevice) // Попытка подключения устройства
        }
        .navigationTitle(String(localized: "RadiaCode"))
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static let options = HardOptions()
    static var previews: some View {
        ContentView()
            .environmentObject(options)
    }
}
