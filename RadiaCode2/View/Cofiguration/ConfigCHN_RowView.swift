//
//  ConfigCHN_RowView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 10.02.2022.
//

import SwiftUI

struct ConfigCHN_RowView: View {
    
    var item: DataStrucktureCHN
    
    var body: some View {
        VStack {
            Group {
                HStack {
                    Text("Канал: \(item.name ?? "Not found")")
                    Spacer()
                    Text("ID: \(item.id)")
                }
                if item.sensor != nil {
                    HStack {
                        Text("Sensor: \(item.sensor ?? "")")
                        Spacer()
                        if item.unit != nil {
                            Text("\(item.unit ?? "")")
                        }
                    }
                }
                HStack { Text("DType: \(item.d_type)"); Spacer() }
                HStack { Text("RType: \(item.r_type)") ;Spacer() }
                if item.scaled_unit != 0 {
                    Text("ScaledUnit: \(item.scaled_unit)")
                }
            }
            Group {
                if item.max_val != item.min_val {
                    HStack { Text("MaxVal: \(item.max_val)") ;Spacer()}
                    HStack { Text("MinVal: \(item.min_val)") ;Spacer()}
                }
                if item.expr != 0 {
                    HStack {
                        Text("Expr: \(item.expr)")
                        Spacer()
                        Text("P1: \(item.p1)")
                        Spacer()
                        Text("P2: \(item.p2)")
                    }
                }
                HStack {
                    Spacer()
                    Text("\(item.position)")
                        .font(.system(size: 8, weight: .light, design: .monospaced))
                }
            }
        }
    }
}

struct ConfigCHN_RowView_Previews: PreviewProvider {
    static var previews: some View {
        let context = PersistenceController.preview.container.viewContext
        let newItem = DataStrucktureCHN(context: context)
            newItem.position = 2
            newItem.id     = 3
            newItem.name   = "CHN_CountRate"
            newItem.sensor = "c10_CsI_C30035"
            newItem.unit   = "\" cps| имп/с\""
            newItem.d_type = 1
            newItem.r_type = 2
            newItem.expr   = 0
            newItem.scaled_unit = 0
            newItem.max_val = 30
            newItem.min_val = 20
            newItem.p1 = 1
            newItem.p2 = 2
        return ConfigCHN_RowView(item: newItem)
                .previewLayout(.fixed(width: 400, height: 210))

    }
}
