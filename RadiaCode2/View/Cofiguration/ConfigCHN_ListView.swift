//
//  ConfigCHN_ListView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 10.02.2022.
//

import SwiftUI


struct ConfigCHN_ListView: View {
    
    var groupName: String = ""
    
    @Environment(\.managedObjectContext)
            private var viewContext
    @FetchRequest(sortDescriptors: [NSSortDescriptor(keyPath: \DataStrucktureCHN.position, ascending: true)], animation: .default)
            private var items: FetchedResults<DataStrucktureCHN>

    var body: some View {
        List {
            ForEach(self.items.filter { $0.grp!.name! == self.groupName }, id: \.self)
            { item in
                ConfigCHN_RowView(item: item)
            }
        }.navigationBarTitle(Text(groupName), displayMode: .inline)
    }
}

struct ConfigCHN_ListView_Previews: PreviewProvider {
    static var previews: some View {
        ConfigCHN_ListView()
            .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)

    }
}
