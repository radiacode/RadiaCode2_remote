//
//  ConfigGRP_ListView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 10.02.2022.
//

import SwiftUI

struct ConfigGRP_ListView: View {
    
    @Environment(\.managedObjectContext)
            private var viewContext
    @FetchRequest(sortDescriptors: [NSSortDescriptor(keyPath: \DataStrucktureGRP.id, ascending: true)], animation: .default)
            private var items: FetchedResults<DataStrucktureGRP>

    var body: some View {
        List {
            ForEach(items) { item in
                NavigationLink(destination: ConfigCHN_ListView(groupName: item.name!)) {
                    VStack {
                        HStack {
                            Text("Группа: \(item.name ?? "Not found")")
                            Spacer()
                            Text("ID: \(item.id)")
                        }
                        if item.flg_msk > 0 {
                            HStack {
                                Spacer()
                                Text("FlgMsk: \(String(item.flg_msk, radix: 2))")
                                    .font(.system(size: 16, weight: .light, design: .monospaced))
                                    .padding(.top)
                            }
                        }
                    }
                }
            }
        }
    }
}

struct ConfigGRP_ListView_Previews: PreviewProvider {
    static var previews: some View {
        ConfigGRP_ListView()
            .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
