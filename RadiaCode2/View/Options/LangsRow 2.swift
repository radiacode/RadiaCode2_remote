//
//  LangsRow.swift
//  TestUI
//
//  Created by Андрей Левчик on 09.02.2022.
//

import SwiftUI

struct LangsRow: View {
    @EnvironmentObject var options: HardOptions
    var body: some View {
        HStack {
            Text("Language")
            Spacer()
            Text(options.curLangStr)
                .foregroundColor(.gray)
        }
    }
}

struct LangsRow_Previews: PreviewProvider {
    static var previews: some View {
        LangsRow()
    }
}
