//
//  LangForm.swift
//  TestUI
//
//  Created by Андрей Левчик on 09.02.2022.
//

import SwiftUI

struct LangForm: View {
    var body: some View {
        Form {
            LangButton(lang: .rus)
            LangButton(lang: .eng)
        }
        .navigationTitle("Device language")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct LangForm_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            LangForm()
        }
    }
}
