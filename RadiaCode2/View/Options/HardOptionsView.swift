//
//  HardOptionsView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 11.02.2022.
//

import SwiftUI

struct HardOptionsView: View {
    
    @EnvironmentObject var options: HardOptions
//  @ObservedObject    var radiaCodeOptions: RadiaCodeOptions // Пока не работает…
    @Environment(\.presentationMode) var mode   : Binding<PresentationMode>    // Окружение для ручного управления закрытием формы
    @Environment(\.managedObjectContext) private var viewContext // DEBUG: Для отладки (удаление журнала)
    
    
    @State          var language: [Lang]      = [.rus, .eng]
    @State          var unit    : [Unit]      = [.r,   .sv ]
    @State          var alarm   : [AlarmMode] = [.many,.one]
    @State          var alarm1StrDRA          = ""           // Значение поля ввода 1-й тревоги
    @State          var alarm2StrDRA          = ""           // Значение поля ввода 2-й тревоги
    @State          var alarm1StrDSA          = ""           // Значение поля ввода 1-й тревоги
    @State          var alarm2StrDSA          = ""           // Значение поля ввода 2-й тревоги
    @State private  var isEditing1            = false        // Факт изменения значения поля 1-й тревоги
//    @State private  var isEditing2            = false        // Факт изменения значения поля 2-й тревоги
//    @State private  var isEditing3            = false        // Факт изменения значения поля 1-й тревоги
//    @State private  var isEditing4            = false        // Факт изменения значения поля 2-й тревоги
    @State private  var showingFormatAlert    = false        // Показ предупреждения неправильного формата ввода десятичного
    @State private  var showingSaveAlert      = false        // Показ предупреждения об несохранённых данных
    @State private  var showingCompareAlert   = false        // Показ предупреждения о превышении 2-го порога 1-м
    @State private  var isToggle = false // Yеменяемое значение-заглушка для выравнивания интерфейса
     
    var body: some View {
        Form {
            Button(action: { // DEBUG: Для отладки (удаление журнала)
                LogRareData.deleteAllData(in: viewContext)
                LogRateDB  .deleteAllData(in: viewContext)
                Event      .deleteAllData(in: viewContext)
                Log        .deleteAllData(in: viewContext)
                PersistenceController.shared.saveContext()
            })                                                      { Text("ОТЛАДКА!!! Delete logs"        ) }
            
            Group {
                Section(header: Text("Main Options")) {
                    Toggle(isOn: $options.DEVICE_CTRL_PWR_ON)         { Text("Power") }
                    Toggle(isOn: $options.DEVICE_CTRL_SOUND_ENA)      { Text("Sound") }
                    Toggle(isOn: $options.isRC_SOUND_CTRL_CLICK_ENA)  { Text("Click") }.disabled(!options.DEVICE_CTRL_SOUND_ENA)
                    Toggle(isOn: $options.DEVICE_CTRL_VIBRO_ENA)      { Text("Vibration") }
                    Toggle(isOn: $options.DEVICE_CTRL_LEDS_ENA)       { Text("LED Indicators") }
                }
                Section(header: Text("Raw Filter 0…30 sec")) {
                    HStack {
                        TextField("Value", text: $options.RAW_FILTERStr)
                            .keyboardType(.numbersAndPunctuation)                           // Клавиатура для ввода цифр и знаков препинания
                            .textFieldStyle(RoundedBorderTextFieldStyle())      // Чтобы текстовое поле было видно (т.к. в строке есть ещё единицы измерения)
                        Text("Seconds").foregroundColor(.gray)
                    }
                }
                Section(header: Text("Language")) {
                    Picker("Language Modes", selection: $options.curLangVal){
                        ForEach( 0 ..< language.count) { index in
                            Text("\( langStr(lang: language[index]) )")
                        }
                    }.pickerStyle(SegmentedPickerStyle())
                    Text("Current choice: \(langStr(lang: language[options.curLangVal]))")
                }
                
                Section(header: Text("Units")) {
                    Picker("Units Modes", selection: $options.curUnitVal){
                        ForEach( 0 ..< unit.count) { index in
                            Text("\( unitStr(unit: unit[index]) )").tag(index)
                        }
                        // Мы изменили единцы измерения нужно поменять везде в окне значения тревог на новые единицы
                        .onChange(of: options.curUnitVal, perform: { value in
                            
                            if value == 0 {
                                
                                // Речь идёт о микрорентгенах
                                let alarm1FloatDRA_sv = Float(alarm1StrDRA)
                                let alarm1FloatDRA_r  = alarm1FloatDRA_sv! * 100.0
                                    alarm1StrDRA      = String(alarm1FloatDRA_r)
                                
                                let alarm2FloatDRA_sv = Float(alarm2StrDRA)
                                let alarm2FloatDRA_r  = alarm2FloatDRA_sv! * 100.0
                                    alarm2StrDRA      = String(alarm2FloatDRA_r)
                                
                                // Речь идёт о миллирентгенах
                                let alarm1FloatDSA_sv = Float(alarm1StrDSA)
                                let alarm1FloatDSA_r  = alarm1FloatDSA_sv! * 100.0
                                    alarm1StrDSA      = String(alarm1FloatDSA_r)
                                
                                let alarm2FloatDSA_sv = Float(alarm2StrDSA)
                                let alarm2FloatDSA_r = alarm2FloatDSA_sv! * 100.0
                                    alarm2StrDSA     = String(alarm2FloatDSA_r)
                                
                            } else {
                                
                                // Речь идёт о микрозивертах
                                let alarm1FloatDRA_r  = Float(alarm1StrDRA)
                                let alarm1FloatDRA_sv = alarm1FloatDRA_r! / 100.0
                                    alarm1StrDRA      = String(alarm1FloatDRA_sv)
                                
                                let alarm2FloatDRA_r  = Float(alarm2StrDRA)
                                let alarm2FloatDRA_sv = alarm2FloatDRA_r! / 100.0
                                    alarm2StrDRA      = String(alarm2FloatDRA_sv)
                                
                                // Речь идёт о миллизивертах
                                let alarm1FloatDSA_r  = Float(alarm1StrDSA)
                                let alarm1FloatDSA_sv = alarm1FloatDSA_r! / 100.0
                                    alarm1StrDSA      = String(alarm1FloatDSA_sv)
                                
                                let alarm2FloatDSA_r  = Float(alarm2StrDSA)
                                let alarm2FloatDSA_sv = alarm2FloatDSA_r! / 100.0
                                    alarm2StrDSA      = String(alarm2FloatDSA_sv)
                            }
                        })
                    }.pickerStyle(SegmentedPickerStyle())
                    Text("Current choice: \(unitStr(unit: unit[options.curUnitVal]))")
                }
                
                Section(header: Text("Alarm repeat")) {
                    Picker("Alarm repeat", selection: $options.curAlarmModeVal){
                        ForEach( 0 ..< alarm.count) { index in
                            Text("\( alarmModeStr(alarm: alarm[index]) )")
                        }
                    }.pickerStyle(SegmentedPickerStyle())
                    Text("Current choice: \(alarmModeStr(alarm: alarm[options.curAlarmModeVal]))")
                }
            }
            
            Group {
                Section(header: Text("Dose Rate Alarm 1")) {                            // Секция настроек 1-й (нижней) тревоги
                    HStack {
                        TextField("Value", text: $alarm1StrDRA) {               // Поле ввода значения 1-й тревоги
                            isEditing in if isEditing {self.isEditing1 = isEditing }// Контроль за фактом изменения тревоги
                        } onCommit: {                                           // Действия при нажатии Return на клавиатуре при редактировании 1-й тревоги
                            validateDRA(alarm1StrDRA)                                 // Проверка формата десятичного числа 1-й тревоги
                            if !showingFormatAlert {                            // Если всё в порядке
                                options.LEV1_uR_hStr = alarm1StrDRA                //  записываем значение 1-й тревоги в прибор
                            }
                        }
                        // TODO: После замены десятичного знака на локализованный можно будет указывать только цифровую клавиатуру
                        .keyboardType(.numbersAndPunctuation)               // Клавиатура для ввода цифр и знаков препинания
                        .textFieldStyle(RoundedBorderTextFieldStyle())      // Чтобы текстовое поле было видно (т.к. в строке есть ещё единицы измерения)
                        // TODO: Изменить при переходе на iOS15 может будет понятнее
                        //                      .submitLabel(.done)                                 // Заработает на iOS15
                        Spacer()
                        Text(String(localized: "µ") + options.curUnitStr + String(localized: "/h"))                  // Отображение реальных единиц измерения
                            .foregroundColor(.gray)                             //  серым цветом
                    }
                    Toggle(isOn: $options.isRC_SOUND_CTRL_DR_ALM_L1_ENA) { Text("Sound") }       // Переключение и запись в прибор установки звука при 1-й тревоге
                    Toggle(isOn: $options.isRC_VIBRO_CTRL_DR_ALM_L1_ENA) { Text("Vibration") }   // Переключение и запись в прибор установки вибрации при 1-й тревоге
                }
                
                Section(header: Text("Dose Rate Alarm 2")) {                            // Секция настроек 1-й (верхней) тревоги
                    HStack {
                        TextField("Value", text: $alarm2StrDRA) {               // Поле ввода значения 2-й тревоги
                            isEditing in if isEditing {self.isEditing1 = isEditing }// Контроль за фактом изменения тревоги
                        } onCommit: {                                           // Действия при нажатии Return на клавиатуре при редактировании 2-й тревоги
                            validateDRA(alarm2StrDRA)                                 // Проверка формата десятичного числа 2-й тревоги
                            if !showingFormatAlert {                            // Если всё в порядке
                                options.LEV2_uR_hStr = alarm2StrDRA                //  записываем значение 2-й тревоги в прибор
                            }
                        }
                        .keyboardType(.numbersAndPunctuation)               // Клавиатура для ввода цифр и знаков препинания
                        .textFieldStyle(RoundedBorderTextFieldStyle())      // Чтобы текстовое поле было видно (т.к. в строке есть ещё единицы измерения)
                        Spacer()
                        Text(String(localized: "µ") + options.curUnitStr + String(localized: "/h"))                   // Отображение реальных единиц измерения
                            .foregroundColor(.gray)                             //  серым цветом
                    }
                    Toggle(isOn: $options.isRC_SOUND_CTRL_DR_ALM_L2_ENA) { Text("Sound") }       // Переключение и запись в прибор установки звука при 2-й тревоге
                    Toggle(isOn: $options.isRC_VIBRO_CTRL_DR_ALM_L2_ENA) { Text("Vibration") }   // Переключение и запись в прибор установки вибрации при 2-й тревоге
                }
                Section(header: Text("Overload")) {                         // Секция настроек тревоги зашкаливания
                    Toggle(isOn: $options.isRC_SOUND_CTRL_DR_ALM_OS_ENA) { Text("Sound") }       // Переключение и запись в прибор установки звука при зашкаливании
                    Toggle(isOn: $options.isRC_VIBRO_CTRL_DR_ALM_OS_ENA) { Text("Vibration") }   // Переключение и запись в прибор установки вибрации при зашкаливании
                }
            
            
                Section(header: Text("Dose Alarm 1")) {                            // Секция настроек 1-й (нижней) тревоги
                    HStack {
                        TextField("Value", text: $alarm1StrDSA) {               // Поле ввода значения 1-й тревоги
                            isEditing in if isEditing {self.isEditing1 = isEditing }// Контроль за фактом изменения тревоги
                        } onCommit: {                                           // Действия при нажатии Return на клавиатуре при редактировании 1-й тревоги
                            validateDSA(alarm1StrDSA)                                 // Проверка формата десятичного числа 1-й тревоги
                            if !showingFormatAlert && !showingCompareAlert {                                  // Если всё в порядке
                                options.LEV1_100uR_Str = alarm1StrDSA             //  записываем значение 1-й тревоги в прибор
                            }
                        }
                        .alert(isPresented: $showingFormatAlert) {
                            Alert(title: Text("ERROR"),
                                  message: Text("Correct Format '0.40'"),
                                  dismissButton: .default(Text("OK")))
                        }
                        
                        // TODO: После замены десятичного знака на локализованный можно будет указывать только цифровую клавиатуру
                        
                        .keyboardType(.numbersAndPunctuation)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        // TODO: Изменить при переходе на iOS15 может будет понятнее
                        //                      .submitLabel(.done)                                 // Заработает на iOS15
                        Spacer()
                        Text(String(localized: "m") + options.curUnitStr)                          // Отображение реальных единиц измерения
                            .foregroundColor(.gray)                             //  серым цветом
                    }
                    Toggle(isOn: $options.isRC_SOUND_CTRL_DS_ALM_L1_ENA) { Text("Sound") }       // Переключение и запись в прибор установки звука при 1-й тревоге
                    Toggle(isOn: $options.isRC_VIBRO_CTRL_DS_ALM_L1_ENA) { Text("Vibration") }   // Переключение и запись в прибор установки вибрации при 1-й тревоге
                }
                
                Section(header: Text("Dose Alarm 2")) {                                            // Секция настроек 1-й (верхней) тревоги
                    HStack {
                        TextField("Value", text: $alarm2StrDSA) {                               // Поле ввода значения 2-й тревоги
                            isEditing in if isEditing {self.isEditing1 = isEditing }// Контроль за фактом изменения тревоги
                        }  onCommit: {                                                          // Действия при нажатии Return на клавиатуре при редактировании 2-й тревоги
                            validateDSA(alarm2StrDSA)                                                 // Проверка формата десятичного числа 2-й тревоги
                            if !showingFormatAlert {                                                  // Если всё в порядке
                                options.LEV2_100uR_Str = alarm2StrDSA                             //  записываем значение 2-й тревоги в прибор
                            }
                        }
                        .keyboardType(.numbersAndPunctuation)                               // Клавиатура для ввода цифр и знаков препинания
                        .textFieldStyle(RoundedBorderTextFieldStyle())                      // Чтобы текстовое поле было видно (т.к. в строке есть ещё единицы измерения)
                        Spacer()
                        Text(String(localized: "m") + options.curUnitStr)                                         // Отображение реальных единиц измерения
                            .foregroundColor(.gray)                                             //  серым цветом
                    }
                    Toggle(isOn: $options.isRC_SOUND_CTRL_DS_ALM_L2_ENA) { Text("Sound") }       // Переключение и запись в прибор установки звука при 2-й тревоге
                    Toggle(isOn: $options.isRC_VIBRO_CTRL_DS_ALM_L2_ENA) { Text("Vibration") }   // Переключение и запись в прибор установки вибрации при 2-й тревоге
                }
                
                Section(header: Text("Overload")) {                                         // Секция настроек тревоги зашкаливания
                    Toggle(isOn: $options.isRC_SOUND_CTRL_DS_ALM_OS_ENA) { Text("Sound") }       // Переключение и запись в прибор установки звука при зашкаливании
                    Toggle(isOn: $options.isRC_VIBRO_CTRL_DS_ALM_OS_ENA) { Text("Vibration") }   // Переключение и запись в прибор установки вибрации при зашкаливании
                }
            }
            Group {
                Section(header: Text("Backlight")) {
                    Button(action: {
                        options.curLightMode = .off
                    }) {
                        HStack {
                            Text("Off")
                            Spacer()
                            Image(systemName: options.curLightMode == .off ? "checkmark.square" : "square")
                        }
                    }
                    Button(action: {
                        options.curLightMode = .ena
                    }) {
                        HStack {
                            Text("On key press")
                            Spacer()
                            Image(systemName: options.curLightMode == .ena ? "checkmark.square" : "square")
                        }
                    }
                    Button(action: {
                        options.curLightMode = .auto
                    }) {
                        HStack {
                            Text("On low light")
                            Spacer()
                            Image(systemName: options.curLightMode == .auto ? "checkmark.square" : "square")
                        }
                    }
               }

                Section(header: Text("Display off on")) {
                    Button(action: {
                        options.curOffTime = .sec05
                    }) {
                        HStack {
                            Text("5 с")
                            Spacer()
                            Image(systemName: options.curOffTime == .sec05 ? "checkmark.square" : "square")
                        }
                    }
                    Button(action: {
                        options.curOffTime = .sec10
                    }) {
                        HStack {
                            Text("10 с")
                            Spacer()
                            Image(systemName: options.curOffTime == .sec10 ? "checkmark.square" : "square")
                        }
                    }
                    Button(action: {
                        options.curOffTime = .sec15

                    }) {
                        HStack {
                            Text("15 с")
                            Spacer()
                            Image(systemName: options.curOffTime == .sec15 ? "checkmark.square" : "square")
                        }
                    }
                    Button(action: {
                        options.curOffTime = .sec30
                    }) {
                        HStack {
                            Text("30 с")
                            Spacer()
                            Image(systemName: options.curOffTime == .sec30 ? "checkmark.square" : "square")
                        }
                    }
                }

                Section(header: Text("Brightness")) {
                    HStack {
                        Slider(
                            value: $options.brightness,
                            in: 0...9,
                             step: 1
                        )
                        Text("\(Int(options.brightness))")
                    }
                }

            }
            Group {
                Section(header: Text("Device signals")) {
                    HStack {
                        Text("On key press")
                        Spacer()
                        Toggle(        isOn: $options.isRC_SOUND_CTRL_BUTTON_ENA) {
                            Image(systemName: options.isRC_SOUND_CTRL_BUTTON_ENA ? "speaker.wave.2" : "speaker.slash")
                        }
                            .tint(options.isRC_SOUND_CTRL_BUTTON_ENA ? .green : .red)
                        Toggle(        isOn: $options.isRC_VIBRO_CTRL_BUTTON_ENA) {
                            Image(systemName: options.isRC_VIBRO_CTRL_BUTTON_ENA ? "square.dashed.inset.filled" : "square")
                        }
                            .tint(            options.isRC_VIBRO_CTRL_BUTTON_ENA ? .green : .red)
                    }.toggleStyle(.button)
                    
                    HStack {
                        Text("On particles registeration")
                        Spacer()
                        Toggle(        isOn: $options.isRC_SOUND_CTRL_CLICK_ENA) {
                            Image(systemName: options.isRC_SOUND_CTRL_CLICK_ENA ? "speaker.wave.2" : "speaker.slash")
                        }
                            .tint(            options.isRC_SOUND_CTRL_CLICK_ENA ? .green : .red)
                        Toggle(        isOn: $isToggle) {
                            Image(systemName: "square")
                        }
                        .disabled(true)
                        .tint( .gray)
                    }.toggleStyle(.button)
                    
                    HStack {
                        Text("On connect/disconnect to app")
                        Spacer()
                        Toggle(        isOn: $options.isRC_SOUND_CTRL_CONNECT_ENA) {
                            Image(systemName: options.isRC_SOUND_CTRL_CONNECT_ENA ? "speaker.wave.2" : "speaker.slash")
                        }
                            .tint(            options.isRC_SOUND_CTRL_CONNECT_ENA ? .green : .red)
                        Toggle(        isOn: $isToggle) {
                            Image(systemName: "square")
                        }
                        .disabled(true)
                        .tint( .gray)
                    }.toggleStyle(.button)
                    
                    HStack {
                        Text("On power on/off")
                        Spacer()
                        Toggle(        isOn: $options.SOUND_CTRL_POWER_ENA) {
                            Image(systemName: options.SOUND_CTRL_POWER_ENA ? "speaker.wave.2" : "speaker.slash")
                        }
                            .tint(            options.SOUND_CTRL_POWER_ENA ? .green : .red)
                        Toggle(        isOn: $isToggle) {
                            Image(systemName: "square")
                        }
                        .disabled(true)
                        .tint( .gray)
                    }.toggleStyle(.button)
                }
                
                Section(header: Text("Display rotation")) {
                    Button(action: {
                        options.curDispMode = .auto
                    }) {
                        HStack {
                            Text("Auto")
                            Spacer()
                            Image(systemName: options.curDispMode == .auto ? "checkmark.square" : "square")
                        }
                    }
                    Button(action: {
                        options.curDispMode = .normal
                    }) {
                        HStack {
                            Text("Normal orientation only")
                            Spacer()
                            Image(systemName: options.curDispMode == .normal ? "checkmark.square" : "square")
                        }
                    }
                    Button(action: {
                        options.curDispMode = .rotate
                    }) {
                        HStack {
                            Text("Reverse orientation only")
                            Spacer()
                            Image(systemName: options.curDispMode == .rotate ? "checkmark.square" : "square")
                        }
                    }
                    
                }
            }
        }   // Конец формы
        .edgesIgnoringSafeArea(.bottom)
//        .onAppear(perform: stopChangeCycle) // Отключаем цикл построения графика
        
        // Настройки навигационной панели
//        .navigationBarTitle(isEditing1 ? String(localized: "Device Options") + " " + "Changed" : String(localized: "Device Options")) // Указание в заголовке настроек и факта редактирования
//        .navigationBarTitleDisplayMode(.inline)                             // Однострочное (мелкое) название формы
//        .navigationBarBackButtonHidden(true)                                // Убираем кнопку перехода обратно по-умолчанию
        .navigationBarItems(leading: Button(action : {                      // Рисуем новую кнопку закрытия формы
             // При закрытии проверяем на факт изменения полей ввода и переспрашиваем
            if isEditing1 { showingSaveAlert = true } else { self.mode.wrappedValue.dismiss() } }){ Image(systemName: "arrow.left") }
        )
        
        // Настройки предупреждений формы
        .alert(isPresented: $showingFormatAlert)  { Alert(title: Text("ERROR"), message: Text("Correct Format '0.40'")   , dismissButton: .default(Text("OK")))}
        .alert(isPresented: $showingCompareAlert) { Alert(title: Text("ERROR"), message: Text("1st value is greater than 2nd"), dismissButton: .default(Text("OK")))}
        .alert(isPresented: $showingSaveAlert) {
            Alert(        title: Text("Attention"),
                        message: Text("Alarm values ​​not saved"),
                  primaryButton: .default(    Text("Save"), action: saveWorkoutData ),
                secondaryButton: .destructive(Text("Delete")  , action: deleteWorkoutData)
            )
        }


    } // Конец struct HardOptionsView
    
    // MARK: - Доп. функции формы
    
    private func stopChangeCycle() {
        print(options.isConnect, options.isFirstChangeCycle)
        options.isFirstChangeCycle          = false // Флаг того, что можно запускать цикл обмена из окна графиков
        options.enableStartDataBufCycle     = true
        options.enableSetOptions            = true
        options.enableStopDataBufCycle      = false
        options.curCommand                  = .STOP
    }
    
    func saveWorkoutData() {
        options.LEV1_uR_hStr    = alarm1StrDRA
        options.LEV2_uR_hStr    = alarm2StrDRA
        options.LEV1_100uR_Str = alarm1StrDSA
        options.LEV2_100uR_Str = alarm2StrDSA

        self.mode.wrappedValue.dismiss()
    }
    
    func deleteWorkoutData() {
        self.mode.wrappedValue.dismiss()
    }

    func validateDRA(_ str: String)  {
        let floatVal = Float(str)
        if  floatVal == nil { showingFormatAlert = true }
        else                { showingFormatAlert = false }
        // TODO: Проверить при неправильном десятичном!
        if let floatVal1 = Float(alarm1StrDRA),
           let floatVal2 = Float(alarm2StrDRA) {
            if floatVal1 > floatVal2 { showingCompareAlert = true
            } else { showingCompareAlert = false }
        }
    }
    
    func validateDSA(_ str: String)  {
        let floatVal = Float(str)
        if  floatVal == nil { showingFormatAlert = true }
        else                { showingFormatAlert = false }
        // TODO: Проверить при неправильном десятичном!
        if let floatVal1 = Float(alarm1StrDSA),
           let floatVal2 = Float(alarm2StrDSA) {
            if floatVal1 > floatVal2 { showingCompareAlert = true
            } else { showingCompareAlert = false }
        }
    }

}

struct HardOptionsViewTEST_Previews: PreviewProvider {
    static let options = HardOptions()
    static var previews: some View {
        Group {
            HardOptionsView()
                .environmentObject(options)
                .environment(\.locale, .init(identifier: "ru"))

            HardOptionsView()
                .environmentObject(options)
                .environment(\.locale, .init(identifier: "en"))

        }

    }
}
