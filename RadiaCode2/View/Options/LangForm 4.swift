//
//  LangForm.swift
//  TestUI
//
//  Created by Андрей Левчик on 09.02.2022.
//

import SwiftUI

struct LangForm: View {
    
    var body: some View {
        Form {
            LangButton(lang: .rus)
            LangButton(lang: .eng)
        }
        .navigationTitle("Язык прибора")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct LangList_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            LangForm()
        }
    }
}
