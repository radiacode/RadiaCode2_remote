//
//  HardOptionsView.swift
//  TestUI
//
//  Created by Андрей Левчик on 09.02.2022.
//

import SwiftUI

struct HardOptionsView: View {
    var body: some View {
        Form {
            NavigationLink(destination: LangForm() ) { LangsRow() }
        }
    }
}

struct HardOptionsView_Previews: PreviewProvider {
    static var previews: some View {
        HardOptionsView()
    }
}
