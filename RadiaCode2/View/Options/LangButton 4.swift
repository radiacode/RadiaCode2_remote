//
//  LangButton.swift
//  TestUI
//
//  Created by Андрей Левчик on 09.02.2022.
//

import SwiftUI

struct LangButton: View {
    
    @EnvironmentObject var options: HardOptions
                       var lang: Lang
    
    var body: some View {
        Button(action: { options.curLang = lang  })
        {
            HStack {
                Text(langStr(lang: lang))
                Spacer()
                Image(systemName: options.isCurLang(lang: lang) ? "checkmark.square" : "square")
            }
        }
    }
}

struct LangButton_Previews: PreviewProvider {
    static let options = HardOptions()
    static var previews: some View {
        Group {
            LangButton(lang: .rus)
            LangButton(lang: .eng)
        }
        .previewLayout(.fixed(width: 300, height: 70))
        .environmentObject(options)
    }
}
