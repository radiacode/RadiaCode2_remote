//
//  LogFlagsView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 10.02.2022.
//

import SwiftUI
struct LogFlagsView: View {
    var flags: RC_CHN_FLAGS = []
    var body: some View {
        HStack (spacing: 3) {
            Group {
                
                if      flags.contains(.cFlgDoseRateAlarmOutOfData) { Image(systemName: "square.slash")     .foregroundStyle(.red)  }
                else if flags.contains(.rcFlgDoseRateAlarmLev1)     { Image(systemName: "1.square")         .foregroundStyle(.red)  }
                else if flags.contains(.rcFlgDoseRateAlarmLev2)     { Image(systemName: "2.square")         .foregroundStyle(.red)  }
                else                                                { Image(systemName: "0.square")         .foregroundStyle(.gray)}
                
                if      flags.contains(.rcFlgDoseRateAlarmPlay)     { Image(systemName: "bell.square")      .foregroundStyle(.red)  }
                else                                                { Image(systemName: "square")           .foregroundStyle(.gray)}
                
                if      flags.contains(.cFlgDoseAlarmOutOfData)     { Image(systemName: "square.slash.fill").foregroundStyle(.red)  }
                else if flags.contains(.rcFlgDoseAlarmLev1)         { Image(systemName: "1.square.fill")    .foregroundStyle(.red)  }
                else if flags.contains(.rcFlgDoseAlarmLev2)         { Image(systemName: "2.square.fill")    .foregroundStyle(.red)  }
                else                                                { Image(systemName: "0.square.fill")    .foregroundStyle(.gray)}
                
                if      flags.contains(.rcFlgDoseAlarmPlay)         { Image(systemName: "bell.square.fill") .foregroundStyle(.red)  }
                else                                                { Image(systemName: "square")           .foregroundStyle(.gray)}
            }
            Group {
                if      flags.contains(.rcFlgTemperatureHot)        { Image(systemName: "thermometer.sun.fill").foregroundStyle(.red)}
                else if flags.contains(.rcFlgTemperatureNorm)       { Image(systemName: "thermometer")      .foregroundStyle(.green)}
                else if flags.contains(.rcFlgTemperatureLow)        { Image(systemName: "thermometer.snowflake").foregroundStyle(.blue)}
                else                                                { Image(systemName: "square.slash")     .foregroundStyle(.red)  }
            }

            Group {
                if      flags.contains(.rcFlgScheduleRun)           { Image(systemName: "calendar")         .foregroundStyle(.green)  }
                else                                                { Image(systemName: "calendar")         .foregroundStyle(.gray) }
                
                if      flags.contains(.rcFlgUserRun)               { Image(systemName: "person.badge.clock.fill").foregroundStyle(.green)  }
                else                                                { Image(systemName: "person")           .foregroundStyle(.gray) }

                if      flags.contains(.rcFlgMotion)                { Image(systemName: "figure.walk")      .foregroundStyle(.green)}
                else                                                { Image(systemName: "figure.stand")     .foregroundStyle(.gray) }

                if      flags.contains(.rcFlg_DB_Write)             { Image(systemName: "square.and.pencil").foregroundStyle(.green)}
                else                                                { Image(systemName: "square")           .foregroundStyle(.gray) }
                
                if      flags.contains(.rcFlgPowerTooLow)           { Image(systemName: "battery.0")        .foregroundStyle(.red)}
                else if flags.contains(.rcFlgPowerUSB)              { Image(systemName: "powerplug.fill")   .foregroundStyle(.green)}
                else if flags.contains(.rcFlgPowerCharged)          { Image(systemName: "battery.100.bolt") .foregroundStyle(.red)}
                else                                                { Image(systemName: "battery.50")       .foregroundStyle(.green)  }
                
                if      flags.contains(.rcFlgOldData)               { Image(systemName: "leaf.fill")        .foregroundStyle(.orange)}
                else                                                { Image(systemName: "leaf")             .foregroundStyle(.green) }
            }

            Spacer()
       }
    }
}

struct LogFlagsView_Previews: PreviewProvider {
    static var previews: some View {
        let flags0: RC_CHN_FLAGS = [.rcFlgTemperatureHot, .rcFlgScheduleRun, .rcFlgUserRun, .rcFlgMotion, .rcFlg_DB_Write, .rcFlgOldData, .rcFlgPowerCharged]
        let flags1: RC_CHN_FLAGS = [.rcFlgDoseRateAlarmLev1, .rcFlgDoseAlarmPlay, .rcFlgTemperatureNorm, .rcFlgPowerTooLow]
        let flags2: RC_CHN_FLAGS = [.rcFlgDoseRateAlarmLev2, .rcFlgDoseRateAlarmPlay, .rcFlgTemperatureLow, .rcFlgPowerUSB]
        let flags3: RC_CHN_FLAGS = [.cFlgDoseRateAlarmOutOfData, .rcFlgDoseAlarmLev1, .rcFlgTemperatureNorm]
        Group {
            LogFlagsView(flags: flags0)
                .previewLayout(.fixed(width: 400, height: 40))
            LogFlagsView(flags: flags1)
                .previewLayout(.fixed(width: 400, height: 40))
            LogFlagsView(flags: flags2)
                .previewLayout(.fixed(width: 400, height: 40))
            LogFlagsView(flags: flags3)
                .previewLayout(.fixed(width: 400, height: 40))

        }
    }
}
