//
//  LogRow.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 21.04.2022.
//

import SwiftUI

struct LogRow: View {
    
                       var item   : Log
    @EnvironmentObject var options: HardOptions
    
    var body: some View {

        VStack {
            HStack {
                Text("\(item.timestamp!, formatter: itemFormatter)").bold().font(.system(size: 18))
                Spacer()
                Text("SN:\(String(format:"%d", item.device?.sn ?? 0))")
                    .fontWeight(.ultraLight)
                    .foregroundColor(.blue)
            }
            if item.rate_db != nil {
                Group {
                    HStack {
                        Text("Count Rate: \(String(format:"%1.2F", item.rate_db!.count_rate)) CPS")
//                        Text("Count: \(String(format:"%1.2F", item.rate_db!.count))")
                        Spacer()
                    }
                    HStack {
                        Text("Dose_rate: \(options.getOptimalVarUnitStr(roentgenVal: Double(item.rate_db!.dose_rate / 10.0), isHour: true))")
//                      Text("Dose_rate: \(String(format:"%1.2F", options.curUnitVal == 0 ? item.rate_db!.dose_rate * 100000.0 : item.rate_db!.dose_rate * 100000.0 / 100.0))\(options.curUnitVal == 0 ? "μR/h" : "μSv/h")")
                        Spacer()
                        Text("±\(String(format:"%1.2F", item.rate_db!.dose_rate_err))%")
                    }
                }.font(.system(size: 16))
            }
            if item.rare_data != nil {
                    HStack {
                        Image(systemName: "thermometer")
                            .foregroundStyle( .red)
                            .imageScale(.small)
                        Text("\(String(format:"%1.1F", item.rare_data!.temperature))")
                        Spacer()
                               if item.rare_data!.charge * 0.01 * 100 >  50 { Image(systemName: "battery.100").foregroundStyle( .green)
                        } else if item.rare_data!.charge * 0.01 * 100 >  20 { Image(systemName: "battery.50") .foregroundStyle( .yellow)
                        } else if item.rare_data!.charge * 0.01 * 100 <= 20 { Image(systemName: "battery.25") .foregroundStyle( .red)
                        }
                        Text("\(String(format:"%1.0F", item.rare_data!.charge * 0.01 * 100))%")
                    }.font(.system(size: 16))
                    HStack {
                        Image(systemName: "timer")
                            .foregroundStyle( .red)
                            .imageScale(.small)
                        let (d, h,m,s) = secondsToHoursMinutesSeconds(item.rare_data!.duration)
                        Text("\(d) days, \(h) hour, \(m) min, \(s) sec").font(.system(size: 14))
                        Spacer()
                        Image(systemName: "sum")
                            .foregroundStyle( .red)
                            .imageScale(.small)
                        Text("\(options.getOptimalVarUnitStr(roentgenVal: Double(item.rare_data!.dose)))").font(.system(size: 14))
//                      Text("\(String(format:"%1.2F", options.curUnitVal == 0 ? item.rare_data!.dose : item.rare_data!.dose / 100.0))\(options.curUnitVal == 0 ? "R" : "Sv")").font(.system(size: 14))
                    }.font(.system(size: 16))
            }
            if item.event != nil {
                    HStack {
                        Image(systemName: eventImageName(by: UInt8(item.event!.event))) // FIXME: Not enough bits to represent the passed value
                            .foregroundStyle( eventColor(by: UInt8(item.event!.event)))
                        Text("\(eventName(by: UInt8(item.event!.event))) ")
                        Spacer()
                    }
            }
            LogFlagsView(flags: RC_CHN_FLAGS(rawValue: UInt16(item.flags)))
        }
    }
}

struct LogRow_Previews: PreviewProvider {
    static let options = HardOptions()

    static var previews: some View {
        let context = PersistenceController.preview.container.viewContext
        let log = Log(context: context)
            log.timestamp = Date()
        let newCout = LogRateDB(context: context)
            newCout.count_rate = 3
            log.rate_db = newCout
        
        return Group {
            LogRow(item: log)
                .environmentObject(options)
                .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
                .previewLayout(.fixed(width: 400, height: 80))
                .environment(\.locale, .init(identifier: "en"))
            
            LogRow(item: log)
                .environmentObject(options)
                .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
                .previewLayout(.fixed(width: 400, height: 80))
                .environment(\.locale, .init(identifier: "ru"))
        }

     }
}
