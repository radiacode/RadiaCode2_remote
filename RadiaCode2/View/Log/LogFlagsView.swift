//
//  LogFlagsView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 10.02.2022.
//

import SwiftUI
struct LogFlagsView: View {
    var flags: RC_CHN_FLAGS = []
    var body: some View {
        HStack (spacing: 3) {
            Group {
                
                if      flags.contains(.cFlgDoseRateAlarmOutOfData) { Image(systemName: "dot.radiowaves.right").foregroundStyle(.gray)  }
                else if flags.contains(.rcFlgDoseRateAlarmLev1)     { Image(systemName: "dot.radiowaves.right").foregroundStyle(.orange)}
                else if flags.contains(.rcFlgDoseRateAlarmLev2)     { Image(systemName: "dot.radiowaves.right").foregroundStyle(.red)   }
                else                                                { Image(systemName: "dot.radiowaves.right").foregroundStyle(.green) }
                
                if      flags.contains(.cFlgDoseAlarmOutOfData)     { Image(systemName: "sum")                  .foregroundStyle(.gray) }
                else if flags.contains(.rcFlgDoseAlarmLev1)         { Image(systemName: "sum")                  .foregroundStyle(.orange)}
                else if flags.contains(.rcFlgDoseAlarmLev2)         { Image(systemName: "sum")                  .foregroundStyle(.red)  }
                else                                                { Image(systemName: "sum")                  .foregroundStyle(.green)}
            }
            Group {
                if      flags.contains(.rcFlgTemperatureHot)        { Image(systemName: "thermometer.sun.fill") .foregroundStyle(.red)  }
                else if flags.contains(.rcFlgTemperatureNorm)       { Image(systemName: "thermometer")          .foregroundStyle(.green)}
                else if flags.contains(.rcFlgTemperatureLow)        { Image(systemName: "thermometer.snowflake").foregroundStyle(.blue) }
                else                                                { Image(systemName: "thermometer")          .foregroundStyle(.gray) }
            }

            Group {
                if      flags.contains(.rcFlgPowerTooLow)           { Image(systemName: "battery.0")            .foregroundStyle(.red)  }
                else if flags.contains(.rcFlgPowerUSB)              { Image(systemName: "powerplug.fill")       .foregroundStyle(.green)}
                else if flags.contains(.rcFlgPowerCharged)          { Image(systemName: "battery.100.bolt")     .foregroundStyle(.red)  }
                else                                                { Image(systemName: "battery.50")           .foregroundStyle(.green)}
            }
            Spacer()
       }
    }
}

struct LogFlagsView_Previews: PreviewProvider {
    static var previews: some View {
        let flags0: RC_CHN_FLAGS = [.rcFlgTemperatureHot, .rcFlgScheduleRun, .rcFlgUserRun, .rcFlgMotion, .rcFlg_DB_Write, .rcFlgOldData, .rcFlgPowerCharged]
        let flags1: RC_CHN_FLAGS = [.rcFlgDoseRateAlarmLev1, .rcFlgDoseAlarmPlay, .rcFlgTemperatureNorm, .rcFlgPowerTooLow]
        let flags2: RC_CHN_FLAGS = [.rcFlgDoseRateAlarmLev2, .rcFlgDoseRateAlarmPlay, .rcFlgTemperatureLow, .rcFlgPowerUSB]
        let flags3: RC_CHN_FLAGS = [.cFlgDoseRateAlarmOutOfData, .rcFlgDoseAlarmLev1, .rcFlgTemperatureNorm]
        Group {
            LogFlagsView(flags: flags0)
                .previewLayout(.fixed(width: 400, height: 40))
            LogFlagsView(flags: flags1)
                .previewLayout(.fixed(width: 400, height: 40))
            LogFlagsView(flags: flags2)
                .previewLayout(.fixed(width: 400, height: 40))
            LogFlagsView(flags: flags3)
                .previewLayout(.fixed(width: 400, height: 40))

        }
    }
}
