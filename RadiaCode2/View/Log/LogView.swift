//
//  LogView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 10.02.2022.
//

import SwiftUI

struct LogView: View {
    
    @EnvironmentObject
                    var options: HardOptions
    @Environment(\.managedObjectContext)
            private var viewContext
//  @FetchRequest(sortDescriptors: [NSSortDescriptor(keyPath: \Log.rate_db?.dose_rate, ascending: false)], animation: .default)
//  @FetchRequest(sortDescriptors: [NSSortDescriptor(keyPath: \Log.timestamp, ascending: false)], predicate: NSPredicate(format: "device.sn == 199"), animation: .default)
    @FetchRequest(sortDescriptors: [NSSortDescriptor(keyPath: \Log.timestamp, ascending: false)], animation: .default)
            private var items: FetchedResults<Log>
//   @FetchRequest var fetchRequest: FetchedResults<Log>
    
    @FetchRequest(sortDescriptors: [NSSortDescriptor(keyPath: \Device.timestamp, ascending: true)],
                  animation: .default)
            private var devices: FetchedResults<Device>

    @State private  var date = Date()
    @State private  var isSearch = true
    @State private  var searchText = ""
    @State private  var isProgress = false
    
    var body: some View {
        
        VStack {
//            Menu("Options") {
//                Button("Order Now", action: placeOrder)
//                Button("Adjust Order", action: adjustOrder)
//                Button("Cancel", action: cancelOrder)
//            }

            ScrollView(.horizontal) {
                HStack(spacing: 15) {
                    Text("SN:")
                    ForEach(devices) { device in
                        Button("\(device.sn)") {
                            if searchText == "\(device.sn)" {
                                searchText = ""
                            } else {
                                searchText = "\(device.sn)"
                            }
                        }
                        .padding(5)
                        .buttonStyle(.bordered)
                        .foregroundColor(.black)
                        .background(self.searchText == "\(device.sn)" ? Color("NewCountColor") : Color.clear)
                    }
                }.padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 0))
            }

            HStack {
                DatePicker("Pick a date", selection: $date, displayedComponents: [.date])
                Toggle(isOn: $isSearch) { Text("Find") }.frame(width: 130)
                    .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 0))
            }.padding(EdgeInsets(top: 0, leading: 10, bottom: 10, trailing: 10))
            
            if options.isShowLogList {
                List {
                    ForEach(
                        items.filter { (!isSearch || (   $0.timestamp!.hasSame(.day,   as: date)
    //                    fetchRequest.filter { (!isSearch || (   $0.timestamp!.hasSame(.day,   as: date)
                                                      && $0.timestamp!.hasSame(.month, as: date)))
                                       && (searchText.isEmpty || "\($0.device?.sn ?? 0)" == searchText )
                                      }
                    ) { item in
                        LogRow(item: item)
                    }//.onDelete(perform: deleteItems)
                }
            }
        }
        .edgesIgnoringSafeArea(.bottom)
        .onAppear(perform: stopChangeCycle) // Отключаем цикл построения графика
    }
    
//    private func deleteItems(offsets: IndexSet) {
//        withAnimation {
//            offsets.map { fetchRequest=[$0] }.forEach(viewContext.delete)
//
//            do {
//                try viewContext.save()
//            } catch {
//                // Replace this implementation with code to handle the error appropriately.
//                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//                let nsError = error as NSError
//                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
//            }
//        }
//    }

    
    private func stopChangeCycle() {
        options.isShowLogList = true
//        print(options.isConnect, options.isFirstChangeCycle)
//        options.isFirstChangeCycle          = false // Флаг того, что можно запускать цикл обмена из окна графиков
//        options.enableStartDataBufCycle     = true
//        options.enableSetOptions            = true
//        options.enableStopDataBufCycle      = false
//        options.curCommand                  = .STOP
    }
    
    func placeOrder() { }
    func adjustOrder() { }
    func cancelOrder() { }


}

struct LogView_Previews: PreviewProvider {
    static let options = HardOptions()
    static var previews: some View {
            LogView()
            .environmentObject(options)
            .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)

    }
}
