//
//  LogListView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 20.04.2022.
//

import SwiftUI

struct LogListView: View {
    
    @Environment(\.managedObjectContext) private var viewContext
    @FetchRequest                                var fetchRequest: FetchedResults<Log>

    init(fromDate: NSDate, toDate: NSDate) {

        let predicateStr = "rate_db.count_rate >= %@ AND %@ <= timestamp AND %@ >= timestamp"
           _fetchRequest = FetchRequest<Log>(sortDescriptors: [NSSortDescriptor(keyPath: \Log.timestamp, ascending: false)],
                                                   predicate:  NSPredicate(format: predicateStr,
                                                                    argumentArray: ["0", fromDate, toDate]
                                                                          )
           )
    }

    var body: some View {
        VStack {
            List {
                ForEach(
                    fetchRequest
                ) { item in
                    LogRow(item: item)
                }
            }
        }
    }
}

struct LogListView2_Previews: PreviewProvider {
    

    static var previews: some View {
        
        let   toDate = Date()
        let fromDate = Calendar.current.date( byAdding: .month, value: -1, to: toDate)

        let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let fromDateStr = df.string(from: fromDate!)
        let   toDateStr = df.string(from: toDate)

        let startDate:NSDate = df.date(from: fromDateStr)! as NSDate
        let   endDate:NSDate = df.date(from:   toDateStr)! as NSDate
        
        return
            LogListView(fromDate: startDate, toDate: endDate)
                .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)

    }
}
