//
//  ContentView2.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 16.02.2022.
//

import SwiftUI

struct ContentView2: View {
    
    @EnvironmentObject                            var options: HardOptions
    @Environment(\.scenePhase)                    var scenePhase
    @Environment(\.managedObjectContext)  private var viewContext
    @ObservedObject                               var router = Router.shared
    
    @State          var menu: [String] = [
                            String(localized: "Devices"),
                            String(localized: "Map"),
                            String(localized: "Log"),
                            String(localized: "Graph")]
//    @State private  var selectedMenu = String(localized: "")
//    @State private  var selectedMenu = String(localized: "Devices")
    @State private  var showingCloseApp      = false        // Показ предупреждения об несохранённых данных

    var body: some View {
        
        NavigationStack(path: $router.path) {
            VStack {
                HStack {
                    Menu {
                        Button("Options", action: { router.showOptions() } )
                    } label: {
                        Label("Menu", systemImage: "ellipsis.circle")
                            .labelStyle(.iconOnly)
                            .imageScale(.large)
                            .padding([.horizontal], 10)
                    }
                    
                    Button(action: { showingCloseApp = true }) {
                        Image(systemName: "power")
                            .imageScale(.large)
                            .foregroundColor(options.isConnect ? .green : .red) // 􀆨
                    }
                    .alert(isPresented: $showingCloseApp) {
                        Alert(        title: Text("Attention"),
                                      message: Text("Close Application?"),
                                      primaryButton: .default(Text("OK"    ), action: closeApp ),
                                      secondaryButton: .cancel (Text("Cancel"), action: { showingCloseApp = false })
                        )
                    }
                    
                    Text(UIApplication.versionBuild + " " + options.statusStr).font(.system(size: 10))
                    Spacer()
                    Text("\(options.groupCount)").font(.system(size: 10))
                }
                Picker("Windows", selection: $options.selectedMenu) {
                    ForEach(menu, id: \.self) {
                        Text($0)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                if options.isConnectionProgerss && !options.isConnect {
                    Spacer()
                    ProgressView("Connection...")
                        .scaleEffect(2)
                    Spacer()
                } else {
                    TabView(selection: $options.selectedMenu) {
                        DevicesListView()
                            .tag(String(localized: "Devices"))
                        LocationView()
                            .tag(String(localized: "Map"))
//                        HardOptionsView(
//                            alarm1StrDRA: options.LEV1_uR_hStr,
//                            alarm2StrDRA: options.LEV2_uR_hStr,
//                            alarm1StrDSA: options.LEV1_100uR_Str,
//                            alarm2StrDSA: options.LEV2_100uR_Str
//                        )
//                        .tag(String(localized: "Options"))
                        LogView()
                            .tag(String(localized: "Log"))
                        FullDiagramView()
                            .tag(String(localized: "Graph"))
                    }
                    
                    .onChange(of: options.selectedMenu) { tabNameStr in
                        if options.statusStr != String(localized: "Timeout…") {
                            manageDeviceCycle(name: tabNameStr)
                        }
                        
                    }
                    .tabViewStyle(.page(indexDisplayMode: .never))
                }
                
                
            }
            .edgesIgnoringSafeArea(.bottom)
            .onChange(of: scenePhase) { newPhase in
                if newPhase == .inactive {
                    print("Inactive")
                    options.curCommand = .INACTIVE
                } else if newPhase == .active {
                    
                    if  options.curCommand == .INACTIVE ||
                            options.curCommand == .BACKGROUND {
                        
                        switch options.curCommand {
                            
                        case .INACTIVE:
                            options.statusStr = String(localized: "Start…")
                        case .BACKGROUND:
                            options.statusStr = String(localized: "From background…")
                        default: options.statusStr = String(localized: "Wrong command…")
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 6) {
                            print("ЗАДЕРЖКА ПОДКЛЮЧЕНИЯ!")
                            print("Выход из фона…")
                            if !self.options.isConnect { // Если у нас отключение…
                                self.options.enableWriteToDeviceFlag = false
                                self.options.SOUND_CTRL  = nil
                                self.options.VIBRO_CTRL  = nil
                                self.options.DISP_CTRL   = nil
                                self.options.DEVICE_CTRL = nil
                                
                                self.options.ble.startCentralManager()
                                self.options.startConnecting()
                            }
                        }
                    }
                    print("Active")
                } else if newPhase == .background {
                    print("Background")
                    //                if  options.isConnect {
                    //                    options.curCommand = .BACKGROUND
                    //                    options.ble.disconnect()
                    //                    options.ble.connectUUID = nil
                    //                }
                }
            }
            .onAppear(perform: connectBLEDevice) // Попытка подключения устройства
            .navigationDestination(for: Route.self) { route in
                switch route {
                case .options:
                    HardOptionsView(
                        alarm1StrDRA: options.LEV1_uR_hStr,
                        alarm2StrDRA: options.LEV2_uR_hStr,
                        alarm1StrDSA: options.LEV1_100uR_Str,
                        alarm2StrDSA: options.LEV2_100uR_Str
                    )
                }
            }
        }
    }
    
    private func manageDeviceCycle(name: String) {
        
        if name == String(localized: "Graph") {
            options.isShowLogList               = false
            options.enableStartDataBufCycle     = false
            options.enableStopDataBufCycle      = true
            options.enableSetOptions            = false
            options.comp.compose_VS_DATA_BUF() // Запускаем цикл обмена данными с сенсоров прибора
            options.curCommand = .NONE
            options.isFirstChangeCycle = true
        } else if name == String(localized: "Options") {
            options.isFirstChangeCycle          = false // Флаг того, что можно запускать цикл обмена из окна графиков
            options.enableStartDataBufCycle     = true
            options.enableSetOptions            = true
            options.enableStopDataBufCycle      = false
            options.curCommand                  = .STOP
        }
    }
    
    private func closeApp() {
//        DispatchQueue.main.asyncAfter(deadline: .now()) {
//                          UIApplication.shared.perform(#selector(NSXPCConnection.invalidate))
//                      }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            UIApplication.shared.perform(#selector(NSXPCConnection.suspend))
             DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
              exit(0)
             }
        }
    }
    
    private func connectBLEDevice() {

//        selectedMenu = String(localized: "Graph")
        if options.curCommand != .INACTIVE && !options.isConnect {
            options.startConnecting()
//            selectedMenu = String(localized: "Graph")
        }
    }   // Подключаемся, если возможно, к устройству по-умолчанию
}

struct ContentView2_Previews: PreviewProvider {
    static let options = HardOptions()
    static var previews: some View {
        ContentView2()
            .environmentObject(options)
            .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)

    }
}
