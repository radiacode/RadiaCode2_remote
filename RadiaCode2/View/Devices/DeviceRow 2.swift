//
//  DeviceRow.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import SwiftUI

struct DeviceRow: View {
    
    @State             var item   : Device
    @State             var isConnect: Bool
    @State             var isConnected = false
    @EnvironmentObject var options: HardOptions
    @Environment(\.managedObjectContext)
               private var viewContext

    var body: some View {
        HStack {
            VStack {
                HStack { Text("Added: \(item.timestamp ?? Date(), formatter: itemFormatter)"); Spacer()}
                HStack { Text("SN: \(Int32(item.sn))"); Spacer() }
                HStack {
                    Text("\(((item.uuid) ?? UUID(uuidString: "00000000-0000-0000-0000-000000000000"))!)")
                        .italic()
                        .padding(2)
                        .scaledToFill()
                        .minimumScaleFactor(0.5)
                    Spacer() }
                Toggle(isOn: $isConnect) {
                    Text(item.is_connected ? "Connect" : "Disconnect")
                }
                    .onChange(of: isConnect) { value in
                        if isConnect {
                            if Device.isConnectedDevice(in: viewContext) {
                                
//                                options.ble.disconnect()
//                                options.ble.connectUUID = item.uuid
//                                options.managedObjectContext = viewContext
//                                options.ble.startScan()

                                isConnected = true
                                isConnect = false
                                options.isError = true
                            } else {
                                options.isDefault = false
                                options.ble.connectUUID = item.uuid
                                options.ble.startScan()
                            }
                        } else {
                            if !options.isError {
                                options.isDefault = false
                                options.ble.disconnect()
                                options.ble.connectUUID = nil
                            }
                        }
                    }

            }
        }
        .alert(isPresented: $isConnected) {
            Alert(
                title:   Text("ERROR"),
                message: Text("You must disconnect other device!"),
                dismissButton: .default(Text("OK")) {
                    isConnect = false
                    options.isError = false
                }
            )
        }
    }
}

// MARK: - Previews

struct DeviceRow_Previews: PreviewProvider {
    static let options = HardOptions()
    static var previews: some View {
        let context = PersistenceController.preview.container.viewContext
        let newItem = Device(context: context)
            newItem.timestamp = Date()
            newItem.name = "RadiaCode-101"
            newItem.sn = 1234
            newItem.uuid = UUID(uuidString: "00000000-0000-0000-0000-000000000000")!
            newItem.chip = "blunrg"
            newItem.is_default = false
            newItem.user_name = String(localized: "My Device")

        return
            Group {
                DeviceRow(item: newItem, isConnect: false)
                    .previewLayout(.fixed(width: 400, height: 190))
                    .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
                    .environment(\.locale, .init(identifier: "en"))
                    .environmentObject(options)
                DeviceRow(item: newItem, isConnect: false)
                    .previewLayout(.fixed(width: 400, height: 190))
                    .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
                    .environment(\.locale, .init(identifier: "ru"))
                    .environmentObject(options)
        }
    }
}
