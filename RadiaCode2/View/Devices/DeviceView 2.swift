//
//  DeviceView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 09.02.2022.
//

import SwiftUI

struct DeviceView: View {
    
    @EnvironmentObject var options: HardOptions
    @Environment(\.managedObjectContext) private var viewContext
    
    @State var item: Device
    @State var userName  = ""
    @State var isDefault = false

    var body: some View {
        Form {
            HStack { Text("Added: \(item.timestamp!, formatter: itemFormatter)").padding()}
            TextField("Add own name to device", text: $userName)
                .textFieldStyle(RoundedBorderTextFieldStyle())
            VStack {
                HStack { Text("Device: \(item.name ?? "Not found")"); Spacer() }
                HStack { Text("SN: \(item.sn)"); Spacer() }
                HStack { Text("Chip: \(item.chip ?? "unknown")"); Spacer() }
            }
            HStack {
                Text("\(((item.uuid) ?? UUID(uuidString: "00000000-0000-0000-0000-000000000000"))!)")
                    .font(.subheadline)
                    .italic()
                    .padding(.trailing, 8)
                    .scaledToFill()
                    .minimumScaleFactor(0.1)
                Spacer()
            }
            Toggle(isOn: $isDefault) {
                Text("Connect by default")
            }        .onChange(of: isDefault) { value in
                saveItem()
            }
        }

        .navigationBarTitle("Selected device:")
        .toolbar {
            ToolbarItemGroup(placement: .navigationBarTrailing) {
                Button(action: saveItem) {
                    Label("Save Item", systemImage: "square.and.arrow.down")
                }
            }
        }
    }
    
    func saveItem() {
        if (item.is_default != isDefault) && isDefault {
            Device.clearAllDefaultTo(false, in: viewContext) // Сбрасываем везде факт "по-умолчанию"
        }
        item.user_name = userName
        item.is_default = isDefault
        do {
            try viewContext.save()
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
}

struct DeviceView_Previews: PreviewProvider {
    static let options = HardOptions()
    static var previews: some View {
        let context = PersistenceController.preview.container.viewContext
        let device = Device(context: context)
            device.timestamp = Date()
            device.name = "RadiaCode-101"
            device.uuid = UUID(uuidString: "12000000-0000-0000-0000-123456789012")!
            device.chip = "bluenrg!"
            device.sn = 1234
        
            return
                NavigationView {
                    Group {
                        DeviceView(item: device)
                            .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
                            .environment(\.locale, .init(identifier: "en"))
                            .environmentObject(options)
                        DeviceView(item: device)
                            .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
                            .environment(\.locale, .init(identifier: "ru"))
                            .environmentObject(options)
                    }
                }
        }
}
