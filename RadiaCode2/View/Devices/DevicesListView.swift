//
//  DevicesListView.swift
//  RadiaCode2
//
//  Created by Андрей Левчик on 10.02.2022.
//

import SwiftUI
import CoreData

struct DevicesListView: View {
    
    @EnvironmentObject
                    var options: HardOptions
    @Environment(\.managedObjectContext)
            private var viewContext
    @FetchRequest(sortDescriptors: [NSSortDescriptor(keyPath: \Device.timestamp, ascending: true)], animation: .default)
            private var items: FetchedResults<Device>
    @State private         var isScanning = false

    var body: some View {
        VStack {
            HStack {
                Spacer()
                if isScanning {
                    ProgressView()
                }
                EditButton()
                Button(action: stop_start_scanBLE) {
                    Label("Scan", systemImage: isScanning ? "nosign" : "arrow.clockwise")
                }
            }.padding(EdgeInsets(top: 5, leading: 0, bottom: 0, trailing: 20))
            List {
                    ForEach(items) { item in
                            HStack {
                                Image(systemName: item.is_default ? "checkmark.circle.fill" : "circle.fill")
                                    .foregroundStyle(item.is_connected ? .green : .red)
                                VStack {
                                    HStack { Text("Device: \(item.name ?? "Not found")"); Spacer() }
                                    HStack { Text("ID: \(item.user_name ?? "Not found")"); Spacer() }
                                    DeviceRow(item: item, isConnect: item.is_connected)
                                }
                            }
                    }
                    .onDelete(perform: deleteItems)
                }

//                .toolbar {
//                    ToolbarItemGroup(placement: .navigationBarTrailing) {
//                        if isScanning {
//                            ProgressView()
//                        }
//                    }
//            }
        }
        .edgesIgnoringSafeArea(.bottom)
//        .onAppear(perform: stopChangeCycle) // Отключаем цикл построения графика
    }
    
//    private func stopChangeCycle() {
//        print(options.isConnect, options.isFirstChangeCycle)
//        if  options.isConnect {
//            options.isFirstChangeCycle          = false // Флаг того, что можно запускать цикл обмена из окна графиков
//            options.enableStartDataBufCycle     = true
//            options.enableSetOptions            = true
//            options.enableStopDataBufCycle      = false
//            options.curCommand                  = .STOP
//        }
//    }
    
    private func stop_start_scanBLE() {
        if isScanning { options.ble.stop_Scan() }
        else          { options.ble.startScan() }
           isScanning.toggle()
    }


    private func deleteItems(offsets: IndexSet) {
        withAnimation {
            offsets.map { items[$0] }.forEach(viewContext.delete)
            do {
                try viewContext.save()
            } catch {
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
}

struct DevicesListView_Previews: PreviewProvider {
    static let options = HardOptions()
    static var previews: some View {
//        NavigationView {
            Group {
                DevicesListView()
                    .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
                    .environment(\.locale, .init(identifier: "en"))
                    .environmentObject(options)
                DevicesListView()
                    .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
                    .environment(\.locale, .init(identifier: "ru"))
                    .environmentObject(options)
            }
//        }
    }
}
