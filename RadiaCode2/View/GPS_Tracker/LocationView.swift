/// Copyright (c) 2022 Razeware LLC
///

import SwiftUI
import CoreLocation
import MapKit

struct PlaceAnnotationView: View {
  
    @State     private var showTitle = true
                       var title     = ""
    
    var body: some View {
        VStack(spacing: 0) {
            Text(title)
                .font(.callout)
                .padding(5)
//                .background(Color(.white))
                .cornerRadius(10)
                .opacity(showTitle ? 0 : 1)     // Делаем текст непрозрачным/прозрачным при тапе

            Image(systemName: "circle.fill")  // Форма отображения текущей геопозиции
                .font(.title)
                .foregroundColor(.green)
                .opacity(0.3)
        }
        .onTapGesture {                     // При тапе на геопозиции
            withAnimation(.easeInOut) {
                showTitle.toggle()
            }
        }

    }
}

struct LocationView: View {
  
    @EnvironmentObject var options: HardOptions
    @StateObject       var model = Model()
    
//    init(options: HardOptions, model: Model) {
//        self.options = options
//        self.model = model
//    }
//    init() {
//        model.pars = options.pars
//    }

    var body: some View {
    
        VStack(alignment: .center, spacing: 20) {
      
            Button(
                action: { model.startStopLocationTracking() },
                label: {
                    HStack {
                        Image(systemName: model.isLocationTrackingEnabled ? "stop" : "location")
                        Text(model.isLocationTrackingEnabled ? "Stop" : "Start")
                    }
                })
            .font(.title)

            Map(coordinateRegion: $model.region, interactionModes: [.all], annotationItems: model.pins) { pin in
                MapAnnotation(coordinate: pin.coordinate) {
                    PlaceAnnotationView(title: "\(options.getOptimalVarUnitStr(roentgenVal: Double((pin.doseRate ?? 1) / 10_000), isHour: true))")
                }
            }
            .ignoresSafeArea()
        }
    }
}

struct LocationView_Previews: PreviewProvider {
  static var previews: some View {
    LocationView()
  }
}
