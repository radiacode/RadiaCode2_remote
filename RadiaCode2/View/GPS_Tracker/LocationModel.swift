/// Copyright (c) 2022 Razeware LLC

import Combine
import CoreLocation
import MapKit
import SwiftUI

extension LocationView {
  
  class Model: NSObject, CLLocationManagerDelegate, ObservableObject {
    
//               var options: HardOptions?
//      var pars: PacketParser?
      /// Последнее значение мощности дозы для вывода в числовое значение диаграммы
      @Published var grpDoseRateLast  : Double = 0

    @Published var isLocationTrackingEnabled = false
    @Published var location: CLLocation?
    @Published var region = MKCoordinateRegion(
      center: CLLocationCoordinate2D(latitude     : 55.7522200, longitude     : 37.6155600),    // Moscow
        span: MKCoordinateSpan      (latitudeDelta: 0.5      , longitudeDelta: 0.5))
    @Published var pins: [PinLocation] = []
      
      var cancellableLastDoseRate  : Cancellable?    // Издатель потока последней мощности дозы

    let mgr: CLLocationManager

      override init() {
        mgr = CLLocationManager()
        mgr.desiredAccuracy = kCLLocationAccuracyBest
        mgr.requestAlwaysAuthorization()
        mgr.allowsBackgroundLocationUpdates = true

      super.init()
          cancellableLastDoseRate   = lastDosePublisher()    // Издатель потока последней мощности дозы

      mgr.delegate = self
    }

    func  enable() { mgr.startUpdatingLocation() }
    func disable() { mgr .stopUpdatingLocation() }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      
      if let  currentLocation = locations.first {
        
        // DEBUG:
        print(currentLocation)
        
        location = currentLocation
        appendPin   (location: currentLocation)
        updateRegion(location: currentLocation)
      }
    }

      func appendPin(location: CLLocation) { pins.append(PinLocation(coordinate: location.coordinate, doseRate: grpDoseRateLast)) }

    func updateRegion(location: CLLocation) {
      region = MKCoordinateRegion(
        center: location.coordinate,
        span: MKCoordinateSpan(latitudeDelta: 0.0015, longitudeDelta: 0.0015)
      )
        
    }

    func startStopLocationTracking() {
      
        isLocationTrackingEnabled.toggle()
        if isLocationTrackingEnabled { enable() } else { disable() }
      
    }
      
      func lastDosePublisher() -> Cancellable { // Издатель потока текущих данных мощности дозы
          return sharePars!.lastDoseRatePublisher
              .receive(on: DispatchQueue.main)
              .sink { [self] value in
                  grpDoseRateLast = value
              }
      }

  }

  struct PinLocation: Identifiable {
      let id = UUID()
      var coordinate: CLLocationCoordinate2D
      var doseRate: Double?
  }
}
